/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

%{

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "common/syn_file.h"
#include "common/syn_track.h"

int yyerror(const char *errmsg, ...);
int yylex();

struct syn_track cur_track;

%}

%union {
    int ival;
    char sval[SYN_MAX_STR_LEN];
    double rval;
}

%token TOK_TRACK
%token TOK_TEMPO
%token TOK_VAL_STR
%token TOK_VAL_INT
%token TOK_VAL_REAL

%%

statement_list : statement | statement statement_list

statement : track | assign

track : TOK_TRACK TOK_VAL_STR '{' beat_list '}' ';'  {
      strncpy(cur_track.name, $<sval>2, TRACK_NAME_LEN);
      cur_track.synth = sin_synth;
}

beat : '{' TOK_VAL_STR ',' TOK_VAL_INT '}' {
     char const *note = $<sval>2;
     int duration = $<ival>4;

     add_beat_to_track(&cur_track, note, duration);
}

beat_list : beat ',' beat_list | beat

assign : tempo_assign

tempo_assign : tempo_assign_real | tempo_assign_int

tempo_assign_real : TOK_TEMPO '=' TOK_VAL_REAL ';'{
    cur_track.tempo = $<rval>3;
}

tempo_assign_int : TOK_TEMPO '=' TOK_VAL_INT ';'{
    cur_track.tempo = $<ival>3;
}

%%

int yyerror(const char *errmsg, ...)
{
    va_list args;

    va_start(args, errmsg);
    vfprintf(stderr, errmsg, args);
    va_end(args);

    return 0;
}
