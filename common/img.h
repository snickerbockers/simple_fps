/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef IMG_H_
#define IMG_H_

#include "draw.h"

/* include game.h if you plan to use this macro */
#define STD_CHROMA_KEY color_pack_rgb(game.sys.scr, 0xff, 0, 0xff)

/* These functions both output the tex in row-major format */
int tex_load_pcx(struct screen *scr, struct tex *img, const char *path);
int tex_load_png(struct screen *scr, struct tex *out, const char *path);

int tex_load(struct screen *scr, struct tex *img, const char *path,
             int col_major);
int tex_load_chroma_key(struct screen *scr, struct tex *img, const char *path,
                        int col_major, color_t key);

void tex_cleanup(struct tex *img);

/* convert to/from column-major/row-major.
 * This does not change the width and height because the intent is not
 * to rotate the image.  It is up to the user to keep track of whether the image
 * is row-major or column-major; generally 3d-rendering code (like the raytracer
 * and billboard sprites) expect column-major, and 2d-rendering code (like the UI
 * overlays) expect row-major
 */
int tex_transpose(struct tex *img);

/**
 * edits the mask so that any pixels which match key (not including the alpha
 * channel) will be masked out.  The mask values for pixels which do not match
 * key will be unaffected, so if you want the standard chroma-keying behavior
 * you should set the mask to all 1s before calling this function.
 *
 * @param img the img to chroma-key
 * @param key the color which will be made invisible
 */
void tex_chroma_key(struct tex *img, color_t key);

#endif













