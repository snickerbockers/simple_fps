/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "real.h"
#include "angle.h"

#ifdef USE_SOFTWARE_DIVISION
real_t real_recip(real_t in)
{
    uint32_t mask;
    real_t ret;
    uint64_t in_times_ret;
    int sign;

    if (in == 0)
    {
        fprintf(stderr, "real_recip: divide by zero\n");
        abort();
    }

    /*
     * It might not be necessary to do this,
     * but IDK I haven't thought it through yet.
     */
    sign = 1;
    if (real_lt(in, REAL_ZERO))
    {
        in = real_neg(in);
        sign = -1;
    }

    mask = 0x80000000;
    ret = 0;
    do
    {
        ret |= mask;
        in_times_ret = ((uint64_t)in * (uint64_t)ret) >> REAL_UNIT_SHIFT;
        if (in_times_ret > REAL_UNIT)
            ret &= ~mask;
        mask >>= 1;
    } while (mask && (!real_eq(in_times_ret, REAL_UNIT)));

    if (sign < 0)
        ret = real_neg(ret);

    /*
     * dividing by REAL_UNIT >> 16 can actually result in a reciprocal of 0
     * because 1 / (REAL_UNIT>>16) should be REAL_UNIT << 16, which is out of
     * real_t's bounds.  One potential solution is to change REAL_SHIFT to 15.
     * This works, but I'm not fully aware of the rammifications yet
     */
    assert(ret);

    return ret;
}
#else
real_t real_recip(real_t in)
{
    return ((double)REAL_UNIT / (double)in) * REAL_UNIT;
}
#endif

real_t real_sqrt(real_t in)
{
    uint32_t mask;
    real_t ret;
    int64_t ret_sq;

    if (in == 0)
        return 0;

    if (in < 0)
    {
        fprintf(stderr, "ERROR: sqrt(%x)\n", (int)in);
        abort();
    }

    mask = 0x80000000;
    ret = 0;

    do
    {
        ret |= mask;
        ret_sq = ((int64_t)ret * (int64_t)ret) >> REAL_UNIT_SHIFT;
        if (ret_sq > in)
            ret &= ~mask;
        mask >>= 1;
    } while (mask && (ret_sq != in));

    return ret;
}

real_t real_from_double(double val)
{
    int neg = 0;
    real_t mask = 0x40000000;
    real_t ret = 0;

    if (val < 0)
    {
        neg = 1;
        val = -val;
    }

    do
    {
        ret |= mask;
        if (real_to_double(ret) > val)
            ret &= ~mask;
        mask >>= 1;
    } while (mask);

    if (neg)
        return real_neg(ret);
    return ret;
}

double real_to_double(real_t val)
{
    return (double)val / (double)REAL_UNIT;
}

real_t real_from_string(char const *str)
{
    return real_from_double(atof(str));
}

real_t real_sin(real_t in)
{
    return angle_sin(angle_from_real(in));
}

real_t real_cos(real_t in)
{
    return angle_cos(angle_from_real(in));
}

real_t real_tan(real_t in)
{
    return angle_tan(angle_from_real(in));
}
