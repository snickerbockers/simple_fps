/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef STREAM_H_
#define STREAM_H_

#include <stdarg.h>
#include <stdlib.h>

enum
{
    STREAM_ERR_NONE,
    STREAM_ERR_UNKNOWN,
    STREAM_ERR_INVAL_OP, /* the corresponding function in stream_ops is null */
    STREAM_ERR_INVAL_ARG, /* calling function fucked up */
    STREAM_ERR_NUM /* implementations will use this as their first err code */
};

enum
{
    STREAM_SEEK_SET,
    STREAM_SEEK_CUR,
    STREAM_SEEK_END
};

typedef int stream_error_t;

struct stream;
typedef struct stream stream_t;

typedef size_t(*stream_read_t)(void *ptr, size_t sz, size_t nitems,
                               stream_t *stream);
typedef size_t(*stream_write_t)(void const *ptr, size_t sz, size_t nitems,
                                stream_t *stream);
typedef int(*stream_seek_t)(stream_t *stream, long offset, int whence);
typedef long(*stream_tell_t)(stream_t *stream);
typedef int(*stream_close_t)(stream_t *stream);
typedef char const*(*stream_errstr_t)(stream_t *stream, int err);

/*
 * Ideally this would be implemented in stream.c as a layer over stream_write
 * with no need for stream_ops to have its own implementation, but
 * practically-speaking, implementing my own printf clone would be complicated.
 * Maybe some day, though...
 */
typedef int (*stream_vprintf_t)(stream_t *stream, char const *format,
                                va_list *argp);

struct stream_ops
{
    stream_read_t read_func;
    stream_write_t write_func;
    stream_seek_t seek_func;
    stream_tell_t tell_func;
    stream_close_t close_func;
    stream_errstr_t errstr_func;
    stream_vprintf_t vprintf_func;
};

struct stream
{
    struct stream_ops const *ops;
    void *stream_ctxt;
    int err;
};

size_t stream_read(void *ptr, size_t sz, size_t nitems, stream_t *stream);
size_t stream_write(void const *ptr, size_t sz, size_t nitems, stream_t *stream);
int stream_seek(stream_t *stream, long offset, int whence);
long stream_tell(stream_t *stream);
int stream_close(stream_t *stream);
char const *stream_errstr(stream_t *stream, int err);
int stream_err(stream_t *stream);
int stream_printf(stream_t *stream, char const *format, ...);
int stream_vprintf(stream_t *stream, char const *format, va_list *argp);

stream_t *stream_stdio_open(char const *path, char const *mode);

#define stream_stdout (&stream_stdout_)
#define stream_stdin  (&stream_stdin_)
#define stream_stderr (&stream_stderr_)

extern stream_t stream_stdout_;
extern stream_t stream_stdin_;
extern stream_t stream_stderr_;

#endif
