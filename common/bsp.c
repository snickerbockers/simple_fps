/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "real.h"
#include "geo.h"
#include "bsp.h"
#include "trace.h"

enum
{
    SWEEP_NO_HIT = 0,
    SWEEP_HIT,
    SWEEP_SLIDE,
    SWEEP_HALT
};

static unsigned gen_bsp_(struct bsp_node **nodes, unsigned *n_nodes,
                         real_t **verts, unsigned *n_verts,
                         struct line_seg **segs_out, unsigned *n_segs_out,
                         const struct line_seg *segs, unsigned n_segs);
static void move_pt_(real_t pt_out[2], real_t const pt_start[2],
                     real_t const pt_end[2], real_t rad,
                     struct bsp_tree const *tree, unsigned root_node_idx,
                     int max_recurse);
static int sweep_circle_(real_t end_out[2], int *node_out,
                         struct sweep_seg* seg, real_t rad,
                         struct bsp_tree const* tree, unsigned node_idx);
static int bsp_in_empty_space_(real_t const pt[2],
                               struct bsp_tree const *tree,
                               unsigned root_node_idx);
static void bsp_clip_pt_(real_t pt[2], struct bsp_tree const *tree,
                         unsigned root_node_idx);
static void trace_ray_(struct ray_cast *cast, struct bsp_tree const *tree,
                       unsigned root_node_idx);
static int bsp_check_circle(real_t const pos[2], real_t rad,
                            struct bsp_tree const *tree, unsigned node_idx);


#define TOL ((real_t)(REAL_UNIT >> 10))

static void move_pt_(real_t pt_out[2], real_t const pt_start[2],
                     real_t const pt_end[2], real_t rad,
                     struct bsp_tree const *tree, unsigned root_node_idx,
                     int max_recurse)
{
    real_t slide_start[2], slide_end[2];
    real_t disp[2], max_disp[2], norm[2], dir[2], slide_disp[2];
    real_t max_dist, dist_rem, slide_dist, dist_actual;
    real_t end_actual[2];
    int node_idx;
    int hit_tp;
    struct sweep_seg sweep;

#ifdef NOCLIP
    pt_out[0] = pt_end[0];
    pt_out[1] = pt_end[1];
#else
    memset(&sweep, 0, sizeof(struct sweep_seg));
    memcpy(sweep.start, pt_start, 2 * sizeof(real_t));
    memcpy(sweep.end, pt_end, 2 * sizeof(real_t));
    node_idx = 0;
    switch (sweep_circle(end_actual, &node_idx, &sweep, rad, tree))
    {
    case SWEEP_NO_HIT:
        pt_out[0] = end_actual[0];
        pt_out[1] = end_actual[1];
        break;
    case SWEEP_HALT:
        pt_out[0] = pt_start[0];
        pt_out[1] = pt_start[1];
        break;
    default:
        pt_out[0] = end_actual[0];
        pt_out[1] = end_actual[1];
        if (--max_recurse >= 0)
        {
            memcpy(norm, tree->nodes[node_idx].line, sizeof(real_t) * 2);
            pt2_sub(max_disp, pt_end, pt_start);
            pt2_mag(&max_dist, max_disp);
            pt2_sub(dir, end_actual, pt_start);
            pt2_mag(&dist_actual, dir);
            pt2_sub(dir, pt_end, pt_start);
            pt2_perp(slide_disp, norm);
            pt2_dot(&slide_dist, slide_disp, dir);
            if (real_lt(slide_dist, REAL_ZERO))
            {
                pt2_scale(slide_disp, slide_disp, real_neg(REAL_UNIT));
                slide_dist = real_neg(slide_dist);
            }
            slide_dist = real_mul(slide_dist, real_sub(max_dist, dist_actual));
            /* do i need an arbitrary tolerance constant here? */
            if (real_gt(slide_dist, REAL_ZERO))
            {
                real_t dir_mag;
                pt2_mag(&dir_mag, dir);
                if (real_eq(dir_mag, REAL_ZERO))
                {
                    pt_out[0] = pt_start[0];
                    pt_out[1] = pt_start[1];
                    break;
                }
                pt2_scale(slide_disp, slide_disp, real_div(slide_dist, dir_mag));
                memcpy(slide_start, pt_out, sizeof(real_t) * 2);
                pt2_add(slide_end, slide_disp, pt_out);
                move_pt_(pt_out, slide_start, slide_end, rad, tree, root_node_idx,
                         max_recurse);
            }
        }
        break;
    }
#endif
}

void move_pt(real_t pt_out[2], real_t const pt_start[2], real_t const pt_end[2],
             real_t rad, struct bsp_tree const *tree)
{
    real_t disp[2];
    move_pt_(pt_out, pt_start, pt_end, rad, tree, tree->root_node_idx, 5);
    pt2_sub(disp, pt_out, pt_start);

#ifdef VERIFY_POS
    assert(!bsp_check_circle(pt_out, rad, tree, tree->root_node_idx));
#endif
}

#define MAX_DIST ((real_t)0x4000000)

/*
 * intersection of a ray and a line given the following representation:
 *
 * ray: pt = r.o + r.d * t
 * line: l.n.dot(pt) - l.d == 0
 *
 * l.n.dot(r.o + r.d * t) - l.d == 0
 * l.n.x * (r.o.x + r.d.x * t) + l.n.y * (r.o.y + r.d.y * t) - l.d == 0
 * l.n.x * r.o.x + l.n.x * r.d.x * t + l.n.y * r.o.y + l.n.y * r.d.y * t - l.d == 0
 * (l.n.x * r.o.x + l.n.y * r.o.y) + t * (l.n.x * r.d.x + l.n.y * r.d.y) - l.d == 0
 * l.n.dot(r.o) + t * l.n.dot(r.d) - l.d == 0
 * t * l.n.dot(r.d) == -l.n.dot(r.o) + l.d
 * t == (l.d - l.n.dot(r.o)) / l.n.dot(r.d)
 *
 */
#define TRACE_RAY_TOL (REAL_UNIT >> 8)
static void trace_ray_(struct ray_cast *cast, struct bsp_tree const *tree,
                       unsigned root_node_idx)
{
    real_t dist_to_origin;
    real_t rel_angle_cos;
    real_t projected_ray_origin;
    struct line_seg *seg;
    const struct bsp_node *node = tree->nodes + root_node_idx;

    assert(!cast->flags);
    assert(root_node_idx);

    pt2_dot(&rel_angle_cos, cast->ray_dir, node->line);
    pt2_dot(&projected_ray_origin, cast->ray_origin, node->line);
    dist_to_origin = real_sub(projected_ray_origin, node->line[2]);

    if (real_gt(rel_angle_cos, REAL_ZERO))
    {
        /*
         * ray points in the same direction as the hyperline normal.
         * This node will not be visible.
         */
        if (real_lte(dist_to_origin, REAL_ZERO))
        {
            if (node->back)
            {
                trace_ray_(cast, tree, node->back);
                if (cast->flags & RAY_HIT)
                    return;
            }

            if (!real_eq(rel_angle_cos, REAL_ZERO))
            {
                /* check this node for a potential hit. */
                real_t t;
                real_t hit[2];
                real_t seg_dir[2];
                real_t seg_start_proj, seg_end_proj;
                real_t hit_proj_seg;

                t = real_div(real_sub(node->line[2], projected_ray_origin),
                             rel_angle_cos);

                /*
                 * if rel_angle_cos is close to REAL_ZERO, then the real_div
                 * call can overflow, which leads to t being less than zero
                 * (in actuality it is/shoud be very large)
                 */
                if (real_gte(t, REAL_ZERO))
                {
                    pt2_scale(hit, cast->ray_dir, t);
                    pt2_add(hit, hit, cast->ray_origin);

                    unsigned i;
                    for (i = 0; i < node->seg_count; i++)
                    {
                        seg = tree->segs + node->seg_base + i;
                        if (seg->r_dat.sign != -1)
                            continue;
                        /* get the 1-dimensional projections of the line segment */
                        pt2_sub(seg_dir, tree->verts + 2 * seg->v[1],
                                tree->verts + 2 * seg->v[0]);
                        pt2_dot(&seg_start_proj, seg_dir,
                                tree->verts + 2 * seg->v[0]);
                        pt2_dot(&seg_end_proj, seg_dir,
                                tree->verts + 2 * seg->v[1]);

                        pt2_dot(&hit_proj_seg, seg_dir, hit);

                        if (real_gte(hit_proj_seg, seg_start_proj - TRACE_RAY_TOL) &&
                            real_lte(hit_proj_seg, seg_end_proj + TRACE_RAY_TOL) &&
                            real_lte(t, MAX_DIST))
                        {
                            /* hit */
                            cast->flags |= RAY_HIT;
                            cast->dist = t;
                            cast->hit = root_node_idx;
                            cast->bary = real_div(
                                real_sub(hit_proj_seg, seg_start_proj),
                                real_sub(seg_end_proj, seg_start_proj)
                                );
                            cast->tex_idx = seg->r_dat.tex_idx;
                            /*
                             * this is one of those moments where C++
                             * operator overload looks real tempting
                             */
                            cast->tc = real_mod(
                                real_add(real_mul(cast->bary, seg->r_dat.tc[1]),
                                         real_mul(real_sub(REAL_UNIT, cast->bary),
                                                  seg->r_dat.tc[0])), REAL_UNIT);
                            cast->y_min = seg->r_dat.y_min;
                            cast->y_max = seg->r_dat.y_max;

                            assert(!real_lt(t, REAL_ZERO));

                            return;
                        }
                    }
                }
            }
        }

        if (node->front)
        {
            trace_ray_(cast, tree, node->front);
            if (cast->flags & RAY_HIT)
                return;
        }
    }
    else
    {
        /*
         * ray points in the opposite direction of the hyperline normal.
         * This node will be visible if the ray hits it.
         */
        if (real_gte(dist_to_origin, REAL_ZERO))
        {
            if (node->front)
            {
                trace_ray_(cast, tree, node->front);
                if (cast->flags & RAY_HIT)
                    return;
            }

            if (!real_eq(rel_angle_cos, REAL_ZERO))
            {
                /* check this node for a potential hit. */
                real_t t;
                real_t hit[2];
                real_t seg_dir[2];
                real_t seg_start_proj, seg_end_proj;
                real_t hit_proj_seg;

                t = real_div(real_sub(node->line[2], projected_ray_origin),
                             rel_angle_cos);

                /*
                 * if rel_angle_cos is close to REAL_ZERO, then the real_div
                 * call can overflow, which leads to t being less than zero
                 * (in actuality it is/shoud be very large)
                 */
                if (real_gte(t, REAL_ZERO))
                {
                    pt2_scale(hit, cast->ray_dir, t);
                    pt2_add(hit, hit, cast->ray_origin);

                    unsigned i;
                    for (i = 0; i < node->seg_count; i++)
                    {
                        seg = tree->segs + node->seg_base + i;
                        if (seg->r_dat.sign != 1)
                            continue;
                        /* get the 1-dimensional projections of the line segment */
                        pt2_sub(seg_dir, tree->verts + 2 * seg->v[1],
                                tree->verts + 2 * seg->v[0]);
                        pt2_dot(&seg_start_proj, seg_dir,
                                tree->verts + 2 * seg->v[0]);
                        pt2_dot(&seg_end_proj, seg_dir,
                                tree->verts + 2 * seg->v[1]);

                        pt2_dot(&hit_proj_seg, seg_dir, hit);
                        if (real_gte(hit_proj_seg, seg_start_proj - TRACE_RAY_TOL) &&
                            real_lte(hit_proj_seg, seg_end_proj + TRACE_RAY_TOL) &&
                            real_lte(t, MAX_DIST))
                        {
                            /* hit */
                            cast->flags |= RAY_HIT;
                            cast->dist = t;
                            cast->hit = root_node_idx;
                            cast->bary = real_div(
                                real_sub(hit_proj_seg, seg_start_proj),
                                real_sub(seg_end_proj, seg_start_proj));
                            cast->tex_idx = seg->r_dat.tex_idx;
                            cast->tc = real_mod(
                                real_add(real_mul(cast->bary, seg->r_dat.tc[1]),
                                         real_mul(real_sub(REAL_UNIT, cast->bary),
                                                  seg->r_dat.tc[0])),
                                REAL_UNIT);
                            cast->y_min = seg->r_dat.y_min;
                            cast->y_max = seg->r_dat.y_max;

                            assert(!real_lt(t, REAL_ZERO));

                            return;
                        }
                    }
                }
            }
        }

        if (node->back)
        {
            trace_ray_(cast, tree, node->back);
            if (cast->flags & RAY_HIT)
                return;
        }

    }
}

void trace_ray(struct ray_cast *cast, struct bsp_tree const *tree)
{
    trace_ray_(cast, tree, tree->root_node_idx);
}

/*
 * if part of the interval between seg_in->start and seg_in->end is in front of line,
 * then this function will return 0 and that part will be returned in start_out
 * and seg_out->end.  else this function will return 1.
 */
static int sweep_front(struct sweep_seg *seg_out,
                       struct sweep_seg const *seg_in, int node_idx,
                       real_t const line_norm[2], real_t line_dist)
{
    real_t t, start_dist, end_dist, rel_dir, v[2], seg_len, t_tmp;

    pt2_dot(&start_dist, seg_in->start, line_norm);
    pt2_dot(&end_dist, seg_in->end, line_norm);
    start_dist = real_sub(start_dist, line_dist);
    end_dist = real_sub(end_dist, line_dist);
    pt2_sub(v, seg_in->end, seg_in->start);
    pt2_mag(&seg_len, v);
    seg_out->start_node = seg_in->start_node;
    seg_out->end_node = seg_in->end_node;

    if (real_eq(seg_len, REAL_ZERO))
    {
        /* TODO: tolerance? */
        if (real_gte(start_dist, REAL_ZERO))
        {
            memcpy(seg_out->start, seg_in->start, 2 * sizeof(real_t));
            memcpy(seg_out->end, seg_in->start, 2 * sizeof(real_t));
            return 0;
        }
        return 1;
    }

    if (real_lte(real_abs(start_dist), TOL))
        start_dist = REAL_ZERO;
    if (real_lte(real_abs(end_dist), TOL))
        end_dist = REAL_ZERO;

    pt2_norm(v, v);
    pt2_dot(&rel_dir, v, line_norm);

    if (real_lt(rel_dir, REAL_ZERO))
    {
        if (real_gte(start_dist, REAL_ZERO))
        {
            memcpy(seg_out->start, seg_in->start, 2 * sizeof(real_t));
            if (real_eq(start_dist, REAL_ZERO))
                seg_out->start_node = node_idx;
            if (real_gte(end_dist, REAL_ZERO))
            {
                if (real_eq(end_dist, REAL_ZERO))
                    seg_out->end_node = node_idx;
                memcpy(seg_out->end, seg_in->end, 2 * sizeof(real_t));
                seg_out->end_node = seg_in->end_node;
                return 0;
            }
            /* t == (l.d - l.n.dot(r.o)) / l.n.dot(r.d) */
            t = real_neg(real_div(start_dist, rel_dir));
            assert(real_valid(t));
            pt2_scale(seg_out->end, v, t);
            pt2_add(seg_out->end, seg_out->end, seg_in->start);

            /*
             * Make sure seg_out->end is not on the wrong side of the line.
             * This can happen sometimes due to real_t's lack of precision.
             */
            pt2_dot(&end_dist, seg_out->end, line_norm);
            end_dist -= line_dist;
            if (end_dist < REAL_ZERO)
            {
                while (t > REAL_ZERO)
                {
                    t_tmp = t - REAL_ATOM;
                    pt2_scale(seg_out->end, v, t_tmp);
                    pt2_add(seg_out->end, seg_out->end, seg_in->start);

                    pt2_dot(&end_dist, seg_out->end, line_norm);
                    end_dist -= line_dist;
                    if (end_dist >= REAL_ZERO)
                    {
                        t = t_tmp;
                        break;
                    }
                    else
                        t = t_tmp;
                }
            }

            pt2_scale(seg_out->end, v, t);
            pt2_add(seg_out->end, seg_out->end, seg_in->start);
            seg_out->end_node = node_idx;
            return 0;
        }
    }
    else if (real_gt(rel_dir, REAL_ZERO))
    {
        if (real_gt(end_dist, REAL_ZERO))
        {
            memcpy(seg_out->end, seg_in->end, 2 * sizeof(real_t));
            if (real_eq(end_dist, REAL_ZERO))
                seg_out->end_node = node_idx;
            if (real_gt(start_dist, REAL_ZERO))
            {
                if (real_eq(start_dist, REAL_ZERO))
                    seg_out->start_node = node_idx;
                memcpy(seg_out->start, seg_in->start, 2 * sizeof(real_t));
                seg_out->start_node = seg_in->start_node;
                return 0;
            }
            /* t == (l.d - l.n.dot(r.o)) / l.n.dot(r.d) */
            t = real_neg(real_div(start_dist, rel_dir));
            assert(real_valid(t));
            pt2_scale(seg_out->start, v, t);
            pt2_add(seg_out->start, seg_out->start, seg_in->start);

            /*
             * Make sure seg_out->start is not on the wrong side of the line.
             * This can happen sometimes due to real_t's lack of precision.
             */
            pt2_dot(&start_dist, seg_out->start, line_norm);
            start_dist -= line_dist;
            if (start_dist < REAL_ZERO)
            {
                while (t < REAL_UNIT)
                {
                    t_tmp = t + REAL_ATOM;
                    pt2_scale(seg_out->start, v, t_tmp);
                    pt2_add(seg_out->start, seg_out->start, seg_in->start);

                    pt2_dot(&start_dist, seg_out->start, line_norm);
                    start_dist -= line_dist;
                    if (start_dist >= REAL_ZERO)
                    {
                        t = t_tmp;
                        break;
                    }
                    else
                        t = t_tmp;
                }
            }

            pt2_scale(seg_out->start, v, t);
            pt2_add(seg_out->start, seg_out->start, seg_in->start);
            seg_out->start_node = node_idx;
            /* memcpy(seg_out->end, seg_in->end, 2 * sizeof(float)); */
            /* seg_out->end_node = seg_in->end_node; */
            return 0;
        }
    }
    else /* rel_dir == 0.0f */
    {
        /* parallel - this requires special handling */
        return -1;
    }

    return 1;
}

/*
 * if part of the interval between seg_in->start and seg_in->end behind line,
 * then this function will return 0 and that part will be returned in seg_out->start
 * and seg_out->end.  else this function will return 1.
 */
static int sweep_back(struct sweep_seg *seg_out,
                      struct sweep_seg const *seg_in, int node_idx,
                      real_t const line_norm[2], real_t line_dist)
{
    real_t t, start_dist, end_dist, rel_dir, v[2], seg_len, t_tmp;

    pt2_dot(&start_dist, seg_in->start, line_norm);
    pt2_dot(&end_dist, seg_in->end, line_norm);
    start_dist = real_sub(start_dist, line_dist);
    end_dist = real_sub(end_dist, line_dist);
    pt2_sub(v, seg_in->end, seg_in->start);
    pt2_mag(&seg_len, v);
    seg_out->start_node = seg_in->start_node;
    seg_out->end_node = seg_in->end_node;

    if (real_eq(seg_len, REAL_ZERO))
    {
        /* TODO: tolerance? */
        if (real_lte(start_dist, REAL_ZERO))
        {
            memcpy(seg_out->start, seg_in->start, 2 * sizeof(real_t));
            memcpy(seg_out->end, seg_in->start, 2 * sizeof(real_t));
            return 0;
        }
        return 1;
    }

    if (real_lte(real_abs(start_dist), TOL))
        start_dist = REAL_ZERO;
    if (real_lte(real_abs(end_dist), TOL))
        end_dist = REAL_ZERO;

    pt2_norm(v, v);
    pt2_dot(&rel_dir, v, line_norm);


    if (real_gt(rel_dir, REAL_ZERO))
    {
        if (real_lt(start_dist, REAL_ZERO))
        {
            memcpy(seg_out->start, seg_in->start, 2 * sizeof(real_t));
            if (real_eq(start_dist, REAL_ZERO))
                seg_out->start_node = node_idx;
            if (real_lt(end_dist, REAL_ZERO))
            {
                if (real_eq(end_dist, REAL_ZERO))
                    seg_out->end_node = node_idx;
                memcpy(seg_out->end, seg_in->end, 2 * sizeof(real_t));
                seg_out->end_node = seg_in->end_node;
                return 0;
            }
            /* t == (l.d - l.n.dot(r.o)) / l.n.dot(r.d) */
            t = real_neg(real_div(start_dist, rel_dir));
            assert(real_valid(t));
            pt2_scale(seg_out->end, v, t);
            pt2_add(seg_out->end, seg_out->end, seg_in->start);

            /*
             * Make sure seg_out->end is not on the wrong side of the line.
             * This can happen sometimes due to real_t's lack of precision.
             */
            pt2_dot(&end_dist, seg_out->end, line_norm);
            end_dist -= line_dist;
            if (end_dist > REAL_ZERO)
            {
                while (t > REAL_ZERO)
                {
                    t_tmp = t - REAL_ATOM;
                    pt2_scale(seg_out->end, v, t_tmp);
                    pt2_add(seg_out->end, seg_out->end, seg_in->start);

                    pt2_dot(&end_dist, seg_out->end, line_norm);
                    end_dist -= line_dist;
                    if (end_dist <= REAL_ZERO)
                    {
                        t = t_tmp;
                        break;
                    }
                    else
                        t = t_tmp;
                }
            }

            pt2_scale(seg_out->end, v, t);
            pt2_add(seg_out->end, seg_out->end, seg_in->start);
            seg_out->end_node = node_idx;
            /* memcpy(seg_out->end, seg_in->end, 2 * sizeof(float)); */
            /* seg_out->end_node = seg_in->end_node; */
            return 0;
        }
    }
    else if (real_lt(rel_dir, REAL_ZERO))
    {
        if (real_lt(end_dist, REAL_ZERO))
        {
            memcpy(seg_out->end, seg_in->end, 2 * sizeof(real_t));
            if (real_eq(end_dist, REAL_ZERO))
                seg_out->end_node = node_idx;

            if (real_gt(start_dist, REAL_ZERO))
            {
                /* t == (l.d - l.n.dot(r.o)) / l.n.dot(r.d) */
                t = real_neg(real_div(start_dist, rel_dir));
                assert(real_valid(t));
                pt2_scale(seg_out->start, v, t);
                pt2_add(seg_out->start, seg_out->start, seg_in->start);

                /*
                 * Make sure seg_out->start is not on the wrong side of the line.
                 * This can happen sometimes due to real_t's lack of precision.
                 */
                pt2_dot(&start_dist, seg_out->start, line_norm);
                start_dist -= line_dist;
                if (start_dist > REAL_ZERO)
                {
                    while (t < REAL_UNIT)
                    {
                        t_tmp = t + REAL_ATOM;
                        pt2_scale(seg_out->start, v, t_tmp);
                        pt2_add(seg_out->start, seg_out->start, seg_in->start);

                        pt2_dot(&start_dist, seg_out->start, line_norm);
                        start_dist -= line_dist;
                        if (start_dist <= REAL_ZERO)
                        {
                            t = t_tmp;
                            break;
                        }
                        else
                            t = t_tmp;
                    }
                }

                pt2_scale(seg_out->start, v, t);
                pt2_add(seg_out->start, seg_out->start, seg_in->start);
                seg_out->start_node = node_idx;
                return 0;
            }
            else if (real_lt(start_dist, REAL_ZERO))
            {
                if (real_eq(start_dist, REAL_ZERO))
                    seg_out->start_node = node_idx;
                memcpy(seg_out->start, seg_in->start, 2 * sizeof(real_t));
                seg_out->start_node = seg_in->start_node;
                return 0;
            }
            else
            {
                memcpy(seg_out->start, seg_in->start, 2 * sizeof(real_t));
                seg_out->start_node = node_idx;
                return 0;
            }
        }
    }
    else
    {
        return -1;
    }

    return 1;
}

/* return 1 on hit, 0 on non-hit */
static int bsp_check_circle(real_t const pos[2], real_t rad,
                            struct bsp_tree const *tree, unsigned node_idx)
{
    struct bsp_node *node;
    real_t signed_dist; /* dist from pos to node (not including rad) */
    real_t front_dist, back_dist;

    node = tree->nodes + node_idx;
    pt2_dot(&signed_dist, pos, node->line);
    signed_dist -= node->line[2];

    front_dist = signed_dist + rad;
    back_dist = signed_dist - rad;

    /*
     * check in front.  Using TOL here (and below when checking behind) in lieu
     * of REAL_ZERO is questionable and may need to be revisited.  The reason
     * is that there are cases where you get false positives because the circle
     * can end up with penetration which is just barely below zero.
     *
     * I actually believe this is returning false positives because it can go
     * down the front subtree when front_dist is between -TOL and REAL_ZERO
     */
    if (front_dist >= -TOL)
        if (node->front)
            if (bsp_check_circle(pos, rad, tree, node->front))
                return 1;

    /* check behind */
    if (back_dist <= -TOL)
    {
        if (node->back)
            return bsp_check_circle(pos, rad, tree, node->back);
        return back_dist < -TOL;
    }

    return 0;
}

static int sweep_circle_(real_t end_out[2], int *node_out,
                         struct sweep_seg* seg, real_t rad,
                         struct bsp_tree const* tree, unsigned node_idx)
{
    struct sweep_seg front_seg, back_seg;
    real_t ray_dir[2];
    real_t rel_dir;
    const struct bsp_node *node;
    real_t const *ln;
    int hit, hit_tmp, node_out_tmp;
    real_t tmp[2];
    int sweep_tp;
    real_t dist_seg_start, sep;
    real_t correction[2], correction_dist;
    real_t dist_seg_end;
    real_t move_dist;

    hit = SWEEP_NO_HIT;
    memcpy(end_out, seg->start, sizeof(real_t) * 2);
    node_out_tmp = 0;
    *node_out = 0;
    node = tree->nodes + node_idx;
    ln = node->line;
    pt2_sub(ray_dir, seg->end, seg->start);

    if (real_eq(ray_dir[0], REAL_ZERO) && real_eq(ray_dir[1], REAL_ZERO))
    {
        if (bsp_check_circle(seg->end, rad, tree, node_idx))
            return SWEEP_HALT;
        return SWEEP_NO_HIT;
    }

    pt2_dot(&rel_dir, ray_dir, node->line);
    memcpy(end_out, seg->end, 2 * sizeof(real_t));
    front_seg.start_node = front_seg.end_node = 0;
    back_seg.start_node = back_seg.end_node = 0;
    pt2_dot(&dist_seg_start, ln, seg->start);
    pt2_dot(&dist_seg_end, ln, seg->end);
    dist_seg_start = real_sub(dist_seg_start, ln[2]);
    dist_seg_end = real_sub(dist_seg_end, ln[2]);

    if (real_lt(rel_dir, REAL_ZERO))
    {
        if ((sweep_tp =
             sweep_front(&front_seg, seg, node_idx, ln, real_sub(ln[2], rad))) == 0)
        {
            if (node->front)
            {
                switch ((hit_tmp = sweep_circle_(tmp, &node_out_tmp, &front_seg,
                                                 rad, tree, node->front)))
                {
                case SWEEP_NO_HIT:
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    break;
                case SWEEP_HALT:
                    hit = SWEEP_HALT;
                    break;
                default:
                    hit = hit_tmp;
                    *node_out = node_out_tmp;
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    memcpy(seg->end, end_out, sizeof(real_t) * 2);
                    break;
                }
            }
        }
        else if (sweep_tp == -1)
        {
            goto parallel;
            /* abort(); */
        }

        if ((sweep_tp = sweep_back(&back_seg, seg, node_idx, ln, real_add(ln[2], rad))) == 0)
        {
            if (node->back)
            {
                switch ((hit_tmp = sweep_circle_(tmp, &node_out_tmp, &back_seg, rad,
                                                 tree, node->back)))
                {
                case SWEEP_NO_HIT:
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    break;
                case SWEEP_HALT:
                    hit = SWEEP_HALT;
                    break;
                default:
                    hit = hit_tmp;
                    *node_out = node_out_tmp;
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    memcpy(seg->end, end_out, sizeof(real_t) * 2);
                    break;
                }
            }
            else
            {
                memcpy(end_out, back_seg.start, sizeof(real_t) * 2);
                memcpy(seg->end, end_out, sizeof(real_t) * 2);
                *node_out = back_seg.start_node ?
                    back_seg.start_node : node_idx;
                assert(*node_out);
                hit = SWEEP_HIT;
            }
        }
        else if (sweep_tp == -1)
        {
            goto parallel;
            /* abort(); */
        }

    }
    else if (real_gt(rel_dir, REAL_ZERO))
    {
        if ((sweep_tp = sweep_back(&back_seg, seg, node_idx, ln, real_add(ln[2], rad))) == 0)
        {
            if (node->back)
            {
                switch ((hit_tmp = sweep_circle_(tmp, &node_out_tmp, &back_seg,
                                                 rad, tree, node->back)))
                {
                case SWEEP_HALT:
                    hit = SWEEP_HALT;
                    break;
                case SWEEP_NO_HIT:
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    break;
                default:
                    hit = hit_tmp;
                    *node_out = node_out_tmp;
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    memcpy(seg->end, end_out, sizeof(real_t) * 2);
                    break;
                }
            }
            else
            {
                memcpy(end_out, back_seg.start, sizeof(real_t) * 2);
                memcpy(seg->end, end_out, sizeof(real_t) * 2);
                *node_out = back_seg.start_node ?
                    back_seg.start_node : node_idx;
                assert(*node_out);
                hit = SWEEP_HIT;
            }
        }
        else if (sweep_tp == -1)
        {
            goto parallel;
            /* abort(); */
        }

        if (node->front)
        {
            if ((sweep_tp = sweep_front(&front_seg, seg, node_idx, ln, real_sub(ln[2], rad))) == 0)
            {
                switch ((hit_tmp = sweep_circle_(tmp, &node_out_tmp, &front_seg,
                                                 rad, tree, node->front)))
                {
                case SWEEP_HALT:
                    hit = SWEEP_HALT;
                    break;
                case SWEEP_NO_HIT:
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    break;
                default:
                    hit = hit_tmp;
                    *node_out = node_out_tmp;
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    memcpy(seg->end, end_out, sizeof(real_t) * 2);
                    break;
                }
            }
        }
    }
    else /* rel_dir == 0.0f */
    {
    parallel:
        pt2_dot(&dist_seg_start, ln, seg->start);
        dist_seg_start = real_sub(dist_seg_start, ln[2]);
        if (real_gte(dist_seg_start, rad))
        {
            if (node->front)
            {
                switch ((hit_tmp = sweep_circle_(tmp, &node_out_tmp, seg,
                                                 rad, tree, node->front)))
                {
                case SWEEP_HALT:
                    hit = SWEEP_HALT;
                    break;
                case SWEEP_NO_HIT:
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    break;
                default:
                    hit = hit_tmp;
                    *node_out = node_out_tmp;
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    memcpy(seg->end, end_out, sizeof(real_t) * 2);
                    break;
                }
            }
        }
        else if (real_lte(dist_seg_start, real_neg(rad)))
        {
            if (node->back)
            {
                switch ((hit_tmp = sweep_circle_(tmp, &node_out_tmp, seg,
                                                 rad, tree, node->back)))
                {
                case SWEEP_HALT:
                    hit = SWEEP_HALT;
                    break;
                case SWEEP_NO_HIT:
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    break;
                default:
                    hit = hit_tmp;
                    *node_out = node_out_tmp;
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    memcpy(seg->end, end_out, sizeof(real_t) * 2);
                    break;
                }
            }
            else
            {
                memcpy(end_out, seg->start, sizeof(real_t) * 2);
                memcpy(seg->end, end_out, sizeof(real_t) * 2);
                *node_out = node_idx;
                hit = SWEEP_HIT;
            }
        }
        else
        {
            /* seg is sliding across node */
            /*
             * The semantics of sliding dont actually fit in very well here;
             * This algo determines where the line seg hits the BSP tree, it
             * doesn't *technically* implement movement
             */
            if (node->front)
            {
                switch ((hit_tmp = sweep_circle_(tmp, &node_out_tmp, seg,
                                                 rad, tree, node->front)))
                {
                case SWEEP_HALT:
                    hit = SWEEP_HALT;
                    goto the_end;
                case SWEEP_NO_HIT:
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    break;
                default:
                    hit = hit_tmp;
                    *node_out = node_out_tmp;
                    memcpy(end_out, tmp, sizeof(real_t) * 2);
                    memcpy(seg->end, end_out, sizeof(real_t) * 2);
                    goto the_end;
                }
            }
            hit = SWEEP_SLIDE;
            memcpy(end_out, seg->end, sizeof(real_t) * 2);
            *node_out = node_idx;
            pt2_dot(&sep, end_out, ln);
            sep = real_sub(sep, real_add(ln[2], rad));
            if (real_lt(sep, REAL_ZERO))
            {
                /* move point along normal */
                pt2_scale(correction, ln, real_neg(sep));
                pt2_add(end_out, end_out, correction);

#if 0
                /* This feels like it should be more correct than the code above,
                 * but somehow it leads to the player getting stuck in walls sometimes.
                 */
                /* move point backwards along ray */
                /* TODO: need to divide correction_dist
                 * and correction by pt2_mag(ray_dir) */
                pt2_dot(&correction_dist, ln, ray_dir);
                if (!real_eq(correction_dist, REAL_ZERO))
                {
                    correction_dist = real_neg(real_div(sep, correction_dist));
                    pt2_scale(correction, ray_dir, correction_dist);
                    pt2_add(end_out, end_out, correction);
                }
                else
                {
                    /* move point along normal */
                    pt2_scale(correction, ln, real_neg(sep));
                    pt2_add(end_out, end_out, correction);
                }
#endif
            }
        }
    }
the_end:
    assert((hit == SWEEP_HALT) || !(hit || *node_out) || (hit && *node_out));

    return hit;
}

int sweep_circle(real_t end_out[2], int *node_out,
                 struct sweep_seg const* seg, real_t rad,
                 struct bsp_tree const* tree)
{
    int ret;
    struct sweep_seg seg_cp;
    memcpy(&seg_cp, seg, sizeof(struct sweep_seg));
    *node_out = 0;
    ret = sweep_circle_(end_out, node_out, &seg_cp,
                        rad, tree, tree->root_node_idx);

#ifdef VERIFY_POS
    switch (ret)
    {
    case SWEEP_HALT:
        assert(!bsp_check_circle(seg->start, rad, tree, tree->root_node_idx));
        break;
    case SWEEP_HIT:
    case SWEEP_SLIDE:
    case SWEEP_NO_HIT:
        assert(!bsp_check_circle(end_out, rad, tree, tree->root_node_idx));
        break;
    default:
        fprintf(stderr, "%s: unrecognized hit type 0x%x\n", __func__, ret);
    }
#endif

    return ret;
}

/*
 * this function assumes counter-clockwise winding of line segments.
 * it may append additional vertices to verts but it will not otherwise
 * modify the vertex array.
 *
 * returns the index of the root node.
 */
static unsigned gen_bsp_(struct bsp_node **nodes, unsigned *n_nodes,
                         real_t **verts, unsigned *n_verts,
                         struct line_seg **segs_out, unsigned *n_segs_out,
                         const struct line_seg *segs, unsigned n_segs)
{
    unsigned ln_idx;
    struct line_seg *front_segs = NULL, *back_segs = NULL;
    unsigned n_front = 0, n_back = 0;
    struct line_seg const *root_seg = segs;
    real_t const *root_p1 = *verts + root_seg->v[0] * 2;
    real_t const *root_p2 = *verts + root_seg->v[1] * 2;
    real_t root_vec[2];
    real_t root_ln[3];

    /* convert the root from a line segment to a hyperline */
    pt2_sub(root_ln, root_p2, root_p1);
    pt2_norm(root_ln, root_ln);
    root_vec[0] = root_ln[0];
    root_vec[1] = root_ln[1];
    assert(root_seg->v[0] < *n_verts && root_seg->v[1] < *n_verts);
    assert(real_valid(root_ln[0]) &&
           real_valid(root_ln[1]) &&
           real_valid(root_ln[2]));
    pt2_perp(root_ln, root_ln);
    pt2_dot(root_ln + 2, root_ln, root_p1);

    struct line_seg *node_line_segs = malloc(sizeof(struct line_seg));
    node_line_segs[0] = *root_seg;
    node_line_segs[0].r_dat.sign = 1;
    unsigned node_line_seg_count = 1;

    for (ln_idx = 1; ln_idx < n_segs; ln_idx++)
    {
        struct line_seg const *ln = segs + ln_idx;
        real_t const *p1 = *verts + ln->v[0] * 2;
        real_t const *p2 = *verts + ln->v[1] * 2;
        real_t ln_vec[2];
        real_t ln_angle;
        real_t d1, d2;

        ln_dist_to(&d1, root_ln, p1);
        ln_dist_to(&d2, root_ln, p2);

        if (real_lt(real_abs(d1), TOL))
            d1 = REAL_ZERO;
        if (real_lt(real_abs(d2), TOL))
            d2 = REAL_ZERO;

        /*
         * get the angle of the line relative to root.  This will be needed if
         * the line coincides with root to decide which direction it is facing.
         */
        pt2_sub(ln_vec, p2, p1);
        pt2_norm(ln_vec, ln_vec);
        pt2_dot(&ln_angle, ln_vec, root_vec);

        /* XXX floating point tolerance? */
        if (real_eq(d1, REAL_ZERO) && real_eq(d2, REAL_ZERO))
        {
            real_t v[2];
            real_t norm_dot;

            node_line_segs = realloc(node_line_segs, sizeof(struct line_seg) * ++node_line_seg_count);
            node_line_segs[node_line_seg_count-1] = *ln;

            pt2_sub(v, p2, p1);
            pt2_norm(v, v);
            pt2_perp(v, v);
            pt2_dot(&norm_dot, v, root_ln);
            if (real_gt(norm_dot, REAL_ZERO))
                node_line_segs[node_line_seg_count - 1].r_dat.sign = 1;
            else
                node_line_segs[node_line_seg_count - 1].r_dat.sign = -1;
        }
        else if (real_gte(d1, REAL_ZERO) && real_gte(d2, REAL_ZERO))
        {
            /* d1 >= 0.0f, d2 >= 0.0f */
            /* add the line segment to the list of front segments */
            front_segs = realloc(front_segs, sizeof(struct line_seg) * ++n_front);
            front_segs[n_front - 1] = *ln;
        }
        else if (real_lte(d1, REAL_ZERO) && real_lte(d2, REAL_ZERO))
        {
            /* d1 < 0.0f, d2 < 0.0f */
            /* add the line segment to the list of back segments */
            back_segs = realloc(back_segs, sizeof(struct line_seg) * ++n_back);
            back_segs[n_back - 1] = *ln;
        }
        else/* if (d1 >= 0.0f)*/
        {
            /* d1 >= 0.0f AND d2 < 0.0f OR d1 < 0.0f AND d2 >= 0.0f*/
            /* split the line segment along root_ln, one seg in front and one seg behind */
            real_t dir[2];
            real_t numer, denom;
            pt2_sub(dir, p2, p1);
            pt2_norm(dir, dir);

            pt2_dot(&denom, root_ln, dir);
            pt2_dot(&numer, root_ln, p1);
            numer = real_sub(root_ln[2], numer);

            /* 
             * barring floating point precision problems, this should not be
             * possible because both points would have both been classified as
             * being in front of the root hyperline
             */
            assert(!real_eq(denom, REAL_ZERO));

            *verts = realloc(*verts, ++*n_verts * 2 * sizeof(real_t));
            p1 = *verts + ln->v[0] * 2;
            real_t *pt_new = *verts + (*n_verts - 1) * 2;
            pt2_scale(pt_new, dir, real_div(numer, denom));
            pt2_add(pt_new, pt_new, p1);

            front_segs = realloc(front_segs, sizeof(struct line_seg) * ++n_front);
            back_segs = realloc(back_segs, sizeof(struct line_seg) * ++n_back);

            if (real_gte(d1, REAL_UNIT))
            {
                real_t proj[3];
                real_t bary, tc_split;

                pt2_dot(proj, (*verts) + 2 * ln->v[0], dir);
                pt2_dot(proj+1, (*verts) + 2 * (*n_verts - 1), dir);
                pt2_dot(proj+2, (*verts) + 2 * ln->v[1], dir);

                bary = real_div(real_sub(proj[1], proj[0]),
                                real_sub(proj[2], proj[0]));
                tc_split = real_add(
                    real_mul(real_sub(REAL_UNIT, bary), ln->r_dat.tc[0]),
                    real_mul(bary, ln->r_dat.tc[1]));

                front_segs[n_front - 1].v[0] = ln->v[0];
                front_segs[n_front - 1].v[1] = *n_verts - 1;
                back_segs[n_back - 1].v[0] = *n_verts - 1;
                back_segs[n_back - 1].v[1] = ln->v[1];

                front_segs[n_front - 1].r_dat.tex_idx = ln->r_dat.tex_idx;
                front_segs[n_front - 1].r_dat.y_min = ln->r_dat.y_min;
                front_segs[n_front - 1].r_dat.y_max = ln->r_dat.y_max;
                front_segs[n_front - 1].r_dat.tc[0] = ln->r_dat.tc[0];
                front_segs[n_front - 1].r_dat.tc[1] = tc_split;

                back_segs[n_back - 1].r_dat.tex_idx = ln->r_dat.tex_idx;
                back_segs[n_back - 1].r_dat.y_min = ln->r_dat.y_min;
                back_segs[n_back - 1].r_dat.y_max = ln->r_dat.y_max;
                back_segs[n_back - 1].r_dat.tc[0] = tc_split;
                back_segs[n_back - 1].r_dat.tc[1] = ln->r_dat.tc[1];
            }
            else
            {
                real_t proj[3];
                real_t bary, tc_split;

                pt2_dot(proj, (*verts) + 2 * ln->v[0], dir);
                pt2_dot(proj+1, (*verts) + 2 * (*n_verts - 1), dir);
                pt2_dot(proj+2, (*verts) + 2 * ln->v[1], dir);

                bary = real_div(real_sub(proj[1], proj[0]),
                                real_sub(proj[2], proj[0]));
                tc_split = real_add(
                    real_mul(real_sub(REAL_UNIT, bary), ln->r_dat.tc[0]),
                    real_mul(bary, ln->r_dat.tc[1]));

                front_segs[n_front - 1].v[0] = *n_verts - 1;
                front_segs[n_front - 1].v[1] = ln->v[1];
                back_segs[n_back - 1].v[0] = ln->v[0];
                back_segs[n_back - 1].v[1] = *n_verts - 1;

                front_segs[n_front - 1].r_dat.tex_idx = ln->r_dat.tex_idx;
                front_segs[n_front - 1].r_dat.y_min = ln->r_dat.y_min;
                front_segs[n_front - 1].r_dat.y_max = ln->r_dat.y_max;
                front_segs[n_front - 1].r_dat.tc[0] = tc_split;
                front_segs[n_front - 1].r_dat.tc[1] = ln->r_dat.tc[1];

                back_segs[n_back - 1].r_dat.tex_idx = ln->r_dat.tex_idx;
                back_segs[n_back - 1].r_dat.y_min = ln->r_dat.y_min;
                back_segs[n_back - 1].r_dat.y_max = ln->r_dat.y_max;
                back_segs[n_back - 1].r_dat.tc[0] = ln->r_dat.tc[0];
                back_segs[n_back - 1].r_dat.tc[1] = tc_split;
            }
        }
    }

    /* add the new node */
    *nodes = realloc(*nodes, sizeof(struct bsp_node) * ++*n_nodes);
    unsigned new_node_idx = *n_nodes - 1;
    struct bsp_node *new_node = *nodes + new_node_idx;
    new_node->line[0] = root_ln[0];
    new_node->line[1] = root_ln[1];
    new_node->line[2] = root_ln[2];
    new_node->front = new_node->back = 0;
    new_node->seg_base = *n_segs_out;
    new_node->seg_count = node_line_seg_count;
    *segs_out = realloc(*segs_out, (*n_segs_out += node_line_seg_count) *
                        sizeof(struct line_seg));
    memcpy(*segs_out + new_node->seg_base, node_line_segs,
           new_node->seg_count * sizeof(struct line_seg));
    free(node_line_segs);

    assert(real_valid(root_ln[0]) &&
           real_valid(root_ln[1]) &&
           real_valid(root_ln[2]));

    /* now recurse */
    if (n_front > 0)
    {
        unsigned front_idx = gen_bsp_(nodes, n_nodes, verts, n_verts, segs_out,
                                      n_segs_out, front_segs, n_front);
        new_node = *nodes + new_node_idx;
        new_node->front = front_idx;
    }

    if (n_back > 0)
    {
        unsigned back_idx = gen_bsp_(nodes, n_nodes, verts, n_verts, segs_out,
                                     n_segs_out, back_segs, n_back);
        new_node = *nodes + new_node_idx;
        new_node->back = back_idx;
    }

    free(front_segs);
    free(back_segs);

    return new_node_idx;
}

void gen_bsp(struct bsp_tree *tree, real_t const *verts, unsigned n_verts,
             const struct line_seg *segs, unsigned n_segs)
{
    tree->nodes = calloc(1, sizeof(struct bsp_node));
    tree->n_nodes = 1;
    tree->verts = (real_t*)malloc(sizeof(real_t) * 2 * n_verts);
    tree->n_verts = n_verts;
    tree->segs = NULL;
    tree->n_segs = 0;
    memcpy(tree->verts, verts, tree->n_verts * sizeof(real_t) * 2);
    tree->root_node_idx = gen_bsp_(&tree->nodes, &tree->n_nodes, &tree->verts,
                                   &tree->n_verts, &tree->segs, &tree->n_segs,
                                   segs, n_segs);
}

int bsp_in_empty_space(real_t const pt[2], struct bsp_tree const *tree)
{
    return bsp_in_empty_space_(pt, tree, tree->root_node_idx);
}

static int bsp_in_empty_space_(real_t const pt[2],
                               struct bsp_tree const *tree,
                               unsigned root_node_idx)
{
    struct bsp_node const *root = tree->nodes + root_node_idx;
    real_t d;

    pt2_dot(&d, root->line, pt);
    d = real_sub(d, root->line[2]);

    if (real_lt(d, REAL_ZERO))
    {
        if (root->back)
            return bsp_in_empty_space_(pt, tree, root->back);
        return 0;
    }

    /* XXX if point is on the node, it gets pushed down the front subtree */
    if (root->front)
        return bsp_in_empty_space_(pt, tree, root->front);
    return 1;
}

void bsp_clip_pt(real_t pt[2], struct bsp_tree const *tree)
{
    bsp_clip_pt_(pt, tree, tree->root_node_idx);
}

static void bsp_clip_pt_(real_t pt[2], struct bsp_tree const *tree,
                 unsigned root_node_idx)
{
    unsigned node_idx = root_node_idx;
    real_t d;
    real_t pen_vec[2], pen_sq_dist = REAL_MAX;
    struct line_seg *seg;

    while (node_idx)
    {
        struct bsp_node const *n = tree->nodes + node_idx;
        pt2_dot(&d, n->line, pt);
        d = real_sub(d, n->line[2]);

        if (real_lte(d, REAL_ZERO))
        {
            node_idx = n->back;

            unsigned i;
            for (i = 0; i < n->seg_count; i++)
            {
                seg = tree->segs + n->seg_base + n->seg_count;
                /* update penetration */
                real_t sq_dist = real_mul(d, d); /* dist being considered */
                if (real_lt(sq_dist, pen_sq_dist))
                {
                    /* Does the projection onto this line lie between the two pts? */
                    real_t pt_proj;
                    real_t ln_dir[2] = { real_neg(n->line[1]), real_neg(n->line[0]) };
                    real_t ln_bnds[2];

                    pt2_dot(ln_bnds, ln_dir, tree->verts + 2 * seg->v[0]);
                    pt2_dot(ln_bnds+1, ln_dir, tree->verts + 2 * seg->v[1]);
                    pt2_dot(&pt_proj, ln_dir, pt);

                    if ((real_gte(pt_proj, ln_bnds[0]) &&
                         real_lte(pt_proj, ln_bnds[1])) ||
                        (real_lte(pt_proj, ln_bnds[0]) &&
                         real_gte(pt_proj, ln_bnds[1])))
                    {
                        pen_sq_dist = sq_dist;
                        pen_vec[0] = n->line[0];
                        pen_vec[1] = n->line[1];

                        pt2_scale(pen_vec, pen_vec, real_from_int(seg->r_dat.sign));
                    }
                }

                real_t disp[2];
                pt2_sub(disp, tree->verts + 2 * seg->v[0], pt);
                sq_dist = real_add(real_mul(disp[0], disp[0]),
                                   real_mul(disp[1], disp[1]));
                if (real_lt(sq_dist, pen_sq_dist))
                {
                    pen_sq_dist = sq_dist;
                    pen_vec[0] = disp[0];
                    pen_vec[1] = disp[1];

                    /* move in the general direction of the plane's normal */
                    real_t c;
                    pt2_dot(&c, pen_vec, n->line);
                    if (real_lt(c, REAL_ZERO))
                        pt2_scale(pen_vec, pen_vec, real_neg(REAL_UNIT));
                }

                pt2_sub(disp, tree->verts + 2 * seg->v[1], pt);
                sq_dist = real_add(real_mul(disp[0], disp[0]),
                                   real_mul(disp[1], disp[1]));
                if (real_lt(sq_dist, pen_sq_dist))
                {
                    pen_sq_dist = sq_dist;
                    pen_vec[0] = disp[0];
                    pen_vec[1] = disp[1];

                    /* move in the general direction of the plane's normal */
                    real_t c;
                    pt2_dot(&c, pen_vec, n->line);
                    if (real_lt(c, REAL_ZERO))
                        pt2_scale(pen_vec, pen_vec, real_neg(REAL_UNIT));
                }
            }
        }
        else
        {
            if (!(node_idx = n->front))
                return;
        }
    }

    pt2_norm(pen_vec, pen_vec);
    pt2_scale(pen_vec, pen_vec, real_sqrt(pen_sq_dist));
    pt2_add(pt, pen_vec, pt);
}

void cleanup_bsp_tree(struct bsp_tree *tree)
{
    if (tree->segs)
        free(tree->segs);
    if (tree->nodes)
        free(tree->nodes);
    if (tree->verts)
        free(tree->verts);
    memset(tree, 0, sizeof(struct bsp_tree));
}

struct level_file_header
{
    unsigned offset_nodes;
    unsigned offset_verts;
    unsigned offset_segs;
    unsigned n_nodes;
    unsigned n_verts;
    unsigned n_segs;
    unsigned root_node_idx;
};

#define WRITE_VAR(var, stream)                                  \
    if (stream_write(&(var), sizeof((var)), 1, stream) != 1)    \
    {                                                           \
        goto on_error;                                          \
    }

#define READ_VAR(var, stream)                                   \
    if (stream_read(&(var), sizeof((var)), 1, stream) != 1)     \
    {                                                           \
        goto on_error;                                          \
    }


static int write_header(stream_t *stream, struct level_file_header const *hdr)
{
    WRITE_VAR(hdr->offset_nodes, stream);
    WRITE_VAR(hdr->offset_verts, stream);
    WRITE_VAR(hdr->offset_segs, stream);
    WRITE_VAR(hdr->n_nodes, stream);
    WRITE_VAR(hdr->n_verts, stream);
    WRITE_VAR(hdr->n_segs, stream);
    WRITE_VAR(hdr->root_node_idx, stream);
    return 0;
on_error:
    return -1;
}

static int read_header(stream_t *stream, struct level_file_header *hdr)
{
    READ_VAR(hdr->offset_nodes, stream);
    READ_VAR(hdr->offset_verts, stream);
    READ_VAR(hdr->offset_segs, stream);
    READ_VAR(hdr->n_nodes, stream);
    READ_VAR(hdr->n_verts, stream);
    READ_VAR(hdr->n_segs, stream);
    READ_VAR(hdr->root_node_idx, stream);
    return 0;
on_error:
    return -1;
}

int save_bsp(stream_t *stream, struct bsp_tree const *tree)
{
    int idx;
    long start_pos;
    struct line_seg *seg;
    struct bsp_node *node;
    struct level_file_header hdr;
    long cur_pos;

    start_pos = stream_tell(stream);
    if (start_pos < 0)
        goto stream_error;

    TRACE_DBG("Writing bsp tree starting at offset %d\n", (int)start_pos);

    /*
     * call write_header solely to advance the write_pointer.
     * We'll write again later when we have the offsets.
     * Also zero out hdr to shut up valgrind.
     */
    memset(&hdr, 0, sizeof(hdr));
    if (write_header(stream, &hdr) != 0)
        goto stream_error;
    hdr.offset_nodes = stream_tell(stream);
    if (hdr.offset_nodes < 0)
        goto stream_error;
    for (idx = 0; idx < tree->n_nodes; idx++)
    {
        node = tree->nodes + idx;
        WRITE_VAR(node->front, stream);
        WRITE_VAR(node->back, stream);
        if (stream_write(node->line, sizeof(real_t), 3, stream) != 3)
            goto on_error;
        WRITE_VAR(node->seg_base, stream);
        WRITE_VAR(node->seg_count, stream);
    }
    hdr.offset_verts = stream_tell(stream);
    if (hdr.offset_verts < 0)
        goto stream_error;

    if (stream_write(tree->verts, 2 * sizeof(real_t), tree->n_verts, stream) !=
        tree->n_verts)
    {
        goto stream_error;
    }

    hdr.offset_segs = stream_tell(stream);
    if (hdr.offset_segs < 0)
        goto stream_error;

    for (idx = 0; idx < tree->n_segs; idx++)
    {
        seg = tree->segs + idx;
        if (stream_write(seg->v, sizeof(unsigned), 2, stream) != 2)
            goto stream_error;
        WRITE_VAR(seg->r_dat.tex_idx, stream);
        if (stream_write(seg->r_dat.tc, sizeof(real_t), 2, stream) != 2)
            goto stream_error;
        WRITE_VAR(seg->r_dat.y_min, stream);
        WRITE_VAR(seg->r_dat.y_max, stream);
        WRITE_VAR(seg->r_dat.sign, stream);
    }
    cur_pos = stream_tell(stream);
    stream_seek(stream, start_pos, STREAM_SEEK_SET);

    hdr.root_node_idx = tree->root_node_idx;
    hdr.n_nodes = tree->n_nodes;
    hdr.n_verts = tree->n_verts;
    hdr.n_segs = tree->n_segs;

    TRACE_DBG("%d nodes\n", hdr.n_nodes);
    TRACE_DBG("%d verts\n", hdr.n_verts);
    TRACE_DBG("%d segs\n", hdr.n_segs);

    if (write_header(stream, &hdr) != 0)
        goto stream_error;

    stream_seek(stream, cur_pos, STREAM_SEEK_SET);

    return 0;

stream_error:
on_error:
    return -1;
}

int load_bsp(stream_t *stream, struct bsp_tree *tree)
{
    int idx;
    struct line_seg *seg;
    struct bsp_node *node;
    struct level_file_header hdr;
    long base;

    base = stream_tell(stream);
    TRACE_DBG("Reading bsp tree starting at offset %d\n", (int)base);

    memset(tree, 0, sizeof(struct bsp_tree));
    if (read_header(stream, &hdr) != 0)
        goto on_error;

    TRACE_DBG("%d nodes\n", hdr.n_nodes);
    TRACE_DBG("%d verts\n", hdr.n_verts);
    TRACE_DBG("%d segs\n", hdr.n_segs);

    tree->n_nodes = hdr.n_nodes;
    tree->n_verts = hdr.n_verts;
    tree->n_segs = hdr.n_segs;
    tree->root_node_idx = hdr.root_node_idx;
    if (!(tree->nodes = (struct bsp_node*)malloc(sizeof(struct bsp_node) *
                                                 hdr.n_nodes)))
    {
        goto on_error;
    }
    if (!(tree->segs = (struct line_seg*)malloc(sizeof(struct line_seg) *
                                                hdr.n_segs)))
    {
        goto on_error;
    }
    if (!(tree->verts = (real_t*)malloc(sizeof(real_t) * 2 * hdr.n_verts)))
        goto on_error;
    stream_seek(stream, hdr.offset_nodes, STREAM_SEEK_SET);
    for (idx = 0; idx < hdr.n_nodes; idx++)
    {
        node = tree->nodes + idx;
        READ_VAR(node->front, stream);
        READ_VAR(node->back, stream);
        if (stream_read(node->line, sizeof(real_t), 3, stream) != 3)
            goto on_error;
        READ_VAR(node->seg_base, stream);
        READ_VAR(node->seg_count, stream);
    }
    stream_seek(stream, hdr.offset_verts, STREAM_SEEK_SET);
    if (stream_read(tree->verts, sizeof(real_t) * 2, tree->n_verts, stream) !=
        tree->n_verts)
        goto on_error;
    stream_seek(stream, hdr.offset_segs, STREAM_SEEK_SET);
    for (idx = 0; idx < hdr.n_segs; idx++)
    {
        seg = tree->segs + idx;
        if (stream_read(seg->v, sizeof(real_t), 2, stream) != 2)
            goto on_error;
        READ_VAR(seg->r_dat.tex_idx, stream);
        if (stream_read(seg->r_dat.tc, sizeof(real_t), 2, stream) != 2)
            goto on_error;
        READ_VAR(seg->r_dat.y_min, stream);
        READ_VAR(seg->r_dat.y_max, stream);
        READ_VAR(seg->r_dat.sign, stream);
    }
    return 0;
on_error:
    if (tree->nodes)
        free(tree->nodes);
    if (tree->segs)
        free(tree->segs);
    if (tree->verts)
        free(tree->verts);
    memset(tree, 0, sizeof(struct bsp_tree));
    return -1;
}

int copy_bsp(struct bsp_tree *dst, struct bsp_tree const *src)
{
    dst->n_segs = src->n_segs;
    dst->n_verts = src->n_verts;
    dst->n_nodes = src->n_nodes;
    dst->root_node_idx = src->root_node_idx;

    dst->segs = (struct line_seg*)malloc(sizeof(struct line_seg) * dst->n_segs);
    if (!dst->segs)
        return -1;
    memcpy(dst->segs, src->segs, sizeof(struct line_seg) * dst->n_segs);

    dst->verts = (real_t*)malloc(sizeof(real_t) * dst->n_verts * 2);
    if (!dst->verts)
    {
        free(dst->segs);
        dst->segs = NULL;
        return -1;
    }
    memcpy(dst->verts, src->verts, sizeof(real_t) * dst->n_verts * 2);

    dst->nodes =
        (struct bsp_node*)malloc(sizeof(struct bsp_node) * dst->n_nodes);
    if (!dst->nodes)
    {
        free(dst->segs);
        free(dst->verts);
        dst->segs = NULL;
        dst->verts = NULL;
        return -1;
    }
    memcpy(dst->nodes, src->nodes, sizeof(struct bsp_node) * dst->n_nodes);

    return 0;
}
