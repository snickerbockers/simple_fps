/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdint.h>
#include <string.h>

#include "sndserv.h"
#include "trace.h"

#include "mixer.h"

static sample_t sample_scale(sample_t in_samp, volume_t vol)
{
    return (sample_t)(((uint32_t)in_samp * (uint32_t)vol) >> 16);
}

/* saturation addition */
static int32_t samp_add(sample_t samp1, sample_t samp2)
{
    return (int32_t)samp1 + (int32_t)samp2;
}

static sample_t mixer_next_sample(struct mixer *mixer)
{
    int i;
    int32_t sample;
    struct mix_channel *channel;

    sample = 0;
    for (i = 0; i < MIXER_N_CHANNELS; i++)
    {
        channel = mixer->channels + i;
        if (channel->cur_sound)
        {
            /* TODO: Scale by channel volume */
            sample = samp_add(sample,
                              channel->cur_sound->callback(
                                  channel->sample_idx++,
                                  channel->cur_sound->callback_data));

            if (channel->cur_sound->n_samples &&
                channel->sample_idx >= channel->cur_sound->n_samples)
            {
                memset(channel, 0, sizeof(&channel));
            }
        }
    }

    if (sample > SAMP_MAX)
        return SAMP_MAX;
    if (sample < SAMP_MIN)
        return SAMP_MIN;

    return sample;
}

static void mixer_get_samps(void *buf, size_t bytes_needed, void *userarg)
{
    size_t idx;
    struct mixer *mixer;
    sample_t *output;
    size_t samples_needed;

    output = (sample_t*)buf;
    mixer = (struct mixer*)userarg;

    /* in theory the compiler will turn this into a logical right-shift */
    samples_needed = bytes_needed / sizeof(sample_t);

    for (idx = 0; idx < samples_needed; idx++)
        output[idx] = mixer_next_sample(mixer);
}

int mixer_init(struct mixer *mixer, struct sndserv *srv)
{
    if (srv->sample_source)
        TRACE_WARN("Overwriting sound server callback method\n");

    memset(mixer, 0, sizeof(*mixer));

    srv->sample_source = mixer_get_samps;
    srv->sample_source_arg = mixer;
    mixer->callbackp = &srv->sample_source;
    mixer->callback_argp = &srv->sample_source_arg;

    return 0;
}

void mixer_cleanup(struct mixer *mixer)
{
    *mixer->callbackp = NULL;
    *mixer->callback_argp = NULL;

    memset(mixer, 0, sizeof(*mixer));
}

int mixer_play_sound(struct mixer *mixer, struct synth_sound const *sound,
                     volume_t volume)
{
    int idx;
    struct mix_channel *channel;

    channel = NULL;
    for (idx = 0; idx < MIXER_N_CHANNELS; idx++)
    {
        if (!mixer->channels[idx].cur_sound)
        {
            channel = &mixer->channels[idx];
            break;
        }
    }

    if (!channel)
        return -1;

    channel->cur_sound = sound;
    channel->volume = volume;
    channel->sample_idx = 0;

    return 0;
}
