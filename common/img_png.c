/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>

#include "img.h"
#include "trace.h"

#include "bitstream.h"

/*
 * limits to prevent integer overflow.
 * This is arbitrary and can be a lot bigger
 * than it is, but practically I'll never need
 * a png this big anyways.
 */
#define MAX_PNG_WIDTH 8192
#define MAX_PNG_HEIGHT 8192

enum
{
    PNG_GREYSCALE = 0,
    PNG_TRUECOLOR = 2,
    PNG_INDEXED = 3,
    PNG_GREYSCALE_ALPHA = 4,
    PNG_TRUECOLOR_ALPHA = 6
};

/* chunk types */
enum
{
    PNG_UNKNOWN,
    PNG_IHDR,
    PNG_PHYS,
    PNG_TIME,
    PNG_IDAT,
    PNG_IEND
};

enum
{
    INTL_NONE = 0,
    INTL_ADAM7 = 1
};

struct png_chunk
{
    uint32_t len;
    int tp;
    uint8_t *data;
    uint32_t crc;
};

#define PNG_IHDR_LEN 13
struct png_ihdr
{
    uint32_t width;
    uint32_t height;
    uint8_t depth;
    uint8_t color_tp;
    uint8_t comp_meth;
    uint8_t filt_meth;
    uint8_t intl_meth;
};

#define PNG_PHYS_LEN 9
struct png_phys
{
    uint32_t ppu_x;
    uint32_t ppu_y;
    uint8_t unit_spec;
};

#define PNG_TIME_LEN 7
struct png_time
{
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
};

struct png_idat
{
    size_t len;
    uint8_t *dat;
};

struct rgba_quad
{
    uint8_t rgba[4];
};

/* PNG file format signature */
static char png_sig[] = { 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a };

/* chunk type signatures */
static char png_ihdr_sig[] = { 0x49, 0x48, 0x44, 0x52 };
static char png_phys_sig[] = { 0x70, 0x48, 0x59, 0x73 };
static char png_time_sig[] = { 0x74, 0x49, 0x4d, 0x45 };
static char png_idat_sig[] = { 0x49, 0x44, 0x41, 0x54 };
static char png_iend_sig[] = { 0x49, 0x45, 0x4e, 0x44 };

static int read_chunk(FILE *fp, struct png_chunk *chunk);
static int classify_chunk(char const *chunk_sig);
static int decode_ihdr(struct png_ihdr *ihdr, struct png_chunk const *chunk);
static int decode_phys(struct png_phys *phys, struct png_chunk const *chunk);
static int decode_time(struct png_time *time, struct png_chunk const *chunk);
static void png_chunk_cleanup(struct png_chunk *chunk);

static int read_pixel(struct rgba_quad *out, struct bit_stream *bits,
                      struct screen const *scr, int color_tp, int depth);

static int read_subimg(struct rgba_quad *output, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen const *scr);

static void png_chunk_cleanup(struct png_chunk *chunk)
{
    if (chunk->data)
        free(chunk->data);
    memset(chunk, 0, sizeof(struct png_chunk));
}

/*
 * read a chunk from fp and return it into *chunk.
 *
 * Return values:
 *    > 0: unrecognized chunk (this is not necessarily an error)
 *    = 0: success
 *    < 0: error
 */
int read_chunk(FILE *fp, struct png_chunk *chunk)
{
    char sig[4];

    chunk->data = NULL;

    if (fread(&chunk->len, sizeof(chunk->len), 1, fp) != 1)
        return -1;
    chunk->len = ntohl(chunk->len);

    if (fread(sig, sizeof(sig[0]), 4, fp) != 4)
        goto on_error;

    if (!(chunk->data = (uint8_t*)malloc(chunk->len)))
        goto on_error;

    if (fread(chunk->data, 1, chunk->len, fp) != chunk->len)
        goto on_error;

    if (fread(&chunk->crc, sizeof(uint32_t), 1, fp) != 1)
        goto on_error;
    chunk->crc = ntohl(chunk->crc);

    if ((chunk->tp = classify_chunk(sig)) == PNG_UNKNOWN)
        return 1;

    return 0;

on_error:
    TRACE_WARN("Error reading chunk: %s\n", strerror(errno));
    free(chunk->data);
    chunk->data = NULL;
    return -1;
}

int classify_chunk(char const *chunk_sig)
{
    if (memcmp(chunk_sig, png_ihdr_sig, sizeof(char) * 4) == 0)
        return PNG_IHDR;
    if (memcmp(chunk_sig, png_phys_sig, sizeof(char) * 4) == 0)
        return PNG_PHYS;
    if (memcmp(chunk_sig, png_time_sig, sizeof(char) * 4) == 0)
        return PNG_TIME;
    if (memcmp(chunk_sig, png_idat_sig, sizeof(char) * 4) == 0)
        return PNG_IDAT;
    if (memcmp(chunk_sig, png_iend_sig, sizeof(char) * 4) == 0)
        return PNG_IEND;

    return PNG_UNKNOWN;
}

static int decode_ihdr(struct png_ihdr *ihdr, struct png_chunk const *chunk)
{
    uint8_t *dat;

    dat = chunk->data;

    if (chunk->tp != PNG_IHDR || chunk->len != PNG_IHDR_LEN)
        return -1;

    ihdr->width = *(uint32_t*)dat;
    dat += 4;
    ihdr->height = *(uint32_t*)dat;
    dat += 4;
    ihdr->depth = *dat;
    dat += 1;
    ihdr->color_tp = *dat;
    dat += 1;
    ihdr->comp_meth = *dat;
    dat += 1;
    ihdr->filt_meth = *dat;
    dat += 1;
    ihdr->intl_meth = *dat;

    if (ihdr->intl_meth != INTL_NONE && ihdr->intl_meth != INTL_ADAM7)
        goto invalid_png;

    ihdr->width = ntohl(ihdr->width);
    ihdr->height = ntohl(ihdr->height);

    /* Validate the header */
    if (ihdr->width > MAX_PNG_WIDTH || ihdr->height > MAX_PNG_HEIGHT)
        goto invalid_png;

    switch (ihdr->color_tp)
    {
    case PNG_GREYSCALE:
        if (ihdr->depth != 1 && ihdr->depth != 2 &&
            ihdr->depth != 8 && ihdr->depth != 16)
            goto invalid_png;
        break;
    case PNG_TRUECOLOR:
        if (ihdr->depth != 8 && ihdr->depth != 16)
            goto invalid_png;
        break;
    case PNG_INDEXED:
        /*
         * I can't figure out how to make GIMP give me an indexed png,
         * so I have no way to test this.  Eventually I'll hand-craft one
         * with a hex editor, but for now I'm leaving this unimplemented.
         */
        TRACE_ERR("Indexed PNG images are unsupported\n");
        goto invalid_png;
        if (ihdr->depth != 1 && ihdr->depth != 2 &&
            ihdr->depth != 4 && ihdr->depth != 8)
            goto invalid_png;
        break;
    case PNG_GREYSCALE_ALPHA:
        if (ihdr->depth != 8 && ihdr->depth != 16)
            goto invalid_png;
        break;
    case PNG_TRUECOLOR_ALPHA:
        if (ihdr->depth != 8 && ihdr->depth != 16)
            goto invalid_png;
        break;
    default:
        goto invalid_png;
    }

    /* TODO: support this */
    if (ihdr->depth == 16)
    {
        /* As with the indexed images, I have no way to test this yet. */
        TRACE_ERR("16-bit PNG images are not supported\n");
        goto invalid_png;
    }
    
    return 0;

invalid_png:
    return -1;
}

static int decode_phys(struct png_phys *phys, struct png_chunk const *chunk)
{
    uint8_t *dat;
    
    dat = chunk->data;

    if (chunk->tp != PNG_PHYS || chunk->len != PNG_PHYS_LEN)
        return -1;

    phys->ppu_x = *(uint32_t*)dat;
    dat += 4;
    phys->ppu_y = *(uint32_t*)dat;
    dat += 4;
    phys->unit_spec = *dat;

    phys->ppu_x = ntohs(phys->ppu_x);
    phys->ppu_y = ntohs(phys->ppu_y);

    /* raise error for invalid values even though we ignore this field */
    if (phys->unit_spec != 0 && phys->unit_spec != 1)
        return -1;

    return 0;
}

static int decode_time(struct png_time *time, struct png_chunk const *chunk)
{
    uint8_t *dat;
    
    dat = chunk->data;

    if (chunk->tp != PNG_TIME || chunk->len != PNG_TIME_LEN)
        return -1;

    time->year = *(uint16_t*)dat;
    dat += 2;
    time->month = *dat;
    dat += 1;
    time->day = *dat;
    dat += 1;
    time->hour = *dat;
    dat += 1;
    time->min = *dat;
    dat += 1;
    time->sec = *dat;

    time->year = ntohs(time->year);

    /*
     * Fun fact: the png spec only says that the day can't be greater than 31;
     * it doesn't take the different lengths of every month into account, so
     * it's totally legal to have a png that was made on february 31.
     */
    if (time->month >= 13 || time->day >= 32 || time->hour >= 24 ||
        time->min >= 60 || time->sec >= 61)
        return -1;

    return 0;
}

static int append_idat(struct png_chunk *idat_dst, struct png_chunk const *idat_src)
{
    uint8_t *new_data, *new_data_tmp;
    size_t new_len;

    /* don't check idat_dst's type because it will be zeroed on first call */
    if (idat_src->tp != PNG_IDAT)
        return -1;

    new_data_tmp = new_data;
    new_len = idat_dst->len + idat_src->len;
    new_data = (uint8_t*)realloc(idat_dst->data, new_len);

    /* handle potential realloc insanities */
    if (new_len == 0)
    {
        /* Follow the convention that a NULL pointer is an empty array */
        if (new_data)
        {
            free(new_data);
            idat_dst->data = NULL;
        }
        return 0;
    }
    else if (!new_data)
    {
        free(new_data_tmp);
        return -1;
    }

    idat_dst->data = new_data;
    memmove(idat_dst->data + idat_dst->len, idat_src->data, idat_src->len);
    idat_dst->len = new_len;

    /* TODO: handle CRC */
    idat_dst->crc = 0;

    idat_dst->tp = PNG_IDAT;

    return 0;
}

static int decode_idat(struct png_idat *idat, struct png_chunk const *chunk)
{
    return inflate(&idat->dat, &idat->len, chunk->data, chunk->len);
}

static uint8_t png_abs(uint8_t val)
{
    if (val < 0)
        return -val;
    return val;
}

static int16_t paeth_predictor(uint8_t a, uint8_t b, uint8_t c)
{
    int16_t pa, pb, pc, p;
    p = (int16_t)a + (int16_t)b - (int16_t)c;

    pa = p - (int16_t)a;
    pb = p - (int16_t)b;
    pc = p - (int16_t)c;

    if (pa < 0)
        pa = -pa;
    if (pb < 0)
        pb = -pb;
    if (pc < 0)
        pc = -pc;

    if (pa <= pb && pa <= pc)
        return a;
    else if (pb <= pc)
        return b;
    return c;
}

/* TODO: this can't handle bit depths less than 8 */
static int filter_pixel(struct rgba_quad *out, struct rgba_quad const *dat,
                        int color_tp, int filter_tp, int depth, int row,
                        int col, int w, int h)
{
    int has_alpha;
    int err = 0, i;
    struct rgba_quad x, a, b, c;
    int n_colors;
    struct rgba_quad in;

    in = dat[row * w + col];

    switch (color_tp)
    {
    case PNG_GREYSCALE:
        /* process_idat already converted it from 1 color to 3 */
        has_alpha = 0;
        n_colors = 3;
        break;
    case PNG_GREYSCALE_ALPHA:
        /* process_idat already converted it from 2 colors to 4 */
        has_alpha = 1;
        n_colors = 4;
        break;
    case PNG_TRUECOLOR:
        has_alpha = 0;
        n_colors = 3;
        break;
    case PNG_TRUECOLOR_ALPHA:
        has_alpha = 1;
        n_colors = 4;
        break;
    default:
        err = -1;
        goto the_end;
    }

    memset(&x, 0, sizeof(struct rgba_quad));
    memset(&a, 0, sizeof(struct rgba_quad));
    memset(&b, 0, sizeof(struct rgba_quad));
    memset(&c, 0, sizeof(struct rgba_quad));

    x = in;

    if (col > 0)
        a = dat[row * w + col - 1];

    if (row > 0)
        b = dat[(row - 1) * w + col];

    if (row > 0 && col > 0)
        c = dat[(row - 1) * w + col - 1];

    err = 0;
    switch (filter_tp)
    {
    case 0:
        *out = x;
        break;
    case 1:
        for (i = 0; i < n_colors; i++)
            out->rgba[i] = x.rgba[i] + a.rgba[i];
        break;
    case 2:
        for (i = 0; i < n_colors; i++)
            out->rgba[i] = x.rgba[i] + b.rgba[i];
        break;
    case 3:
        for (i = 0; i < n_colors; i++)
            out->rgba[i] = x.rgba[i] + (a.rgba[i] + b.rgba[i]) / 2;
        break;
    case 4:
        for (i = 0; i < n_colors; i++)
        {
            out->rgba[i] = x.rgba[i] +
                paeth_predictor(a.rgba[i], b.rgba[i], c.rgba[i]);
        }
        break;
    default:
        err = -1;
    }

    if (!has_alpha)
        out->rgba[3] = 0xff;
the_end:
    return err;
}

/* read one pixel from bits and store it in *out */
static int read_pixel(struct rgba_quad *out, struct bit_stream *bits,
                      struct screen const *scr, int color_tp, int depth)
{
    int i, tmp[4];

    memset(tmp, 0, 4 * sizeof(tmp[0]));

    if (color_tp == PNG_TRUECOLOR_ALPHA)
    {
        switch (depth)
        {
        case 8:
            if (bit_stream_shift_out(bits, tmp, 8) < 0)
                goto out_of_data;
            if (bit_stream_shift_out(bits, tmp+1, 8) < 0)
                goto out_of_data;
            if (bit_stream_shift_out(bits, tmp+2, 8) < 0)
                goto out_of_data;
            if (bit_stream_shift_out(bits, tmp+3, 8) < 0)
                goto out_of_data;
            break;
        default:
            TRACE_DBG("unexpected bit depth %d\n", depth);
            goto corrupted_png;
        }
    }
    else if (color_tp == PNG_TRUECOLOR)
    {
        switch (depth)
        {
        case 8:
            if (bit_stream_shift_out(bits, tmp, 8) < 0)
                goto out_of_data;
            if (bit_stream_shift_out(bits, tmp+1, 8) < 0)
                goto out_of_data;
            if (bit_stream_shift_out(bits, tmp+2, 8) < 0)
                goto out_of_data;
            tmp[3] = 0xff;
            break;
        default:
            TRACE_DBG("unexpected bit depth %d\n", depth);
            goto corrupted_png;
        }
    }
    else if (color_tp == PNG_GREYSCALE)
    {
        switch (depth)
        {
        case 1:
            if (bit_stream_shift_out(bits, tmp, 1) < 0)
                goto out_of_data;
            tmp[0] *= 255;
            break;
        case 2:
            if (bit_stream_shift_out(bits, tmp, 2) < 0)
                goto out_of_data;
            tmp[0] *= 85;
            break;
        case 4:
            if (bit_stream_shift_out(bits, tmp, 4) < 0)
                goto out_of_data;
            tmp[0] *= 17;
            break;
        case 8:
            if (bit_stream_shift_out(bits, tmp, 8) < 0)
                goto out_of_data;
            break;
        default:
            TRACE_DBG("unexpected bit depth %d\n", (int)depth);
            goto corrupted_png;
        }
        tmp[1] = tmp[2] = tmp[0];
        tmp[3] = 0xff;
    }
    else if (color_tp == PNG_GREYSCALE_ALPHA)
    {
        switch (depth)
        {
        case 8:
            if (bit_stream_shift_out(bits, tmp, 8) < 0)
                goto out_of_data;
            if (bit_stream_shift_out(bits, tmp+3, 8) < 0)
                goto out_of_data;
            tmp[1] = tmp[2] = tmp[0];
            break;
        default:
            TRACE_DBG("unexpected bit depth %d\n", depth);
            goto corrupted_png;
        }
    }
    else
    {
        TRACE_DBG("unexpected color type %d\n", color_tp);
        goto corrupted_png;
    }

    for (i = 0; i < 4; i++)
        out->rgba[i] = tmp[i];

    return 0;

out_of_data:
corrupted_png:
    return -1;
}

static int read_subimg(struct rgba_quad *output, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen const *scr)
{
    int col, row;
    int filter_tp;

    for (row = 0; row < height; row++)
    {
        if (bit_stream_get_byte(bits, &filter_tp) < 0)
            goto corrupted_png;

        for (col = 0; col < width; col++)
        {
            if (read_pixel(output + row * width + col, bits,
                           scr, color_tp, depth) < 0)
                goto corrupted_png;

            if (filter_pixel(output + row * width + col, output,
                             color_tp, filter_tp, depth, row, col,
                             width, height) < 0)
                goto corrupted_png;
        }
    }

    return 0;

corrupted_png:
    return -1;
}

static int adam7_pass1(struct rgba_quad *out, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen *scr)
{
    int row, col, dst_row, dst_col;
    int subimg_w, subimg_h;
    struct rgba_quad *subimg;

    subimg_w = (width + 8 - 1) / 8;
    subimg_h = (height + 8 - 1) / 8;

    if (subimg_w <= 0 || subimg_h <= 0)
        return 0;

    subimg = (struct rgba_quad*)malloc(sizeof(struct rgba_quad) *
                                       subimg_w * subimg_h);
    if (!subimg)
        return -1;

    if (read_subimg(subimg, bits, subimg_w, subimg_h, color_tp, depth, scr) < 0)
    {
        free(subimg);
        return -1;
    }

    for (row = 0; row < subimg_h; row++)
    {
        dst_row = row * 8;

        for (col = 0; col < subimg_w; col++)
        {
            dst_col = col * 8;
            out[dst_row * width + dst_col] = subimg[row * subimg_w + col];
        }
    }

    free(subimg);
    return 0;
}

static int adam7_pass2(struct rgba_quad *out, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen *scr)
{
    int row, col, dst_row, dst_col;
    int subimg_w, subimg_h;
    struct rgba_quad *subimg;

    subimg_w = (width - 4 + 8 - 1) / 8;
    subimg_h = (height + 8 - 1) / 8;

    if (subimg_w <= 0 || subimg_h <= 0)
        return 0;

    subimg = (struct rgba_quad*)malloc(sizeof(struct rgba_quad) *
                                       subimg_w * subimg_h);
    if (!subimg)
        return -1;

    if (read_subimg(subimg, bits, subimg_w, subimg_h, color_tp, depth, scr) < 0)
    {
        free(subimg);
        return -1;
    }

    for (row = 0; row < subimg_h; row++)
    {
        dst_row = row * 8;

        for (col = 0; col < subimg_w; col++)
        {
            dst_col = col * 8 + 4;
            out[dst_row * width + dst_col] = subimg[row * subimg_w + col];
        }
    }

    free(subimg);
    return 0;
}

static int adam7_pass3(struct rgba_quad *out, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen *scr)
{
    int row, col, dst_row, dst_col;
    int subimg_w, subimg_h;
    struct rgba_quad *subimg;

    subimg_w = (width + 4 - 1) / 4;
    subimg_h = (height - 4 + 8 - 1) / 8;

    if (subimg_w <= 0 || subimg_h <= 0)
        return 0;

    subimg = (struct rgba_quad*)malloc(sizeof(struct rgba_quad) *
                                       subimg_w * subimg_h);
    if (!subimg)
        return -1;

    if (read_subimg(subimg, bits, subimg_w, subimg_h, color_tp, depth, scr) < 0)
    {
        free(subimg);
        return -1;
    }

    for (row = 0; row < subimg_h; row++)
    {
        dst_row = row * 8 + 4;

        for (col = 0; col < subimg_w; col++)
        {
            dst_col = (col / 2) * 8 + 4 * (col % 2);
            out[dst_row * width + dst_col] = subimg[row * subimg_w + col];
        }
    }

    free(subimg);
    return 0;
}

static int adam7_pass4(struct rgba_quad *out, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen *scr)
{
    int row, col, dst_row, dst_col;
    int subimg_w, subimg_h;
    struct rgba_quad *subimg;

    subimg_w = (width - 2 + 4 - 1) / 4;
    subimg_h = (height /* - 3 */ + 4 - 1) / 4;

    if (subimg_w <= 0 || subimg_h <= 0)
        return 0;

    subimg = (struct rgba_quad*)malloc(sizeof(struct rgba_quad) *
                                       subimg_w * subimg_h);
    if (!subimg)
        return -1;

    if (read_subimg(subimg, bits, subimg_w, subimg_h, color_tp, depth, scr) < 0)
    {
        free(subimg);
        return -1;
    }

    for (row = 0; row < subimg_h; row++)
    {
        dst_row = (row / 2) * 8 + 4 * (row % 2);

        for (col = 0; col < subimg_w; col++)
        {
            dst_col = 2 + (col / 2) * 8 + 4 * (col % 2);
            out[dst_row * width + dst_col] = subimg[row * subimg_w + col];
        }
    }

    free(subimg);
    return 0;
}

static int adam7_pass5(struct rgba_quad *out, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen *scr)
{
    int row, col, dst_row, dst_col;
    int subimg_w, subimg_h;
    struct rgba_quad *subimg;

    subimg_w = (width + 2 - 1) / 2;
    subimg_h = (height - 2 + 4 - 1) / 4;

    if (subimg_w <= 0 || subimg_h <= 0)
        return 0;

    subimg = (struct rgba_quad*)malloc(sizeof(struct rgba_quad) *
                                       subimg_w * subimg_h);
    if (!subimg)
        return -1;

    if (read_subimg(subimg, bits, subimg_w, subimg_h, color_tp, depth, scr) < 0)
    {
        free(subimg);
        return -1;
    }

    for (row = 0; row < subimg_h; row++)
    {
        dst_row = (row / 2) * 8 + 4 * (row % 2) + 2;

        for (col = 0; col < subimg_w; col++)
        {
            dst_col = (col / 4) * 8 + 2 * (col % 4);
            out[dst_row * width + dst_col] = subimg[row * subimg_w + col];
        }
    }

    free(subimg);
    return 0;
}

static int adam7_pass6(struct rgba_quad *out, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen *scr)
{
    int row, col, dst_row, dst_col;
    int subimg_w, subimg_h;
    struct rgba_quad *subimg;

    subimg_w = (width - 1 + 2 - 1) / 2;
    subimg_h = (height + 2 - 1) / 2;

    if (subimg_w <= 0 || subimg_h <= 0)
        return 0;

    subimg = (struct rgba_quad*)malloc(sizeof(struct rgba_quad) *
                                       subimg_w * subimg_h);
    if (!subimg)
        return -1;

    if (read_subimg(subimg, bits, subimg_w, subimg_h, color_tp, depth, scr) < 0)
    {
        free(subimg);
        return -1;
    }

    for (row = 0; row < subimg_h; row++)
    {
        dst_row = (row / 4) * 8 + 2 * (row % 4);

        for (col = 0; col < subimg_w; col++)
        {
            dst_col = 1 + (col / 4) * 8 + 2 * (col % 4);
            out[dst_row * width + dst_col] = subimg[row * subimg_w + col];
        }
    }

    free(subimg);
    return 0;
}

static int adam7_pass7(struct rgba_quad *out, struct bit_stream *bits,
                       int width, int height, int color_tp, int depth,
                       struct screen *scr)
{
    int row, col, dst_row, dst_col;
    int subimg_w, subimg_h;
    struct rgba_quad *subimg;

    subimg_w = width;
    subimg_h = (height - 1 + 2 - 1) / 2;

    if (subimg_w <= 0 || subimg_h <= 0)
        return 0;

    subimg = (struct rgba_quad*)malloc(sizeof(struct rgba_quad) *
                                       subimg_w * subimg_h);
    if (!subimg)
        return -1;

    if (read_subimg(subimg, bits, subimg_w, subimg_h, color_tp, depth, scr) < 0)
    {
        free(subimg);
        return -1;
    }

    for (row = 0; row < subimg_h; row++)
    {
        dst_row = 1 + (row / 4) * 8 + 2 * (row % 4);

        for (col = 0; col < subimg_w; col++)
        {
            dst_col = col;
            out[dst_row * width + dst_col] = subimg[row * subimg_w + col];
        }
    }

    free(subimg);
    return 0;
}

static int process_idat(struct screen *scr, struct tex *out,
                        struct png_idat const *idat,
                        struct png_ihdr const *ihdr)
{
    int i;
    int row, col;
    int tmp[4];
    color_t pixel;
    struct rgba_quad *img_dat = NULL, *cur_pix;
    int filter_tp;
    struct bit_stream bits;

    out->data = NULL;
    out->w = ihdr->width;
    out->h = ihdr->height;

    if (out->w == 0 || out->h == 0)
        goto corrupted_png;

    img_dat = (struct rgba_quad*)malloc(sizeof(struct rgba_quad) *
                                       out->w * out->h);
    if (!img_dat)
        goto failed_allocation;

    memset(img_dat, 0, sizeof(struct rgba_quad) * out->w * out->h);

    bit_stream_init(&bits, idat->dat, idat->len);

    switch (ihdr->intl_meth)
    {
    case INTL_NONE:
        if (read_subimg(img_dat, &bits, out->w, out->h,
                        ihdr->color_tp, ihdr->depth, scr) < 0)
            goto corrupted_png;
        break;
    case INTL_ADAM7:
        if (adam7_pass1(img_dat, &bits, out->w, out->h,
                        ihdr->color_tp, ihdr->depth, scr) < 0)
            goto corrupted_png;
        if (adam7_pass2(img_dat, &bits, out->w, out->h,
                        ihdr->color_tp, ihdr->depth, scr) < 0)
            goto corrupted_png;
        if (adam7_pass3(img_dat, &bits, out->w, out->h,
                        ihdr->color_tp, ihdr->depth, scr) < 0)
            goto corrupted_png;
        if (adam7_pass4(img_dat, &bits, out->w, out->h,
                        ihdr->color_tp, ihdr->depth, scr) < 0)
            goto corrupted_png;
        if (adam7_pass5(img_dat, &bits, out->w, out->h,
                        ihdr->color_tp, ihdr->depth, scr) < 0)
            goto corrupted_png;
        if (adam7_pass6(img_dat, &bits, out->w, out->h,
                        ihdr->color_tp, ihdr->depth, scr) < 0)
            goto corrupted_png;
        if (adam7_pass7(img_dat, &bits, out->w, out->h,
                        ihdr->color_tp, ihdr->depth, scr) < 0)
            goto corrupted_png;
        break;
    default:
        /*
         * this should never actually happen because intl_meth was
         * already validated in decode_ihdr
         */
        goto corrupted_png;
    }

    out->data = (color_t*)malloc(sizeof(color_t) * out->w * out->h);
    if (!out->data)
        goto failed_allocation;

    for (row = 0; row < out->h; row++)
        for (col = 0; col < out->w; col++)
        {
            cur_pix = img_dat + row * out->w + col;
            
            out->data[row * out->w + col] = color_pack_rgba(scr,
                                                            cur_pix->rgba[0],
                                                            cur_pix->rgba[1],
                                                            cur_pix->rgba[2],
                                                            cur_pix->rgba[3]);
        }

    free(img_dat);

    return 0;

out_of_data:
failed_allocation:
corrupted_png:
    TRACE_ERR("PNG image is corrupted\n");

    if (img_dat)
        free(img_dat);
    if (out->data)
    {
        free(out->data);
        out->data = NULL;
    }

    return -1;
}

int tex_load_png(struct screen *scr, struct tex *out, const char *path)
{
    FILE *fp;
    int chunk_tp;
    int idat_valid = 0, idat_finished = 0;
    struct png_chunk ihdr_chunk, cur_chunk, idat_chunk;
    struct png_ihdr ihdr;
    struct png_idat idat;
    char file_sig[8];
    int err;

    memset(&idat, 0, sizeof(struct png_idat));
    memset(&idat_chunk, 0, sizeof(struct png_chunk));
    
    fp = fopen(path, "rb");
    if (!fp)
    {
        TRACE_ERR("unable to open %s\n", path);
        return -1;
    }

    if (fread(file_sig, sizeof(char), 8, fp) != 8)
        goto on_error;

    if (memcmp(file_sig, png_sig, 8) != 0)
        goto on_error;

    if (read_chunk(fp, &ihdr_chunk) != 0)
        goto on_error;
    if (decode_ihdr(&ihdr, &ihdr_chunk) != 0)
        goto on_error;

    do
    {
        chunk_tp = PNG_UNKNOWN;
        if ((err = read_chunk(fp, &cur_chunk)) < 0)
            goto on_error;
        else if (err > 0)
            continue;

        chunk_tp = cur_chunk.tp;
        if (chunk_tp == PNG_IDAT)
        {
            /* PNG spec says idat chunks must be consecutive */
            if (idat_finished)
                goto on_error;
            idat_valid = 1;
            append_idat(&idat_chunk, &cur_chunk);
        }
        else if (idat_valid && !idat_finished)
        {
            /* IDAT stream is complete, handle state transition */
            idat_finished = 1;
            err = decode_idat(&idat, &idat_chunk);

            png_chunk_cleanup(&idat_chunk);

            if (err < 0)
                goto on_error;
        }
        png_chunk_cleanup(&cur_chunk);
    } while (chunk_tp != PNG_IEND);

    if (!idat_valid)
    {
        TRACE_DBG("Failed to locate IDAT chunk\n");
        goto on_error;
    }

    err = process_idat(scr, out, &idat, &ihdr);
    if (idat.dat)
        free(idat.dat);
    return err;

on_error:
    TRACE_DBG("Error loading %s\n", path);
    fclose(fp);
    return -1;
}
