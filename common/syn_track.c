/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sndserv.h" /* for SNDSERV_SAMPLE_FREQ */

#include "syn_track.h"

struct note
{
    char const* name;
    double freq;
} notes[] = {
    { "0", 0 },

    { "C3", 131 },
    { "D3", 147 },
    { "E3", 165 },
    { "F3", 175 },
    { "G3", 196 },
    { "A3", 220 },
    { "B3", 247 },
    { "C4", 262 },
    { "D4", 294 },
    { "E4", 330 },
    { "F4", 350 },
    { "G4", 392 },
    { "A4", 440 },
    { "B4", 494 },
    { "C5", 523 },
    { "D5", 587 },
    { "E5", 659 },
    { "F5", 698 },
    { "G5", 784 },
    { "A5", 880 },
    { "B5", 988 },

    /*
     * XXX - My synth can't handle non-integer frequencies very well
     * (it causes a "popping" noise on note changes), but these are the
     * accurate frequencies according to
     * http://www.phy.mtu.edu/~suits/notefreqs.html
     */
    /* { "C3", 130.81 }, */
    /* { "D3", 146.83 }, */
    /* { "E3", 164.81 }, */
    /* { "F3", 174.61 }, */
    /* { "G3", 196.00 }, */
    /* { "A3", 220.00 }, */
    /* { "B3", 246.94 }, */
    /* { "C4", 261.63 }, */
    /* { "D4", 293.66 }, */
    /* { "E4", 329.63 }, */
    /* { "F4", 349.23 }, */
    /* { "G4", 392.00 }, */
    /* { "A4", 440.00 }, */
    /* { "B4", 493.88 }, */
    /* { "C5", 523.25 }, */
    /* { "D5", 587.33 }, */
    /* { "E5", 659.25 }, */
    /* { "F5", 698.46 }, */
    /* { "G5", 783.99 }, */
    /* { "A5", 880.00 }, */
    /* { "B5", 987.77 }, */

    { NULL, 0.0 }
};

double get_note_freq(char const *name)
{
    struct note *cursor;

    cursor = notes;
    while (cursor->name)
    {
        if (strcmp(cursor->name, name) == 0)
            return cursor->freq;

        cursor++;
    }

    fprintf(stderr, "WARNING: unrecognized note \"%s\"\n", name);
    return 0.0;
}

void add_beat_to_track(struct syn_track *track, char const *note,
                       unsigned duration)
{
    track->beats =
        (struct syn_beat*)realloc(track->beats,
                                  sizeof(struct syn_beat) * ++track->n_beats);
    strncpy(track->beats[track->n_beats - 1].note_name, note,
            sizeof(note_str_t) / sizeof(char));
    track->beats[track->n_beats - 1].note_name[NOTE_NAME_LEN] = '\0';
    track->beats[track->n_beats - 1].duration = duration;
}

double sin_synth(double freq, double t)
{
    return (double)sin(t * M_PI * 2.0 * freq);
}

unsigned samples_per_beat(double tempo, unsigned samp_freq)
{
    /*
     * tempo is seconds / beat
     * samp_freq is samples / second
     * need samples_per beat, which should be tempo * samp_freq
     */
    return tempo * samp_freq;
}

/* XXX it sounds kind of like a percussion instrument when BEAT_FADE == SNDSERV_SAMPLE_FREQ / 2 */
#define BEAT_FADE 1000

void read_samples(int16_t *buf, int n_samples, struct syn_track *track)
{
    while (n_samples--)
    {
        if (track->fade_in && track->vol_scale < 1.0)
        {
            track->vol_scale += 1.0 / (double)BEAT_FADE;
            if (track->vol_scale > 1.0)
                track->vol_scale = 1.0;
        }

        if (!track->fade_in && track->vol_scale > 0.0)
        {
            track->vol_scale -= 1.0 / (double)BEAT_FADE;
            if (track->vol_scale < 0.0)
                track->vol_scale = 0.0;
        }

        *buf++ = track->synth(
            get_note_freq(track->beats[track->current_beat].note_name),
            track->current_beat_sample / (double)SNDSERV_SAMPLE_FREQ) *
            track->vol_scale * INT16_MAX;

        track->current_beat_sample++;
        track->next_sample++;

        if (track->current_beat_sample >=
            samples_per_beat(track->tempo, SNDSERV_SAMPLE_FREQ) *
            track->beats[track->current_beat].duration)
        {
            track->fade_in = 1;
            track->vol_scale = 0.0;
            track->current_beat_sample = 0;
            track->current_beat = (track->current_beat + 1) % track->n_beats;
        }
        else if (track->fade_in && track->current_beat_sample >=
                 (samples_per_beat(track->tempo, SNDSERV_SAMPLE_FREQ) *
                 track->beats[track->current_beat].duration - BEAT_FADE))
        {
            track->fade_in = 0;
            track->vol_scale = 1.0;
        }
    }
}
