/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef REAL_H_
#define REAL_H_

#include <stdint.h>
#include <math.h>
#include <assert.h>

/* How far to right-shift to get the whole part of the number */
#define REAL_UNIT_SHIFT 16

#define REAL_ATOM ((real_t)1)
#define REAL_MAX ((real_t)0x7fffffff)
#define REAL_MIN ((real_t)0x80000000)
#define REAL_ZERO ((real_t)0)
#define REAL_HALF ((real_t)(1 << (REAL_UNIT_SHIFT - 1)))
#define REAL_UNIT ((real_t)(1 << REAL_UNIT_SHIFT))
#define REAL_PI ((real_t)0x3243f)
#define REAL_DEGS_PER_RAD ((real_t)0x394bb8)
#define REAL_RADS_PER_DEG ((real_t)0x477)

/* fixed-point arithmetic */
typedef int32_t real_t;

real_t real_from_double(double val);
double real_to_double(real_t val);
real_t real_from_string(char const *str);
real_t real_recip(real_t in);
real_t real_sqrt(real_t in);
real_t real_sin(real_t in);
real_t real_cos(real_t in);
real_t real_tan(real_t in);

/* returns false if val is NaN */
static inline int real_valid(real_t val)
{
    return 1;
}

static inline real_t real_add(real_t lhs, real_t rhs)
{
    return lhs + rhs;
}

static inline real_t real_sub(real_t lhs, real_t rhs)
{
    return lhs - rhs;
}

static inline real_t real_mul(real_t lhs, real_t rhs)
{
    return ((int64_t)lhs * (int64_t)rhs) >> REAL_UNIT_SHIFT;
}

static inline real_t real_div(real_t lhs, real_t rhs)
{
#ifdef USE_SOFTWARE_DIVISION
    return real_mul(lhs, real_recip(rhs));
#else
    double ratio;
    assert(rhs != REAL_ZERO);
    ratio = (double)lhs / (double)rhs;
    assert(isfinite(ratio));
    return ((double)lhs / (double)rhs) * REAL_UNIT;
#endif
}

static inline real_t real_mod(real_t lhs, real_t rhs)
{
    /* TODO: does this *actually* work ? */
    return lhs % rhs;
}

static inline real_t real_neg(real_t in)
{
    return -in;
}

static inline real_t real_abs(real_t in)
{
    return in >= 0 ? in : -in;
}

static inline int real_eq(real_t lhs, real_t rhs)
{
    return lhs == rhs;
}

static inline int real_lt(real_t lhs, real_t rhs)
{
    return lhs < rhs;
}

static inline int real_gt(real_t lhs, real_t rhs)
{
    return lhs > rhs;
}

static inline int real_lte(real_t lhs, real_t rhs)
{
    return lhs <= rhs;
}

static inline int real_gte(real_t lhs, real_t rhs)
{
    return lhs >= rhs;
}

static inline real_t real_from_int(int val)
{
    return val << REAL_UNIT_SHIFT;
}

static inline int real_to_int(real_t val)
{
    return val >> REAL_UNIT_SHIFT;
}

#endif
