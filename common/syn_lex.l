/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/
%{

#include "common/syn_file.h"
#include "syn_yacc.h"
#include "common/syn_track.h"

%}

%%

\"[^\"]*\"       {
                     strncpy(yylval.sval, yytext + 1, SYN_MAX_STR_LEN);
                     yylval.sval[SYN_MAX_STR_LEN - 1] = '\0';
                     *strrchr(yylval.sval, '\"') = '\0';
                     return TOK_VAL_STR;
                 }
[0-9]+\.[0-9]+   {
                     yylval.rval = atof(yytext);
                     return TOK_VAL_REAL;
                 }
[0-9]+           {
                     yylval.ival = atoi(yytext);
                     return TOK_VAL_INT;
                 }

\{      return '{';
\}      return '}';
\,      return ',';
\=      return '=';
\;      return ';';
track   return TOK_TRACK;
tempo   return TOK_TEMPO;
[ \t\n] ;

%%

void parse_syn(FILE *fp, struct syn_track *track_out)
{
    yyin = fp;
    memset(&cur_track, 0, sizeof(cur_track));
    cur_track.tempo = 0.5;
    yyparse();

    /*
     * We set these values here because they're part of the track's state, not
     * permanent characteristics of the track.
     */
    cur_track.vol_scale = 0.0;
    cur_track.fade_in = 1;

    memcpy(track_out, &cur_track, sizeof(*track_out));
}

int yywrap(void)
{
    return 1;
}
