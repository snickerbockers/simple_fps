/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef SNDSERV_H_
#define SNDSERV_H_

/*
 * This is a pretty thin software layer on top of whatever sound backend is
 * used (SDL audio, pulseaudio, ALSA, OpenAL, whatever Windows has, etc).
 * Most of the cool stuff happens in the software audio-mixing code.
 */

#define SNDSERV_SAMPLE_FREQ 44100

#include "pulse/sndserv_impl.h"

typedef void(*sndserv_callback_t)(void *buf, size_t bytes_needed,
                                  void *userarg);

struct sndserv
{
    struct sndserv_impl impl;

    /*
     * called whenever the sound server is ready for more bytes.
     *
     * you MUST write to the entire buffer, failure is not an option.
     * The reason for this is that pulse (and probably other backends as well)
     * will only call the callback the first time it decides it needs more
     * samples; it will not schedule another callback until the required number
     * of samples have been written.
     */
    sndserv_callback_t sample_source;
    void *sample_source_arg;
};

int sndserv_init(struct sndserv *srv);
void sndserv_cleanup(struct sndserv *srv);

int sndserv_impl_init(struct sndserv *srv);
void sndserv_impl_cleanup(struct sndserv *srv);

#endif
