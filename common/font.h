/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

/*
 * font.h - bitmap font support.  Currently only supports the digits 0-9
 *          because I don't feel like taking the time to make a readable
 *          font.  Someday.
 */

#ifndef FONT_H_
#define FONT_H_

#include "draw.h"
#include "screen.h"

struct font
{
    struct tex bmp;

    int glyph_w, glyph_h;
};

int font_load(struct screen *scr, struct font *font, char const *path,
              int glyph_w, int glyph_h);

void font_print(struct screen *scr, struct font *font, int x, int y,
                char const *msg);

/*
 * Will insert 0s at the beginning to fill up pad_to digits if the length of
 * the printed int is less than pad_to.  This behavior can be disabled by
 * setting pad_to = 0
 */
void font_print_int(struct screen *scr, struct font *font, int x, int y,
                    int val, int pad_to);
void font_print_bcd(struct screen *scr, struct font *font, int x, int y,
                    int *bcd, int n_digits);

#endif
