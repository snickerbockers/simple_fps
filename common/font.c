/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <string.h>

#include "trace.h"
#include "draw.h"
#include "game.h"
#include "img.h"

#include "font.h"

static int font_get_char_xy(int *x, int *y, struct font const *font, char c);

int font_load(struct screen *scr, struct font *font, char const *path,
              int glyph_w, int glyph_h)
{
    int ret;

    if (ret = (tex_load_chroma_key(scr, &font->bmp,
                                   path, 0, STD_CHROMA_KEY) < 0))
    {
        return ret;
    }

    if (glyph_w >= font->bmp.w || glyph_h >= font->bmp.h ||
        font->bmp.w % glyph_w || font->bmp.h % glyph_h)
    {
        TRACE_ERR("%s: Bad glyph dimensions (%d, %d).  "
                  "The img dimensions are (%d, %d)\n", path, glyph_w, glyph_h,
                  font->bmp.w, font->bmp.h);
        return -1;
    }

    font->glyph_w = glyph_w;
    font->glyph_h = glyph_h;

    return 0;
}

void font_print(struct screen *scr, struct font *font, int x, int y,
                char const *msg)
{
    int x_src, y_src;

    while (*msg)
    {
        if (font_get_char_xy(&x_src, &y_src, font, *msg) == 0)
        {
            blit_blended_tex(scr, &font->bmp, x, y, x_src, y_src,
                             font->glyph_w, font->glyph_h);
        }
        msg++;
        x += font->glyph_w;
    }
}

void font_print_bcd(struct screen *scr, struct font *font, int x, int y,
                    int *bcd, int n_digits)
{
    int x_src, y_src;
    int digit_idx;

    for (digit_idx = 0; digit_idx < n_digits; digit_idx++)
    {
        if (bcd[digit_idx] >= 0 && bcd[digit_idx] <= 9 &&
            font_get_char_xy(&x_src, &y_src, font, bcd[digit_idx] + '0') == 0)
        {
            blit_blended_tex(scr, &font->bmp, x, y, x_src, y_src,
                             font->glyph_w, font->glyph_h);
        }
        x += font->glyph_w;
    }
}

/*
 * This function outputs 1 bcd per int in bcd_out.  It does not pack 8 bcds into
 * every element (which would be the optimal thing to do).
 */
static void int_to_bcd(int *bcd_out, int *bcd_used, int bcd_len, int val, int pad_to)
{
    int pad_len;
    int n_digits;
    int pos;

    if (!bcd_len)
    {
        *bcd_used = 0;
        return;
    }

    /* put in a special case for this because i am incompetent */
    if (!val)
    {
        bcd_out[0] = 0;
        n_digits = 1;
        goto pad_output;
    }

    n_digits = 0;
    pos = 1000000000;

    while (pos)
    {
        bcd_out[n_digits] = val / pos;

        if (!bcd_out[n_digits] && !n_digits)
        {
            pos /= 10;
            continue;
        }

        n_digits++;
        if (n_digits >= bcd_len)
            break;

        val %= pos;
        pos /= 10;
    }

pad_output:
    /* can't have more padding than space */
    if (pad_to > bcd_len)
        pad_to = bcd_len;

    if (n_digits < pad_to)
    {
        pad_len = pad_to - n_digits;
        memmove(bcd_out + pad_len, bcd_out, n_digits * sizeof(int));
        memset(bcd_out, 0, sizeof(int) * pad_len);
        n_digits = pad_to;
    }

    *bcd_used = n_digits;
}

void font_print_int(struct screen *scr, struct font *font, int x, int y,
                    int val, int pad_to)
{
    int digit;
    int pos;
    int bcd[32];
    int bcd_digits;

    int_to_bcd(bcd, &bcd_digits, 32, val, pad_to);
    font_print_bcd(scr, font, x, y, bcd, bcd_digits);
}

static int font_get_char_xy(int *x, int *y, struct font const *font, char c)
{
    int row, col;
    /* TODO: Maybe in the future a lookup table would be better */
    switch (c)
    {
    case '0':
        col = 0;
        row = 0;
        break;
    case '1':
        col = 1;
        row = 0;
        break;
    case '2':
        col = 2;
        row = 0;
        break;
    case '3':
        col = 3;
        row = 0;
        break;
    case '4':
        col = 4;
        row = 0;
        break;
    case '5':
        col = 0;
        row = 1;
        break;
    case '6':
        col = 1;
        row = 1;
        break;
    case '7':
        col = 2;
        row = 1;
        break;
    case '8':
        col = 3;
        row = 1;
        break;
    case '9':
        col = 4;
        row = 1;
        break;
    case '/':
        col = 5;
        row = 0;
        break;
    default:
        return -1;
    }

    *x = col * font->glyph_w;
    *y = row * font->glyph_h;
    return 0;
}
