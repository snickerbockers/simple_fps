/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "trace.h"

stream_t *trace_sinks[TRACE_LEVEL_COUNT] = {

#ifdef NO_TRACE_INFO
    NULL,
#else
    stream_stdout,
#endif

#ifdef NO_TRACE_ERR
    NULL,
#else
    stream_stderr,
#endif

#ifdef NO_TRACE_WARN
    NULL,
#else
    stream_stderr,
#endif

#ifdef NO_TRACE_DBG
    NULL,
#else
    stream_stdout,
#endif
};

void trace(int trace_level, char const *fmt, ...)
{
    va_list arg_list;
    stream_t *sink;

    sink = trace_sinks[trace_level];
    if (sink)
    {
        va_start(arg_list, fmt);
        stream_vprintf(sink, fmt, &arg_list);
        va_end(arg_list);
    }
}
