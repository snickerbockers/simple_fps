/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/
#ifndef BITSTREAM_H_
#define BITSTREAM_H_

#include <stdlib.h>
#include <stdint.h>

/**
 * A stream of information that is read in serially one bit at a time.
 * Writing is not currently supported.
 */
struct bit_stream
{
    /* len is in bytes, pos is in bits. */
    size_t pos; /**< Position in the stream, in bits */
    size_t len; /**< length of the stream, in bytes */
    uint8_t *stream; /**< pointer to the beginning of the stream */
};

/**
 * Read one bit from a stream and advance to the next bit
 * @param the stream to read from
 * @return 1 if the bit is set, 0 if it is not, <0 if there was an error
 */
int bit_stream_get(struct bit_stream *stream);

/**
 * read n bits from the stream
 * Read n bits and shift them into out starting from the LSB.
 * Also advances the stream by n bits.
 * @param stream the stream to read from
 * @param out pointer to the output.  Should be initialized before calling
 * @param n how many bits to read.
 * @return 0 for success, <0 if there was an error.
 */
int bit_stream_shift_out(struct bit_stream *stream, unsigned *out, int n);

/**
 * Initialize a new bit_stream structure.
 * No new memory is allocated, so there is no cleanup function to match this.
 * @param stream the uninitialized bit_stream structure
 * @param dat the data the stream will read from.
 * @param bye_len the length of dat, in bytes
 */
void bit_stream_init(struct bit_stream *stream, void const *dat,
                     size_t byte_len);

/**
 * Read a whole byte from the stream.
 * If the stream position is not byte-aligned,
 * it will be advanced to the next byte first.
 * @param stream teh stream to read from
 * @param byte where to output the byte that is read.
 * @return 0 for success, <0 if there was an error
 */
int bit_stream_get_byte(struct bit_stream *stream, int *byte);

#endif
