/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef LIST_H_
#define LIST_H_

#include <stdint.h>
#include <string.h>

/*
 * linked-list implementation.
 * It's actually more like a stack than anything else.
 */

#define OFFSET_OF(outer_type, member)                   \
    ((uintptr_t) (&(((outer_type*)(NULL))->member)))

struct list_entry
{
    struct list_entry *next;

    /*
     * pointer to the previous list_entry's
     * next pointer, or the list_head's
     * front pointer if this is the front.
     */
    struct list_entry **pprev;
};

struct list_head
{
    struct list_entry *front;
};

static inline struct list_entry*
list_front(struct list_head *head)
{
    return head->front;
}

#define LIST_DEREF(containing_tp, ent_name, ent)                        \
    ((ent) ?                                                            \
     ((containing_tp*)((uint8_t*)(ent) -                                \
                       OFFSET_OF(containing_tp, ent_name))) :           \
     NULL)

static inline void
list_insert_before(struct list_entry *pos, struct list_entry *new_ent)
{
    new_ent->pprev = pos->pprev;
    new_ent->next = pos;
    (*new_ent->pprev) = new_ent;
    pos->pprev = &new_ent->next;
}

static inline void
list_push_front(struct list_head *head, struct list_entry *new_ent)
{
    if (head->front)
        list_insert_before(head->front, new_ent);
    else
    {
        head->front = new_ent;
        new_ent->pprev = &head->front;
    }
}

static inline void
list_remove(struct list_entry *ent)
{
    if (ent->next)
        ent->next->pprev = ent->pprev;
    *ent->pprev = ent->next;
}

static inline struct list_entry *
list_pop_front(struct list_head *head)
{
    struct list_entry *entry;

    entry = list_front(head);
    if (entry)
        list_remove(entry);

    return entry;
}

static inline void
list_head_init(struct list_head *head)
{
    memset(head, 0, sizeof(*head));
}

#define LIST_FOREACH(head, containing_tp, cursor, list_ent_name)        \
    for ((cursor) = LIST_DEREF(containing_tp, list_ent_name,            \
                               list_front(&(head)));                    \
         (cursor);                                                      \
         (cursor) = LIST_DEREF(containing_tp, list_ent_name,            \
                               (cursor)->list_ent_name.next))

/*
 * like LIST_FOREACH, but it doesn't initialize the cursor
 * so you can start the iteration at any arbitrary position.
 */
#define LIST_FOREACH_LIMITED(containing_tp, cursor, list_ent_name)      \
    for (; (cursor);                                                    \
         (cursor) = LIST_DEREF(containing_tp, list_ent_name,            \
                               (cursor)->list_ent_name.next))

#endif
