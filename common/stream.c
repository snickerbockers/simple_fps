/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "stream.h"

#define STREAM_STDIO_ERR_CHECK_ERRNO STREAM_ERR_NUM

static size_t stream_stdio_read(void *ptr, size_t sz, size_t nitems,
                                stream_t *stream);
static size_t stream_stdio_write(void const *ptr, size_t sz, size_t nitems,
                                 stream_t *stream);
static int stream_stdio_seek(stream_t *stream, long offset, int whence);
static long stream_stdio_tell(stream_t *stream);
static int stream_stdio_close(stream_t *stream);
static char const* stream_stdio_errstr(stream_t *stream, int err);
static int stream_stdio_vprintf(stream_t *stream, char const *format, va_list *argp);

struct stream_stdio_ctxt
{
    FILE *fp;
    int errno_val;
};

static struct stream_ops stream_stdio_ops = {
    .read_func = stream_stdio_read,
    .write_func = stream_stdio_write,
    .seek_func = stream_stdio_seek,
    .tell_func = stream_stdio_tell,
    .close_func = stream_stdio_close,
    .errstr_func = stream_stdio_errstr,
    .vprintf_func = stream_stdio_vprintf
};

/*
 * These are handlers for stream_t's which
 * are wrappers over stdout/stdin/stderr.
 * Using stream_stdio_ops instead of coding a
 * special stream_t for these three files is possible,
 * but them I'd have to initialize it, and I like this better
 */
static size_t stream_dflt_stdio_read(void *ptr, size_t sz, size_t nitems,
                                     stream_t *stream);
static size_t stream_dflt_stdio_write(void const *ptr, size_t sz, size_t nitems,
                                      stream_t *stream);
static int stream_dflt_stdio_vprintf(stream_t *stream, char const *format,
                                     va_list *argp);

static struct stream_ops stream_dflt_stdio_ops = {
    .read_func = stream_dflt_stdio_read,
    .write_func = stream_dflt_stdio_write,
    .vprintf_func = stream_dflt_stdio_vprintf
};

#define STREAM_STDIN_FILENO  ((void*)0)
#define STREAM_STDOUT_FILENO ((void*)1)
#define STREAM_STDERR_FILENO ((void*)2)

stream_t stream_stdout_ = {
    .stream_ctxt = STREAM_STDOUT_FILENO,
    .ops = &stream_dflt_stdio_ops
};

stream_t stream_stdin_ = {
    .stream_ctxt = STREAM_STDIN_FILENO,
    .ops = &stream_dflt_stdio_ops
};

stream_t stream_stderr_ = {
    .stream_ctxt = STREAM_STDERR_FILENO,
    .ops = &stream_dflt_stdio_ops
};

size_t stream_read(void *ptr, size_t sz, size_t nitems, stream_t *stream)
{
    if (stream->ops->read_func)
        return stream->ops->read_func(ptr, sz, nitems, stream);

    stream->err = STREAM_ERR_INVAL_OP;
    return 0;
}

size_t stream_write(void const *ptr, size_t sz, size_t nitems, stream_t *stream)
{
    if (stream->ops->write_func)
        return stream->ops->write_func(ptr, sz, nitems, stream);

    stream->err = STREAM_ERR_INVAL_OP;
    return 0;
}

int stream_seek(stream_t *stream, long offset, int whence)
{
    if (stream->ops->seek_func)
        return stream->ops->seek_func(stream, offset, whence);

    stream->err = STREAM_ERR_INVAL_OP;
    return -1;
}

long stream_tell(stream_t *stream)
{
    if (stream->ops->tell_func)
        return stream->ops->tell_func(stream);

    stream->err = STREAM_ERR_INVAL_OP;
    return -1;
}

int stream_close(stream_t *stream)
{
    if (stream->ops->close_func)
        return stream->ops->close_func(stream);

    /*
     * If there's no close implementation, that doesn't mean that close is
     * invalid, it just means that there's nothing the stream implementation
     * needs to do.  Ergo, don't return STREAM_ERR_INVAL_OP like with other
     * functions
     */
    return 0;
}

char const *stream_errstr(stream_t *stream, int err)
{
    char const *ret;

    switch (err)
    {
    case STREAM_ERR_NONE:
        return "no error";
    case STREAM_ERR_UNKNOWN:
        return "unknown error";
    case STREAM_ERR_INVAL_OP:
        return "invalid operation";
    default:
        if (stream->ops->errstr_func &&
            (ret = stream->ops->errstr_func(stream, err)))
            return ret;
    }

    return "unrecognized error; there may be a problem with the "
        "stream implementation";
}

int stream_err(stream_t *stream)
{
    return stream->err;
}

int stream_printf(stream_t *stream, char const *format, ...)
{
    va_list arg_list;
    int ret_val;

    va_start(arg_list, format);
    ret_val = stream_vprintf(stream, format, &arg_list);
    va_end(arg_list);

    return ret_val;
}

int stream_vprintf(stream_t *stream, char const *format, va_list *argp)
{
    int ret_val;

    if (stream->ops->vprintf_func)
    {
        ret_val = stream->ops->vprintf_func(stream, format, argp);
        return ret_val;
    }

    stream->err = STREAM_ERR_INVAL_OP;
    return -1;
}

stream_t *stream_stdio_open(char const *path, char const *mode)
{
    FILE *fp;
    stream_t *stream;
    struct stream_stdio_ctxt *ctxt;

    fp = fopen(path, mode);
    if (!fp)
        return NULL;

    stream = (stream_t*)calloc(1, sizeof(stream_t));
    if (!stream)
    {
        fclose(fp);
        return NULL;
    }

    ctxt = (struct stream_stdio_ctxt*)malloc(sizeof(struct stream_stdio_ctxt));
    if (!ctxt)
    {
        fclose(fp);
        free(stream);
        return NULL;
    }

    ctxt->fp = fp;

    stream->ops = &stream_stdio_ops;
    stream->stream_ctxt = ctxt;

    return stream;
}

static size_t stream_stdio_read(void *ptr, size_t sz, size_t nitems,
                                stream_t *stream)
{
    size_t ret_val;
    struct stream_stdio_ctxt *ctxt;

    ctxt = (struct stream_stdio_ctxt*)stream->stream_ctxt;
    ret_val = fread(ptr, sz, nitems, ctxt->fp);

    if (ret_val == 0 && (nitems == 0 || sz == 0))
    {
        stream->err = STREAM_STDIO_ERR_CHECK_ERRNO;
        ctxt->errno_val = errno;
    }

    return ret_val;
}

static size_t stream_stdio_write(void const *ptr, size_t sz, size_t nitems,
                                 stream_t *stream)
{
    size_t ret_val;
    struct stream_stdio_ctxt *ctxt;

    ctxt = (struct stream_stdio_ctxt*)stream->stream_ctxt;
    ret_val = fwrite(ptr, sz, nitems, ctxt->fp);

    if (ret_val == 0 && (nitems == 0 || sz == 0))
    {
        stream->err = STREAM_STDIO_ERR_CHECK_ERRNO;
        ctxt->errno_val = errno;
    }

    return ret_val;
}

static int stream_stdio_seek(stream_t *stream, long offset, int whence)
{
    int ret_val;
    int stdio_whence;
    struct stream_stdio_ctxt *ctxt;

    ctxt = (struct stream_stdio_ctxt*)stream->stream_ctxt;

    switch (whence)
    {
    case STREAM_SEEK_SET:
        stdio_whence = SEEK_SET;
        break;
    case STREAM_SEEK_CUR:
        stdio_whence = STREAM_SEEK_CUR;
        break;
    case STREAM_SEEK_END:
        stdio_whence = STREAM_SEEK_END;
        break;
    default:
        return STREAM_ERR_INVAL_ARG;
    }

    ret_val = fseek(ctxt->fp, offset, stdio_whence);
    if (ret_val < 0)
    {
        stream->err = STREAM_STDIO_ERR_CHECK_ERRNO;
        ctxt->errno_val = errno;
    }

    return ret_val;
}

static long stream_stdio_tell(stream_t *stream)
{
    long ret_val;
    struct stream_stdio_ctxt *ctxt;

    ctxt = (struct stream_stdio_ctxt*)stream->stream_ctxt;

    ret_val = ftell(ctxt->fp);

    if (ret_val < 0)
    {
        stream->err = STREAM_STDIO_ERR_CHECK_ERRNO;
        ctxt->errno_val = errno;
    }

    return ret_val;
}

static int stream_stdio_close(stream_t *stream)
{
    int ret_val;
    struct stream_stdio_ctxt *ctxt;

    ctxt = (struct stream_stdio_ctxt*)stream->stream_ctxt;

    ret_val = fclose(ctxt->fp);
    if (ret_val)
    {
        /* XXX: should I free stream->ctxt and stream ? */
        fprintf(stderr, "ERROR: unable to close stdio stream\n");
        return ret_val;
    }

    free(ctxt);
    free(stream);
    return 0;
}

static char const* stream_stdio_errstr(stream_t *stream, int err)
{
    struct stream_stdio_ctxt *ctxt;

    ctxt = (struct stream_stdio_ctxt*)stream->stream_ctxt;

    if (err == STREAM_STDIO_ERR_CHECK_ERRNO)
        return strerror(ctxt->errno_val);

    return NULL;
}

static int stream_stdio_vprintf(stream_t *stream, char const *format,
                                va_list *argp)
{
    int ret_val;
    struct stream_stdio_ctxt *ctxt;

    ctxt = (struct stream_stdio_ctxt*)stream->stream_ctxt;

    ret_val = vfprintf(ctxt->fp, format, *argp);
    if (ret_val < 0)
    {
        stream->err = STREAM_STDIO_ERR_CHECK_ERRNO;
        ctxt->errno_val = errno;
    }

    return ret_val;
}

static FILE *stream_dflt_stdio_fp(void *fileno)
{
    if (fileno == STREAM_STDIN_FILENO)
        return stdin;
    else if (fileno == STREAM_STDOUT_FILENO)
        return stdout;
    else if (fileno == STREAM_STDERR_FILENO)
        return stderr;
    return NULL;
}

static size_t stream_dflt_stdio_read(void *ptr, size_t sz, size_t nitems,
                                     stream_t *stream)
{
    FILE *fp;
    size_t ret_val;

    fp = stream_dflt_stdio_fp(stream->stream_ctxt);

    if (!fp || fp == stdout || fp == stderr)
    {
        stream->err = STREAM_ERR_INVAL_ARG;
        return 0;
    }

    ret_val = fread(ptr, sz, nitems, fp);

    if (ret_val == 0 && (nitems == 0 || sz == 0))
        stream->err = STREAM_ERR_UNKNOWN;

    return ret_val;
}

static size_t stream_dflt_stdio_write(void const *ptr, size_t sz, size_t nitems,
                                      stream_t *stream)
{
    FILE *fp;
    size_t ret_val;

    fp = stream_dflt_stdio_fp(stream->stream_ctxt);

    if (!fp || fp == stdin)
    {
        stream->err = STREAM_ERR_INVAL_ARG;
        return 0;
    }

    ret_val = fwrite(ptr, sz, nitems, fp);

    if (ret_val == 0 && (nitems == 0 || sz == 0))
        stream->err = STREAM_ERR_UNKNOWN;

    return ret_val;
}

static int stream_dflt_stdio_vprintf(stream_t *stream, char const *format,
                                     va_list *argp)
{
    FILE *fp;
    int ret_val;

    fp = stream_dflt_stdio_fp(stream->stream_ctxt);

    ret_val = vfprintf(fp, format, *argp);
    if (ret_val < 0)
        stream->err = STREAM_STDIO_ERR_CHECK_ERRNO;

    return ret_val;
}
