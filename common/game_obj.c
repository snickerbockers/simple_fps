/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdlib.h>

#include "trace.h"
#include "img.h"
#include "game.h"
#include "hud.h"
#include "player_weapon.h"

#include "game_obj.h"

#define DRUM_WIDTH REAL_HALF
#define PLAYER_WIDTH REAL_HALF
#define BULLET_BOX_WIDTH REAL_UNIT
#define HEALTH_PICKUP_WIDTH REAL_HALF
#define END_BUTTON_WIDTH (REAL_HALF >> 1)

#define MAX_USE_DIST REAL_HALF

struct player_weapon weapon_list[] = {
    { WEAPON_UNARMED, NULL },
    {
        .name = WEAPON_PISTOL,
        .on_attack = pistol_attack,
        .draw_hud = hud_draw_pistol,
        .get_ammo = pistol_get_ammo,
        .get_clip_ammo = pistol_get_clip_ammo
    },
    { NULL }
};

static struct tex drum_tex;
static struct tex bullet_box_tex;
static struct tex health_pickup_tex;
static struct tex end_button_tex;

static void drum_draw(struct game_obj *obj);
static void drum_on_death(struct game_obj *obj);
static void drum_on_use(struct game_obj *obj);

static void player_on_create(struct game_obj *obj);

#define BULLET_BOX_AMMO_COUNT 10
static void bullet_box_draw(struct game_obj *obj);
static int bullet_box_on_collide(struct game_obj *obj,
                                 struct game_obj *other_obj);

#define HEALTH_PICKUP_HEALTH_COUNT 25
static void health_pickup_draw(struct game_obj *obj);
static int health_pickup_on_collide(struct game_obj *obj,
                                    struct game_obj *other_obj);

static void end_button_on_use(struct game_obj *obj);
static void end_button_draw(struct game_obj *obj);

static void cleanup_individual_game_obj(struct game_obj *obj);

static int game_obj_on_collide(struct game_obj *obj,
                               struct game_obj *other_obj);

/*
 * Test to see if obj1 and obj2 intersect.
 * If so, separate them (if they're both tangible) and call
 * the appropriate callbacks.
 */
static void test_game_obj_intersection(struct game_obj *obj1,
                                       struct game_obj *obj2);

struct player_weapon const* get_weapon_by_name(char const *name)
{
    struct player_weapon const* cursor;

    for (cursor = weapon_list; cursor->name; cursor++)
        if (strcmp(cursor->name, name) == 0)
            return cursor;

    return NULL;
}

static struct game_obj_class game_obj_class_list[] = {
    {
        .class_name = "drum",
        .draw_col = drum_draw,
        .on_death = drum_on_death,
        .rad = DRUM_WIDTH >> 1,
        .flags = FLAG_IMMOVABLE,
        .on_use = drum_on_use,
        .hp_max = 5
    },
    {
        /*
         * the player obj represents a player in the game world.
         * The reason this is needed is so that there don't have to be separate
         * callbacks for game_obj/player interactions and game_obj/game_obj
         * interactions.
         */
        .class_name = "player",
        .rad = PLAYER_WIDTH >> 1,
        .flags = 0,
        .state_sz = sizeof(struct player_state),
        .on_create = player_on_create
    },
    {
        .class_name = "bullet_box",
        .rad = BULLET_BOX_WIDTH >> 1,
        .flags = FLAG_IMMOVABLE | FLAG_INTANGIBLE,
        .draw_col = bullet_box_draw,
        .on_collide = bullet_box_on_collide
    },
    {
        .class_name = "health_pickup",
        .rad = HEALTH_PICKUP_WIDTH >> 1,
        .flags = FLAG_IMMOVABLE | FLAG_INTANGIBLE,
        .draw_col = health_pickup_draw,
        .on_collide = health_pickup_on_collide
    },
    {
        .class_name = "end_button",
        .rad = END_BUTTON_WIDTH >> 1,
        .flags = FLAG_IMMOVABLE,
        .draw_col = end_button_draw,
        .on_use = end_button_on_use
    },
    { NULL }
};

void draw_game_obj(struct game_obj *obj)
{
    /*
     * XXX might be better to give invisible objs
     * a noop draw_col handler to avoid branching.
     */
    if (obj->class->draw_col)
        obj->class->draw_col(obj);
}

static void drum_draw(struct game_obj *obj)
{
    draw_billboard(game.sys.scr, &drum_tex, obj->pos, DRUM_WIDTH);
}

static void drum_on_death(struct game_obj *obj)
{
    delete_game_obj(obj);
}

static void bullet_box_draw(struct game_obj *obj)
{
    draw_billboard(game.sys.scr, &bullet_box_tex, obj->pos,
                       BULLET_BOX_WIDTH);
}

static int bullet_box_on_collide(struct game_obj *obj,
                                 struct game_obj *other_obj)
{
    if (other_obj == game.state.player_obj)
    {
        pistol_add_ammo(BULLET_BOX_AMMO_COUNT);
        return 1;
    }

    return 0;
}

static void health_pickup_draw(struct game_obj *obj)
{
    draw_billboard(game.sys.scr, &health_pickup_tex, obj->pos,
                   HEALTH_PICKUP_WIDTH);
}

static int health_pickup_on_collide(struct game_obj *obj,
                                    struct game_obj *other_obj)
{
    if (other_obj == game.state.player_obj &&
        get_player_state()->health < PLAYER_HEALTH_MAX)
    {
        player_add_health(HEALTH_PICKUP_HEALTH_COUNT);
        return 1;
    }

    return 0;
}

static void end_button_draw(struct game_obj *obj)
{
    draw_billboard(game.sys.scr, &end_button_tex, obj->pos, END_BUTTON_WIDTH);
}

static void end_button_on_use(struct game_obj *obj)
{
    /* end the game at the end of this frame */
    game.state.end_level = 1;
}

static void player_on_create(struct game_obj *obj)
{
    struct player_state *state = (struct player_state*)obj->state;

    state->weapon = get_weapon_by_name(WEAPON_PISTOL);
    state->health = PLAYER_HEALTH_MAX;
}

void player_attack(void)
{
    struct game_obj *player;
    struct player_weapon const *wpn;

    player = game.state.player_obj;
    wpn = ((struct player_state*)player->state)->weapon;

    if (wpn->on_attack)
        wpn->on_attack(wpn);
}


int init_game_objs(void)
{
    if (tex_load_chroma_key(game.sys.scr, &drum_tex,
                            DATA_DIR"/drum.png", 1, STD_CHROMA_KEY) < 0)
        return -1;
    if (tex_load_chroma_key(game.sys.scr, &bullet_box_tex,
                            DATA_DIR"/bullet_box.png", 1, STD_CHROMA_KEY) < 0)
        return -1;
    if (tex_load_chroma_key(game.sys.scr, &health_pickup_tex,
                            DATA_DIR"/health_pickup.png", 1,
                            STD_CHROMA_KEY) < 0)
        return -1;
    if (tex_load_chroma_key(game.sys.scr, &end_button_tex,
                            DATA_DIR"/end_button.png", 1,
                            STD_CHROMA_KEY) < 0)
        return -1;

    return 0;
}

void cleanup_game_objs(void)
{
    struct list_entry *cursor;
    struct game_obj *obj;

    while (list_front(&game.state.game_obj_list))
    {
        cursor = list_pop_front(&game.state.game_obj_list);
        obj = LIST_DEREF(struct game_obj, list, cursor);
        free(obj);
    }

    tex_cleanup(&drum_tex);
    tex_cleanup(&bullet_box_tex);
    tex_cleanup(&health_pickup_tex);
    tex_cleanup(&end_button_tex);
    memset(&drum_tex, 0, sizeof(drum_tex));
    memset(&bullet_box_tex, 0, sizeof(bullet_box_tex));
    memset(&health_pickup_tex, 0, sizeof(health_pickup_tex));
}

struct game_obj *game_obj_hitscan(real_t *dist_out,
                                  struct game_obj const *source,
                                  real_t const *ray_start,
                                  real_t const *ray_dir)
{
    struct game_obj *cur, *hit;
    real_t dist, closest_dist;

    hit = NULL;
    closest_dist = REAL_MAX;

    LIST_FOREACH(game.state.game_obj_list, struct game_obj, cur, list)
    {
        if (cur == source)
            continue;

        if (game_obj_intangible(cur))
            continue;

        /*
         * Take the vector from ray_start to pt, call it v1
         * project pt onto the ray to get the closest point on the ray to pt
         *     (call it pt_closest).
         * take the vector from ray_start to pt_closest, call it v2
         * let v3 be the vector from pt_closest to pt.  |v3| is the distance we
         *     need.
         * it is self-evident that v1 == v2 + v3, ergo v3 == v1 - v2
         */
        real_t v1[2], v2[2], v3[2];
        real_t v1_proj;

        /* get v1 */
        pt2_sub(v1, cur->pos, ray_start);
        pt2_dot(&v1_proj, v1, ray_dir);

        /* discard any hits behind the ray's start */
        if (v1_proj < REAL_ZERO)
            continue;

        /* get v2 */
        pt2_scale(v2, ray_dir, v1_proj);

        /* get magnitude, v3 */
        pt2_sub(v3, v1, v2);
        pt2_mag(&dist, v3);

        if (dist <= cur->class->rad)
        {
            /*
             * ray passes through the game_obj,
             * now let's get the distance to the game_obj.
             *
             * The way this is done is a little weird because instead of taking
             * the distance to the cylinder, we take the distance to the plane
             * that goes through the center of the cylinder.  This may cause
             * some minor oddities when shooting at game_objs which are standing
             * behind corners, but I'm not sure.  Even if it does, I expect it
             * will be minor and nigh un-noticable.
             */
            pt2_mag(&dist, v2);
            if (dist <= closest_dist)
            {
                *dist_out = closest_dist = dist;
                hit = cur;
            }
        }
    }

    return hit;
}

struct game_obj_class const* find_game_obj_class(char const *class_name)
{
    struct game_obj_class const *cursor = game_obj_class_list;

    while (cursor->class_name)
    {
        if (strcmp(cursor->class_name, class_name) == 0)
            return cursor;
        cursor++;
    }

    TRACE_ERR("Unable to locate class \"%s\"\n", class_name);

    return NULL;
}

struct game_obj *game_obj_new(char const *class_name,
                              real_t pos[2])
{
    struct game_obj_class const *class;
    struct game_obj *new_obj;

    class = find_game_obj_class(class_name);

    if (!class)
        return NULL;

    new_obj = (struct game_obj*)calloc(1, sizeof(struct game_obj));

    if (!new_obj)
    {
        TRACE_ERR("%s - Failed allocation\n", __func__);
        return NULL;
    }

    new_obj->pos[0] = pos[0];
    new_obj->pos[1] = pos[1];

    /* TODO: maybe make this an argument to the function ? */
    new_obj->dir[0] = REAL_ZERO;
    new_obj->dir[1] = REAL_UNIT;

    new_obj->hp = class->hp_max;

    if (class->state_sz)
        new_obj->state = malloc(class->state_sz);

    list_push_front(&game.state.game_obj_list, &new_obj->list);
    new_obj->class = class;

    if (new_obj->class->on_create)
        new_obj->class->on_create(new_obj);

    return new_obj;
}

void delete_game_obj(struct game_obj *obj)
{
    struct game_state *state = &game.state;

    /* TODO: consider a destructor function here */
    if (obj->state)
        free(obj->state);

    if (obj == state->player_obj)
    {
        TRACE_ERR("attempt to delete player_obj denied\n");
        return;
    }

    list_remove(&obj->list);
    free(obj);
}

void draw_game_objs(void)
{
    real_t vec_to_obj[2];
    real_t player_z_axis[2];
    real_t const *player_pos;
    real_t max_dist;
    struct game_obj *obj, *closest_obj;
    struct list_head depth_list;

    player_pos = game.state.player_obj->pos;
    get_cam_forward(player_z_axis);

    list_head_init(&depth_list);

    LIST_FOREACH(game.state.game_obj_list, struct game_obj, obj, list)
    {
        /* calculate the distance to each game_obj along the player's z-axis */
        pt2_sub(vec_to_obj, obj->pos, player_pos);
        pt2_dot(&obj->dist_from_player, player_z_axis, vec_to_obj);

        memset(&obj->depth_sort_list_entry, 0, sizeof(obj->depth_sort_list_entry));
        list_push_front(&depth_list, &obj->depth_sort_list_entry);
    }

    while (list_front(&depth_list))
    {
        max_dist = REAL_MIN;
        closest_obj = NULL;

        LIST_FOREACH(depth_list, struct game_obj, obj, depth_sort_list_entry)
        {
            if (obj->dist_from_player > max_dist)
            {
                max_dist = obj->dist_from_player;
                closest_obj = obj;
            }
        }

        /* XXX I don't believe this is necessary... */
        if (!closest_obj)
            return;

        list_remove(&closest_obj->depth_sort_list_entry);
        draw_game_obj(closest_obj);
    }
}

static void cleanup_individual_game_obj(struct game_obj *obj)
{
    free(obj);
}

static int game_obj_on_collide(struct game_obj *obj,
                                struct game_obj *other_obj)
{
    if (obj->class->on_collide)
        return obj->class->on_collide(obj, other_obj);

    return 0;
}

static void test_game_obj_intersection(struct game_obj *obj1,
                                       struct game_obj *obj2)
{
    struct game_sys *sys;
    struct game_state *state;
    real_t v[2], dist_squared, r_squared, old_obj1_pos[2], old_obj2_pos[2],
        new_obj1_pos[2], new_obj2_pos[2];
    int kill_obj1, kill_obj2;

    sys = &game.sys;
    state = &game.state;

    r_squared = obj1->class->rad + obj2->class->rad;
    r_squared = real_mul(r_squared, r_squared);
    pt2_sub(v, obj2->pos, obj1->pos);
    dist_squared = real_mul(v[0], v[0]) + real_mul(v[1], v[1]);

    if (dist_squared < r_squared)
    {
        if (!game_obj_intangible(obj1) && !game_obj_intangible(obj2))
        {
            /* need to separate obj1 and obj2 */

            /*
             * need to move so that sqrt(dist_squared) == sqrt(r_squared)
             * ergo the distance_to_move == sqrt(r_squared) - sqrt(dist_squared)
             */
            pt2_norm(v, v);
            pt2_scale(v, v, real_sqrt(r_squared) - real_sqrt(dist_squared));

            if (!game_obj_immovable(obj1) && !game_obj_immovable(obj2))
            {
                /* move both objs by an equal distance in opposite directions */
                v[0] >>= 1;
                v[1] >>= 1;

                memcpy(old_obj1_pos, obj1->pos, sizeof(real_t) * 2);
                memcpy(old_obj2_pos, obj2->pos, sizeof(real_t) * 2);

                pt2_sub(new_obj1_pos, obj1->pos, v);
                pt2_add(new_obj2_pos, obj2->pos, v);

                move_pt(obj1->pos, old_obj1_pos, new_obj1_pos,
                        obj1->class->rad, &sys->cbsp);
                move_pt(obj2->pos, old_obj2_pos, new_obj2_pos,
                        obj2->class->rad, &sys->cbsp);
            }
            else if (!game_obj_immovable(obj1) && game_obj_immovable(obj2))
            {
                /* move obj1 */
                memcpy(old_obj1_pos, obj1->pos, sizeof(real_t) * 2);

                pt2_sub(new_obj1_pos, obj1->pos, v);

                move_pt(obj1->pos, old_obj1_pos, new_obj1_pos,
                        obj1->class->rad, &sys->cbsp);
            }
            else if (game_obj_immovable(obj1) && !game_obj_immovable(obj2))
            {
                /* move obj2 */
                memcpy(old_obj2_pos, obj2->pos, sizeof(real_t) * 2);

                pt2_add(new_obj2_pos, obj2->pos, v);

                move_pt(obj2->pos, old_obj2_pos, new_obj2_pos,
                        obj2->class->rad, &sys->cbsp);
            }
            else
            {
                /*
                 * level creators should be careful enough
                 * to make sure that this never happens.
                 */
                TRACE_WARN("Two immovable objects intersected!\n");
            }
        }

        kill_obj1 = game_obj_on_collide(obj1, obj2);
        kill_obj2 = game_obj_on_collide(obj2, obj1);

        if (kill_obj1)
            delete_game_obj(obj1);
        if (kill_obj2)
            delete_game_obj(obj2);
    }
}

void game_obj_intersection()
{
    struct game_obj *go1, *go2;

    LIST_FOREACH(game.state.game_obj_list, struct game_obj, go1, list)
    {
        go2 = LIST_DEREF(struct game_obj, list, go1->list.next);

        LIST_FOREACH_LIMITED(struct game_obj, go2, list)
        {
            test_game_obj_intersection(go1, go2);
        }
    }
}

void game_obj_use(struct game_obj *player)
{
    struct game_obj *obj;    /* the game_obj to be used */
    real_t dist;             /* dist to the object */
    real_t dir[2];

    get_cam_forward(dir);
    obj = game_obj_hitscan(&dist, player, player->pos, dir);

    if (obj && dist <= MAX_USE_DIST && obj->class->on_use)
        obj->class->on_use(obj);
}


static sample_t drum_use_sound_callback(sample_idx_t sample_idx, void *userarg)
{
    return INT16_MAX * sin(((double)sample_idx / (double)SNDSERV_SAMPLE_FREQ) *
                           M_PI * 2.0 * 350);
}

static struct synth_sound drum_use_sound =
{
    .callback = drum_use_sound_callback,
    .callback_data = NULL,
    .n_samples = SNDSERV_SAMPLE_FREQ,
    .flags = 0
};

static void drum_on_use(struct game_obj *obj)
{
    mixer_play_sound(&game.sys.mixer, &drum_use_sound, VOL_FULL);
}

struct player_weapon const* get_player_weapon(void)
{
    return get_player_state()->weapon;
}

struct player_state *get_player_state(void)
{
    return (struct player_state*)game.state.player_obj->state;
}

static void player_add_health(int hp)
{
    struct player_state *player_state = get_player_state();

    player_state->health += hp;
    if (player_state-> health > PLAYER_HEALTH_MAX)
        player_state->health = PLAYER_HEALTH_MAX;
}
