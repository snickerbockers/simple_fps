/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef GAME_H_
#define GAME_H_

#include "list.h"
#include "bsp.h"
#include "screen.h"
#include "draw.h"
#include "game_obj.h"
#include "font.h"
#include "sndserv.h"
#include "mixer.h"

struct game_state
{
    struct list_head game_obj_list;

    struct game_obj *player_obj;
    real_t player_vert_pos;

    /* player movement directions, these correspond to keys being pressed */
    int left, right, forward, back, up, down, fire, use;

    int mouse_motion_x;

    unsigned last_frame_ticks;
    unsigned first_frame_ticks;

    unsigned this_frame_ticks;
    real_t frame_time;

    /* when set to 1, the current level will end. */
    int end_level;
};

struct game_sys
{
    struct bsp_tree rbsp;    /* bsp tree used for rendering */
    struct bsp_tree cbsp;    /* bsp tree used for collision detection */
    struct tex *tex_array;
    struct screen *scr;
    struct sndserv sndserv;
    struct mixer mixer;
    struct font overlay_font;
    int n_tex;
    int ceil_tex_idx, floor_tex_idx;
};

struct game
{
    struct game_sys sys;
    struct game_state state;
};

extern struct game game;

#endif
