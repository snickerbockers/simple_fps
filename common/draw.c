/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <assert.h>
#include <math.h>

#include "real.h"
#include "geo.h"
#include "viewport.h"
#include "game.h"

#include "draw.h"

void draw_tex_col(struct screen *scr, struct tex const *src, int src_x,
                  int dst_x, int dst_y1, int dst_y2)
{
    int row;
    int height;
    int dst_y1_clamp, dst_y2_clamp;
    color_t *frame_buffer;
    int screen_w, screen_h;
    real_t dst_y1_as_real, dst_y2_as_real;
    real_t t;

    assert(dst_y1 <= dst_y2);

    dst_y1_as_real = real_from_int(dst_y1);
    dst_y2_as_real = real_from_int(dst_y2);
    dst_y1_clamp = dst_y1;
    dst_y2_clamp = dst_y2;
    screen_w = scr->screen_w;
    screen_h = scr->screen_h;

    if (dst_y1 < 0)
        dst_y1_clamp = 0;
    if (dst_y1 >= screen_h)
        dst_y1_clamp = screen_h - 1;
    if (dst_y2 <= 0)
        dst_y2_clamp = 0;
    if (dst_y2 >= screen_h)
        dst_y2_clamp = screen_h - 1;

    assert(dst_y1_clamp <= dst_y2_clamp);
    frame_buffer = scr->frame_buffer;

    if (real_eq(dst_y1_as_real, dst_y2_as_real))
    {
        /*
         * special case to avoid divide-by-zero.
         *
         * This is a very rare case that can only happen when we only
         * need to render a single pixel; it may be possible to ignore it
         * in the interest of eliminating branching
         */
        int y_idx = src->h - 1;
        color_t mask = src->mask[src_x * src->h + y_idx];
        color_t src_pix = src->data[src_x * src->h + y_idx];
        color_t *dst_pix = frame_buffer + screen_w * dst_y1_clamp + dst_x;

        *dst_pix = (src_pix & mask) | ((*dst_pix) & ~mask);

        return;
    }

    for (row = dst_y1_clamp; row <= dst_y2_clamp; row++)
    {
        t = real_div(real_sub(dst_y2_as_real, real_from_int(row)),
                     real_sub(dst_y2_as_real, dst_y1_as_real));

        t = t >= REAL_ZERO ? t : REAL_ZERO;
        t = t <= REAL_UNIT ? t : REAL_UNIT;
        /*
         * t can get out of bounds if the vertical camera pos is
         * too far above/below the floor/ceiling
         */
        int y_idx = real_to_int(real_mul(
                                    real_sub(REAL_UNIT, t),
                                    real_from_int(src->h - 1)));
        color_t src_pix = src->data[src_x * src->h + y_idx];
        color_t mask = src->mask[src_x * src->h + y_idx];
        color_t *dst_pix = frame_buffer + screen_w * row + dst_x;

        *dst_pix = (src_pix & mask) | ((*dst_pix) & ~mask);
    }
}


void draw_ceil_col(struct screen *scr, struct tex const *src, int col,
                   int y_start, int y_end, real_t const cam_pos[2],
                   real_t ceil_pos)
{
    color_t *frame_buffer;
    int screen_w, screen_h;

    screen_w = scr->screen_w;
    screen_h = scr->screen_h;

    if (y_start < 0)
        y_start = 0;
    if (y_end >= screen_h)
        y_end = screen_h - 1;

    angle_t r = get_cam_rot() + ANGLE_90;
    real_t mat_row1[2] = { angle_cos(r), real_neg(angle_sin(r)) };
    real_t mat_row2[2] = { angle_sin(r), angle_cos(r) };

    int row;
    real_t t;

    frame_buffer = scr->frame_buffer;

    for (row = y_start; row <= y_end; row++)
    {
        real_t xz[2], st[2];

        /*
         * pixel coordinates on the projection plane.
         */
        real_t proj[2] = {
            /* x_proj * (2.0f * ((float)col) / ((float)(screen_w - 1)) - 1.0f) */
            pixel_proj_cols[col],
            /* 1 / (y_proj * (2.0f * ((float)row) / ((float)(screen_h - 1)) - 1.0f)) */
            pixel_proj_rows[row]
        };
        xz[1] = real_mul(real_mul(ceil_pos, z_proj), proj[1]);
        xz[0] = real_mul(real_mul(xz[1], proj[0]), z_proj_recip);

        pt2_dot(st, mat_row1, xz);
        pt2_dot(st+1, mat_row2, xz);

        pt2_add(st, st, cam_pos);

        /*
         * XXX If I ever want to scale the texture,
         * this is the place to do it.
         */

        assert(real_valid(st[0]) && real_valid(st[1]));

        st[0] = (REAL_UNIT & st[0] >> 31 - REAL_UNIT_SHIFT) +
            real_mod(st[0], REAL_UNIT);
        st[1] = (REAL_UNIT & st[1] >> 31 - REAL_UNIT_SHIFT) +
            real_mod(st[1], REAL_UNIT);

        /*
         * for ceilings, ceil_pos > 0, so there is no need to invert st[0].
         * This should be the only difference between this function and
         * draw_floor_col
         */
        int src_x = real_to_int(real_mul(st[0], real_from_int(src->w)));
        int src_y = real_to_int(real_mul(st[1], real_from_int(src->h)));
        frame_buffer[row * screen_w + col] =
            src->data[src_x * src->h + src_y];
    }
}

void draw_floor_col(struct screen *scr, struct tex const *src, int col,
                    int y_start, int y_end, real_t const cam_pos[2],
                    real_t floor_pos)
{
    color_t *frame_buffer;
    int screen_w, screen_h;

    screen_w = scr->screen_w;
    screen_h = scr->screen_h;

    if (y_start < 0)
        y_start = 0;
    if (y_end >= screen_h)
        y_end = screen_h - 1;

    angle_t r = get_cam_rot() + ANGLE_90;
    real_t mat_row1[2] = { angle_cos(r), real_neg(angle_sin(r)) };
    real_t mat_row2[2] = { angle_sin(r), angle_cos(r) };

    int row;
    real_t t;

    frame_buffer = scr->frame_buffer;

    for (row = y_start; row <= y_end; row++)
    {
        real_t xz[2], st[2];

        /*
         * pixel coordinates on the projection plane.
         */
        real_t proj[2] = {
            /* x_proj * (2.0f * ((float)col) / ((float)(screen_w - 1)) - 1.0f) */
            pixel_proj_cols[col],
            /* 1 / (y_proj * (2.0f * ((float)row) / ((float)(screen_h - 1)) - 1.0f)) */
            pixel_proj_rows[row]
        };
        xz[1] = real_mul(real_mul(floor_pos, z_proj), proj[1]);
        xz[0] = real_mul(real_mul(xz[1], proj[0]), z_proj_recip);

        pt2_dot(st, mat_row1, xz);
        pt2_dot(st+1, mat_row2, xz);

        pt2_add(st, st, cam_pos);

        /*
         * XXX If I ever want to scale the texture,
         * this is the place to do it.
         */

        assert(real_valid(st[0]) && real_valid(st[1]));

        st[0] = (REAL_UNIT & st[0] >> 31 - REAL_UNIT_SHIFT) +
            real_mod(st[0], REAL_UNIT);
        st[1] = (REAL_UNIT & st[1] >> 31 - REAL_UNIT_SHIFT) +
            real_mod(st[1], REAL_UNIT);

        /*
         * for floors, floor_pos < 0, so we need to invert st[0].
         * This should be the only difference between this function and
         * draw_ceil_col
         */
        int src_x = real_to_int(real_mul(REAL_UNIT - st[0], real_from_int(src->w)));
        int src_y = real_to_int(real_mul(st[1], real_from_int(src->h)));
        frame_buffer[row * screen_w + col] =
            src->data[src_x * src->h + src_y];
    }
}

void draw_billboard(struct screen *scr, struct tex const *tex,
                    real_t const spr_pos[2], real_t width)
{
    int x_px, res_x;
    int scr_span[2];
    real_t tc;
    real_t spr_min[2];
    real_t spr_max[2];
    real_t cam_x_axis[2], cam_z_axis[2];
    real_t spr_dist, dist, spr_rad;
    real_t inter_pt[2];
    real_t orthog_dist;
    real_t spr_ratio;
    real_t spr_height;
    real_t ray_start[2], ray_dir[2];

    res_x = scr->screen_w;

    get_cam_forward(cam_z_axis);
    pt2_perp(cam_x_axis, cam_z_axis);
    pt2_scale(cam_x_axis, cam_x_axis, -REAL_UNIT);

    spr_rad = width >> 1;
    pt2_scale(spr_min, cam_x_axis, -spr_rad);
    pt2_scale(spr_max, cam_x_axis, spr_rad);
    pt2_add(spr_min, spr_pos, spr_min);
    pt2_add(spr_max, spr_pos, spr_max);

    /* get nominal height */
    spr_ratio = real_div(real_from_int(tex->h),
                         real_from_int(tex->w));
    spr_height = real_mul(width, spr_ratio);

    for (x_px = 0; x_px < res_x; x_px++)
    {
        get_ray(game.state.player_obj->pos, ray_start, ray_dir, x_px);

        if (ray_seg_inter(&spr_dist, inter_pt, ray_start, ray_dir, spr_min,
                          spr_max) && spr_dist > REAL_ZERO)
        {
            /* get orthogonal distance */
            pt2_dot(&orthog_dist, ray_dir, cam_z_axis);
            orthog_dist = real_mul(orthog_dist, spr_dist);

            if (orthog_dist <= scr->depth_buffer[x_px])
            {
                scr->depth_buffer[x_px] = orthog_dist;

                /* get vertical span (shifted down 0.5) */
                proj_col(scr_span, -real_from_double(0.5),
                         spr_height - real_from_double(0.5),
                         orthog_dist);

                /* now get the horizontal texture coordinate */
                pt2_dist(&tc, inter_pt, spr_min);
                assert(tc <= width);
                tc = real_div(tc, width);

                /* render */
                draw_tex_col(scr, tex,
                             real_to_int(real_mul(tc, real_from_int(tex->w))),
                             x_px, scr_span[0], scr_span[1]);
            }
        }
    }
}

void blit_tex(struct screen *scr, struct tex const *tex,
              unsigned x_dst, unsigned y_dst, unsigned x_src, unsigned y_src,
              unsigned w, unsigned h)
{
    int row;
    color_t *src_buf, *dst_buf;

    if (x_src > tex->w - 1)
        x_src = tex->w - 1;
    if (y_src > tex->h - 1)
        y_src = tex->h - 1;

    if (x_src + w > tex->w - 1)
        w = tex->w - 1 - x_src;
    if (y_src + h > tex->h - 1)
        h = tex->h - 1 - y_src;

    if (x_dst > scr->screen_w - 1)
        x_dst = scr->screen_w - 1;
    if (y_dst > scr->screen_h - 1)
        y_dst = scr->screen_h - 1;

    if (x_dst + w > scr->screen_w - 1)
        w = scr->screen_w - 1 - x_dst;
    if (y_dst + h > scr->screen_h - 1)
        h = scr->screen_h - 1 - y_dst;

    src_buf = tex->data + y_src * tex->w + x_src;
    dst_buf = scr->frame_buffer + y_dst * scr->screen_w + x_dst;
    for (row = y_dst; row < y_dst + h; row++)
    {
        memcpy(dst_buf, src_buf, sizeof(color_t) * w);
        dst_buf += scr->screen_w;
        src_buf += tex->w;
    }
}

void blit_blended_tex(struct screen *scr, struct tex const *tex,
                      unsigned x_dst, unsigned y_dst,
                      unsigned x_src, unsigned y_src,
                      unsigned w, unsigned h)
{
    int row, col;
    color_t *src_buf, *dst_buf, *mask;

    if (x_src > tex->w - 1)
        x_src = tex->w - 1;
    if (y_src > tex->h - 1)
        y_src = tex->h - 1;

    if (x_src + w > tex->w - 1)
        w = tex->w - 1 - x_src;
    if (y_src + h > tex->h - 1)
        h = tex->h - 1 - y_src;

    if (x_dst > scr->screen_w - 1)
        x_dst = scr->screen_w - 1;
    if (y_dst > scr->screen_h - 1)
        y_dst = scr->screen_h - 1;

    if (x_dst + w > scr->screen_w - 1)
        w = scr->screen_w - 1 - x_dst;
    if (y_dst + h > scr->screen_h - 1)
        h = scr->screen_h - 1 - y_dst;

    src_buf = tex->data + y_src * tex->w + x_src;
    mask = tex->mask + y_src * tex->w + x_src;
    dst_buf = scr->frame_buffer + y_dst * scr->screen_w + x_dst;
    for (row = y_dst; row < y_dst + h; row++)
    {
        for (col = 0; col < w; col++)
        {
            dst_buf[col] = (src_buf[col] & mask[col]) |
                (dst_buf[col] & ~mask[col]);
        }

        dst_buf += scr->screen_w;
        src_buf += tex->w;
        mask += tex->w;
    }
}
