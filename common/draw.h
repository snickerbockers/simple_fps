/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef DRAW_H_
#define DRAW_H_

#include "screen.h"
#include "real.h"

struct tex
{
    int w, h;
    color_t *data; // this is stored in a column-major format
    color_t *mask;
};

void draw_tex_col(struct screen *scr, struct tex const *src, int src_x,
                   int dst_x, int dst_y1, int dst_y2);


/*
 * Draw one column ofthe floor/ceiling.  These could be the same function,
 * but then I'd need to put a conditional inside of a loop that gets called
 * alot, and I'd rather have code duplication than a slow renderer.
 *
 * floor_pos should be negative, and ceil_pos should be positive.
 */
void draw_floor_col(struct screen *scr, struct tex const *src, int col,
                    int y_start, int y_end, real_t const cam_pos[2],
                    real_t floor_pos);
void draw_ceil_col(struct screen *scr, struct tex const *src, int col,
                   int y_start, int y_end, real_t const cam_pos[2],
                   real_t ceil_pos);

/**
 * Draws one column from the given tex if it passes a ray-cast
 * and the depth test.  The sprite will automatically be rotated so that it
 * always faces the player.
 *
 * @param tex the sprite to draw
 * @param spr_pos the position of the sprite in world-coordinates
 * @param width the width of the sprite in world-coordinates.
 */
void draw_billboard(struct screen *scr, struct tex const *tex,
                    real_t const spr_pos[2], real_t width);

/*
 * draw the given texture onto the framebuffer.  This function requires tex to
 * be stored in row-major format.
 */
void blit_tex(struct screen *scr, struct tex const *tex,
              unsigned x_dst, unsigned y_dst, unsigned x_src, unsigned y_src,
              unsigned w, unsigned h);

void blit_blended_tex(struct screen *scr, struct tex const *tex,
                      unsigned x_dst, unsigned y_dst,
                      unsigned x_src, unsigned y_src,
                      unsigned w, unsigned h);

#endif
