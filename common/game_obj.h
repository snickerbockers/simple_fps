/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef GAME_OBJ_H_
#define GAME_OBJ_H_

#include "list.h"
#include "screen.h"
#include "real.h"

struct game;
struct game_state;
struct player_weapon;

typedef int hitpoint_t;

struct game_obj
{
    struct game_obj_class const *class;

    real_t pos[2];
    real_t dir[2]; /* direction this object is facing (forwards) */

    hitpoint_t hp;

    /*
     * pointer to additional state.  This should be used only for attributes
     * which are specific to this entity class (such as AI/behavior state,
     * currently selected weapon, etc).
     */
    void *state;

    struct list_entry list;

    /*
     * The distance from the player.
     * this is used to depth-sort game_objs so they can be drawn
     * back-to-front.  This has no meaning outside of the rendering
     * code, so don't rely on it.
     */
    real_t dist_from_player;

    /*
     * a list of all game_objs, depth-sorted based on dist_from_player.
     * Like dist_from_player, this is of interest only to a small but
     * important part of the rendering code.
     */
    struct list_entry depth_sort_list_entry;
};

typedef void(*game_obj_draw_func)(struct game_obj *obj);
typedef void(*game_obj_on_death_func)(struct game_obj *obj);

typedef int(*game_obj_on_collide_func)(struct game_obj *obj,
                                       struct game_obj *other_obj);

typedef void(*game_obj_on_use_func)(struct game_obj *obj);

typedef void(*game_obj_on_create)(struct game_obj *obj);

struct game_obj_class
{
    char const *class_name;

    game_obj_draw_func draw_col;
    game_obj_on_death_func on_death; /* called when hp <= 0 */

    /*
     * Called when two game_objs intersect.  This handler is called for both
     * objs, so it's not safe to delete a game_obj from within this handler.
     *
     * This handler can return non-zero to indicate that the object should be
     * deleted after the handler has been called for both objects.
     */
    game_obj_on_collide_func on_collide;

    /*
     * Called when the player stands close to an object, looks at it, and
     * presses the 'use' key (default binding: e).  This is used to implement
     * buttons, doors and such.
     */
    game_obj_on_use_func on_use;

    /* called to initialize obj's state */
    game_obj_on_create on_create;

    /* radius of the bounding-cylinder, used for collision detection/hitscan */
    real_t rad;

    /*
     * maximum hp for this class.  0 means it has no hp (for items that are
     * invincible)
     */
    hitpoint_t hp_max;

     /* size of structure used to hold additional state (or 0 for none). */
    size_t state_sz;

    /*
     * this obj allows other objs to pass through it
     * unhindered (but on_collide will still get called).
     */
#define FLAG_INTANGIBLE 1

    /*
     * This flag is some what of a misnomer, it means that the object will not
     * be displaced at all when reconciling collision with another object.
     * The object can still move of its own volition.
     */
#define FLAG_IMMOVABLE 2

    /* indicates that this game_obj represents a player. */
#define FLAG_PLAYER 4
    int flags;
};

struct player_state
{
    struct player_weapon const *weapon;

    /*
     * XXX: Weapon states really should be per-weapon.  In addition to being
     * messy, the current implementation has the unfortunate side effect that
     * the ammo will reset to its default upon switching guns.
     */

#define PLAYER_HEALTH_MAX 100
    int health;
};

static inline int game_obj_intangible(struct game_obj *obj)
{
    return !!(obj->class->flags & FLAG_INTANGIBLE);
}

static inline int game_obj_immovable(struct game_obj *obj)
{
    return !!(obj->class->flags & FLAG_IMMOVABLE);
}

static inline int game_obj_player(struct game_obj *obj)
{
    return !!(obj->class->flags & FLAG_PLAYER);
}

int init_game_objs(void);

/*
 * frees up all the game_objs and all of the resources associated with both
 * them and their classes.
 */
void cleanup_game_objs();

/*
 * deletes the game_obj and removes it from the game.
 * Code iterating through the game_obj_list should take
 * care to get the next pointer before calling any function
 * which might call delete_game_obj to avoid memory corruption;
 */
void delete_game_obj(struct game_obj *obj);

/*
 * find_game_obj_class does not need the game_state because obj classes are
 * hard-coded in game_obj.c
 */
struct game_obj_class const* find_game_obj_class(char const *class_name);

struct game_obj *game_obj_new(char const *class_name, real_t pos[2]);

void draw_game_obj(struct game_obj *obj);

void draw_game_objs(void);

/**
 * does a hitscan and returns the first game_obj hit (or NULL if it missed
 * them all).  This method does not take level geometry into account.
 *
 * @param dist_out This will hold the distance to the hit obj if there is a hit
 * @param source a game_obj to ignore during the hitscan (this can be NULL)
 * @param ray_start the origin of the ray
 * @param ray_dir the direction vector of the ray
 *
 * @return the first game_obj hit by the ray, or NULL if the ray hits nothing
 */
struct game_obj *game_obj_hitscan(real_t *dist_out,
                                  struct game_obj const *source,
                                  real_t const *ray_start,
                                  real_t const *ray_dir);

/* handles intersections between game_objs */
void game_obj_intersection(void);

/**
 * Call to possibly 'use' whatever object is directly in front of the player.
 *
 * @param state the game_state
 * @param player usually the player obj, but this can be any obj
 */
void game_obj_use(struct game_obj *player);

/**
 * Called to make the player attack by firing his equipped weapon.
 * @param game the game's state and sys
 */
void player_attack(void);

#define WEAPON_UNARMED "unarmed"
#define WEAPON_PISTOL "pistol"

typedef void(*weapon_attack_fn)(struct player_weapon const *weapon);
typedef void(*weapon_draw_hud_fn)(struct player_weapon const *weapon);
typedef int(*weapon_get_ammo_fn)(void);
typedef int(*weapon_get_clip_ammo_fn)(void);

struct player_weapon
{
    char const *name;
    weapon_attack_fn on_attack;
    weapon_draw_hud_fn draw_hud;
    weapon_get_ammo_fn get_ammo;
    weapon_get_clip_ammo_fn get_clip_ammo;
};

struct player_weapon const* get_weapon_by_name(char const *name);
struct player_weapon const* get_player_weapon(void);
struct player_state *get_player_state(void);
static void player_add_health(int hp);

#endif
