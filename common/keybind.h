/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef KEYBIND_H_
#define KEYBIND_H_

enum
{
    KEY_FWD = 1,
    KEY_BACK,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_UP,
    KEY_DOWN,
    KEY_FIRE,
    KEY_USE,

    N_KEYBINDS
};

struct screen;

/* will return 0 for no binding found */
int find_keybind(keycode_t keycode);

/* reset to default key bindings; to be implemented by window manager */
void config_default_bindings(struct screen *scr);

void cleanup_keybinds(void);

#endif
