/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef EVENT_H_
#define EVENT_H_

struct screen;

enum event_type
{
    EVENT_TYPE_NONE,
    EVENT_TYPE_KEY,
    EVENT_TYPE_MOUSEMOTION,
    EVENT_TYPE_QUIT
};
typedef enum event_type event_type_t;

enum key_state
{
    KEY_RELEASED = 0,
    KEY_PRESSED = 1
};
typedef enum key_state key_state_t;

struct key_event
{
    unsigned int keycode;
    key_state_t state;
    int binding;
};

struct mouse_event
{
    int x_pos, y_pos;
    int x_rel, y_rel;
};

struct event
{
    event_type_t type;
    union
    {
        struct key_event key_event;
        struct mouse_event mouse_event;
    };
};

typedef int keycode_t;

int input_poll(struct screen *scr, struct event *event);
void get_mouse_pos(struct screen *scr, int *x, int *y);
void set_mouse_pos(struct screen *scr, int x, int y);

/* associates the binding with the keycode; bind to 0 to unbind. */
void bind_key(keycode_t keycode, int binding);
void cleanup_keybinds(void);

#endif
