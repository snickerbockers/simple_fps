/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>
#include <err.h>

#include "draw.h"
#include "img.h"
#include "geo.h"
#include "bsp.h"
#include "viewport.h"
#include "event.h"
#include "game_map.h"
#include "trace.h"
#include "game_obj.h"
#include "keybind.h"
#include "game.h"
#include "sndserv.h"
#include "mixer.h"
#include "angle.h"

#define DEFAULT_SCREEN_WIDTH 800
#define DEFAULT_SCREEN_HEIGHT 600

static int res_x = DEFAULT_SCREEN_WIDTH;
static int res_y = DEFAULT_SCREEN_HEIGHT;

static int bind_states[N_KEYBINDS];

struct game game;

#define FOVY ANGLE_45

static struct option longopts[] = {
    {
        "resolution",
        1,
        NULL,
        'r'
    },
    {
    }
};

void print_usage(char const *cmd)
{
    fprintf(stderr, "Usage: %s [ -r|resolution WidthxHeight ] level\n", cmd);
}

static void render_frame(void)
{
    int x_px;
    struct ray_cast ray;
    real_t cam_z_axis[2];
    int res_x;
    struct screen *scr;
    struct bsp_tree *bsp_tree;
    struct tex *tex_array;

    scr = game.sys.scr;
    bsp_tree = &game.sys.rbsp;
    tex_array = game.sys.tex_array;

    res_x = scr->screen_w;

#ifdef CLEAR_ON_REFRESH
    clear_screen(scr, color_pack_rgb(scr, 0xff, 0xff, 0xff));
#endif

    get_cam_forward(cam_z_axis);

    for (x_px = 0; x_px < res_x; x_px++)
    {
        memset(&ray, 0, sizeof(ray));
        get_ray(game.state.player_obj->pos, ray.ray_origin, ray.ray_dir, x_px);
        trace_ray(&ray, bsp_tree);

        if ((ray.flags & RAY_HIT) && real_gt(ray.dist, REAL_ZERO))
        {
            int span[2];
            real_t dist;

            /* orthogonal distance */
            pt2_dot(&dist, cam_z_axis, ray.ray_dir);
            dist = real_mul(dist, ray.dist);
            scr->depth_buffer[x_px] = dist;

            if (real_gt(dist, REAL_ZERO))
            {
                /* I feel tempted to discard the 80-char limit here... */
                proj_col(span,
                         real_sub(ray.y_min, game.state.player_vert_pos),
                         real_sub(ray.y_max, game.state.player_vert_pos),
                         dist);
                draw_tex_col(scr, tex_array + ray.tex_idx,
                             real_to_int(
                                 real_mul(ray.tc,
                                          real_from_int(
                                              tex_array[ray.tex_idx].w))),
                             x_px,
                             span[0], span[1]);

                /*
                 * If I made the guarantee that y_min==y_max, then the ceiling and floor
                 * could share the same calculations and nearly halve the time it takes
                 * to render them both (since the texture coordinates are just mirrors
                 * of each other), but then it would be impossible to support DOOM-like
                 * pseudo-3d levels.
                 */
                draw_ceil_col(scr, tex_array + game.sys.ceil_tex_idx, x_px, 0,
                              span[0] - 1, game.state.player_obj->pos,
                              real_sub(ray.y_max, game.state.player_vert_pos));
                draw_floor_col(scr, tex_array + game.sys.floor_tex_idx, x_px,
                               span[1] + 1, res_y - 1,
                               game.state.player_obj->pos,
                               real_sub(ray.y_min, game.state.player_vert_pos));
            }
            else
            {
                scr->depth_buffer[x_px] = REAL_MAX;
            }
        }
        else
        {
            scr->depth_buffer[x_px] = REAL_MAX;
        }
    }

    draw_game_objs();

    hud_draw();
}

/* returns nonzero if the game should end */
static int handle_events(void)
{
    int ret = 0;
    int mouse_pos_x;
    struct screen *scr;
    struct game_state *state;

    scr = game.sys.scr;
    state = &game.state;

    struct event event;
    while (input_poll(scr, &event))
    {
        switch (event.type)
        {
        case EVENT_TYPE_KEY:
            if (bind_states[event.key_event.binding] != event.key_event.state)
            {
                switch (event.key_event.binding)
                {
                case KEY_FWD:
                    state->forward = event.key_event.state;
                    break;
                case KEY_LEFT:
                    state->left = event.key_event.state;
                    break;
                case KEY_BACK:
                    state->back = event.key_event.state;
                    break;
                case KEY_RIGHT:
                    state->right = event.key_event.state;
                    break;
                case KEY_UP:
                    state->up = event.key_event.state;
                    break;
                case KEY_DOWN:
                    state->down = event.key_event.state;
                    break;
                case KEY_FIRE:
                    state->fire = event.key_event.state;
                    break;
                case KEY_USE:
                    state->use = event.key_event.state;
                    break;
                }
                bind_states[event.key_event.binding] = event.key_event.state;
            }
            break;
        case EVENT_TYPE_QUIT:
            ret = 1;
            break;
        }
    }

    get_mouse_pos(scr, &mouse_pos_x, NULL);
    state->mouse_motion_x = mouse_pos_x - (res_x >> 1);

    set_mouse_pos(scr, scr->screen_w >> 1, scr->screen_h >> 1);

    return ret;
}

static void update_game(void)
{
    struct game_sys *sys = &game.sys;
    struct game_state *state = &game.state;

    if (state->mouse_motion_x)
    {
        real_t rot = get_cam_rot();
        /* TODO: there has got to be a better way... */
        rot -= angle_from_real(
            real_mul(REAL_PI<<1,
                     real_div(real_from_int(state->mouse_motion_x),
                              real_from_int(res_x))));
        game.state.player_obj->dir[0] = angle_cos(rot);
        game.state.player_obj->dir[1] = angle_sin(rot);
        set_cam_rot(rot);
    }

    int moved = 0;
    real_t cam_disp[2] = { REAL_ZERO, REAL_ZERO };
    if (state->forward)
    {
        cam_disp[0] = real_add(cam_disp[0], REAL_UNIT);
        moved = 1;
    }
    if (state->left)
    {
        moved = 1;
        cam_disp[1] = real_add(cam_disp[1], REAL_UNIT);
    }
    if (state->right)
    {
        moved = 1;
        cam_disp[1] = real_sub(cam_disp[1], REAL_UNIT);
    }
    if (state->back)
    {
        moved = 1;
        cam_disp[0] = real_sub(cam_disp[0], REAL_UNIT);
    }

    if (state->up)
        state->player_vert_pos =
            real_add(state->player_vert_pos,
                     real_mul(REAL_HALF, state->frame_time));
    if (state->down)
        state->player_vert_pos =
            real_sub(state->player_vert_pos,
                     real_mul(REAL_HALF, state->frame_time));

    if (state->fire)
    {
        real_t dir[2];
        struct game_obj *victim;
        get_cam_forward(dir);

        player_attack();
        /* no autmatic weapons here, brother */
        state->fire = 0;
    }

    if (state->use)
    {
        game_obj_use(game.state.player_obj);
        state->use = 0;
    }

    if (real_gt(real_add(real_mul(cam_disp[0], cam_disp[0]),
                         real_mul(cam_disp[1], cam_disp[1])),
                real_from_double(1.0 / (1024.0))))
    {
        angle_t cam_rot = get_cam_rot();
        real_t s = angle_sin(cam_rot);
        real_t c = angle_cos(cam_rot);
        real_t m[2] = { c, real_neg(s) };
        real_t pt_start[2], pt_end[2];
        real_t cam_disp_trans[2];

        pt2_dot(cam_disp_trans, cam_disp, m);
        m[0] = s;
        m[1] = c;
        pt2_dot(cam_disp_trans+1, cam_disp, m);

        pt2_norm(cam_disp_trans, cam_disp_trans);
        pt2_scale(cam_disp_trans, cam_disp_trans, state->frame_time << 2);

        pt_start[0] = state->player_obj->pos[0];
        pt_start[1] = state->player_obj->pos[1];
        pt2_add(pt_end, state->player_obj->pos, cam_disp_trans);
        move_pt(state->player_obj->pos, pt_start, pt_end, real_from_double(0.1),
                &sys->cbsp);
    }

    game_obj_intersection();
}

static void new_frame(void)
{
    struct game_state *state = &game.state;

    state->last_frame_ticks = state->this_frame_ticks;
    state->this_frame_ticks = get_ticks();
    state->frame_time = real_mul(
        /* TODO: should there also be a real_from_unsigned ? */
        real_from_int(state->this_frame_ticks - state->last_frame_ticks),
        real_from_double(0.001));
}

static void main_loop(void)
{
    int is_running;
    struct game_sys *sys;
    struct game_state *state;

    state = &game.state;
    sys = &game.sys;

    do
    {
        new_frame();

        is_running = !handle_events();

        update_game();

        render_frame();

#ifdef SHOW_FPS
        hud_print_framerate();
#endif
        hud_draw_player_health();

        flip_screen(sys->scr);
    } while (is_running && !game.state.end_level);
}

static void game_init(int argc, char **argv)
{
    struct game_map game_map;
    stream_t *level_file;
    int idx;
    char const *cmd = argv[0];
    char const *level_path = NULL;
    char optchar;
    char const *cmd_str;
    char const *res_str;
    int res;
    struct game_state *state;
    struct game_sys *sys;

    state = &game.state;
    sys = &game.sys;

    cmd_str = argv[0];
    while ((optchar = getopt_long(argc, argv, "r:", longopts, NULL)) > 0)
    {
        switch (optchar)
        {
        case 'r':
            /* parse resolution string */
            res_str = optarg;
            if ((res = sscanf(res_str, "%dx%d", &res_x, &res_y))
                != 2)
            {
                print_usage(cmd_str);
                return;
            }
            break;
        default:
        case '?':
            print_usage(cmd_str);
            return;
        }
    }

    argc -= optind;
    argv += optind;

    if (argc != 1)
    {
        print_usage(cmd_str);
        exit(-1);
    }
    level_path = argv[0];

    game_map_init(&game_map);

    sys->scr = create_screen(res_x, res_y);
    if (!sys->scr)
        err(-1, "unable to create render context\n");

    if (sndserv_init(&sys->sndserv) != 0)
        err(-1, "unable to initialize sound server\n");

    if (mixer_init(&sys->mixer, &sys->sndserv) != 0)
        err(-1, "unable to initialize audio mixer\n");

    if (!(level_file = stream_stdio_open(level_path, "rb")))
        err(-1, "unable to open %s\n", level_path);
    if (load_map(&game_map, level_file) < 0)
    {
        fprintf(stderr, "Unable to load %s\n", level_path);
        exit(-1);
    }

    if (copy_bsp(&sys->rbsp, &game_map.rbsp) < 0)
    {
        TRACE_ERR("Failed to copy bsp from game_map\n");
        exit(-1);
    }

    if (copy_bsp(&sys->cbsp, &game_map.cbsp) < 0)
    {
        TRACE_ERR("Failed to copy bsp from game_map\n");
        exit(-1);
    }

    init_game_objs();
    for (idx = 0; idx < game_map.obj_list.n_objs; idx++)
    {
        game_obj_new(game_map.obj_list.game_objs[idx].class,
                     game_map.obj_list.game_objs[idx].pos);
    }

    state->player_obj = game_obj_new("player", game_map.player_spawn);
    state->player_vert_pos = REAL_ZERO;

    sys->floor_tex_idx = game_map.tex.floor_tex_idx;
    sys->ceil_tex_idx = game_map.tex.ceil_tex_idx;

    sys->n_tex = game_map.tex.n_tex;
    sys->tex_array = (struct tex*)malloc(sizeof(struct tex) * sys->n_tex);
    if (!sys->tex_array)
    {
        TRACE_ERR("failed allocation\n");
        exit(1);
    }

    for (idx = 0; idx < sys->n_tex; idx++)
    {
        unsigned total_path_len = strlen(game_map.tex.tex_paths[idx]) +
            strlen(DATA_DIR"/") + 1;
        char *total_path = malloc(sizeof(char) * total_path_len);
        if (!total_path)
        {
            TRACE_ERR("failed allocation\n");
            exit(1);
        }

        total_path[total_path_len - 1] = '\0';
        strcpy(total_path, DATA_DIR"/");
        strcat(total_path, game_map.tex.tex_paths[idx]);

        TRACE_INFO("Loading %s...\n", total_path);
        if (tex_load(sys->scr, sys->tex_array + idx, total_path, 1) != 0)
        {
            TRACE_ERR("Unable to load %s\n", total_path);
            exit(1);
        }
        free(total_path);
    }

    if (font_load(sys->scr, &sys->overlay_font, DATA_DIR"/digits.png", 16, 32) < 0)
    {
        TRACE_ERR("Error loading font\n");
        exit(1);
    }

    game_map_cleanup(&game_map);

    init_viewport(FOVY, res_x, res_y);

    state->last_frame_ticks = get_ticks();
    state->first_frame_ticks = state->last_frame_ticks;

    set_mouse_pos(sys->scr, res_x >> 1, res_y >> 1);

    config_default_bindings(sys->scr);

    if (hud_init() < 0)
        errx(1, "Unable to initialize hud\n");

    if (player_weapons_init() < 0)
        errx(1, "Unable to initialize player weapons\n");
}

int main(int argc, char **argv)
{
    int idx;

    game_init(argc, argv);

    main_loop();

    player_weapons_cleanup();
    hud_cleanup();

    cleanup_game_objs();

    for (idx = 0; idx < game.sys.n_tex; idx++)
        tex_cleanup(game.sys.tex_array + idx);

    mixer_cleanup(&game.sys.mixer);
    sndserv_cleanup(&game.sys.sndserv);
    destroy_screen(game.sys.scr);
    cleanup_bsp_tree(&game.sys.cbsp);
    cleanup_bsp_tree(&game.sys.rbsp);

    return 0;
}
