/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdio.h>
#include <string.h>

#include "trace.h"
#include "deflate.h"

#include "bitstream.h"

/*
 * val must never be less than zero because we use that to indicate failure in
 * huff_tbl_search.  DEFLATE doesn't use any value less than 0, anyways.
 */
struct huff_symbol
{
    int ignore;
    int val;
    unsigned key;
    unsigned len;
};

/* preset dictionary flag */
#define FLAG_FDICT 0x20

static uint32_t adler32(uint8_t *dat, size_t len);
static int do_inflate(uint8_t **out, size_t *len_out, struct bit_stream *bits);
static void get_default_code_val(unsigned code_array[288]);

static struct huff_symbol const*
huff_tbl_search(struct huff_symbol const *tbl, unsigned tbl_len,
                unsigned key, unsigned key_len);

static void get_default_code_len_and_val(unsigned len_array[288],
                                         unsigned code_array[288]);

static void get_default_dist_code_len_and_val(unsigned len_array[32],
                                              unsigned code_array[32]);

static int huff_tbl_from_len(struct huff_symbol *tbl,
                             unsigned const *code_lengths,
                             unsigned const *vals, unsigned n_symbols);

static struct huff_symbol const *
bit_stream_read_huff(struct bit_stream *bits,
                     struct huff_symbol const *huff_tbl,
                     unsigned tbl_len);

static int do_inflate_uncompressed_block(uint8_t **buf_out, size_t *buf_len_out,
                                         struct bit_stream *bits);

static int
read_dynamic_huff_tables(struct huff_symbol **lit_len_tbl_out,
                         struct huff_symbol **dist_tbl_out,
                         unsigned *lit_len_tbl_len_out,
                         unsigned *dist_tbl_len_out,
                         struct bit_stream *bits);

static int
default_tables(struct huff_symbol **lit_len_tbl_out,
               struct huff_symbol **dist_tbl_out,
               unsigned *lit_len_tbl_len_out,
               unsigned *dist_tbl_len_out);

static int do_inflate_compressed_block(uint8_t **bufp, size_t *buf_lenp,
                                       struct huff_symbol const *lit_len_tbl,
                                       size_t lit_len_tbl_len,
                                       struct huff_symbol const *dist_tbl,
                                       size_t dist_tbl_len,
                                       struct bit_stream *bits);

static int do_inflate(uint8_t **out, size_t *len_out, struct bit_stream *bits);


/* code == index + 257 */
static struct code_tbl_entry
{
    int extra_bits;
    int length;
} code_tbl[31] = {
    { 0, 3 },
    { 0, 4 },
    { 0, 5 },
    { 0, 6 },
    { 0, 7 },
    { 0, 8 },
    { 0, 9 },
    { 0, 10 },
    { 1, 11 },
    { 1, 13 },
    { 1, 15 },
    { 1, 17 },
    { 2, 19 },
    { 2, 23 },
    { 2, 27 },
    { 2, 31 },
    { 3, 35 },
    { 3, 43 },
    { 3, 51 },
    { 3, 59 },
    { 4, 67 },
    { 4, 83 },
    { 4, 99 },
    { 4, 115 },
    { 5, 131 },
    { 5, 163 },
    { 5, 195 },
    { 5, 227 },
    { 0, 258 },

    /* 286 and 287 are illegal */
    { 0, 0 },
    { 0, 0 }
};

static struct dist_code_tbl_entry
{
    int extra_bits;
    int dist;
} dist_code_tbl[] = {
    { 0, 1 },
    { 0, 2 },
    { 0, 3 },
    { 0, 4 },
    { 1, 5 },
    { 1, 7 },
    { 2, 9 },
    { 2, 13 },
    { 3, 17 },
    { 3, 25 },
    { 4, 33 },
    { 4, 49 },
    { 5, 65 },
    { 5, 97 },
    { 6, 129 },
    { 6, 193 },
    { 7, 257 },
    { 7, 385 },
    { 8, 513 },
    { 8, 769 },
    { 9, 1025 },
    { 9, 1537 },
    { 10, 2049 },
    { 10, 3073 },
    { 11, 4097 },
    { 11, 6145 },
    { 12, 8193 },
    { 12, 12289 },
    { 13, 16385 },
    { 13, 24577 },

    /* 30-31 are illegal */
    { 0, 0 },
    { 0, 0 }
};

static uint32_t adler32(uint8_t *dat, size_t len)
{
    size_t i;
    uint32_t a, b;

    a = 1;
    b = 0;

    for (i = 0; i < len; i++)
    {
        a = (a + dat[i]) % 65521;
        b = (b + a) % 65521;
    }

    return (b << 16) | a;
}

static struct huff_symbol const*
huff_tbl_search(struct huff_symbol const *tbl, unsigned tbl_len,
                unsigned key, unsigned key_len)
{
    unsigned idx;

    for (idx = 0; idx < tbl_len; idx++)
    {
        if (tbl[idx].ignore)
            continue;
        if (tbl[idx].key == key && tbl[idx].len == key_len)
            return tbl + idx;
    }
    return NULL;
}

static void get_default_code_val(unsigned code_array[288])
{
    unsigned i;

    for (i = 0; i < 288; i++)
        code_array[i] = i;
}

static void get_default_code_len_and_val(unsigned len_array[288], unsigned code_array[288])
{
    unsigned i;

    for (i = 0; i <= 143; i++)
        len_array[i] = 8;
    for (; i <= 255; i++)
        len_array[i] = 9;
    for (; i <= 279; i++)
        len_array[i] = 7;
    for (; i <= 287; i++)
        len_array[i] = 8;

    get_default_code_val(code_array);
}

static void get_default_dist_code_len_and_val(unsigned len_array[32],
                                              unsigned code_array[32])
{
    unsigned i;

    for (i = 0; i < 32; i++)
    {
        len_array[i] = 5;
        code_array[i] = i;
    }
}

/* all arrays should have a length of n_symbols */
static int huff_tbl_from_len(struct huff_symbol *tbl,
                             unsigned const *code_lengths,
                             unsigned const *vals, unsigned n_symbols)
{
    int err = 0;
    unsigned idx, len;
    unsigned max_len = 0;
    unsigned *bl_count = NULL, *next_code = NULL;

    /* find maximum code length */
    for (idx = 0; idx < n_symbols; idx++)
        if (code_lengths[idx] > max_len)
            max_len = code_lengths[idx];

    if (max_len == 0)
        return -1;

    if (!(bl_count = (unsigned*)calloc(max_len+1, sizeof(unsigned))))
    {
        err = -1;
        goto failed_allocation;
    }

    if (!(next_code = (unsigned*)calloc(max_len+1, sizeof(unsigned))))
    {
        err = -1;
        goto failed_allocation;
    }

    for (idx = 0; idx < n_symbols; idx++)
        bl_count[code_lengths[idx]]++;

    /* with dynamic huff codes, 0 means unused */
    bl_count[0] = 0;

    for (len = 1; len <= max_len; len++)
    {
        next_code[len] = (next_code[len-1] + bl_count[len-1]) << 1;
    }

    for (idx = 0; idx < n_symbols; idx++)
    {
        if (!code_lengths[idx])
        {
            tbl[idx].ignore = 1;
            continue;
        }

        tbl[idx].ignore = 0;
        tbl[idx].key = next_code[code_lengths[idx]]++;
        tbl[idx].val = vals[idx];
        tbl[idx].len = code_lengths[idx];
    }

on_cleanup:
failed_allocation:
    if (bl_count)
        free(bl_count);
    if (next_code)
        free(next_code);
    return err;
}

static struct huff_symbol const *
bit_stream_read_huff(struct bit_stream *bits,
                     struct huff_symbol const *huff_tbl,
                     unsigned tbl_len)
{
    int key = 0;
    unsigned key_len = 0;
    struct huff_symbol const *ret;
    int tmp;

    do
    {
        /* TODO: figure out if this can actually happen */
        if (key >= (1 << 20))
        {
            fprintf(stderr, "CRITICAL: unlimited loop at line "
                    "%d of %s\n", __LINE__, __FILE__);
            return NULL;
        }

        if ((tmp = bit_stream_get(bits)) < 0)
            return NULL;
        key = (key << 1) | tmp;
        ret = huff_tbl_search(huff_tbl, tbl_len, key, ++key_len);
    } while (!ret);

    return ret;
}

/* LZ77/compression type 0 */
static int do_inflate_uncompressed_block(uint8_t **buf_out, size_t *buf_len_out,
                                         struct bit_stream *bits)
{
    int tmp;
    unsigned short len, nlen, idx;
    size_t buf_len;
    uint8_t *buf, *buf_tmp;

    buf_len = *buf_len_out;
    buf = *buf_out;

    /* read len */
    if (bit_stream_get_byte(bits, &tmp) < 0)
        goto out_of_data;
    len = tmp;
    if (bit_stream_get_byte(bits, &tmp) < 0)
        goto out_of_data;
    len = (tmp << 8) | len;
    if (bit_stream_get_byte(bits, &tmp) < 0)
        goto out_of_data;
    nlen = tmp;
    if (bit_stream_get_byte(bits, &tmp) < 0)
        goto out_of_data;
    nlen = (tmp << 8) | nlen;

    if (len != (unsigned short)~nlen)
        goto corrupted_stream;

    for (idx = 0; idx < len; idx++)
    {
        /* realloc one byte at a time because IDGAF */
        buf_tmp = (uint8_t*)realloc(buf, ++buf_len);
        if (!buf_tmp)
        {
            free(buf);
            buf = NULL;
            goto failed_allocation;
        }
        buf = buf_tmp;
        if (bit_stream_get_byte(bits, &tmp) < 0)
            goto out_of_data;
        buf[buf_len - 1] = tmp;
    }

    *buf_len_out = buf_len;
    *buf_out = buf;
    return 0;

out_of_data:
corrupted_stream:
failed_allocation:
    *buf_len_out = buf_len;
    return -1;
}

static int
read_dynamic_huff_tables(struct huff_symbol **lit_len_tbl_out,
                         struct huff_symbol **dist_tbl_out,
                         unsigned *lit_len_tbl_len_out,
                         unsigned *dist_tbl_len_out,
                         struct bit_stream *bits)
{
#define N_LIT_LEN_CODES 288
#define N_DIST_CODES 32
    int i;
    unsigned n_codes;
    unsigned hlit, hdist, hclen;
    unsigned *lit_len_code_lengths, *dist_code_lengths;
    unsigned code_length_lengths[19];
    struct huff_symbol *code_len_tbl, *dist_tbl, *lit_len_tbl;
    unsigned n_code_len;
    unsigned *code_array;

    /* used for loading the code lengths for the dynamic huffman tree */
    unsigned code_len_alph_remap[19] = {
        16, 17, 18, 0,
        8,  7,  9, 6,
        10,  5, 11, 4,
        12,  3, 13, 2,
        14,  1, 15
    };

    /* dynamic huffman codes */
    hlit = hdist = hclen = 0;

    if (bit_stream_shift_out(bits, &hlit, 5) < 0)
        goto out_of_data;
    if (bit_stream_shift_out(bits, &hdist, 5) < 0)
        goto out_of_data;
    if (bit_stream_shift_out(bits, &hclen, 4) < 0)
        goto out_of_data;

    hclen += 4;
    hlit += 257;
    hdist += 1;

    memset(code_length_lengths, 0, sizeof(unsigned) * 19);

    for (i = 0; i < hclen; i++)
    {
        unsigned len = 0;
        if (bit_stream_shift_out(bits, &len, 3) < 0)
            goto out_of_data;
        /* code_length_lengths[code_len_alph_remap[i]] = len; */
        code_length_lengths[i] = len;
    }

    n_codes = 19;
    code_len_tbl = (struct huff_symbol*)calloc(n_codes,
                                               sizeof(struct huff_symbol));
    if (!code_len_tbl)
        goto failed_allocation;

    /* sort the code length lengths based on code_len_alph_remap */
    unsigned pos;
    for (pos = 0; pos < n_codes; pos++)
    {
        unsigned idx;
        for (idx = pos + 1; idx < n_codes; idx++)
        {
            if (code_len_alph_remap[idx] < code_len_alph_remap[pos])
            {
                unsigned tmp = code_length_lengths[pos];
                code_length_lengths[pos] = code_length_lengths[idx];
                code_length_lengths[idx] = tmp;

                tmp = code_len_alph_remap[pos];
                code_len_alph_remap[pos] = code_len_alph_remap[idx];
                code_len_alph_remap[idx] = tmp;
            }
        }
    }

    if (huff_tbl_from_len(code_len_tbl, code_length_lengths,
                          code_len_alph_remap, n_codes) < 0)
        goto failed_allocation;

    lit_len_code_lengths =
        (unsigned*)calloc(N_LIT_LEN_CODES, sizeof(unsigned));
    if (!lit_len_code_lengths)
        goto failed_allocation;

    dist_code_lengths =
        (unsigned*)calloc(N_DIST_CODES, sizeof(unsigned));
    if (!dist_code_lengths)
        goto failed_allocation;

    /*  read in code lengths */
    n_code_len = 0;
    while (n_code_len < hlit)
    {
        unsigned key = 0;
        int val;
        unsigned extra_bits, base_rep, extra_rep;
        unsigned key_len = 0;
        struct huff_symbol const *res;

        res = bit_stream_read_huff(bits, code_len_tbl, n_codes);
        if (!res)
            goto corrupted_stream;

        val = res->val;
        if (val <= 15)
        {
            lit_len_code_lengths[n_code_len++] = val;
            continue;
        }
        else if (val == 16)
        {
            if (n_code_len == 0)
                goto corrupted_stream;
            val = lit_len_code_lengths[n_code_len-1];
            base_rep = 3;
            extra_bits = 2;
        }
        else if (val == 17)
        {
            base_rep = 3;
            extra_bits = 3;
            val = 0;
        }
        else if (val == 18)
        {
            base_rep = 11;
            extra_bits = 7;
            val = 0;
        }
        else
        {
            fprintf(stderr, "This should be impossible "
                    "(see line %d of %s)\n", __LINE__, __FILE__);
            abort();
        }

        extra_rep = 0;
        if (bit_stream_shift_out(bits, &extra_rep, extra_bits) < 0)
            goto corrupted_stream;
        for (i = 0; i < (base_rep + extra_rep); i++)
        {
            if (n_code_len >= hlit)
                goto corrupted_stream;
            lit_len_code_lengths[n_code_len++] = val;
        }
    }

    /*  read in dist code lengths */
    n_code_len = 0;
    while (n_code_len < hdist)
    {
        unsigned key_len = 0;
        unsigned key = 0;
        int val;
        unsigned extra_bits, base_rep, extra_rep;
        struct huff_symbol const *res;

        res = bit_stream_read_huff(bits, code_len_tbl, n_codes);
        if (!res)
            goto corrupted_stream;

        val = res->val;

        if (val <= 15)
        {
            dist_code_lengths[n_code_len++] = val;
            continue;
        }
        else if (val == 16)
        {
            if (n_code_len == 0)
                goto corrupted_stream;
            val = dist_code_lengths[n_code_len-1];
            base_rep = 3;
            extra_bits = 2;
        }
        else if (val == 17)
        {
            base_rep = 3;
            extra_bits = 3;
            val = 0;
        }
        else if (val == 18)
        {
            base_rep = 11;
            extra_bits = 7;
            val = 0;
        }
        else
        {
            fprintf(stderr, "This should be impossible "
                    "(see line %d of %s)\n", __LINE__, __FILE__);
            abort();
        }

        extra_rep = 0;
        if (bit_stream_shift_out(bits, &extra_rep, extra_bits) < 0)
            goto corrupted_stream;
        for (i = 0; i < (base_rep + extra_rep); i++)
        {
            if (n_code_len >= hdist)
                goto corrupted_stream;
            dist_code_lengths[n_code_len++] = val;
        }
    }

    /* generate huff tables */
    n_codes = N_LIT_LEN_CODES;
    code_array = (unsigned*)calloc(n_codes, sizeof(unsigned));
    if (!code_array)
        goto failed_allocation;

    lit_len_tbl = (struct huff_symbol*)
        calloc(n_codes, sizeof(struct huff_symbol));
    if (!lit_len_tbl)
        goto failed_allocation;
    dist_tbl = (struct huff_symbol*)
        calloc(32, sizeof(struct huff_symbol));

    get_default_code_val(code_array);
    if (huff_tbl_from_len(lit_len_tbl, lit_len_code_lengths,
                          code_array, n_codes) < 0)
        goto failed_allocation;
    if (huff_tbl_from_len(dist_tbl, dist_code_lengths,
                          code_array, N_DIST_CODES) < 0)
        goto failed_allocation;

    *lit_len_tbl_out = lit_len_tbl;
    *lit_len_tbl_len_out = n_codes;
    *dist_tbl_out = dist_tbl;
    *dist_tbl_len_out = 32;

    return 0;

failed_allocation:
corrupted_stream:
out_of_data:
    return -1;
}

static int
default_tables(struct huff_symbol **lit_len_tbl_out,
               struct huff_symbol **dist_tbl_out,
               unsigned *lit_len_tbl_len_out,
               unsigned *dist_tbl_len_out)
{
#define N_DEFAULT_DIST_CODES 32
#define DEFAULT_DIST_TBL_LEN N_DEFAULT_DIST_CODES
#define DEFAULT_LIT_LEN_TBL_LEN 288
    struct huff_symbol *lit_len_tbl = NULL, *dist_tbl = NULL;
    unsigned *len_array = NULL, *code_array = NULL;

    len_array = (unsigned*)calloc(DEFAULT_LIT_LEN_TBL_LEN, sizeof(unsigned));
    if (!len_array)
        goto failed_allocation;

    code_array = (unsigned*)calloc(DEFAULT_LIT_LEN_TBL_LEN, sizeof(unsigned));
    if (!code_array)
        goto failed_allocation;

    lit_len_tbl = (struct huff_symbol*)calloc(DEFAULT_LIT_LEN_TBL_LEN, sizeof(
                                                  struct huff_symbol));
    if (!lit_len_tbl)
        goto failed_allocation;

    dist_tbl = (struct huff_symbol*)
        calloc(DEFAULT_DIST_TBL_LEN, sizeof(struct huff_symbol));
    if (!dist_tbl)
        goto failed_allocation;

    /* TODO: handle failed allocations, also do memory cleanup */

    get_default_code_len_and_val(len_array, code_array);
    if (huff_tbl_from_len(lit_len_tbl, len_array, code_array, DEFAULT_LIT_LEN_TBL_LEN) < 0)
        goto failed_allocation;

    unsigned dist_len_array[N_DEFAULT_DIST_CODES],
        dist_code_array[N_DEFAULT_DIST_CODES];
    get_default_dist_code_len_and_val(dist_len_array, dist_code_array);
    if (huff_tbl_from_len(dist_tbl, dist_len_array,
                          dist_code_array, N_DEFAULT_DIST_CODES) < 0)
        goto failed_allocation;

    free(code_array);
    free(len_array);
    code_array = NULL;
    len_array = NULL;

    *lit_len_tbl_len_out = DEFAULT_LIT_LEN_TBL_LEN;
    *dist_tbl_len_out = DEFAULT_DIST_TBL_LEN;
    *lit_len_tbl_out = lit_len_tbl;
    *dist_tbl_out = dist_tbl;

    return 0;

failed_allocation:
    if (code_array)
        free(code_array);
    if (len_array)
        free(len_array);
    if (lit_len_tbl)
        free(lit_len_tbl);
    if (dist_tbl)
        free(dist_tbl);
    return -1;
}

static int do_inflate_compressed_block(uint8_t **bufp, size_t *buf_lenp,
                                       struct huff_symbol const *lit_len_tbl,
                                       size_t lit_len_tbl_len,
                                       struct huff_symbol const *dist_tbl,
                                       size_t dist_tbl_len,
                                       struct bit_stream *bits)
{
    uint8_t *buf_start = NULL, *buf_start_tmp;
    size_t buf_len;
    unsigned i;
    int tmp;
    int val;

    buf_start = *bufp;
    buf_len = *buf_lenp;

    do
    {
        int key = 0;
        unsigned key_len = 0;
        struct huff_symbol const *res;

        res = bit_stream_read_huff(bits, lit_len_tbl, lit_len_tbl_len);
        if (!res)
            goto corrupted_stream;

        val = res->val;
        key = res->key;
        key_len = res->len;

        if (val < 256)
        {
            /* copy literal to output stream */
            buf_start_tmp = buf_start;
            buf_start = (uint8_t*)realloc(buf_start, ++buf_len);
            if (!buf_start)
            {
                free(buf_start_tmp);
                goto failed_allocation;
            }
            buf_start[buf_len - 1] = val;
        }
        else if (val > 256)
        {
            /* decode literal/length */
            if (val >= 286)
            {
                /*
                 * 286 and 287 are possible but illegal;
                 * values >= 288 should be impossible
                 */
                goto corrupted_stream;
            }

            int len = code_tbl[val - 257].length;
            int extra_bits = code_tbl[val - 257].extra_bits;
            int extra_len = 0;

            if (bit_stream_shift_out(bits, &extra_len, extra_bits) < 0)
                goto corrupted_stream;
            len += extra_len;

            int dist;
            int dist_code = 0;
            int dist_extra_bits;
            int dist_extra = 0;

            /* if (bit_stream_shift_out(bits, &dist_code, 5) < 0) */
            /*     goto corrupted_stream; */
            if (!(res = bit_stream_read_huff(bits, dist_tbl, dist_tbl_len)))
                goto corrupted_stream;

            dist_code = res->val;

            if (dist_code >= 30)
                goto corrupted_stream;

            dist_extra_bits = dist_code_tbl[dist_code].extra_bits;
            dist = dist_code_tbl[dist_code].dist;

            if (bit_stream_shift_out(bits, &dist_extra, dist_extra_bits) < 0)
                goto corrupted_stream;
            dist += dist_extra;

            /* TODO: double-check for off-by-one mistakes */
            if (dist >= buf_len)
                goto corrupted_stream;

            unsigned src_start_idx = buf_len - dist;
            unsigned dst_start_idx = buf_len;

            buf_start_tmp = buf_start;
            buf_start = (uint8_t*)realloc(buf_start, buf_len += len);
            if (!buf_start)
            {
                free(buf_start_tmp);
                goto failed_allocation;
            }

            for (i = 0; i < len; i++)
                buf_start[dst_start_idx + i] = buf_start[src_start_idx + i];
        }
    } while (val != 256);

    *bufp = buf_start;
    *buf_lenp = buf_len;

    return 0;

failed_allocation:
corrupted_stream:
    if (buf_start)
        free(buf_start);
    return -1;
}

static int do_inflate(uint8_t **out, size_t *len_out, struct bit_stream *bits)
{
    int i;
    int last;
    int comp_tp;
    int tmp;
    uint8_t *buf = NULL;
    size_t buf_len = 0;
    unsigned short idx;
    uint32_t csum_file, csum_dat;
    int val;

    do
    {
        /* read block header: BFINAL and BTYPE */
        last = bit_stream_get(bits);
        if (last < 0)
            goto out_of_data;
        comp_tp = 0;
        if (bit_stream_shift_out(bits, &comp_tp, 2) < 0)
            goto out_of_data;

        if (comp_tp == 0)
        {
            if (do_inflate_uncompressed_block(&buf, &buf_len, bits) < 0)
                goto corrupted_stream;
        }
        else
        {
            struct huff_symbol *lit_len_tbl, *dist_tbl;
            unsigned lit_len_tbl_len, dist_tbl_len;
            
            if (comp_tp == 3)
                goto corrupted_stream; /* invalid compression type */
            if (comp_tp == 2)
            {
                if (read_dynamic_huff_tables(&lit_len_tbl, &dist_tbl,
                                             &lit_len_tbl_len,
                                             &dist_tbl_len, bits) < 0)
                    goto corrupted_stream;
            }
            else
            {
                if (default_tables(&lit_len_tbl, &dist_tbl, &lit_len_tbl_len, &dist_tbl_len) < 0)
                    goto corrupted_stream;
            }

            if (do_inflate_compressed_block(&buf, &buf_len, lit_len_tbl,
                                            lit_len_tbl_len, dist_tbl,
                                            dist_tbl_len, bits) < 0)
                goto corrupted_stream;
            
        }
    } while (!last);

    /* do the checksum */
    if (bit_stream_get_byte(bits, &tmp) < 0)
        goto out_of_data;
    csum_file = tmp;
    if (bit_stream_get_byte(bits, &tmp) < 0)
        goto out_of_data;
    csum_file |= tmp << 8;
    if (bit_stream_get_byte(bits, &tmp) < 0)
        goto out_of_data;
    csum_file |= tmp << 16;
    if (bit_stream_get_byte(bits, &tmp) < 0)
        goto out_of_data;
    csum_file |= tmp << 24;
    csum_file = ntohl(csum_file);

    csum_dat = adler32(buf, buf_len);

    if (csum_dat != csum_file)
    {
        fprintf(stderr, "failed checksum: expected %08x, got %08x\n",
                csum_file, csum_dat);
        goto corrupted_stream;
    }

    *out = buf;
    *len_out = buf_len;

    return 0;

corrupted_stream:
    fprintf(stderr, "Bit stream corrupted\n");
    return -1;

out_of_data:
    fprintf(stderr, "Bit stream terminated prematurely\n");
    return -1;

failed_allocation:
    fprintf(stderr, "Failed allocation\n");
    return -1;
}

int inflate(uint8_t **out, size_t *len_out,
            void const *stream, size_t len)
{
    struct bit_stream bits;
    unsigned short win_len;
    uint8_t flags;
    uint8_t const *dat = (uint8_t*)stream;

    if (len <= 2)
    {
        fprintf(stderr, "What is this, a data stream for ANTS!?\n");
        return -1;
    }

    if ((dat[0] & 0x0f) != 8)
    {
        fprintf(stderr, "corrupted data stream, (non-DEFLATE)\n");
        return -1;
    }

    win_len = 1 << ((dat[0] >> 4) + 8);

    flags = dat[1];

    /* PNG spec says preset dictionaries are a no-go.  If I ever use DEFLATE
     * for anything else, I may want to consider giving callers the option to
     * supply a list of acceptable presets.
     */
    if (flags & FLAG_FDICT)
    {
        TRACE_ERR("Corrupted data stream (unrecognized preset dictionary)\n");
        return -1;
    }

    /* make sure the cmf and flags are a multiple of 31 */
    if ((256 * dat[0] + dat[1]) % 31)
    {
        TRACE_ERR("Corrupted data stream (invalid compression method/flags)\n");
        return -1;
    }

    bit_stream_init(&bits, dat + 2, len - 2);
    return do_inflate(out, len_out, &bits);
}
