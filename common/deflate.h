/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/
#ifndef DEFLATE_H_
#define DEFLATE_H_

#include <stdlib.h>
#include <stdint.h>

/*
 * Implementation of the LZ77/DEFLATE algorithm (decompression/inflation only).
 *
 * See https://www.w3.org/Graphics/PNG/RFC-1951 for details.
 *
 * I could have save a lot of time and effor by using zlib, but like so many
 * other aspects of simple_fps, I implemented it myself from scratch because I
 * wanted to.
 */

/**
 * Decompress a stream of data encoded with the DEFLATE algorithm
 * @param out where to place a pointer to the decompressed stream.
 *        Users will need to free *out
 * @param len_out where to write the length of *out
 * @param stream the input stream to decompress
 * @param len the length of @stream
 * @return 0 for success, <0 if there was an error
 */
int inflate(uint8_t **out, size_t *len_out,
            void const *stream, size_t len);


#endif
