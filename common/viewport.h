/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef VIEWPORT_H_
#define VIEWPORT_H_

#include "real.h"
#include "angle.h"

/* left, right, bottom, top, depth */
real_t viewport_get_near(real_t vp[5]);
real_t viewport_get_far(real_t vp[5]);

/* given an x-position, calculate the ray to cast */
void get_ray(real_t const cam_pos[2], real_t ray_origin[2], real_t ray_dir[2],
             unsigned x_pix);
void proj_col(int span[2], real_t y_min, real_t y_max, real_t z);
real_t scale_height(real_t height_nom, real_t z);

void set_cam_pos(const real_t pos[2]);
void get_cam_pos(real_t pos[2]);
angle_t get_cam_rot(void);
void set_cam_rot(angle_t rot);
void get_cam_forward(real_t forward[2]);

void init_viewport(angle_t fovy, int screen_width, int screen_height);
void config_viewport(angle_t fovy, int screen_width, int screen_height);

/*
 * viewport dimensions, these should be treated
 * as read-only outside of viewport.c
 *
 * height of the project plane is not included because it is always 1.
 * This is to reduce complexity of calculations.
 */
extern real_t aspect_ratio, x_proj, z_proj, z_proj_recip;
extern angle_t half_fovy;

/*
 * cached calculations used by draw_plane_col to project
 * the floor and ceiling onto the projction plane.
 * These map pixel coordinates to the project plane.
 * pixel_proj_rows is actually stored as the reciprocal
 * to eliminate a divison.
 * See the comment in draw_plane_col in draw.c
 */
extern real_t *pixel_proj_rows, *pixel_proj_cols;

#endif
