/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <math.h>
#include <assert.h>

#include "geo.h"

#include "draw.h"
#include "viewport.h"
#include "screen.h"

/* vertical field-of-view */
angle_t half_fovy;
real_t aspect_ratio;
real_t x_proj, z_proj, z_proj_recip;

real_t *pixel_proj_rows, *pixel_proj_cols;

static real_t cam_rot;

static int screen_w, screen_h;

static real_t *pix_vecs;

static void get_pix_vec(real_t *out, unsigned x_pix);

void config_viewport(angle_t fovy, int screen_width, int screen_height)
{
    int row, col;

    screen_w = screen_width;
    screen_h = screen_height;
    half_fovy = fovy >> 1;
    aspect_ratio = real_div(real_from_int(screen_w), real_from_int(screen_h));
    z_proj_recip = angle_tan(half_fovy);
    z_proj = real_div(REAL_UNIT, z_proj_recip);
    x_proj = aspect_ratio;

    pixel_proj_rows = (real_t*)malloc(sizeof(real_t) * screen_height);
    pixel_proj_cols = (real_t*)malloc(sizeof(real_t) * screen_width);

    for (row = 0; row < screen_height; row++)
    {
        pixel_proj_rows[row] = real_div(REAL_UNIT,
                                        real_sub(
                                            real_div(real_from_int(2 * row),
                                                     real_from_int(
                                                         screen_h - 1)),
                                            REAL_UNIT));
    }

    pix_vecs = (real_t*)malloc(sizeof(real_t) * (screen_width << 1));

    for (col = 0; col < screen_width; col++)
    {
        /*
         * getting real close to changing my
         * mind about the 80-character limit now
         */
        pixel_proj_cols[col] = real_mul(x_proj,
                                        real_sub(real_div(
                                                     real_from_int(2 * col),
                                                     real_from_int(
                                                         screen_w - 1)),
                                                 REAL_UNIT));
        get_pix_vec(pix_vecs + (col << 1), col);
    }
}

void init_viewport(angle_t fovy, int screen_width, int screen_height)
{
    cam_rot = ANGLE_0;

    config_viewport(fovy, screen_width, screen_height);
}

static void get_pix_vec(real_t *out, unsigned x_pix)
{
    real_t t = real_div(real_from_int(x_pix), real_from_int(screen_w - 1));
    real_t screen_right = x_proj;
    real_t screen_left = real_neg(screen_right);
    real_t screen_x = real_add(real_mul(screen_right, t),
                               real_mul(real_sub(REAL_UNIT, t), screen_left));
    out[0] = z_proj;
    out[1] = -screen_x;
    pt2_norm(out, out);
}

void get_ray(real_t const cam_pos[2], real_t ray_origin[2], real_t ray_dir[2],
             unsigned x_pix)
{
    real_t cam_xaxis[2], cam_yaxis[2];
    real_t ray_dir_untrans[2];

    ray_origin[0] = cam_pos[0];
    ray_origin[1] = cam_pos[1];

    ray_dir_untrans[0] = pix_vecs[x_pix << 1];
    ray_dir_untrans[1] = pix_vecs[1 + (x_pix << 1)];

    cam_yaxis[0] = angle_sin(cam_rot);
    cam_yaxis[1] = angle_cos(cam_rot);
    cam_xaxis[0] = cam_yaxis[1];
    cam_xaxis[1] = real_neg(cam_yaxis[0]);
    pt2_dot(ray_dir, cam_xaxis, ray_dir_untrans);
    pt2_dot(ray_dir + 1, cam_yaxis, ray_dir_untrans);
}

real_t scale_height(real_t height_nom, real_t z)
{
    return real_div(real_mul(height_nom, z_proj), z);
}

void proj_col(int span[2], real_t y_min, real_t y_max, real_t z)
{
    real_t screen_h_as_real;
    /*
     * Derivation of below equations because I keep forgetting it:
     * t == (y - y_min) / (y_max - y_min)
     * t == (y - -y_proj) / (y_proj - -y_proj)
     * t == (y + y_proj) / (y_proj + y_proj)
     * t == (y + y_proj) / (2 * y_proj)
     * t == 0.5 * (y + y_proj) / y_proj
     */
    real_t t2_top = real_add(REAL_UNIT, scale_height(y_max, z)) >> 1;
    real_t t2_bot = real_add(REAL_UNIT, scale_height(y_min, z)) >> 1;
    screen_h_as_real = real_from_int(screen_h);

    /*
     * t2_top*(screen_h_as_real-1) and t2_bot*(screen_h_as_real)-1 can
     * sometimes overflow the size of a real_t (max/min values are
     * 65535 and -65536), so this does the multiplication as int64_t and then
     * directly converts to int.
     */
    span[0] = screen_h - (int32_t)(((int64_t)t2_top *
                                    (int64_t)real_sub(screen_h_as_real, REAL_UNIT))
                                   >> (REAL_UNIT_SHIFT << 1));
    span[1] = screen_h - (int32_t)(((int64_t)t2_bot *
                                    (int64_t)real_sub(screen_h_as_real, REAL_UNIT))
                                   >> (REAL_UNIT_SHIFT << 1));
    assert(real_lte(span[0], span[1]));
}

angle_t get_cam_rot(void)
{
    return cam_rot;
}

void set_cam_rot(angle_t rot)
{
    cam_rot = rot;
}

void get_cam_forward(real_t forward[2])
{
    forward[0] = angle_cos(cam_rot);
    forward[1] = angle_sin(cam_rot);
}

struct screen *create_screen(int w, int h)
{
    struct screen *scr;

    if (!(scr = (struct screen*)malloc(sizeof(struct screen))))
        err(1, "Failed to allocate screen structure\n");
    if (create_screen_impl(scr, w, h) != 0)
        err(1, "call to create_screen_impl(%d, %d) failed\n", w, h);
    scr->depth_buffer = (real_t*)malloc(sizeof(real_t) * w);
    return scr;
}

void destroy_screen(struct screen *scr)
{
    destroy_screen_impl(scr);
    free(scr->depth_buffer);
    free(scr);
}
