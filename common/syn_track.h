/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef SYN_TRACK_H_
#define SYN_TRACK_H_

#include <stdint.h>

#define NOTE_NAME_LEN 8

#define TRACK_NAME_LEN 16

/*
 * synth funcitons should return the the given sample;
 * t will be scaled by sampling frequency before this function
 * is called, and volume scaling will happen after.
 *
 * This function should return a value between -1.0 and 1.0.
 */
typedef double(*synth_fn)(double freq, double t);

double sin_synth(double freq, double t);

typedef char note_str_t[NOTE_NAME_LEN];

struct syn_beat
{
    /*
     * We always reference notes using their names, it makes saving to and
     * loading from files a little bit simpler.
     */
    note_str_t note_name;

    /*
     * The duration is an integer that describes how long the beat plays.
     * The shortest duration is 1.  The actual length in seconds depends
     * on the tempo.
     */
    unsigned duration;
};

struct syn_track
{
    char name[TRACK_NAME_LEN];
    synth_fn synth;

    struct syn_beat *beats;
    unsigned n_beats;

    /* the shortest possible beat length, in seconds */
    double tempo;

    unsigned next_sample;
    unsigned current_beat;
    unsigned current_beat_sample;

    /*
     * TODO: I should attach these to the synth instead since not all
     * instruments will have the same characteristics.  It may also be
     * beneficial to implement some sort of modular effects infrastructure
     * instead.
     */
    double vol_scale;
    int fade_in;
};

void add_beat_to_track(struct syn_track *track, char const *note,
                       unsigned duration);

unsigned samples_per_beat(double tempo, unsigned samp_freq);

double get_note_freq(char const *name);

#endif
