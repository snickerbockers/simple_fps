/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef MIXER_H_
#define MIXER_H_

#include <stdint.h>

#include "sndserv.h"

typedef unsigned sample_idx_t;
typedef uint16_t volume_t;
typedef int16_t sample_t;

#define MIXER_N_CHANNELS 8

/* maximum possible intensity of a sample */
#define SAMP_MAX INT16_MAX
#define SAMP_MIN INT16_MIN

/*
 * there is actually a problem with representing volumes in this way:
 * VOL_FULL will slightly reduce sample magnitudes (see the fixed-point
 * arithmetic in sample_scale in mixer.c).  This representation may need
 * to be reworked in the future so there's a better way to represent full
 * intensity.
 *
 * It might be a good idea to translate the values over by one bit so that
 * ~0 represents 1.0 and 0 represents the smallest volume greater than zero.
 * having a volume of 0 is somewhat useless since it's effectively the same
 * as muting the channel or not playing anything at all.
 */
#define VOL_FULL ((volume_t)0xffff)
#define VOL_HALF ((volume_t)0x7fff)

/* synth_sound flags */
#define MIX_LOOP 1 /* restart the synth_sound at the end */

/* TODO: Get more than one sample from the synth at a time */
typedef sample_t(*mixer_callback_t)(sample_idx_t sample_idx, void *userarg);

struct synth_sound
{
    mixer_callback_t callback;
    void *callback_data;
    sample_idx_t n_samples; /* if this is 0, the sound plays forever */
    int flags;
};

struct mix_channel
{
    struct synth_sound const *cur_sound;
    sample_idx_t sample_idx;
    volume_t volume;
};

struct mixer
{
    struct mix_channel channels[MIXER_N_CHANNELS];

    /* used to clear the sndserv's callback from mixer_cleanup */
    sndserv_callback_t *callbackp;
    void **callback_argp;
};

int mixer_init(struct mixer *mixer, struct sndserv *srv);
void mixer_cleanup(struct mixer *mixer);

int mixer_play_sound(struct mixer *mixer, struct synth_sound const *sound,
                     volume_t volume);

#endif
