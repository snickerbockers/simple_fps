/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "stream.h"

enum
{
    /* messages conveying high-level information about what the game is doing */
    TRACE_LEVEL_INFO,

    /* Messages concerning major failures that cannot be ignored */
    TRACE_LEVEL_ERR,

    /* messages concerning non-critical failures */
    TRACE_LEVEL_WARN,

    /* messages that are only useful when you're trying to debug a game */
    TRACE_LEVEL_DBG,

    TRACE_LEVEL_COUNT
};

#ifdef NO_TRACE_INFO
#define TRACE_INFO(...)
#else
#define TRACE_INFO(...) trace(TRACE_LEVEL_INFO, __VA_ARGS__)
#endif

#ifdef NO_TRACE_ERR
#define TRACE_ERR(...)
#else
#define TRACE_ERR(...) trace(TRACE_LEVEL_ERR, __VA_ARGS__)
#endif

#ifdef NO_TRACE_WARN
#define TRACE_WARN(...)
#else
#define TRACE_WARN(...) trace(TRACE_LEVEL_WARN, __VA_ARGS__)
#endif

#ifdef NO_TRACE_DBG
#define TRACE_DBG(...)
#else
#define TRACE_DBG(...) trace(TRACE_LEVEL_DBG, __VA_ARGS__)
#endif

void trace(int trace_level, char const *fmt, ...);
