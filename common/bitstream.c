/*****************************************************************************
 **
 ** Copyright (c) 2016, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "bitstream.h"

/*
 * returns 1 if the next bit is set, 0 if it is not and -1
 * if there are no more bits.
 */
int bit_stream_get(struct bit_stream *stream)
{
    size_t byte_pos = stream->pos >> 3;
    
    if (byte_pos < stream->len)
        return stream->stream[byte_pos] & (1 << (stream->pos++ % 8)) ? 1 : 0;
    return -1;
}

int bit_stream_shift_out(struct bit_stream *stream, unsigned *out, int n)
{
    int bit, idx;
    unsigned val;
    unsigned shift = 0;

    val = *out;
    for (idx = 0; idx < n; idx++)
    {
        bit = bit_stream_get(stream);
        if (bit < 0)
            return bit;
        val |= (bit << shift++);
    }

    *out = val;
    return 0;
}

void bit_stream_init(struct bit_stream *stream, void const *dat,
                     size_t byte_len)
{
    stream->pos = 0;
    stream->len = byte_len;
    stream->stream = (uint8_t*)dat;
}

/*
 * If the stream is not byte aligned, advance to the beginning of the next byte.
 * Then return the current byte into *byte and advance by one byte.
 * Returns 0 on success, < 0 on end-of-stream
 */
int bit_stream_get_byte(struct bit_stream *stream, int *byte)
{
    if ((stream->pos >> 3) >= stream->len)
        return -1;

    /*
     * I'm aware there's a way to do this in O(1) by adding 8 and int-dividing
     * by 8 and then multiplying by 8 or something like that but I'm simply not
     * in the mood right now.
     */
    while (stream->pos % 8)
        stream->pos++;

    if ((stream->pos >> 3) >= stream->len)
        return -1;

    *byte = stream->stream[stream->pos >> 3];
    stream->pos += 8;
    return 0;
}
