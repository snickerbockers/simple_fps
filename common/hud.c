/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "game.h"
#include "draw.h"
#include "img.h"
#include "game_obj.h"

#include "hud.h"

#define HUD_HEALTH_ICON_X_POS 0
#define HUD_HEALTH_ICON_Y_POS 0

#define HUD_FPS_X_POS 0
#define HUD_FPS_Y_POS 32

/*
 * the x-pos here is subtracted from the screen width so that it gets
 * drawn on the right-side of the screen.
 */
#define HUD_AMMO_X_POS 96
#define HUD_AMMO_Y_POS 0

static struct tex health_icon;
static struct tex crosshairs_icon;
static struct tex ammo_icon;

int hud_init(void)
{
    if (tex_load_chroma_key(game.sys.scr, &health_icon,
                            DATA_DIR"/health_hud_icon.png",
                            0, STD_CHROMA_KEY) < 0)
        return -1;

    if (tex_load_chroma_key(game.sys.scr, &crosshairs_icon,
                            DATA_DIR"/crosshairs.png",
                            0, STD_CHROMA_KEY) < 0)
        return -1;

    if (tex_load_chroma_key(game.sys.scr, &ammo_icon,
                            DATA_DIR"/ammo_icon.png",
                            0, STD_CHROMA_KEY) < 0)
        return -1;

    return 0;
}

void hud_cleanup(void)
{
    tex_cleanup(&health_icon);
    tex_cleanup(&crosshairs_icon);
    tex_cleanup(&ammo_icon);
}

void hud_draw(void)
{
    struct player_weapon const *wpn;

    wpn = get_player_weapon();
    if (wpn->draw_hud)
        wpn->draw_hud(wpn);
    hud_draw_ammo_counter();
    hud_draw_crosshairs();
}

void hud_print_framerate(void)
{
    real_t fps;
    int fps_as_int;

    fps = real_recip(game.state.frame_time);
    fps_as_int = real_to_int(fps);

    font_print_int(game.sys.scr, &game.sys.overlay_font,
                   HUD_FPS_X_POS, HUD_FPS_Y_POS, fps_as_int, 0);
}

void hud_draw_player_health(void)
{
    int health = get_player_state()->health;

    blit_blended_tex(game.sys.scr, &health_icon,
                     HUD_HEALTH_ICON_X_POS, HUD_HEALTH_ICON_Y_POS,
                     0, 0, health_icon.w, health_icon.h);
    font_print_int(game.sys.scr, &game.sys.overlay_font,
                   health_icon.w, HUD_HEALTH_ICON_Y_POS, health, 3);
}

void hud_draw_ammo_counter(void)
{
    struct player_weapon const *player_weapon;
    int ammo, clip_ammo;

    player_weapon = get_player_weapon();
    ammo = player_weapon->get_ammo();
    clip_ammo = player_weapon->get_clip_ammo();

    blit_blended_tex(game.sys.scr, &ammo_icon,
                     game.sys.scr->screen_w - HUD_AMMO_X_POS, HUD_AMMO_Y_POS,
                     0, 0, ammo_icon.w, ammo_icon.h);
    font_print_int(game.sys.scr, &game.sys.overlay_font,
                   game.sys.scr->screen_w - HUD_AMMO_X_POS + 32, HUD_AMMO_Y_POS,
                   ammo, 2);
    font_print(game.sys.scr, &game.sys.overlay_font,
               game.sys.scr->screen_w - HUD_AMMO_X_POS + 64, HUD_AMMO_Y_POS,
               "/");
    font_print_int(game.sys.scr, &game.sys.overlay_font,
                   game.sys.scr->screen_w - HUD_AMMO_X_POS + 80, HUD_AMMO_Y_POS,
                   clip_ammo, 0);
}

void hud_draw_crosshairs(void)
{
    int x_pos = (game.sys.scr->screen_w >> 1) - (crosshairs_icon.w >> 1);
    int y_pos = (game.sys.scr->screen_h >> 1) - (crosshairs_icon.h >> 1);

    blit_blended_tex(game.sys.scr, &crosshairs_icon, x_pos, y_pos, 0, 0,
                     crosshairs_icon.w, crosshairs_icon.h);
}
