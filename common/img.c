/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "game.h"

#include "img.h"

int tex_load_chroma_key(struct screen *scr, struct tex *img, const char *path,
                        int col_major, color_t key)
{
    int err;

    err = tex_load(scr, img, path, col_major);

    if (err < 0)
        return err;

    tex_chroma_key(img, key);

    return 0;
}

int tex_load(struct screen *scr, struct tex *img, const char *path, int col_major)
{
    int idx;
    int err;
    char const *ext, *cursor;

    /* can't use strchr because const */
    ext = NULL;
    cursor = path;
    while (*cursor)
    {
        if (*cursor == '.')
            ext = cursor;
        cursor++;
    }

    if (!ext)
        return -1;
    if (strcmp(ext, ".pcx") == 0)
        if ((err = tex_load_pcx(scr, img, path)) < 0)
            return err;
    if (strcmp(ext, ".png") == 0)
        if ((err = tex_load_png(scr, img, path)) < 0)
            return err;

    img->mask = (color_t*)malloc(sizeof(color_t) * img->w * img->h);
    if (!img->mask)
    {
        tex_cleanup(img);
        return -1;
    }

    memset(img->mask, ~0, sizeof(color_t) * img->w * img->h);

    if (col_major)
        if (tex_transpose(img) < 0)
        {
            tex_cleanup(img);
            return -1;
        }

    return 0;
}

struct pcx_header
{
    uint8_t manufacturer;// I think this is supposed to be 10
    uint8_t version;
    uint8_t encoding;// if 1, then RLE
    uint8_t bpp;// number of bits per pixel per plane: 1, 2, 4, or 8
    struct
    {
        uint16_t xmin;
        uint16_t ymin;

        uint16_t xmax;
        uint16_t ymax;
    } win;

    uint16_t x_dpi;// horizontal resolution in dpi
    uint16_t y_dpi;// horizontal resolution in dpi

    uint8_t colormap[48];
    uint8_t reserved;// must be zero
    uint8_t n_planes;// number of colors.  should be 3 (for RGB) or 4 (for RGBA)
    uint16_t scan_line_sz;
    uint16_t palette_info;
    uint16_t screen_width;
    uint16_t screen_height;
    uint8_t filler[54];//nothing, really
};

#define IMG_READ(file, var)                                     \
    do                                                          \
    {                                                           \
        size_t sz = fread(&(var), sizeof(var), 1, (file));      \
        if (!sz)                                                \
            goto fileops_error;                                 \
    } while(0)

static int load_header(FILE* file, struct pcx_header* hdr)
{
    int error = 0;
    size_t sz;

    IMG_READ(file, hdr->manufacturer);
    IMG_READ(file, hdr->version);
    IMG_READ(file, hdr->encoding);
    IMG_READ(file, hdr->bpp);
    IMG_READ(file, hdr->win.xmin);
    IMG_READ(file, hdr->win.ymin);
    IMG_READ(file, hdr->win.xmax);
    IMG_READ(file, hdr->win.ymax);
    IMG_READ(file, hdr->x_dpi);
    IMG_READ(file, hdr->y_dpi);

    if ((sz = fread(hdr->colormap, sizeof(hdr->colormap), 1, file)) != 1)
        goto fileops_error;

    IMG_READ(file, hdr->reserved);
    IMG_READ(file, hdr->n_planes);
    IMG_READ(file, hdr->scan_line_sz);
    IMG_READ(file, hdr->palette_info);
    IMG_READ(file, hdr->screen_width);
    IMG_READ(file, hdr->screen_height);

    if ((sz = fread(hdr->filler, sizeof(hdr->filler), 1, file)) != 1)
        goto fileops_error;

fileops_error:
    return error;
}

int tex_load_pcx(struct screen *scr, struct tex *out, const char *path)
{
    int error = 0;
    FILE* file = NULL;
    struct pcx_header hdr;
    size_t cmlen = 0;
    uint8_t *colormap = NULL;
    size_t scanline_len;/* length of one scanline in the file, not in the actual image */
    size_t out_idx = 0;/* current position in the image data */
    size_t in_idx = 0;/* current position in the scanline */
    uint8_t* scanline = NULL; /* buffer used to read in scanlines from the file */
    size_t row, col;
    size_t i;
    size_t sz;
    const char* errmsg = NULL;
    unsigned n_colors, img_w, img_h, img_bpp;
    uint8_t *img_data = NULL;

    out->w = out->h = 0;
    out->data = NULL;

    file = fopen(path, "rb");
    if (!file)
        goto fileops_error;

    if ((error = load_header(file, &hdr)))
        goto fileops_error;

    /* 256 color color-mapped image */
    if (hdr.n_planes == 1)
    {
        n_colors = 3;
        cmlen = 768;
        colormap = (uint8_t*)malloc(cmlen);

        if ((error = fseek(file, 768, SEEK_END)) != 0)
            goto fileops_error;
        if ((sz = fread(colormap, sizeof(uint8_t), cmlen, file)) != cmlen)
            goto fileops_error;
        if ((error = fseek(file, 128, SEEK_SET)) != 0)
            goto fileops_error;
    }
    else
        n_colors = hdr.n_planes;

    scanline_len = hdr.scan_line_sz * hdr.n_planes;
    scanline = (uint8_t*)malloc(scanline_len);

    if (!scanline)
    {
        fprintf(stderr, "Out of memory\n");
        goto return_error;
    }
     
    img_w = hdr.win.xmax + 1 - hdr.win.xmin;
    img_h = hdr.win.ymax + 1 - hdr.win.ymin;
    img_bpp = hdr.bpp * n_colors;

    img_data = (uint8_t*)malloc(img_w * n_colors * img_h);
    if (!img_data)
    {
        fprintf(stderr, "Out of memory\n");
        goto return_error;
    }

    for (row = 0; row < img_h; row++)
    {
        in_idx = 0;
        memset(scanline, 0, scanline_len);

        while (in_idx < scanline_len)
        {
            uint8_t first_byte, count, data_byte;

            IMG_READ(file, first_byte);

            if ((first_byte & 0xC0) == 0xC0)
            {
                count = first_byte & 0x3f;
                IMG_READ(file, data_byte);

                for (i = 0; i < count; i++)
                    scanline[in_idx++] = data_byte;
            }
            else
                scanline[in_idx++] = first_byte;
        }

        if (colormap)
        {
            for (col = 0; col < img_w; col++)
            {
                for (i = 0; i < n_colors; i++)
                {
                    unsigned idx = 3 * scanline[col] + i;

                    if (idx > cmlen)
                    {
                        fprintf(stderr, "colormap overflow\n");
                        goto return_error;
                    }

                    img_data[in_idx++] = colormap[idx];
                }
            }
        }
        else
            for (col = 0; col < img_w; col++)
                for (i = 0; i < n_colors; i++)
                    img_data[out_idx++] = scanline[col + i * hdr.scan_line_sz];
    }

    fclose(file);

    /* now convert the image to a column-based texture format */
    
    out->w = img_w;
    out->h = img_h;
    if ((out->data = malloc(sizeof(color_t) * img_w * img_h)) == NULL)
    {
        fprintf(stderr, "out of memory\n");
        goto return_error;
    }

    for (col = 0; col < img_w; col++)
    {
        for (row = 0; row < img_h; row++)
        {
            color_t pix;
            uint8_t *in;
            switch (n_colors)
            {
            case 1:
                in = img_data + row * img_w + col;
                pix = color_pack_rgb(scr, *in, *in, *in);
                break;
            case 3:
                in = img_data + (row * img_w + col) * 3;
                pix = color_pack_rgb(scr, in[0], in[1], in[2]);
                break;
            case 4:
                in = img_data + (row * img_w + col) * 4;
                pix = color_pack_rgba(scr, in[0], in[1], in[2], in[3]);
                break;
            default:
                fprintf(stderr, "corrupt image %s\n", path);
                goto return_error;
            }

            out->data[row * img_w + col] = pix;
        }
    }
    free(img_data);

    return 0;

fileops_error:
    fprintf(stderr, "File i/o error");

return_error:
    if (img_data)
        free(img_data);
    if (file)
        fclose(file);
    if (out)
    {
        if (out->data)
            free(out->data);
    }

    return -1;
}

void tex_cleanup(struct tex *tex)
{
    if (tex->data)
        free(tex->data);
    if (tex->mask)
        free(tex->mask);
    tex->data = NULL;
    tex->w = tex->h = 0;
}

int tex_transpose(struct tex *img)
{
    int row, col;
    color_t *new_img, *old_img, *new_mask, *old_mask;

    old_img = img->data;
    new_img = (color_t*)malloc(sizeof(color_t) * img->w * img->h);

    if (!new_img)
        return -1;

    old_mask = img->mask;
    new_mask = (color_t*)malloc(sizeof(color_t) * img->w * img->h);

    if (!new_mask)
    {
        free(new_img);
        return -1;
    }

    for (row = 0; row < img->h; row++)
        for (col = 0; col < img->w; col++)
        {
            new_img[col * img->h + row] = old_img[row * img->w + col];
            new_mask[col * img->h + row] = old_mask[row * img->w + col];
        }

    img->data = new_img;
    img->mask = new_mask;
    free(old_mask);
    free(old_img);

    return 0;
}

void tex_chroma_key(struct tex *img, color_t key)
{
    struct screen *scr = game.sys.scr;
    color_t cur_pix;
    int idx;

    for (idx = 0; idx < img->w * img->h; idx++)
    {
        cur_pix = img->data[idx];

        if (color_red(scr, key) == color_red(scr, cur_pix) &&
            color_green(scr, key) == color_green(scr, cur_pix) &&
            color_blue(scr, key) == color_blue(scr, cur_pix))
        {
            img->mask[idx] = 0;
        }
    }
}
