/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef GEO_H_
#define GEO_H_

#include "real.h"

void pt2_add(real_t out[2], real_t const lhs[2], real_t const rhs[2]);
void pt2_sub(real_t out[2], real_t const lhs[2], real_t const rhs[2]);
void pt2_scale(real_t out[2], real_t const lhs[2], real_t s);
void pt2_dot(real_t *out, real_t const lhs[2], real_t const rhs[2]);
void pt2_perp(real_t out[2], real_t const in[2]);
void pt2_cross(real_t *out, real_t const lhs[2], real_t const rhs[2]);
void pt2_sq_mag(real_t *out, real_t const in[2]);
void pt2_mag(real_t *out, real_t const in[2]);
void pt2_norm(real_t out[2], real_t const in[2]);
void pt2_dist(real_t *out, real_t const p1_in[2], real_t const p2_in[2]);

void ln_dist_to(real_t *out, real_t const line[3], real_t const pt[2]);

/*
 * returns nonzero if the ray intersects the line segment from p1 to p2.
 * If there is an intersection, the distance from ray_start to that
 * intersection scaled by the magnitude of ray_dir is returned in *dist and the
 * intersection point itself is saved in inter.
 * else, *dist and *inter are unaltered
 */
int ray_seg_inter(real_t *dist, real_t inter[2], real_t const ray_start[2],
                  real_t const ray_dir[2], real_t const p1[2],
                  real_t const p2[2]);

#endif
