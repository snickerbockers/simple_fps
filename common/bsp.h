/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef BSP_H_
#define BSP_H_

#include "draw.h"
#include "real.h"
#include "stream.h"

/* additional data associated with the line_seg used by the rbsp code */
struct r_line_seg
{
    /*
     * horizontal texture coordinates are clamped in the
     * range [0, 1] and repeat after that.  tc[0] corresponds
     * to the texture coordinate at v[0] and tc[1] corresponds to
     * the texture coordinate at v[1]
     */
    int tex_idx;
    real_t tc[2];
    real_t y_min, y_max;

    /*
     * this will be 1 if the line_seg's normal points in the same
     * direction as the bsp_node's normal and -1 if it does not.
     */
    int sign;
};

struct line_seg
{
    /* vertex indices */
    unsigned v[2];

    /* only valid for rbsp trees, contents are undefined for cbsp */
    struct r_line_seg r_dat;
};

struct bsp_node
{
    /*
     * all bsp nodes are stored in a single array
     * and referenced by their index.  This is to
     * make it easier to save them to and load
     * them from files.  index zero is considered invalid.
     */
    unsigned front, back;

    /*
     * line[0], line[1] are the normal
     * line[2] is the distance from the origin
     * along that normal
     */
    real_t line[3];

    /* these members are only used for rendering. */
    unsigned seg_base, seg_count;
};

/* flag indicating that a ray trace hit something */
#define RAY_HIT 0x1

/* structure used to convey the status of a ray trace */
struct ray_cast
{
    /* ray to cast.  This field should be filled before calling trace_ray */
    real_t ray_origin[2];

    union
    {
        real_t ray_dir[2]; /* renderer: ray goes on forever */
        real_t ray_end[2]; /* collision detection: ray is actually a line seg */
    };

    /*
     * radius of the circle if we're sweeping a circular volume.
     * This is only used by trace_circle
     */
    real_t rad;

    /* normal of the surface where the ray hit */
    real_t norm[2];

    /* location where the ray hit.  the exact distance is equal to this * |dir|. */
    real_t dist;

    /* barycentric coordinate of point on segment */
    real_t bary;

    unsigned hit;

    /* flags should be initialized to zero before calling trace_ray */
    int flags;

    unsigned tex_idx;
    real_t tc;
    real_t y_min, y_max;
};

/* used for sweep_circle, sweep_front and sweep_back */
struct sweep_seg
{
    real_t start[2], end[2];
    int start_node, end_node;
};

struct bsp_tree
{
    struct line_seg *segs;
    struct bsp_node *nodes;
    real_t *verts;
    unsigned n_nodes, n_verts, n_segs;
    unsigned root_node_idx;
};

void move_pt(real_t pt_out[2], real_t const pt_start[2], real_t const pt_end[2],
             real_t rad, struct bsp_tree const *tree);

void cleanup_bsp_tree(struct bsp_tree *tree);

/*
 * This is kinda messy, but there are two separate raytracers for the remderomg
 * and collision detection.  Ideally there would be only one raytracer (but
 * there might still be two separate trees).  I do plan on eventually unifying
 * these two functions
 */
void trace_ray(struct ray_cast *cast, struct bsp_tree const *tree);
int sweep_circle(real_t end_out[2], int *node_out,
                 struct sweep_seg const* seg, real_t rad,
                 struct bsp_tree const* tree);
void gen_bsp(struct bsp_tree *tree, real_t const *verts, unsigned n_verts,
             const struct line_seg *segs, unsigned n_segs);
int bsp_in_empty_space(real_t const pt[2], struct bsp_tree const *tree);
void bsp_clip_pt(real_t pt[2], struct bsp_tree const *tree);

int save_bsp(stream_t *stream, struct bsp_tree const *tree);
int load_bsp(stream_t *stream, struct bsp_tree *tree);

/* deep copy of src onto dst. */
int copy_bsp(struct bsp_tree *dst, struct bsp_tree const *src);

#endif
