/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef SCREEN_H_SDL2_
#define SCREEN_H_SDL2_

#include <SDL2/SDL.h>

#include "real.h"

typedef Uint32 color_t;

struct screen_impl; /* this is defined by the window manager code */

/*
 * Everything is rendered onto an SDL_Texture which is then rendered onto the
 * screen because new SDL doesn't actually support framebuffer access like SDL
 * classic did.  This approach was recommended on some SDL2 migration guide I
 * found somewhere.
 */
struct screen
{
    struct screen_impl *impl;

    color_t *frame_buffer;
    int screen_w, screen_h;

    /* distance to each pixel; used to clip sprites against walls */
    real_t *depth_buffer;
};

color_t *screen_framebuffer(struct screen *scr);

struct screen *create_screen(int w, int h);
void destroy_screen(struct screen *scr);
int create_screen_impl(struct screen *scr, int w, int h);
void destroy_screen_impl(struct screen *scr);
void flip_screen(struct screen *scr);
void clear_screen(struct screen *scr, color_t color);

color_t color_pack_rgb(struct screen const *scr, color_t r, color_t g,
                       color_t b);
color_t color_pack_rgba(struct screen const *scr, color_t r, color_t g,
                        color_t b, color_t a);
color_t color_red(struct screen const *scr, color_t c);
color_t color_blue(struct screen const *scr, color_t c);
color_t color_green(struct screen const *scr, color_t c);
color_t color_alpha(struct screen const *scr, color_t c);

/*
 * this doesn't really belong here, but I don't have a platform-dependent
 * subdir yet, so it goes wit hthe windowing stuff for now.
 */
unsigned get_ticks(void);

#endif
