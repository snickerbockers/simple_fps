/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/
#ifndef GAME_MAP_H_
#define GAME_MAP_H_

#include <stdint.h>

#include "bsp.h"
#include "draw.h"
#include "stream.h"

/*
 *                   GAME MAP FILE STRUCTURE
 *
 * All chunks begin with a 4-byte integer determining their type
 * folowed by chunk-specific data.  For the three chunk types currently
 * defined, only one of each chunk type is allowed.  This may not hold for
 * future chunk types.
 * CHUNK_META_DATA:
 *     name (1023 bytes + null-terminator)
 *     author (1023 bytes + null-terminator)
 *     desc (1023 bytes + null-terminator)
 *     license (1023 bytes + null-terminator)
 *     copyright (1023 bytes + null-terminator)
 * CHUNK_BSP_LEVEL_DATA:
 *     See bsp.c and bsp.h.
 *     I don't remember how this works,
 *     I wrote it way back in September or something.
 * CHUNK_TEXTURE_INFO:
 *     number of textures (4 bytes)
 *     index of the floor texture (4 bytes)
 *     index of the ceiling texture (4 bytes)
 *     texture paths (512 bytes including null terminator per texture)
 * CHUNK_PLAYER_START:
 *     x and z coordinates of player spawn point (8 bytes total)
 */

/* These lengths don't include the null-terminator at the end */
#define MAP_MAX_NAME_LEN 1023
#define MAP_MAX_AUTHOR_LEN 1023
#define MAP_MAX_DESC_LEN 1023
#define MAP_MAX_LICENSE_LEN 1023
#define MAP_MAX_COPYRIGHT_LEN 1023

/* this length does include the null-terminator at the end */
#define MAP_MAX_TEX_PATH_LEN 512

/*
 * this is supposed to match TAG_LEN in mklvl.h.
 * Eventually I'm going to get rid of TAG_LEN and replace it with
 * fine-grained lengths that are defined for each attribute.
 *
 * This length includes a null-terminator at the end.
 */
#define GAME_OBJ_CLASS_LEN 64

/*
 * put a cap on the maximum number of objects so levels can't
 * make obscene demands.
 */
#define GAME_MAP_MAX_OBJS 256

#define MAX_TEX_COUNT 32

/*
 * string-based metadata for .bsp files which
 * does not have any impact on gameplay.
 */
struct game_map_meta
{
    char *name;
    char *author;
    char *desc;
    char *license;
    char *copyright;
};

struct game_map_tex
{
    uint32_t ceil_tex_idx;
    uint32_t floor_tex_idx;
    uint32_t n_tex;
    
    /* wall texture paths */
    /* char (*tex_paths)[MAP_MAX_TEX_PATH_LEN]; */ /* fuck it, too difficult */
    char **tex_paths;
};

struct game_map_game_obj
{
    char class[GAME_OBJ_CLASS_LEN];
    real_t pos[2];
};

struct game_map_game_obj_list
{
    unsigned n_objs;
    struct game_map_game_obj *game_objs;
};

struct game_map
{
    struct game_map_meta meta;
    struct bsp_tree cbsp;
    struct bsp_tree rbsp;
    struct game_map_tex tex;
    struct game_map_game_obj_list obj_list;

    real_t player_spawn[2];
};

int write_map(struct game_map const *map, stream_t *stream);
int load_map(struct game_map *out, stream_t *stream);

void game_map_init(struct game_map *map);

/* frees all the members of map, does not free map itself */
void game_map_cleanup(struct game_map *map);

void game_map_meta_print(struct game_map_meta const *meta, FILE *fp);

void game_map_set_name(struct game_map *map, char const *name);
void game_map_set_author(struct game_map *map, char const *author);
void game_map_set_desc(struct game_map *map, char const *desc);
void game_map_set_license(struct game_map *map, char const *license);
void game_map_set_copyright(struct game_map *map, char const *copyright);

int game_map_add_tex(struct game_map *map, char const *tex);

int game_map_add_obj(struct game_map *map, char const *class,
                      real_t const pos[2]);

#endif
