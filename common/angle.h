/*****************************************************************************
 **
 ** Copyright (c) 2015, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef ANGLE_H_
#define ANGLE_H_

#include <stdint.h>

#include "real.h"

#define ANGLE_MIN ((angle_t)0)
#define ANGLE_MAX (~(angle_t)0)

#define ANGLE_0 ANGLE_MIN
#define ANGLE_45 ((angle_t)0x2000)
#define ANGLE_90 ((angle_t)0x4000)
#define ANGLE_135 ((angle_t)0x6000)
#define ANGLE_180 ((angle_t)0x8000)
#define ANGLE_225 ((angle_t)0xa000)
#define ANGLE_270 ((angle_t)0xc000)
#define ANGLE_315 ((angle_t)0xe000)

typedef uint16_t angle_t;

extern real_t angle_sin_tbl[65536];
extern real_t angle_cos_tbl[65536];
extern real_t angle_tan_tbl[65536];

static inline angle_t angle_from_real(real_t theta)
{
    return real_mul(theta, real_div((real_t)((uint32_t)ANGLE_180), REAL_PI));
}

static inline real_t angle_to_real(angle_t theta)
{
    return real_mul(theta, real_div(REAL_PI, (real_t)((uint32_t)ANGLE_180)));
}

static inline real_t angle_sin(angle_t theta)
{
    return angle_sin_tbl[theta];
}

static inline real_t angle_cos(angle_t theta)
{
    return angle_cos_tbl[theta];
}

static inline real_t angle_tan(angle_t theta)
{
    return angle_tan_tbl[theta];
}

#endif
