/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <assert.h>

#include "list.h"
#include "trace.h"
#include "event.h"

#include "keybind.h"

/*
 * Of course a linked-list is not the most efficient way to do this,
 * but there won't be very many bindings anyways.
 */
struct binding
{
    keycode_t keycode;
    int binding;

    struct list_entry list;
};

static struct list_head keybinds;

/* will return 0 for no binding found */
int find_keybind(keycode_t keycode)
{
    struct binding *cursor;

    LIST_FOREACH(keybinds, struct binding, cursor, list)
    {
        if (cursor->keycode == keycode)
            return cursor->binding;
    }

    return 0;
}

void bind_key(keycode_t keycode, int binding)
{
    struct binding *cursor;

    LIST_FOREACH(keybinds, struct binding, cursor, list)
    {
        if (cursor->keycode == keycode)
        {
            if (cursor->binding)
                TRACE_INFO("Unbinding %d to make way for binding %d\n",
                           cursor->binding, binding);
            cursor->binding = binding;
            return;
        }

        if (cursor->binding == binding)
            TRACE_WARN("%d is bound to more than one key!\n", binding);
    }

    cursor = (struct binding*)calloc(1, sizeof(struct binding));
    if (!cursor)
    {
        TRACE_ERR("Unable to bind %d to key: failed allocation\n");
        return;
    }

    TRACE_INFO("Bind %d created (binded to %d)\n", binding, (int)keycode);

    cursor->keycode = keycode;
    cursor->binding = binding;
    list_push_front(&keybinds, &cursor->list);
}

void cleanup_keybinds(void)
{
    struct binding *cursor;

    while (list_front(&keybinds))
    {
        cursor = LIST_DEREF(struct binding, list, list_pop_front(&keybinds));
        free(cursor);
    }
}
