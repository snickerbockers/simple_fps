/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdint.h>

#include "bsp.h"
#include "trace.h"

#include "game_map.h"

enum
{
    CHUNK_TYPE_META,
    CHUNK_TYPE_RBSP,
    CHUNK_TYPE_TEX,
    CHUNK_TYPE_PLAYER_SPAWN,
    CHUNK_TYPE_GAME_OBJ,
    CHUNK_TYPE_CBSP
};

static void game_map_meta_cleanup(struct game_map_meta *meta);
static void game_map_tex_cleanup(struct game_map_tex *tex);

static int game_map_meta_read(stream_t *stream, struct game_map_meta *meta);
static int game_map_meta_write(stream_t *stream, struct game_map_meta const *meta);

static int game_map_tex_read(stream_t *stream, struct game_map_tex *tex);
static int game_map_tex_write(stream_t *stream, struct game_map_tex const *tex);

static int game_map_player_spawn_read(stream_t *stream, real_t spawn[2]);
static int game_map_player_spawn_write(stream_t *stream, real_t const spawn[2]);

static int
game_map_game_obj_list_write(stream_t *stream,
                             struct game_map_game_obj_list const *obj_list);
static int
game_map_game_obj_list_read(stream_t *stream,
                            struct game_map_game_obj_list *obj_list);

#define WRITE_VAR(var, stream)                                  \
    if (stream_write(&(var), sizeof((var)), 1, stream) != 1)    \
    {                                                           \
        goto on_error;                                          \
    }

#define READ_VAR(var, stream)                                   \
    if (stream_read(&(var), sizeof((var)), 1, stream) != 1)     \
    {                                                           \
        goto on_error;                                          \
    }

static int write_meta_str(stream_t *stream, size_t max_len, char const *str)
{
    if (stream_write(str, sizeof(char), max_len, stream) != max_len)
        goto on_error;

    return 0;
on_error:
    return -1;
}

static int game_map_meta_write(stream_t *stream, struct game_map_meta const *meta)
{
    TRACE_DBG("%s - stream_tell() == %d\n", __func__, (int)stream_tell(stream));

    if (write_meta_str(stream, MAP_MAX_NAME_LEN, meta->name) < 0)
        goto on_error;
    if (write_meta_str(stream, MAP_MAX_AUTHOR_LEN, meta->author) < 0)
        goto on_error;
    if (write_meta_str(stream, MAP_MAX_DESC_LEN, meta->desc) < 0)
        goto on_error;
    if (write_meta_str(stream, MAP_MAX_LICENSE_LEN, meta->license) < 0)
        goto on_error;
    if (write_meta_str(stream, MAP_MAX_COPYRIGHT_LEN, meta->copyright) < 0)
        goto on_error;

    return 0;
on_error:
    return -1;
}

static int read_meta_str(stream_t *stream, size_t max_len, char *out)
{
    char *buf;

    if (stream_read(out, sizeof(char), max_len, stream) != max_len)
        goto io_error;

    /* this is safe because the allocated length is max_len+1 */
    out[max_len] = '\0';
    
    return 0;
io_error:
    return -1;
}

static int game_map_meta_read(stream_t *stream, struct game_map_meta *meta)
{
    int len;

    TRACE_DBG("%s - stream_tell() == %d\n", __func__, (int)stream_tell(stream));

    if (read_meta_str(stream, MAP_MAX_NAME_LEN, meta->name) < 0)
        goto on_error;
    if (read_meta_str(stream, MAP_MAX_AUTHOR_LEN, meta->author) < 0)
        goto on_error;
    if (read_meta_str(stream, MAP_MAX_DESC_LEN, meta->desc) < 0)
        goto on_error;
    if (read_meta_str(stream, MAP_MAX_LICENSE_LEN, meta->license) < 0)
        goto on_error;
    if (read_meta_str(stream, MAP_MAX_COPYRIGHT_LEN, meta->copyright) < 0)
        goto on_error;

    return 0;
on_error:
    return -1;
}

static int game_map_tex_read(stream_t *stream, struct game_map_tex *tex)
{
    int idx;

    TRACE_DBG("%s - stream_tell() == %d\n", __func__, (int)stream_tell(stream));

    memset(tex, 0, sizeof(struct game_map_tex));

    READ_VAR(tex->ceil_tex_idx, stream);
    READ_VAR(tex->floor_tex_idx, stream);
    READ_VAR(tex->n_tex, stream);

    if (tex->n_tex > MAX_TEX_COUNT)
    {
        TRACE_ERR("Too many textures\n");
        return -1;
    }
    
    tex->tex_paths = calloc(tex->n_tex, sizeof(char) * MAP_MAX_TEX_PATH_LEN);

    for (idx = 0; idx < tex->n_tex; idx++)
    {
        tex->tex_paths[idx] = malloc(sizeof(char) * MAP_MAX_TEX_PATH_LEN);
        if (stream_read(tex->tex_paths[idx], sizeof(char),
                        MAP_MAX_TEX_PATH_LEN, stream) < 0)
            goto on_error;
        if (tex->tex_paths[idx][MAP_MAX_TEX_PATH_LEN-1] != '\0')
        {
            TRACE_ERR("Unterminated string\n");
            return -1;
        }

        TRACE_DBG("tex_paths[%d] == \"%s\"\n", idx, tex->tex_paths[idx]);
    }

    return 0;

on_error:
    return -1;
}

static int game_map_tex_write(stream_t *stream, struct game_map_tex const *tex)
{
    int idx;

    TRACE_DBG("%s - stream_tell() == %d\n", __func__, (int)stream_tell(stream));

    WRITE_VAR(tex->ceil_tex_idx, stream);
    WRITE_VAR(tex->floor_tex_idx, stream);
    WRITE_VAR(tex->n_tex, stream);

    for (idx = 0; idx < tex->n_tex; idx++)
    {
        TRACE_DBG("Adding texture \"%s\"\n", tex->tex_paths[idx]);
        if (stream_write(tex->tex_paths[idx], sizeof(char),
                         MAP_MAX_TEX_PATH_LEN, stream) < 0)
        {
            goto on_error;
        }
    }

    return 0;

on_error:
    return -1;
}

static int game_map_player_spawn_read(stream_t *stream, real_t spawn[2])
{
    TRACE_DBG("%s - stream_tell() == %d\n", __func__, (int)stream_tell(stream));

    READ_VAR(spawn[0], stream);
    READ_VAR(spawn[1], stream);
    return 0;

on_error:
    return -1;
}
static int game_map_player_spawn_write(stream_t *stream, real_t const spawn[2])
{
    TRACE_DBG("%s - stream_tell() == %d\n", __func__, (int)stream_tell(stream));

    WRITE_VAR(spawn[0], stream);
    WRITE_VAR(spawn[1], stream);
    return 0;

on_error:
    return -1;
}

static int
game_map_game_obj_list_read(stream_t *stream,
                            struct game_map_game_obj_list *obj_list)
{
    int idx;

    READ_VAR(obj_list->n_objs, stream);

    if (obj_list->n_objs > GAME_MAP_MAX_OBJS)
        return -1;

    obj_list->game_objs = (struct game_map_game_obj*)
        calloc(obj_list->n_objs, sizeof(obj_list->game_objs[0]));

    if (!obj_list->game_objs)
    {
        TRACE_ERR("Failed allocation\n");
        return -1;
    }

    for (idx = 0; idx < obj_list->n_objs; idx++)
    {
        if (stream_read(obj_list->game_objs[idx].class,
                        sizeof(obj_list->game_objs[idx].class[0]),
                        GAME_OBJ_CLASS_LEN, stream) != GAME_OBJ_CLASS_LEN)
            goto on_error;
        obj_list->game_objs[idx].class[GAME_OBJ_CLASS_LEN - 1] = '\0';
        if (stream_read(obj_list->game_objs[idx].pos,
                        sizeof(obj_list->game_objs[idx].pos[0]),
                        2, stream) != 2)
            goto on_error;
        TRACE_DBG("%s - Object %s located at (%f, %f)\n", __func__,
                  obj_list->game_objs[idx].class,
                  real_to_double(obj_list->game_objs[idx].pos[0]),
                  real_to_double(obj_list->game_objs[idx].pos[1]));
    }

    return 0;
on_error:
    free(obj_list->game_objs);
    return -1;
}

static int
game_map_game_obj_list_write(stream_t *stream,
                             struct game_map_game_obj_list const *obj_list)
{
    int idx;

    WRITE_VAR(obj_list->n_objs, stream);

    for (idx = 0; idx < obj_list->n_objs; idx++)
    {
        if (stream_write(obj_list->game_objs[idx].class,
                         sizeof(obj_list->game_objs[idx].class[0]),
                         GAME_OBJ_CLASS_LEN, stream) != GAME_OBJ_CLASS_LEN)
            goto on_error;
        if (stream_write(obj_list->game_objs[idx].pos,
                         sizeof(obj_list->game_objs[idx].pos[0]),
                         2, stream) != 2)
            goto on_error;
        TRACE_DBG("%s - Object %s located at (%f, %f)\n", __func__,
                  obj_list->game_objs[idx].class,
                  real_to_double(obj_list->game_objs[idx].pos[0]),
                  real_to_double(obj_list->game_objs[idx].pos[1]));
    }

    return 0;
on_error:
    return -1;
}

int write_map(struct game_map const *map, stream_t *stream)
{
    uint32_t next_chunk_start_pos, next_chunk_start;
    int32_t chunk_tp;

    chunk_tp = CHUNK_TYPE_META;
    WRITE_VAR(chunk_tp, stream);
    next_chunk_start_pos = stream_tell(stream);
    stream_seek(stream, 4, STREAM_SEEK_CUR);
    if (game_map_meta_write(stream, &map->meta) < 0)
        goto on_stream_error;
    next_chunk_start = stream_tell(stream);
    stream_seek(stream, next_chunk_start_pos, STREAM_SEEK_SET);
    WRITE_VAR(next_chunk_start, stream);
    stream_seek(stream, next_chunk_start, STREAM_SEEK_SET);

    chunk_tp = CHUNK_TYPE_RBSP;
    WRITE_VAR(chunk_tp, stream);
    next_chunk_start_pos = stream_tell(stream);
    stream_seek(stream, 4, STREAM_SEEK_CUR);
    if (save_bsp(stream, &map->rbsp) < 0)
        goto on_stream_error;
    next_chunk_start = stream_tell(stream);
    stream_seek(stream, next_chunk_start_pos, STREAM_SEEK_SET);
    WRITE_VAR(next_chunk_start, stream);
    stream_seek(stream, next_chunk_start, STREAM_SEEK_SET);

    chunk_tp = CHUNK_TYPE_CBSP;
    WRITE_VAR(chunk_tp, stream);
    next_chunk_start_pos = stream_tell(stream);
    stream_seek(stream, 4, STREAM_SEEK_CUR);
    if (save_bsp(stream, &map->cbsp) < 0)
        goto on_stream_error;
    next_chunk_start = stream_tell(stream);
    stream_seek(stream, next_chunk_start_pos, STREAM_SEEK_SET);
    WRITE_VAR(next_chunk_start, stream);
    stream_seek(stream, next_chunk_start, STREAM_SEEK_SET);

    chunk_tp = CHUNK_TYPE_TEX;
    WRITE_VAR(chunk_tp, stream);
    next_chunk_start_pos = stream_tell(stream);
    stream_seek(stream, 4, STREAM_SEEK_CUR);
    if (game_map_tex_write(stream, &map->tex) < 0)
        goto on_stream_error;
    next_chunk_start = stream_tell(stream);
    stream_seek(stream, next_chunk_start_pos, STREAM_SEEK_SET);
    WRITE_VAR(next_chunk_start, stream);
    stream_seek(stream, next_chunk_start, STREAM_SEEK_SET);

    chunk_tp = CHUNK_TYPE_PLAYER_SPAWN;
    WRITE_VAR(chunk_tp, stream);
    next_chunk_start_pos = stream_tell(stream);
    stream_seek(stream, 4, STREAM_SEEK_CUR);
    if (game_map_player_spawn_write(stream, map->player_spawn) < 0)
        goto on_stream_error;
    next_chunk_start = stream_tell(stream);
    stream_seek(stream, next_chunk_start_pos, STREAM_SEEK_SET);
    WRITE_VAR(next_chunk_start, stream);
    stream_seek(stream, next_chunk_start, STREAM_SEEK_SET);

    chunk_tp = CHUNK_TYPE_GAME_OBJ;
    WRITE_VAR(chunk_tp, stream);
    next_chunk_start_pos = stream_tell(stream);
    stream_seek(stream, 4, STREAM_SEEK_CUR);
    if (game_map_game_obj_list_write(stream, &map->obj_list) < 0)
        goto on_stream_error;
    next_chunk_start = 0;
    stream_seek(stream, next_chunk_start_pos, STREAM_SEEK_SET);
    WRITE_VAR(next_chunk_start, stream);
    stream_seek(stream, next_chunk_start, STREAM_SEEK_SET);

    return 0;

on_error:
on_stream_error:
    TRACE_ERR("stream write error: %s\n",
              stream_errstr(stream, stream_err(stream)));
    return -1;

}

int load_map(struct game_map *out, stream_t *stream)
{
    int have_meta, have_rbsp, have_cbsp, have_tex, have_spawn;
    int32_t chunk_tp;
    uint32_t next_chunk = 0;

    have_meta = have_rbsp = have_cbsp = have_tex = have_spawn = 0;

    while (stream_err(stream) == STREAM_ERR_NONE)
    {
        /* TODO: detect condition where next_chunk <= previous next_chunk */
        stream_seek(stream, next_chunk, STREAM_SEEK_SET);
        TRACE_DBG("%s - stream_tell() == %d\n", __func__, (int)stream_tell(stream));
        chunk_tp = 0;
        if (stream_read(&chunk_tp, sizeof(chunk_tp), 1, stream)  != 1)
            goto on_stream_error;
        READ_VAR(next_chunk, stream);

        switch (chunk_tp)
        {
        case CHUNK_TYPE_META:
            if (game_map_meta_read(stream, &out->meta) < 0)
                goto on_stream_error;
            have_meta = 1;
            break;
        case CHUNK_TYPE_RBSP:
            if (load_bsp(stream, &out->rbsp) < 0)
                goto on_stream_error;
            have_rbsp = 1;
            break;
        case CHUNK_TYPE_CBSP:
            if (load_bsp(stream, &out->cbsp) < 0)
                goto on_stream_error;
            have_cbsp = 1;
            break;
        case CHUNK_TYPE_TEX:
            if (game_map_tex_read(stream, &out->tex) < 0)
                goto on_stream_error;
            have_tex = 1;
            break;
        case CHUNK_TYPE_PLAYER_SPAWN:
            if (game_map_player_spawn_read(stream, out->player_spawn) < 0)
                goto on_stream_error;
            have_spawn = 1;
            break;
        case CHUNK_TYPE_GAME_OBJ:
            if (game_map_game_obj_list_read(stream, &out->obj_list) < 0)
                goto on_stream_error;
            break;
        }

        if (next_chunk == 0)
            break;
    }

    if (!(have_meta && have_tex && have_rbsp && have_cbsp && have_spawn))
    {
        TRACE_ERR("Did not collect all chunks\n");
        return -1;
    }

    return 0;

on_stream_error:
on_error:
    TRACE_ERR("stream read error: %s\n",
              stream_errstr(stream, stream_err(stream)));
    return -1;
}

/* frees all the members of map, does not free map itself */
void game_map_cleanup(struct game_map *map)
{
    game_map_meta_cleanup(&map->meta);
    cleanup_bsp_tree(&map->cbsp);
    cleanup_bsp_tree(&map->rbsp);
    game_map_tex_cleanup(&map->tex);

    memset(map, 0, sizeof(struct game_map));
}

static void game_map_meta_cleanup(struct game_map_meta *meta)
{
    if (meta->name)
        free(meta->name);
    if (meta->author)
        free(meta->author);
    if (meta->desc)
        free(meta->desc);
    if (meta->license)
        free(meta->license);
}

static void game_map_tex_cleanup(struct game_map_tex *tex)
{
    int idx;

    if (tex->tex_paths)
        for (idx = 0; idx < tex->n_tex; idx++)
            free(tex->tex_paths[idx]);
    free(tex->tex_paths);
}

void game_map_set_name(struct game_map *map, char const *name)
{
    strncpy(map->meta.name, name, MAP_MAX_NAME_LEN);

    /* allocated size is MAP_MAX_NAME_LEN + 1, so this is safe */
    map->meta.name[MAP_MAX_NAME_LEN] = '\0';
}

void game_map_set_author(struct game_map *map, char const *author)
{
    strncpy(map->meta.author, author, MAP_MAX_AUTHOR_LEN);

    /* allocated size is MAP_MAX_AUTHOR_LEN + 1, so this is safe */
    map->meta.author[MAP_MAX_AUTHOR_LEN] = '\0'; 
}

void game_map_set_desc(struct game_map *map, char const *desc)
{
    strncpy(map->meta.desc, desc, MAP_MAX_DESC_LEN);

    /* allocated size is MAP_MAX_DESC_LEN + 1, so this is safe */
    map->meta.desc[MAP_MAX_DESC_LEN] = '\0';
}

void game_map_set_license(struct game_map *map, char const *license)
{
    strncpy(map->meta.license, license, MAP_MAX_LICENSE_LEN);

    /* allocated size is MAP_MAX_LICENSE_LEN + 1, so this is safe */
    map->meta.license[MAP_MAX_LICENSE_LEN] = '\0';
}

void game_map_set_copyright(struct game_map *map, char const *copyright)
{
    strncpy(map->meta.copyright, copyright, MAP_MAX_COPYRIGHT_LEN);

    /* allocated size is MAP_MAX_COPYRIGHT_LEN + 1, so this is safe */
    map->meta.copyright[MAP_MAX_COPYRIGHT_LEN] = '\0';
}

void game_map_init(struct game_map *map)
{
    memset(map, 0, sizeof(struct game_map));

    map->meta.name = (char*)calloc(MAP_MAX_NAME_LEN + 1,
                                   sizeof(char));
    map->meta.author = (char*)calloc(MAP_MAX_AUTHOR_LEN + 1,
                                     sizeof(char));
    map->meta.desc = (char*)calloc(MAP_MAX_DESC_LEN + 1,
                                   sizeof(char));
    map->meta.license = (char*)calloc(MAP_MAX_LICENSE_LEN + 1,
                                      sizeof(char));
    map->meta.copyright = (char*)calloc(MAP_MAX_COPYRIGHT_LEN + 1,
                                        sizeof(char));
}

int game_map_add_tex(struct game_map *map, char const *tex)
{
    char *new_tex_path;
    char **tex_paths;

    TRACE_DBG("%s - adding %s\n", __func__, tex);
    
    new_tex_path = (char*)malloc(sizeof(char) * MAP_MAX_TEX_PATH_LEN);
    if (!new_tex_path)
        goto failed_allocation;

    tex_paths = (char**)realloc(map->tex.tex_paths,
                                ++map->tex.n_tex * sizeof(char*));
    if (!tex_paths)
        goto failed_allocation;

    strncpy(new_tex_path, tex, MAP_MAX_TEX_PATH_LEN);
    new_tex_path[MAP_MAX_TEX_PATH_LEN - 1] = '\0';

    tex_paths[map->tex.n_tex - 1] = new_tex_path;
    map->tex.tex_paths = tex_paths;

    return 0;

failed_allocation:
    TRACE_ERR("%s - unable to add texture due to failed allocation\n",
              __func__);
    return -1;
}

int game_map_add_obj(struct game_map *map, char const *class,
                     real_t const pos[2])
{
    struct game_map_game_obj *game_obj_new;
    struct game_map_game_obj *obj_out;

    game_obj_new = (struct game_map_game_obj*)
        realloc(map->obj_list.game_objs,
                (map->obj_list.n_objs + 1) *
                sizeof(map->obj_list.game_objs[0]));

    if (!game_obj_new)
        return -1;

    obj_out = game_obj_new + map->obj_list.n_objs;

    strncpy(obj_out->class, class, GAME_OBJ_CLASS_LEN);
    obj_out->pos[0] = pos[0];
    obj_out->pos[1] = pos[1];

    map->obj_list.game_objs = game_obj_new;
    map->obj_list.n_objs++;

    return 0;
}
