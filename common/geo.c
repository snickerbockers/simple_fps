/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "geo.h"

void pt2_add(real_t out[2], real_t const lhs[2], real_t const rhs[2])
{
    out[0] = real_add(lhs[0], rhs[0]);
    out[1] = real_add(lhs[1], rhs[1]);
}

void pt2_sub(real_t out[2], real_t const lhs[2], real_t const rhs[2])
{
    out[0] = real_sub(lhs[0], rhs[0]);
    out[1] = real_sub(lhs[1], rhs[1]);
}

void pt2_scale(real_t out[2], real_t const lhs[2], real_t s)
{
    out[0] = real_mul(s, lhs[0]);
    out[1] = real_mul(s, lhs[1]);
}

void pt2_dot(real_t *out, real_t const lhs[2], real_t const rhs[2])
{
    *out = real_add(real_mul(lhs[0], rhs[0]), real_mul(lhs[1], rhs[1]));
}

void pt2_perp(real_t out[2], real_t const in[2])
{
    /* the temporary copy of in[0] is necessary in case in == out */
    real_t tmp = in[0];

    out[0] = real_neg(in[1]);
    out[1] = tmp;
}

void pt2_cross(real_t *out, real_t const lhs[2], real_t const rhs[2])
{
    real_t tmp[2];

    pt2_perp(tmp, lhs);
    pt2_dot(out, tmp, rhs);
}

void pt2_mag(real_t *out, real_t const in[2])
{
    *out = real_sqrt(real_add(real_mul(in[0], in[0]), real_mul(in[1], in[1])));
}

void pt2_sq_mag(real_t *out, real_t const in[2])
{
    *out = real_add(real_mul(in[0], in[0]), real_mul(in[1], in[1]));
}

void pt2_norm(real_t out[2], real_t const in[2])
{
    real_t mag;

    pt2_mag(&mag, in);
    pt2_scale(out, in, real_div(real_from_double(1.0), mag));
}

void ln_dist_to(real_t *out, real_t const line[3], real_t const pt[2])
{
    real_t proj;

    pt2_dot(&proj, line, pt);
    *out = real_sub(proj, line[2]);
}

int ray_seg_inter(real_t *dist, real_t inter[2], real_t const ray_start[2],
                  real_t const ray_dir[2], real_t const p1[2],
                  real_t const p2[2])
{
    real_t seg_dir[2], seg_line[3];
    real_t rel_angle_cos, inter_dist, projected_ray_origin;
    real_t dist_to_origin;
    real_t inter_pt[2], inter_proj_seg, seg_start_proj, seg_end_proj;

    /* calculate the hyperline of the line segment */
    pt2_sub(seg_dir, p2, p1);
    pt2_norm(seg_dir, seg_dir);
    pt2_perp(seg_line, seg_dir);
    pt2_dot(seg_line+2, p1, seg_line);

    /*
     * relative angle of the normal of the
     * hyperline with the dir of the ray
     */
    pt2_dot(&rel_angle_cos, seg_line, ray_dir);

    /* signed distance from hyperline to ray origin */
    ln_dist_to(&dist_to_origin, seg_line, ray_start);

    /* ray_start projected onto the normal of the hyperline */
    pt2_dot(&projected_ray_origin, ray_start, seg_line);

    if (rel_angle_cos > REAL_ZERO)
    {
        if (dist_to_origin >= REAL_ZERO)
            return 0;
    }
    else if (rel_angle_cos < REAL_ZERO)
    {
        if (dist_to_origin <= REAL_ZERO)
            return 0;
    }
    else
        return 0;

    /*
     * ray starts behind line and points in same dir as its norm
     *
     * OR
     *
     * ray starts in front of line and points in opposite dir as its norm
     */
    inter_dist = real_div(real_sub(seg_line[2], projected_ray_origin),
                          rel_angle_cos);

    /* Next check to see if the point of intersection lies between p1 and p2 */
    pt2_scale(inter_pt, ray_dir, inter_dist);
    pt2_add(inter_pt, ray_start, inter_pt);

    pt2_dot(&inter_proj_seg, seg_dir, inter_pt);
    pt2_dot(&seg_start_proj, seg_dir, p1);
    pt2_dot(&seg_end_proj, seg_dir, p2);

    if (inter_proj_seg >= seg_start_proj && inter_proj_seg <= seg_end_proj)
    {
        memcpy(inter, inter_pt, sizeof(real_t) * 2);
        *dist = inter_dist;
        return 1;
    }

    return 0;
}

void pt2_dist(real_t *out, real_t const p1_in[2], real_t const p2_in[2])
{
    real_t vec[2];

    pt2_sub(vec, p2_in, p1_in);
    pt2_mag(out, vec);
}
