/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "bsp.h"
#include "game.h"
#include "real.h"
#include "img.h"

#include "player_weapon.h"

/* the number of ticks between successive pistol shots */
#define PISTOL_SHOT_PERIOD 100

/* how long to display the pistol muzzle flash */
#define PISTOL_MUZZLE_FLASH_TIME 100

#define PISTOL_CLIP_SIZE 5

/* how long it takes the pistol to reload */
#define PISTOL_RELOAD_TIME 1000

#define PISTOL_AMMO_MAX (7 * PISTOL_CLIP_SIZE)
#define PISTOL_AMMO_DEFAULT PISTOL_AMMO_MAX

enum
{
    PISTOL_READY,
    PISTOL_RELOAD
};

static struct tex pistol_spr;

static struct pistol_state
{
    /* whether or not the weapon is reloading */
    int state;

    /*
     * how much ammo is in the weapon's current clip.
     * This triggers a reload when 0.
     */
    int clip_ammo;

    /* how much total ammo the weapon has */
    int ammo;

    /*
     * used to track the amount of time since the pistol last fired.
     * it is set to this_frame_ticks every time the player switches to
     * the pistol.  It is also used to time reloading.
     */
    unsigned timer;
} player_pistol;

int player_weapons_init(void)
{
    player_pistol.timer = game.state.this_frame_ticks;
    player_pistol.state = PISTOL_READY;
    player_pistol.clip_ammo = PISTOL_CLIP_SIZE;
    player_pistol.ammo = PISTOL_AMMO_DEFAULT;

    if (tex_load_chroma_key(game.sys.scr, &pistol_spr,
                            DATA_DIR"/pistol_first_person.png",
                            0, STD_CHROMA_KEY) < 0)
        return -1;

    return 0;
}

void player_weapons_cleanup(void)
{
    tex_cleanup(&pistol_spr);
}

void pistol_attack(struct player_weapon const *weapon)
{
    real_t dist;
    struct game_obj *hit;
    struct ray_cast ray;
    struct game_obj *player, *victim;
    real_t *pos, *dir;
    struct player_state *player_state;

    player_state = get_player_state();

    /* can't fire if the gun is reloading */
    if (player_pistol.state == PISTOL_RELOAD)
    {
        /* check if it should be done reloading. */
        if (game.state.this_frame_ticks - player_pistol.timer >=
            PISTOL_RELOAD_TIME)
        {
            /*
             * XXX If PISTOL_SHOT_PERIOD is greater than PISTOL_RELOAD_TIME
             * (which it probably never will be), the player will have to wait
             * an additional PISTOL_SHOT_PERIOD-PISTOL_RELOAD_TIME milliseconds
             * before he can fire.
             */
            player_pistol.state = PISTOL_READY;
        }
        else
            return;
    }

    /*
     * XXX Begin reloading.  The semantics of this are that
     * the reload happens the first time you try to fire while
     * the clip is empty.  This is annoying and I'd prefer to do
     * it as soon as the clip is empty, but this way is also a tad
     * bit easier to implement given the current state of the code.
     * It's also pretty common behavior in a lot of FPS games, so it's
     * not too inexcusable.
     */
    if (!player_pistol.clip_ammo)
    {
        /* reload instead of firing, but only if there's ammo left */
        if (player_pistol.ammo)
        {
            player_pistol.state = PISTOL_RELOAD;
            player_pistol.timer = game.state.this_frame_ticks;

            if (player_pistol.ammo >= PISTOL_CLIP_SIZE)
            {
                player_pistol.clip_ammo = PISTOL_CLIP_SIZE;
                player_pistol.ammo -= PISTOL_CLIP_SIZE;
            }
            else
            {
                player_pistol.clip_ammo = player_pistol.ammo;
                player_pistol.ammo = 0;
            }
        }

        /*
         * TODO: play a sound effect (and maybe a little twitch animation) to
         * indicate that the gun can't fire when it's out of ammo.
         */
        return;
    }

    if (game.state.this_frame_ticks - player_pistol.timer < PISTOL_SHOT_PERIOD)
        return;

    player_pistol.timer = game.state.this_frame_ticks;
    player_pistol.clip_ammo--;

    victim = NULL;
    player = game.state.player_obj;
    pos = player->pos;
    dir = player->dir;

    hit = game_obj_hitscan(&dist, player, pos, dir);

    if (hit && dist >= REAL_ZERO)
    {
        memset(&ray, 0, sizeof(ray));
        memcpy(ray.ray_origin, pos, sizeof(real_t) * 2);
        memcpy(ray.ray_dir, dir, sizeof(real_t) * 2);

        trace_ray(&ray, &game.sys.rbsp);
        if (ray.flags & RAY_HIT)
        {
            if (ray.dist < dist)
            {
                /* ray hits a wall before a game_obj */
                return;
            }
        }
        victim = hit;
    }
    if (victim)
    {
        /* TODO: implement variable damage depending on gun type */
        if (victim->class->hp_max)
        {
            victim->hp--;
            if (victim->hp <= 0 && victim->class->on_death)
                victim->class->on_death(victim);
        }
    }
}

void hud_draw_pistol(struct player_weapon const *weapon)
{
    int y_pos;
    unsigned elapsed_ticks;
    real_t interp;
    struct player_state *player_state;

    player_state = get_player_state();

    y_pos = game.sys.scr->screen_h - pistol_spr.h;
    if (player_pistol.state == PISTOL_RELOAD)
    {
        elapsed_ticks = game.state.this_frame_ticks - player_pistol.timer;

        if (elapsed_ticks < PISTOL_RELOAD_TIME / 2)
        {
            /* pistol is moving down */
            interp = real_div(real_from_int(elapsed_ticks),
                              real_from_int(PISTOL_RELOAD_TIME / 2));
            y_pos += real_to_int(real_mul(interp, real_from_int(pistol_spr.h)));
        }
        else
        {
            /* pistol is moving up */
            y_pos += pistol_spr.h;
            elapsed_ticks -= PISTOL_RELOAD_TIME / 2;

            /*
             * cap it because the pistol will stay in PISTOL_RELOAD state until
             * the player tries to shoot again.
             */
            if (elapsed_ticks >= PISTOL_RELOAD_TIME / 2)
                elapsed_ticks = PISTOL_RELOAD_TIME / 2;

            interp = real_div(real_from_int(elapsed_ticks),
                              real_from_int(PISTOL_RELOAD_TIME / 2));
            y_pos -= real_to_int(real_mul(interp, real_from_int(pistol_spr.h)));
        }
    }

    if (game.state.this_frame_ticks - player_pistol.timer
        >= PISTOL_MUZZLE_FLASH_TIME || player_pistol.state == PISTOL_RELOAD)
    {
        /* gun not firing - no muzzle flash */
        blit_blended_tex(game.sys.scr, &pistol_spr,
                         game.sys.scr->screen_w - pistol_spr.w / 2, y_pos,
                         0, 0, pistol_spr.w / 2, pistol_spr.h);
    }
    else
    {
        /* gun firing - muzzle flash */
        blit_blended_tex(game.sys.scr, &pistol_spr,
                         game.sys.scr->screen_w - pistol_spr.w / 2, y_pos,
                         pistol_spr.w / 2, 0, pistol_spr.w / 2, pistol_spr.h);
    }
}

int pistol_get_ammo(void)
{
    return player_pistol.ammo;
}

void pistol_add_ammo(int rounds)
{
    player_pistol.ammo += rounds;
}

int pistol_get_clip_ammo(void)
{
    return player_pistol.clip_ammo;
}
