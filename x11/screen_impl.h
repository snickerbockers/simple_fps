/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef SCREEN_IMPL_XLIB_H_
#define SCREEN_IMPL_XLIB_H_

#include <X11/Xlib.h>

#include "screen.h"

#define EVENT_MASK_X11 (PointerMotionMask | FocusChangeMask | ExposureMask | \
                        ButtonPressMask | ButtonReleaseMask | KeyPressMask | \
                        KeyReleaseMask)
/*
 * Currently the frame_buffer is stored in this off-screen buffer and then
 * copied to the XImage which is then drawn to the window.  This is because
 * the rendering code doesn't have any concept of padding yet, so it can't
 * write directly to the XImage.
 */
struct screen_impl
{
    Display *disp;
    XImage *img;
    Window win;
    int screen;
    GC gc;

    /* The cursor we tell X11 to use, this is generally blank */
    Cursor cursor;
    int scanline_len;

    /*
     * bpp is the number of bits used in each pixel
     * pixel_size is the size of each pixel in bytes including any padding
     */
    int bpp, pixel_size;

    color_t red_mask, green_mask, blue_mask, alpha_mask;
    int red_shift, green_shift, blue_shift, alpha_shift;
};

#endif
