/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <X11/Xlib.h>
#include <X11/keysym.h>

#include "screen.h"
#include "screen_impl.h"
#include "event.h"
#include "keybind.h"

static int mouse_x, mouse_y;

static int handle_mouse_motion(struct screen *scr, XEvent *xev,
                               struct event *event);
static int handle_key_event(struct screen *scr, XEvent *xev,
                            struct event *event);

int input_poll(struct screen *scr, struct event *event)
{
    int status;
    XEvent xev;

    while (XCheckWindowEvent(scr->impl->disp, scr->impl->win,
                             EVENT_MASK_X11, &xev))
    {
        if (xev.type == ClientMessage)
        {
            /*
             * TODO: need to check for the WM_DELETE_WINDOW
             * atom before returning EVENT_TYPE_QUIT
             */
            event->type = EVENT_TYPE_QUIT;
            return 1;
        }
        else if (xev.type == MotionNotify)
        {
            if ((status = handle_mouse_motion(scr, &xev, event)))
                return status;
        }
        else if (xev.type == KeyPress || xev.type == KeyRelease)
        {
            if ((status = handle_key_event(scr, &xev, event)))
                return status;
        }
        else if (xev.type == FocusIn)
        {
            XGrabPointer(scr->impl->disp, scr->impl->win, True,
                         PointerMotionMask, GrabModeAsync, GrabModeAsync,
                         scr->impl->win, None, CurrentTime);
        }
        else if (xev.type == FocusOut)
        {
            XUngrabPointer(scr->impl->disp, CurrentTime);
        }
    }

    return 0;
}

void get_mouse_pos(struct screen *scr, int *x, int *y)
{
    /*
     * Should I be calling XQueryPointer here instead of relying upon the last
     * motion notify event?
     */
    if (x)
        *x = mouse_x;
    if (y)
        *y = mouse_y;
}

void set_mouse_pos(struct screen *scr, int x, int y)
{
    mouse_x = x;
    mouse_y = y;

    XWarpPointer(scr->impl->disp, None, scr->impl->win, 0, 0, 0, 0, x, y);
    XSync(scr->impl->disp, False);
}

static int handle_key_event(struct screen *scr, XEvent *xev,
                            struct event *event)
{
    KeySym kc = xev->xkey.keycode;
    int binding;

    if ((binding = find_keybind(kc)))
    {
        event->type = EVENT_TYPE_KEY;
        event->key_event.keycode = kc;
        event->key_event.state = xev->type == KeyPress;
        event->key_event.binding = binding;

        return 1;
    }

    return 0;
}

static int handle_mouse_motion(struct screen *scr, XEvent *xev,
                               struct event *event)
{
    int new_x, new_y;

    new_x = xev->xmotion.x;
    new_y = xev->xmotion.y;

    event->type = EVENT_TYPE_MOUSEMOTION;
    event->mouse_event.x_pos = new_x;
    event->mouse_event.y_pos = new_y;
    event->mouse_event.x_rel = new_x - mouse_x;
    event->mouse_event.y_rel = new_y - mouse_y;

    mouse_x = new_x;
    mouse_y = new_y;

    return 1;
}

void config_default_bindings(struct screen *scr)
{
    Display *disp;

    disp = scr->impl->disp;
    cleanup_keybinds();

    bind_key(XKeysymToKeycode(disp, XK_w), KEY_FWD);
    bind_key(XKeysymToKeycode(disp, XK_s), KEY_BACK);
    bind_key(XKeysymToKeycode(disp, XK_a), KEY_LEFT);
    bind_key(XKeysymToKeycode(disp, XK_d), KEY_RIGHT);
    bind_key(XKeysymToKeycode(disp, XK_Page_Up), KEY_UP);
    bind_key(XKeysymToKeycode(disp, XK_Page_Down), KEY_DOWN);
    bind_key(XKeysymToKeycode(disp, XK_Control_L), KEY_FIRE);
    bind_key(XKeysymToKeycode(disp, XK_e), KEY_USE);
}
