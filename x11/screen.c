/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <err.h>
#include <time.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "screen.h"
#include "screen_impl.h"

static int init_screen_impl(struct screen_impl *impl, unsigned w, unsigned h);
static int get_shift(int mask);
static void wait_for_expose(struct screen_impl *impl);
static void create_cursor(struct screen_impl *impl);

int create_screen_impl(struct screen *scr, int w, int h)
{
    struct screen_impl *impl =
        (struct screen_impl*)malloc(sizeof(struct screen_impl));

    if (!impl)
        errx(1, "Unable to allocate rendering context\n");
    init_screen_impl(impl, w, h);
    scr->impl = impl;
    scr->screen_w = w;
    scr->screen_h = h;
    scr->frame_buffer = (color_t*)malloc(sizeof(color_t) * scr->screen_w *
                                         scr->screen_h);
    return 0;
}

static int init_screen_impl(struct screen_impl *impl, unsigned w, unsigned h)
{
    Visual *vis;
    XVisualInfo vinfo;
    XSetWindowAttributes win_attr;

    impl->pixel_size = sizeof(color_t);
    impl->scanline_len = w * impl->pixel_size;
    
    impl->disp = XOpenDisplay(NULL);
    impl->screen = XDefaultScreen(impl->disp);

    /* TODO: try a few other formats if there's a failure */
    impl->bpp = 24;
    if (!XMatchVisualInfo(impl->disp, impl->screen,
                          impl->bpp, TrueColor, &vinfo))
    {
        printf("Unable to obtain 24-bit TrueColor visual\n");
        return -1;
    }

    vis = vinfo.visual;
    impl->win = XCreateWindow(impl->disp, XDefaultRootWindow(impl->disp), 0, 0,
                              w, h, 0, impl->bpp, InputOutput, vis, 0,
                              &win_attr);

    impl->gc = XCreateGC(impl->disp, impl->win, 0, 0);
    XSelectInput(impl->disp, impl->win, EVENT_MASK_X11);
    XMapRaised(impl->disp, impl->win);

    impl->red_mask = (color_t)vinfo.red_mask;
    impl->green_mask = (color_t)vinfo.green_mask;
    impl->blue_mask = (color_t)vinfo.blue_mask;
    impl->red_shift = get_shift(impl->red_mask);
    impl->green_shift = get_shift(impl->green_mask);
    impl->blue_shift = get_shift(impl->blue_mask);

    /* Reserve the remaining 8 bits for the alpha channel */
    impl->alpha_mask = ~(impl->red_mask | impl->green_mask | impl->blue_mask);
    impl->alpha_shift = get_shift(impl->alpha_mask);

    char *img_data = (char*)malloc(sizeof(char) * impl->scanline_len * h);
    impl->img = XCreateImage(impl->disp, vis, impl->bpp, ZPixmap, 0, img_data,
                             w, h, 8 * impl->pixel_size, impl->scanline_len);
    if (!impl->img)
        err(1, "unable to allocate frame buffer\n");

    create_cursor(impl);

    wait_for_expose(impl);

    return 0;
}

static void wait_for_expose(struct screen_impl *impl)
{
    for (;;)
    {
        XEvent event;
        XNextEvent(impl->disp, &event);
        if (event.type == Expose)
            return;
    }
}

#include "transparent_cursor.xbm"

static void create_cursor(struct screen_impl *impl)
{
    XColor black_exact, black_screen;
    Pixmap cursor_xpm;

    if (!XLookupColor(impl->disp, XDefaultColormap(impl->disp, impl->screen),
                      "black", &black_exact, &black_screen))
    {
        fprintf(stderr, "unable to lookup color \"black\"\n");
        exit(1);
    }
    cursor_xpm = XCreateBitmapFromData(impl->disp, impl->win,
                                       transparent_cursor_bits,
                                       transparent_cursor_width,
                                       transparent_cursor_height);
    if (cursor_xpm == None)
    {
        fprintf(stderr, "unable to create pixmap cursor\n");
        exit(1);
    }
    impl->cursor = XCreatePixmapCursor(impl->disp, cursor_xpm,
                                      cursor_xpm, &black_screen,
                                      &black_screen, 0, 0);
    XFreePixmap(impl->disp, cursor_xpm);
    XDefineCursor(impl->disp, impl->win, impl->cursor);
}

static int get_shift(int mask)
{
    unsigned i, n_bits;
    for (i = 1, n_bits = 0; i && !(mask & i); i <<= 1, n_bits++)
        ;
    return n_bits;
}

color_t color_pack_rgba(struct screen const *scr, color_t r, color_t g, color_t b, color_t a)
{
    return ((r << scr->impl->red_shift) & scr->impl->red_mask) |
        ((g << scr->impl->green_shift) & scr->impl->green_mask) |
        ((b << scr->impl->blue_shift) & scr->impl->blue_mask) |
        ((a << scr->impl->alpha_shift) & scr->impl->alpha_mask);
}

color_t color_pack_rgb(struct screen const *scr, color_t r, color_t g, color_t b)
{
    return color_pack_rgba(scr, r, g, b, 0xff);
}

color_t color_red(struct screen const *scr, color_t c)
{
    return (c & scr->impl->red_mask) >> scr->impl->red_shift;
}

color_t color_blue(struct screen const *scr, color_t c)
{
    return (c & scr->impl->blue_mask) >> scr->impl->blue_shift;
}

color_t color_green(struct screen const *scr, color_t c)
{
    return (c & scr->impl->green_mask) >> scr->impl->green_shift;
}

color_t color_alpha(struct screen const *scr, color_t c)
{
    return (c & scr->impl->alpha_mask) >> scr->impl->alpha_shift;
}

void flip_screen(struct screen *scr)
{
    color_t pix_src;
    int col, row;
    char *pix_dst;
    struct screen_impl *impl;

    impl = scr->impl;

    for (row = 0; row < scr->screen_h; row++)
    {
        for (col = 0; col < scr->screen_w; col++)
        {
            pix_dst = impl->img->data + row * impl->scanline_len +
                col * impl->pixel_size;
            pix_src = scr->frame_buffer[row * scr->screen_w + col];
            *((color_t*)pix_dst) = color_pack_rgb(scr, color_red(scr, pix_src),
                                                  color_green(scr, pix_src),
                                                  color_blue(scr, pix_src));
        }
    }

    XPutImage(impl->disp, impl->win, impl->gc, impl->img, 0, 0, 0, 0,
              scr->screen_w, scr->screen_h);
}

void destroy_screen_impl(struct screen *scr)
{
    struct screen_impl *impl;

    impl = scr->impl;
    XFreeCursor(impl->disp, impl->cursor);
    XDestroyImage(impl->img);
    XFreeGC(impl->disp, impl->gc);
    XDestroyWindow(impl->disp, impl->win);
    XCloseDisplay(impl->disp);
    /* TODO: free the rest of the scr structure */
}

void clear_screen(struct screen *scr, color_t color)
{
    // FIXME: using memset is not viable because it casts the value to a char
    unsigned i, n_pixels = scr->screen_w * scr->screen_h;
    for (i = 0; i < n_pixels; i++)
        scr->frame_buffer[i] = color;
}

unsigned get_ticks(void)
{
    clock_t ticks;

    ticks = clock();
    /* TODO:
     *      There's going to be some error if CLOCKS_PER_SEC is not evenly
     *      divisible by 1000.  On my PC, it's actually 1000000l.
     *      IDK if that's big enough of an error to be concerned about, but it
     *      might be a good idea to use double as an intermediate type here.
     */
    return (unsigned)((ticks * 1000) / CLOCKS_PER_SEC);
}
