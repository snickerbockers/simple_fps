/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include "tool/mklvl/mklvl.h"
#include "bsp.h"
#include "geo.h"
#include "trace.h"
#include "game_map.h"

extern FILE *yyin;

static size_t n_types;
static struct type **types;

struct type *tp_str, *tp_array, *tp_seq, *tp_int, *tp_func, *tp_real, *tp_pt2;

struct attr *attr_map_find(struct attr_map *am, char const tag[TAG_LEN])
{
    int i;
    for (i = 0; i < am->n_attrs; i++)
    {
        if (strcmp(tag, am->attrs[i].tag) == 0)
            return am->attrs + i;
    }

    return NULL;
}

struct object *attr_map_find_obj(struct attr_map *am, char const tag[TAG_LEN])
{
    struct attr *attr;

    attr = attr_map_find(am, tag);    
    if (attr)
        return attr->obj;
    return NULL;
}

void attr_map_init(struct attr_map *am)
{
    memset(am, 0, sizeof(struct attr_map));
}

void attr_map_cleanup(struct attr_map *am)
{
    int i;
    for (i = 0; i < am->n_attrs; i++)
        if (am->attrs[i].obj)
            obj_free(am->attrs[i].obj);
    free(am->attrs);
}

void entity_map_init(struct entity_map *em)
{
    memset(em, 0, sizeof(struct entity_map));
}

void entity_map_cleanup(struct entity_map *em)
{
    int i;
    for (i = 0; i < em->n_ents; i++)
        attr_map_cleanup(&em->ents[i].am);
    free(em->ents);
}

struct entity *entity_map_find(struct entity_map *em, char const tag[TAG_LEN])
{
    int i;
    for (i = 0; i < em->n_ents; i++)
    {
        if (strcmp(tag, em->ents[i].tag) == 0)
            return em->ents + i;
    }

    return NULL;
}

void entity_map_add(struct entity_map *em, struct entity *ent)
{
    assert(!entity_map_find(em, ent->tag));

    em->ents = (struct entity*)realloc(em->ents, sizeof(struct entity) * ++em->n_ents);
    memcpy(em->ents+em->n_ents-1, ent, sizeof(struct entity));
}

void attr_map_add(struct attr_map *am, struct attr *attr)
{
    assert(!attr_map_find(am, attr->tag));

    am->attrs = (struct attr*)realloc(am->attrs, sizeof(struct attr) * ++am->n_attrs);
    memcpy(am->attrs+am->n_attrs-1, attr, sizeof(struct attr));
}

void tp_add(struct type *tp)
{
    types = realloc(types, sizeof(struct type*) * ++n_types);
    types[n_types - 1] = tp;
}

struct type* tp_find(const char *name)
{
    int idx;
    for (idx = 0; idx < n_types; idx++)
        if (strcmp(name, types[idx]->name) == 0)
            return types[idx];
    return NULL;
}

struct type* tp_array_of(struct type const *encap)
{
    struct type *tp;
    char tp_name[TAG_LEN];

    snprintf(tp_name, TAG_LEN, "%s[]", encap->name);
    tp_name[TAG_LEN-1] = '\0';/* maybe unnecessary? */

    if ((tp = tp_find(tp_name)))
        return tp;

    tp = calloc(1, sizeof(struct type));

    strncpy(tp->name, tp_name, TAG_LEN);
    tp->name[TAG_LEN-1] = '\0';
    tp->sz = sizeof(struct lvl_array);
    tp->tp_encap = encap;
    tp->encap_len = -1;
    tp->op_move = array_move;
    tp->init = array_init;
    tp->op_index = array_index;
    tp->op_len = array_len;
    tp->op_code = array_as_code;
    tp->op_dtor = array_dtor;
    tp->tp_canon_name_ = array_canon_name;
    tp->tp_as_code_ = array_tp_as_code;

    types = realloc(types, ++n_types * sizeof(struct type*));
    types[n_types - 1] = tp;

    return tp;
}

void array_tp_as_code(struct type const *tp, char const *var_name, char *str_out, int len)
{
    char base_tp[TAG_LEN];
    char dims[TAG_LEN], dims_rev[TAG_LEN];
    char *encap_name_end;
    char *str_in;
    int dims_rev_len = TAG_LEN;
    char *dims_wrt = dims_rev;

    strncpy(base_tp, tp->name, TAG_LEN);
    base_tp[TAG_LEN - 1] = '\0';

    encap_name_end = strchr(base_tp, '[');

    if (!encap_name_end)
    {
        lvl_err_msg("Error: Array type \"%s\" has no dimensions\n", base_tp);
        abort();
    }

    strncpy(dims, encap_name_end, TAG_LEN);
    dims[TAG_LEN-1] = '\0';
    *encap_name_end = '\0';

    while (dims_rev_len > 0 && (str_in = strrchr(dims, '[')))
    {
        strncpy(dims_wrt, str_in, dims_rev_len);
        dims_wrt[dims_rev_len - 1] = '\0';
        dims_rev_len -= strlen(str_in);
        dims_wrt += strlen(str_in);
        *str_in = '\0';
    }

    snprintf(str_out, len, "%s %s%s", base_tp, var_name, dims_rev);
}

void array_canon_name(struct type const *tp, char *str_out, int len)
{
    char tp_name[TAG_LEN];
    char *encap_name_end;
    char *str_in;

    strncpy(tp_name, tp->name, TAG_LEN);
    tp_name[TAG_LEN - 1] = '\0';

    encap_name_end = strchr(tp_name, '[');

    if (!encap_name_end)
    {
        /* Meh, fuck it */
        strncpy(str_out, tp_name, len);
        return;
    }

    *encap_name_end = '\0';
    strncpy(str_out, tp_name, len);
    str_out[len - 1] = '\0';

    len -= strlen(tp_name);
    str_out += strlen(tp_name);

    if (len == 0)
        return;
    assert(len > 0);

    *encap_name_end = '[';

    while (len > 0 && (str_in = strrchr(tp_name, '[')))
    {
        strncpy(str_out, str_in, len);
        str_out[len - 1] = '\0';
        len -= strlen(str_in);
        str_out += strlen(str_in);
        *str_in = '\0';
    }
}

/*
 * TODO: This gets the row/column major reversed
 * when deriving the name of multidimensional arrays
 */
struct type* tp_fixed_array_of(struct type const *encap, int len)
{
    struct type *tp;
    char tp_name[TAG_LEN];

    snprintf(tp_name, TAG_LEN, "%s[%d]", encap->name, len);
    tp_name[TAG_LEN-1] = '\0';/* maybe unnecessary? */

    if ((tp = tp_find(tp_name)))
        return tp;

    tp = calloc(1, sizeof(struct type));

    strncpy(tp->name, tp_name, TAG_LEN);
    tp->name[TAG_LEN-1] = '\0';
    tp->sz = sizeof(struct lvl_array);
    tp->tp_encap = encap;
    tp->encap_len = len;
    tp->op_move = array_move;
    tp->init = array_init;
    tp->op_index = array_index;
    tp->op_len = array_len;
    tp->op_code = array_as_code;
    tp->op_dtor = array_dtor;
    tp->tp_canon_name_ = array_canon_name;
    tp->tp_as_code_ = array_tp_as_code;

    types = realloc(types, ++n_types * sizeof(struct type*));
    types[n_types - 1] = tp;

    return tp;
}

struct object *obj_alloc(struct type const *tp)
{
    struct object *obj;

    assert(tp->sz);
    if (!(obj = (struct object*)malloc(tp->sz)))
    {
        lvl_err_msg("failed alloc");
        return NULL;
    }

    obj->tp = tp;
    return obj;
}

void obj_free(struct object *obj)
{
    assert(obj);

    if (obj->tp->op_dtor)
        obj->tp->op_dtor(obj);
    free(obj);
}

int obj_init(struct object *obj)
{
    if (obj->tp->init)
        return obj->tp->init(obj);

    return 0;
}

int obj_move(struct object **output, struct object *lhs,
                      struct object *rhs)
{
    if (lhs->tp->op_move)
        return lhs->tp->op_move(output, lhs, rhs);

    return -1;
}

void obj_str(struct object *obj, char *str_out, int len)
{
    if (obj->tp->op_str)
        obj->tp->op_str(obj, str_out, len);
    else
        memset(str_out, 0, sizeof(char) * len);
}

void obj_code(struct object *obj, char *str_out, int len)
{
    if (obj->tp->op_code)
        obj->tp->op_code(obj, str_out, len);
    else
        memset(str_out, 0, sizeof(char) * len);
}

int obj_call(struct object *obj, struct object **output, struct object *args)
{
    if (obj->tp->op_call)
        return obj->tp->op_call(obj, output, args);

    return -1;
}

int obj_len(struct object *obj, int *len)
{
    if (obj->tp->op_len)
    {
        *len = obj->tp->op_len(obj);
        return 0;
    }
    return -1;
}

struct object *obj_index(struct object *obj, int idx)
{
    if (obj->tp->op_index)
        return obj->tp->op_index(obj, idx);
    return NULL;
}

struct object *obj_negative(struct object *obj)
{
    if (obj->tp->op_negative)
        return obj->tp->op_negative(obj);
    return NULL;
}

int obj_raw_int(struct object *obj, int *out)
{
    if (obj->tp->raw_int)
        return obj->tp->raw_int(obj, out);
    return -1;
}

int obj_raw_real(struct object *obj, real_t *out)
{
    if (obj->tp->raw_real)
        return obj->tp->raw_real(obj, out);
    return -1;
}

int func_init(struct object *obj)
{
    struct lvl_func *as_func;

    if (obj->tp != tp_func)
        lvl_err_msg("unexpected type (was expecting function)");
    as_func = (struct lvl_func*)obj;
    as_func->impl = NULL;

    return 0;
}

void func_set_impl(struct object *obj, int (*impl)(struct object**, struct object*))
{
    struct lvl_func *as_func;
    if (obj->tp != tp_func)
        lvl_err_msg("unexpected type (was expecting function)");
    as_func = (struct lvl_func*)obj;
    as_func->impl = impl;
}

int func_call(struct object *obj, struct object **output, struct object *args)
{
    struct lvl_func *as_func;

    if (obj->tp != tp_func)
        lvl_err_msg("unexpected type (was expecting function)");
    as_func = (struct lvl_func*)obj;
    return as_func->impl(output, args);
}

void str_set(struct object *obj, char const *txt)
{
    struct lvl_str *as_str;

    if (obj->tp != tp_str)
        lvl_err_msg("unexpected type (was expecting str)");

    as_str = (struct lvl_str*)obj;
    strncpy(as_str->txt, txt, STR_LEN);
    as_str->txt[STR_LEN-1] = '\0';
}

int str_init(struct object *obj)
{
    struct lvl_str *str;

    if (obj->tp != tp_str)
        lvl_err_msg("unexpected type (was expecting str)");

    str = (struct lvl_str*)obj;
    memset(str->txt, 0, sizeof(char) * STR_LEN);

    return 0;
}

int str_move(struct object **output, struct object *lhs, struct object *rhs)
{
    struct lvl_str *lhs_str;
    struct lvl_str *rhs_str;

    assert(lhs->tp == tp_str);

    if (rhs->tp != tp_str)
        return -1;

    lhs_str = (struct lvl_str*)lhs;
    rhs_str = (struct lvl_str*)rhs;

    strncpy(lhs_str->txt, rhs_str->txt, STR_LEN);
    lhs_str->txt[STR_LEN-1] = '\0';

    memset(rhs_str->txt, 0, sizeof(char) * STR_LEN);

    if (output)
        *output = lhs;

    return 0;
}

void str_as_str(struct object *obj, char *str_out, int len)
{
    struct lvl_str *obj_as_str;

    obj_as_str = (struct lvl_str*)obj;

    strncpy(str_out, obj_as_str->txt, len);
    str_out[len - 1] = '\0';
}

void str_as_code(struct object *obj, char *str_out, int len)
{
    struct lvl_str *obj_as_str;

    obj_as_str = (struct lvl_str*)obj;

    snprintf(str_out, len, "\"%s\"", obj_as_str->txt);
    str_out[len - 1] = '\0';
}

int lvl_int_move(struct object **output, struct object *lhs, struct object *rhs)
{
    struct lvl_int *lhs_int;

    lhs_int = (struct lvl_int*)lhs;

    if (rhs->tp->raw_int)
    {
        obj_raw_int(rhs, &lhs_int->val);
        return 0;
    }

    return -1;
}

void lvl_int_as_code(struct object *obj, char *str_out, int len)
{
    struct lvl_int *as_int;

    as_int = (struct lvl_int*)obj;
    snprintf(str_out, len, "%d", as_int->val);
    str_out[len-1]='\0';
}

int lvl_int_raw_int(struct object *obj, int *out)
{
    *out = ((struct lvl_int*)obj)->val;
    return 0;
}

int lvl_int_raw_real(struct object *obj, real_t *out)
{
    struct lvl_int *as_int;

    if (obj->tp != tp_int)
    {
        lvl_err_msg("lvl_int_raw_real: was expecting an int\n");
        exit(-1);
    }

    as_int = (struct lvl_int*)obj;
    *out = real_from_int(as_int->val);
    return 0;
}

void lvl_int_set(struct object *obj, int val)
{
    struct lvl_int *as_int;

    as_int = (struct lvl_int*)obj;
    as_int->val = val;
}

struct lvl_real *lvl_real_from_obj(struct object *obj)
{
    if (obj->tp == tp_real)
        return (struct lvl_real*)obj;
    return NULL;
}

struct object *lvl_int_negative(struct object *obj)
{
    struct object *ret = obj_alloc(tp_int);
    if (!ret)
    {
        lvl_err_msg("Failed allocation\n");
        return NULL;
    }
    obj_init(ret);
    ((struct lvl_int*)ret)->val = -((struct lvl_int*)obj)->val;
    return ret;
}

int lvl_int_mult(struct object *lhs, struct object **output,
                 struct object *rhs)
{
    struct object *dst;
    struct lvl_int *lhs_as_int;
    int rhs_ival;

    if (lhs->tp != tp_int)
    {
        lvl_err_msg("lvl_int_mult: called on an object that is not a int\n");
        return -1;
    }
    lhs_as_int = (struct lvl_int*)lhs;
    obj_raw_int(rhs, &rhs_ival);
    dst = obj_alloc(tp_int);
    obj_init(dst);
    lvl_int_set(dst, lhs_as_int->val * rhs_ival);
    *output = dst;
    return 0;
}

int lvl_int_div(struct object *lhs, struct object **output,
                struct object *rhs)
{
    struct object *dst;
    struct lvl_int *lhs_as_int;
    int rhs_ival;

    if (lhs->tp != tp_int)
    {
        lvl_err_msg("lvl_int_div: called on an object that is not a int\n");
        return -1;
    }
    lhs_as_int = (struct lvl_int*)lhs;
    obj_raw_int(rhs, &rhs_ival);
    dst = obj_alloc(tp_int);
    obj_init(dst);
    lvl_int_set(dst, lhs_as_int->val / rhs_ival);
    *output = dst;
    return 0;
}

int lvl_int_add(struct object *lhs, struct object **output,
                struct object *rhs)
{
    struct object *dst;
    struct lvl_int *lhs_as_int;
    int rhs_ival;

    if (lhs->tp != tp_int)
    {
        lvl_err_msg("lvl_int_add: called on an object that is not a int\n");
        return -1;
    }
    lhs_as_int = (struct lvl_int*)lhs;
    obj_raw_int(rhs, &rhs_ival);
    dst = obj_alloc(tp_int);
    obj_init(dst);
    lvl_int_set(dst, lhs_as_int->val + rhs_ival);
    *output = dst;
    return 0;
}

int lvl_int_sub(struct object *lhs, struct object **output,
                struct object *rhs)
{
    struct object *dst;
    struct lvl_int *lhs_as_int;
    int rhs_ival;

    if (lhs->tp != tp_int)
    {
        lvl_err_msg("lvl_int_add: called on an object that is not a int\n");
        return -1;
    }
    lhs_as_int = (struct lvl_int*)lhs;
    obj_raw_int(rhs, &rhs_ival);
    dst = obj_alloc(tp_int);
    obj_init(dst);
    lvl_int_set(dst, lhs_as_int->val - rhs_ival);
    *output = dst;
    return 0;
}

int lvl_real_move(struct object **output, struct object *lhs, struct object *rhs)
{
    struct lvl_real *lhs_real;

    lhs_real = lvl_real_from_obj(lhs);
    if (!lhs_real)
    {
        lvl_err_msg("HOLY SHIT\n");
        return -1;
    }

    if (rhs->tp->raw_real)
        return rhs->tp->raw_real(rhs, &lhs_real->val);

    /*
     * should I consider trying raw_int at this
     * point and casting to real_t?
     */

    return -1;
}

void lvl_real_as_code(struct object *obj, char *str_out, int len)
{
    struct lvl_real* as_real;

    as_real = lvl_real_from_obj(obj);

    if (!as_real)
    {
        lvl_err_msg("need real error handling");
        exit(1);
    }

    snprintf(str_out, len, "%f", real_to_double(as_real->val));
    str_out[len - 1] = '\0';
}

int lvl_real_raw_real(struct object *obj, real_t *out)
{
    struct lvl_real* as_real;

    as_real = lvl_real_from_obj(obj);

    if (!as_real)
    {
        lvl_err_msg("lvl_real_raw_real called on object that is not a real");
        return -1;
    }

    *out = as_real->val;
    return 0;
}

int lvl_real_raw_int(struct object *obj, int *out)
{
    struct lvl_real *as_real;

    if (obj->tp != tp_real)
    {
        lvl_err_msg("lvl_real_raw_int: called on object that is not a real\n");
        return -1;
    }

    as_real = (struct lvl_real*)obj;

    *out = real_to_int(as_real->val);
    return 0;
}

struct object *lvl_real_negative(struct object *obj)
{
    struct object *ret = obj_alloc(tp_real);
    if (!ret)
    {
        lvl_err_msg("Failed allocation\n");
        return NULL;
    }
    obj_init(ret);
    ((struct lvl_real*)ret)->val = real_neg(((struct lvl_real*)obj)->val);
    return ret;
}

void lvl_real_set(struct object *obj, real_t val)
{
    struct lvl_real* as_real;

    as_real = lvl_real_from_obj(obj);

    if (!as_real)
    {
        lvl_err_msg("lvl_real_set: called on object that is not a real");
        abort();
    }

    as_real->val = val;
}

int lvl_real_mult(struct object *lhs, struct object **output,
                  struct object *rhs)
{
    struct object *dst;
    struct lvl_real *lhs_as_real;
    real_t rhs_real_val;

    lhs_as_real = lvl_real_from_obj(lhs);
    if (!lhs_as_real)
    {
        lvl_err_msg("lvl_real_mult: called on an object that is not a real\n");
        return -1;
    }

    obj_raw_real(rhs, &rhs_real_val);
    dst = obj_alloc(tp_real);
    obj_init(dst);
    lvl_real_set(dst, real_mul(lhs_as_real->val, rhs_real_val));
    *output = dst;
    return 0;
}

int lvl_real_div(struct object *lhs, struct object **output,
                 struct object *rhs)
{
    struct object *dst;
    struct lvl_real *lhs_as_real;
    real_t rhs_real_val;

    lhs_as_real = lvl_real_from_obj(lhs);
    if (!lhs_as_real)
    {
        lvl_err_msg("lvl_real_mult: called on an object that is not a real\n");
        return -1;
    }

    obj_raw_real(rhs, &rhs_real_val);
    dst = obj_alloc(tp_real);
    obj_init(dst);
    lvl_real_set(dst, real_div(lhs_as_real->val, rhs_real_val));
    *output = dst;
    return 0;
}

int lvl_real_add(struct object *lhs, struct object **output,
                  struct object *rhs)
{
    struct object *dst;
    struct lvl_real *lhs_as_real;
    real_t rhs_real_val;

    lhs_as_real = lvl_real_from_obj(lhs);
    if (!lhs_as_real)
    {
        lvl_err_msg("lvl_real_add: called on an object that is not a real\n");
        return -1;
    }

    obj_raw_real(rhs, &rhs_real_val);
    dst = obj_alloc(tp_real);
    obj_init(dst);
    lvl_real_set(dst, real_add(lhs_as_real->val, rhs_real_val));
    *output = dst;
    return 0;
}

int lvl_real_sub(struct object *lhs, struct object **output,
                 struct object *rhs)
{
    struct object *dst;
    struct lvl_real *lhs_as_real;
    real_t rhs_real_val;

    lhs_as_real = lvl_real_from_obj(lhs);
    if (!lhs_as_real)
    {
        lvl_err_msg("lvl_real_sub: called on an object that is not a real\n");
        return -1;
    }

    obj_raw_real(rhs, &rhs_real_val);
    dst = obj_alloc(tp_real);
    obj_init(dst);
    lvl_real_set(dst, real_sub(lhs_as_real->val, rhs_real_val));
    *output = dst;
    return 0;
}

int lvl_pt2_init(struct object *obj)
{
    struct lvl_pt2 *as_pt2;

    if (obj->tp != tp_pt2)
    {
        lvl_err_msg("pt2_init: was expecting a pt2");
        return -1;
    }
    as_pt2 = (struct lvl_pt2*)obj;
    as_pt2->val[0] = as_pt2->val[1] = REAL_ZERO;

    return 0;
}

void lvl_pt2_as_code(struct object *obj, char *str_out, int len)
{
    struct lvl_pt2 *as_pt2;

    if (obj->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_as_code: was expecting a pt2");
        abort();
    }
    as_pt2 = (struct lvl_pt2*)obj;
    snprintf(str_out, len, "mk_pt2(%f, %f)",
             real_to_double(as_pt2->val[0]),
             real_to_double(as_pt2->val[1]));
    str_out[len - 1] = '\0';
}

void lvl_pt2_set(struct object *obj, real_t const pt_in[2])
{
    struct lvl_pt2 *as_pt2;

    if (obj->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_set: was expecting a pt2");
        abort();
    }
    as_pt2 = (struct lvl_pt2*)obj;
    as_pt2->val[0] = pt_in[0];
    as_pt2->val[1] = pt_in[1];
}

int lvl_pt2_move(struct object **output, struct object *lhs, struct object *rhs)
{
    struct lvl_pt2 *lhs_as_pt2, *rhs_as_pt2;

    if (lhs->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_move: was expecting a pt2 on lhs");
        return -1;
    }
    if (rhs->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_move: was expecting a pt2 on rhs");
        return -1;
    }
    lhs_as_pt2 = (struct lvl_pt2*)lhs;
    rhs_as_pt2 = (struct lvl_pt2*)rhs;
    lhs_as_pt2->val[0] = rhs_as_pt2->val[0];
    lhs_as_pt2->val[1] = rhs_as_pt2->val[1];
    rhs_as_pt2->val[0] = rhs_as_pt2->val[1] = REAL_ZERO;

    return 0;
}

/*
 * TODO: Why the fuck does this exist !?
 */
int lvl_pt2_mult(struct object *lhs, struct object **output,
                 struct object *rhs)
{
    int err = 0;
    struct object *dst;
    struct lvl_pt2 *lhs_as_pt2, *dst_as_pt2;
    real_t rhs_real_val;

    if (lhs->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_mult: was expecting a pt2 on lhs");
        return -1;
    }
    lhs_as_pt2 = (struct lvl_pt2*)lhs;
    if ((err = obj_raw_real(rhs, &rhs_real_val)) != 0)
    {
        lvl_err_msg("lvl_pt2_mult: failed to convert rhs to real\n");
        return err;
    }

    dst = obj_alloc(tp_pt2);
    obj_init(dst);

    dst_as_pt2 = (struct lvl_pt2*)dst;
    dst_as_pt2->val[0] = real_mul(lhs_as_pt2->val[0], rhs_real_val);
    dst_as_pt2->val[1] = real_mul(lhs_as_pt2->val[1], rhs_real_val);

    *output = dst;
    return err;
}

int lvl_pt2_add(struct object *lhs, struct object **output,
                struct object *rhs)
{
    int err = 0;
    struct object *dst;
    struct lvl_pt2 *lhs_as_pt2, *rhs_as_pt2, *dst_as_pt2;

    if (lhs->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_mult: was expecting a pt2 on lhs");
        return -1;
    }
    if (rhs->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_mult: cannot add a %s to a pt2", rhs->tp->name);
        return -1;
    }
    lhs_as_pt2 = (struct lvl_pt2*)lhs;
    rhs_as_pt2 = (struct lvl_pt2*)rhs;

    if (!(dst = obj_alloc(tp_pt2)))
    {
        lvl_err_msg("failed to alloc new tp_pt2");
        return -1;
    }
    obj_init(dst);
    dst_as_pt2 = (struct lvl_pt2*)dst;
    pt2_add(dst_as_pt2->val, lhs_as_pt2->val, rhs_as_pt2->val);
    *output = dst;
    return 0;
}

int lvl_pt2_sub(struct object *lhs, struct object **output,
                struct object *rhs)
{
    int err = 0;
    struct object *dst;
    struct lvl_pt2 *lhs_as_pt2, *rhs_as_pt2, *dst_as_pt2;

    if (lhs->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_mult: was expecting a pt2 on lhs");
        return -1;
    }
    if (rhs->tp != tp_pt2)
    {
        lvl_err_msg("lvl_pt2_mult: cannot subtrace a %s from a pt2", rhs->tp->name);
        return -1;
    }
    lhs_as_pt2 = (struct lvl_pt2*)lhs;
    rhs_as_pt2 = (struct lvl_pt2*)rhs;

    if (!(dst = obj_alloc(tp_pt2)))
    {
        lvl_err_msg("failed to alloc new tp_pt2");
        return -1;
    }
    obj_init(dst);
    dst_as_pt2 = (struct lvl_pt2*)dst;
    pt2_sub(dst_as_pt2->val, lhs_as_pt2->val, rhs_as_pt2->val);
    *output = dst;
    return 0;
}

int array_init(struct object *obj)
{
    struct lvl_array *as_array;

    /* if (obj->tp != tp_array) */
    /*     lvl_err_msg("unexpected type (was expecting array)"); */

    as_array = (struct lvl_array*)obj;

    as_array->len = 0;
    as_array->arr = NULL;

    return 0;
}

int array_move(struct object **output, struct object *lhs_in, struct object *rhs_in)
{
    int idx;
    struct lvl_array *lhs;
    struct lvl_seq *rhs;
    struct object *i_am_a_dumbass;
    struct object *src_obj;

    /* assert(lhs_in->tp == tp_array); */
    if (rhs_in->tp != tp_seq)
        return -1;

    lhs = (struct lvl_array*)lhs_in;
    rhs = (struct lvl_seq*)rhs_in;

    if ((lhs_in->tp->encap_len >= 0) && (lhs_in->tp->encap_len != rhs->len))
        return -1;

    array_clear(lhs_in);

    if (rhs->len == 0)
    {
        /* special  case, an empty array */
        lhs->arr = NULL;
        lhs->len = 0;
    }
    else
    {
        lhs->arr = (struct object**)calloc(rhs->len, sizeof(struct object*));
        lhs->len = rhs->len;
        for (idx = 0; idx < rhs->len; idx++)
        {
            lhs->arr[idx] = obj_alloc(lhs->obj.tp->tp_encap);
            obj_init(lhs->arr[idx]);
            src_obj = rhs->seq[idx];
            rhs->seq[idx] = NULL;
            if (obj_move(&i_am_a_dumbass, lhs->arr[idx], src_obj) < 0)
            {
                lvl_err_msg("Failed cast at index %d\n", idx);

                for (idx = 0; idx < lhs->len; idx++)
                    if (lhs->arr[idx])
                        obj_free(lhs->arr[idx]);

                return -1;
            }
            obj_free(src_obj);
        }
    }
    free(rhs->seq);
    rhs->len = 0;
    rhs->seq = NULL;

    *output = (struct object*)lhs;

    return 0;
}

struct object *array_index(struct object *obj, int idx)
{
    struct lvl_array *as_array;

    /* if (obj->tp != tp_array) */
    /* { */
    /*     lvl_err_msg("unexpected type (was expecting array)"); */
    /*     return NULL; */
    /* } */

    as_array = (struct lvl_array*)obj;

    return as_array->arr[idx];
}

int array_len(struct object *obj)
{
    struct lvl_array *as_array;

    /* if (obj->tp != tp_array) */
    /* { */
    /*     lvl_err_msg("unexpected type (was expecting array)"); */
    /*     return -1; */
    /* } */

    as_array = (struct lvl_array*)obj;

    return as_array->len;
}

int array_append(struct object *obj, struct object *new_elem)
{
    struct lvl_array *as_array;

    /* if (obj->tp != tp_array) */
    /* { */
    /*     lvl_err_msg("unexpected type (was expecting array)"); */
    /*     return -1; */
    /* } */

    as_array = (struct lvl_array*)obj;

    as_array->arr = realloc(as_array->arr, sizeof(struct object*) * ++as_array->len);
    as_array->arr[as_array->len - 1] = new_elem;

    return 0;
}

int array_prepend(struct object *obj, struct object *new_elem)
{
    struct lvl_array *as_array;

    /* if (obj->tp != tp_array) */
    /* { */
    /*     lvl_err_msg("unexpected type (was expecting array)"); */
    /*     return -1; */
    /* } */

    as_array = (struct lvl_array*)obj;

    as_array->arr = realloc(as_array->arr, sizeof(struct object*) * ++as_array->len);
    memmove(as_array->arr + 1, as_array->arr, sizeof(struct object*) * (as_array->len - 1));
    as_array->arr[0] = new_elem;

    return 0;
}

void array_as_code(struct object *obj, char *str_out, int len)
{
    int idx;
    int res;
    struct lvl_array *as_array;

    res = len;
    as_array = (struct lvl_array*)obj;

    memset(str_out, 0, len * sizeof(char));

    if (res > 1)
        str_out[len - res--] = '{';

    for (idx = 0; idx < as_array->len; idx++)
    {
        if (idx)
        {
            if (res > 1)
                str_out[len - res--] = ',';
        }

        if (res > 1)
            str_out[len - res--] = ' ';

        if (res > 1)
        {
            obj_code(as_array->arr[idx], str_out + (len - res), res);
            assert(!str_out[len - 1]);
            res = len - strlen(str_out);
        }
    }

    if (res > 1)
        str_out[len - res--] = ' ';
    if (res > 1)
        str_out[len - res--] = '}';

    /* just in case */
    str_out[len - 1] = '\0';
}

void array_dtor(struct object *obj)
{
    array_clear(obj);
}

void array_clear(struct object *obj)
{
    int i;
    struct lvl_array *as_array;

    as_array = (struct lvl_array*)obj;

    if (as_array->arr)
    {
        assert(as_array->len);
        for (i = 0; i < as_array->len; i++)
            obj_free(as_array->arr[i]);
        free(as_array->arr);
    }

    as_array->arr = NULL;
    as_array = 0;
}

int seq_init(struct object *obj)
{
    struct lvl_seq *as_seq;

    assert(obj->tp == tp_seq);
    as_seq = (struct lvl_seq*)obj;

    as_seq->len = 0;
    as_seq->seq = NULL;

    return 0;
}

struct object *seq_index(struct object *obj, int idx)
{
    struct lvl_seq *as_seq;

    assert(obj->tp == tp_seq);
    as_seq = (struct lvl_seq*)obj;

    assert(idx < as_seq->len && idx >= 0);

    return as_seq->seq[idx];
}

int seq_len(struct object *obj)
{
    struct lvl_seq *as_seq;

    assert(obj->tp == tp_seq);
    as_seq = (struct lvl_seq*)obj;

    return as_seq->len;
}

int seq_move(struct object **output, struct object *lhs, struct object *rhs)
{
    struct lvl_seq *lhs_seq, *rhs_seq;

    if (lhs->tp != tp_seq || rhs->tp != tp_seq)
    {
        lvl_err_msg("unexpected type (was expecting sequence)");
        return -1;
    }

    lhs_seq = (struct lvl_seq*)lhs;
    rhs_seq = (struct lvl_seq*)rhs;

    lhs_seq->len = rhs_seq->len;
    lhs_seq->seq = rhs_seq->seq;
    rhs_seq->len = 0;
    rhs_seq->seq = NULL;

    return 0;
}

int seq_append(struct object *obj, struct object *new_elem)
{
    struct lvl_seq *as_seq;

    if (obj->tp != tp_seq)
    {
        lvl_err_msg("unexpected type (was expecting sequence)");
        return -1;
    }

    as_seq = (struct lvl_seq*)obj;

    as_seq->seq = realloc(as_seq->seq, sizeof(struct object*) * ++as_seq->len);
    as_seq->seq[as_seq->len - 1] = new_elem;

    return 0;
}

int seq_prepend(struct object *obj, struct object *new_elem)
{
    struct lvl_seq *as_seq;

    if (obj->tp != tp_seq)
    {
        lvl_err_msg("unexpected type (was expecting sequence)");
        return -1;
    }

    as_seq = (struct lvl_seq*)obj;

    as_seq->seq = realloc(as_seq->seq, sizeof(struct object*) * ++as_seq->len);
    memmove(as_seq->seq + 1, as_seq->seq, sizeof(struct object*) * (as_seq->len - 1));
    as_seq->seq[0] = new_elem;

    return 0;
}

void seq_dtor(struct object *obj)
{
    int i;
    struct lvl_seq *as_seq;

    if (obj->tp != tp_seq)
        lvl_err_msg("unexpected type (was expecting sequence)");

    as_seq = (struct lvl_seq*)obj;

    if (as_seq->seq)
    {
        assert(as_seq->len);
        for (i = 0; i < as_seq->len; i++)
            if (as_seq->seq[i])
                obj_free(as_seq->seq[i]);

        free(as_seq->seq);
    }
    else
    {
        assert(!as_seq->len);
    }
}

char const* str_get(struct object *obj)
{
    struct lvl_str *as_str;

    if (obj->tp != tp_str)
        lvl_err_msg("unexpected type (was expecting str)");

    as_str = (struct lvl_str*)obj;

    return as_str->txt;
}

void tp_canon_name(struct type const *tp, char *str_out, int len)
{
    if (tp->tp_canon_name_)
        tp->tp_canon_name_(tp, str_out, len);
    else
    {
        strncpy(str_out, tp->name, len);
        str_out[len - 1] = '\0';
    }
}

void tp_as_code(struct type const *tp, char const *var_name, char *str_out, int len)
{
    char tp_canon[STR_LEN];

    if (tp->tp_as_code_)
        tp->tp_as_code_(tp, var_name, str_out, len);
    else
    {
        tp_canon_name(tp, tp_canon, STR_LEN);

        snprintf(str_out, len, "%s %s", tp_canon, var_name);
        str_out[len - 1] = '\0';
    }
}

void lvl_err_msg(char const *what, ...)
{
    va_list args;

    va_start(args, what);
    lvl_err_msg_v(what, args);
    va_end(args);
}

void lvl_err_msg_v(const char *what, va_list args)
{
    vfprintf(stderr, what, args);
}

void parser_cleanup(void)
{
    cleanup_types();
}

void cleanup_types(void)
{
    size_t i;

    if (types)
    {
        for (i = 0; i < n_types; i++)
            free(types[i]);
        free(types);
    }
    types = NULL;
    n_types = 0;
}

void entity_as_code(struct entity *ent, char* out, int len)
{
    int attr_idx;
    int chars_written;
    struct attr_map *am = &ent->am;
    char decl[TAG_LEN], obj_as_code[STR_LEN];

    chars_written = snprintf(out, len, "entity %s {\n", ent->tag);

    len -= chars_written;
    out += chars_written;
    if (len <= 1)
        return;

    for (attr_idx = 0; attr_idx < am->n_attrs; attr_idx++)
    {
        struct attr *attr = am->attrs + attr_idx;
        tp_as_code(attr->obj->tp, attr->tag, decl, TAG_LEN);

        if (attr->obj)
        {
            obj_code(attr->obj, obj_as_code, STR_LEN);
            chars_written = snprintf(out, len, "\t%s = %s;\n", decl, obj_as_code);
        }
        else if (attr->tp_expect)
        {
            chars_written = snprintf(out, len, "\t%s;\n", decl);
        }

        len -= chars_written;
        out += chars_written;
        if (len <= 1)
            return;
    }

    snprintf(out, len, "};\n");
    out[len-1] = '\0';
}

static int unity_impl(struct object **out, struct object *args)
{
    struct object *ret;

    ret = obj_alloc(tp_int);
    obj_init(ret);
    lvl_int_set(ret, 1);
    *out = ret;

    return 0;
}

static int identity_impl(struct object **out, struct object *args)
{
    struct object *ret;
    struct object *in;
    struct object *bollocks;

    if (seq_len(args) != 1)
    {
        lvl_err_msg("identity function called with %d args\n",  seq_len(args));
        return -1;
    }

    in = seq_index(args, 0);

    ret = obj_alloc(in->tp);
    obj_init(ret);
    obj_move(&bollocks, ret, in);

    *out = ret;
    return 0;
}

static int mk_pt2_impl(struct object **out, struct object *args)
{
    struct object *ret, *in1, *in2;
    real_t pt2_in[2];

    switch (seq_len(args))
    {
    case 0:
        pt2_in[0] = pt2_in[1] = REAL_ZERO;
        break;
    case 2:
        in1 = seq_index(args, 0);
        in2 = seq_index(args, 1);

        if (!in1 || obj_raw_real(in1, pt2_in) != 0)
        {
            lvl_err_msg("mk_pt2_impl: Error processing arg0\n");
            return -1;
        }
        if (!in2 || obj_raw_real(in2, pt2_in+1) != 0)
        {
            lvl_err_msg("mk_pt2_impl: Error processing arg1\n");
            return -1;
        }
        break;
    default:
        lvl_err_msg("mk_pt2 function called with %d args\n", seq_len(args));
    }

    ret = obj_alloc(tp_pt2);
    if (!ret)
    {
        lvl_err_msg("mk_pt2: unable to allocate pt2 object\n");
        return -1;
    }
    obj_init(ret);
    lvl_pt2_set(ret, pt2_in);
    *out = ret;

    return 0;
}

static int sqrt_impl(struct object **out, struct object *args)
{
    struct object *in_obj, *ret;
    int n_args;
    real_t real_val;

    if ((n_args = seq_len(args)) != 1)
    {
        lvl_err_msg("sqrt function called with %d args\n", n_args);
        return -1;
    }
    in_obj = seq_index(args, 0);
    if (!in_obj)
    {
        lvl_err_msg("unable to process arg 0 to sqrt\n");
        return -1;
    }
    if (obj_raw_real(in_obj, &real_val) != 0)
    {
        lvl_err_msg("unable to convert object to real\n");
        return -1;
    }

    real_val = real_sqrt(real_val);
    ret = obj_alloc(tp_real);
    if (!ret)
    {
        lvl_err_msg("unable to allocate real object\n");
        return -1;
    }
    obj_init(ret);
    lvl_real_set(ret, real_val);
    *out = ret;
    return 0;
}

static int sin_impl(struct object **out, struct object *args)
{
    struct object *in_obj, *ret;
    int n_args;
    real_t real_val;

    if ((n_args = seq_len(args)) != 1)
    {
        lvl_err_msg("sin function called with %d args\n", n_args);
        return -1;
    }
    in_obj = seq_index(args, 0);
    if (!in_obj)
    {
        lvl_err_msg("unable to process arg 0 to sin\n");
        return -1;
    }
    if (obj_raw_real(in_obj, &real_val) != 0)
    {
        lvl_err_msg("unable to convert object to real\n");
        return -1;
    }

    real_val = real_sin(real_val);
    ret = obj_alloc(tp_real);
    if (!ret)
    {
        lvl_err_msg("unable to allocate real object\n");
        return -1;
    }
    obj_init(ret);
    lvl_real_set(ret, real_val);
    *out = ret;
    return 0;
}

static int cos_impl(struct object **out, struct object *args)
{
    struct object *in_obj, *ret;
    int n_args;
    real_t real_val;

    if ((n_args = seq_len(args)) != 1)
    {
        lvl_err_msg("cos function called with %d args\n", n_args);
        return -1;
    }
    in_obj = seq_index(args, 0);
    if (!in_obj)
    {
        lvl_err_msg("unable to process arg 0 to cos\n");
        return -1;
    }
    if (obj_raw_real(in_obj, &real_val) != 0)
    {
        lvl_err_msg("unable to convert object to real\n");
        return -1;
    }

    real_val = real_cos(real_val);
    ret = obj_alloc(tp_real);
    if (!ret)
    {
        lvl_err_msg("unable to allocate real object\n");
        return -1;
    }
    obj_init(ret);
    lvl_real_set(ret, real_val);
    *out = ret;
    return 0;
}

static int tan_impl(struct object **out, struct object *args)
{
    struct object *in_obj, *ret;
    int n_args;
    real_t real_val;

    if ((n_args = seq_len(args)) != 1)
    {
        lvl_err_msg("tan function called with %d args\n", n_args);
        return -1;
    }
    in_obj = seq_index(args, 0);
    if (!in_obj)
    {
        lvl_err_msg("unable to process arg 0 to tan\n");
        return -1;
    }
    if (obj_raw_real(in_obj, &real_val) != 0)
    {
        lvl_err_msg("unable to convert object to real\n");
        return -1;
    }

    real_val = real_tan(real_val);
    ret = obj_alloc(tp_real);
    if (!ret)
    {
        lvl_err_msg("unable to allocate real object\n");
        return -1;
    }
    obj_init(ret);
    lvl_real_set(ret, real_val);
    *out = ret;
    return 0;
}

static int deg2rad_impl(struct object **out, struct object *args)
{
    struct object *in_obj, *ret;
    int n_args;
    real_t real_val;

    if ((n_args = seq_len(args)) != 1)
    {
        lvl_err_msg("deg2rad function called with %d args\n", n_args);
        return -1;
    }
    in_obj = seq_index(args, 0);
    if (!in_obj)
    {
        lvl_err_msg("unable to process arg 0 to deg2rad\n");
        return -1;
    }
    if (obj_raw_real(in_obj, &real_val) != 0)
    {
        lvl_err_msg("unable to convert object to real\n");
        return -1;
    }

    real_val = real_mul(real_val, REAL_RADS_PER_DEG);
    ret = obj_alloc(tp_real);
    if (!ret)
    {
        lvl_err_msg("unable to allocate real object\n");
        return -1;
    }
    obj_init(ret);
    lvl_real_set(ret, real_val);
    *out = ret;
    return 0;
}

static int rad2deg_impl(struct object **out, struct object *args)
{
    struct object *in_obj, *ret;
    int n_args;
    real_t real_val;

    if ((n_args = seq_len(args)) != 1)
    {
        lvl_err_msg("rad2deg function called with %d args\n", n_args);
        return -1;
    }
    in_obj = seq_index(args, 0);
    if (!in_obj)
    {
        lvl_err_msg("unable to process arg 0 to rad2deg\n");
        return -1;
    }
    if (obj_raw_real(in_obj, &real_val) != 0)
    {
        lvl_err_msg("unable to convert object to real\n");
        return -1;
    }

    real_val = real_mul(real_val, REAL_DEGS_PER_RAD);
    ret = obj_alloc(tp_real);
    if (!ret)
    {
        lvl_err_msg("unable to allocate real object\n");
        return -1;
    }
    obj_init(ret);
    lvl_real_set(ret, real_val);
    *out = ret;
    return 0;
}

static int mult_impl(struct object **out, struct object *args)
{
    struct object *in_lhs, *in_rhs, *ret;
    int n_args, ret_code;

    ret = NULL;
    ret_code = 0;
    if ((n_args = seq_len(args)) != 2)
    {
        lvl_err_msg("mult function called with %d args\n", n_args);
        return -1;
    }
    in_lhs = seq_index(args, 0);
    in_rhs = seq_index(args, 1);
    if (!in_lhs || !in_rhs)
    {
        lvl_err_msg("unable to process args to mult\n");
        return -1;
    }

    if (!in_lhs->tp->op_mult)
    {
        lvl_err_msg("%s does not have a multiplication implementation\n",
                    in_lhs->tp->name);
        return -1;
    }
    ret_code = in_lhs->tp->op_mult(in_lhs, &ret, in_rhs);
    *out = ret;
    return ret_code;
}

static int div_impl(struct object **out, struct object *args)
{
    struct object *in_lhs, *in_rhs, *ret;
    int n_args, ret_code;

    ret = NULL;
    ret_code = 0;
    if ((n_args = seq_len(args)) != 2)
    {
        lvl_err_msg("div function called with %d args\n", n_args);
        return -1;
    }
    in_lhs = seq_index(args, 0);
    in_rhs = seq_index(args, 1);
    if (!in_lhs || !in_rhs)
    {
        lvl_err_msg("unable to process args to div\n");
        return -1;
    }

    if (!in_lhs->tp->op_mult)
    {
        lvl_err_msg("%s does not have a division implementation\n",
                    in_lhs->tp->name);
        return -1;
    }
    ret_code = in_lhs->tp->op_div(in_lhs, &ret, in_rhs);
    *out = ret;
    return ret_code;
}

static int add_impl(struct object **out, struct object *args)
{
    struct object *in_lhs, *in_rhs, *ret;
    int n_args, ret_code;

    ret = NULL;
    ret_code = 0;
    if ((n_args = seq_len(args)) != 2)
    {
        lvl_err_msg("add function called with %d args\n", n_args);
        return -1;
    }
    in_lhs = seq_index(args, 0);
    in_rhs = seq_index(args, 1);
    if (!in_lhs || !in_rhs)
    {
        lvl_err_msg("unable to process args to add\n");
        return -1;
    }

    if (!in_lhs->tp->op_add)
    {
        lvl_err_msg("%s does not have an addition implementation\n",
                    in_lhs->tp->name);
        return -1;
    }
    ret_code = in_lhs->tp->op_add(in_lhs, &ret, in_rhs);
    *out = ret;
    return ret_code;
}

static int sub_impl(struct object **out, struct object *args)
{
    struct object *in_lhs, *in_rhs, *ret;
    int n_args, ret_code;

    ret = NULL;
    ret_code = 0;
    if ((n_args = seq_len(args)) != 2)
    {
        lvl_err_msg("sub function called with %d args\n", n_args);
        return -1;
    }
    in_lhs = seq_index(args, 0);
    in_rhs = seq_index(args, 1);
    if (!in_lhs || !in_rhs)
    {
        lvl_err_msg("unable to process args to add\n");
        return -1;
    }

    if (!in_lhs->tp->op_sub)
    {
        lvl_err_msg("%s does not have a subtraction implementation\n",
                    in_lhs->tp->name);
        return -1;
    }
    ret_code = in_lhs->tp->op_sub(in_lhs, &ret, in_rhs);
    *out = ret;
    return ret_code;
}

static void print_usage(char const *cmd)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t%s [ -o out_file ]  in_file\n", cmd ? cmd : "mklvl");
}

static struct lvl_func_tbl
{
    char const *name;
    int (*impl)(struct object**, struct object*);
} func_tbl[] = {
    { "unity",    unity_impl },
    { "identity", identity_impl },
    { "mk_pt2",   mk_pt2_impl },
    { "sqrt",     sqrt_impl },
    { "sin",      sin_impl },
    { "cos",      cos_impl },
    { "tan",      tan_impl },
    { "deg2rad",  deg2rad_impl },
    { "rad2deg",  rad2deg_impl },
    { "mult",     mult_impl },
    { "add",      add_impl },
    { "sub",      sub_impl },
    { "div",      div_impl },
    { NULL,       NULL }
};

/**
 * Add a new function to the given lvl_ctxt
 * @param ctxt the level context
 * @param func_name the name of the function as it will appear to .lvl scripts
 * @param impl pointer to the function implementation
 * @return 0 for success, <0 if there was an error
 */
static int lvl_add_func(struct lvl_ctxt *ctxt, char const *func_name,
                        int (*impl)(struct object**, struct object*))
{
    struct object *func_obj;
    struct attr func_attr;

    func_obj = obj_alloc(tp_func);
    if (func_obj < 0)
        return -1;

    if (obj_init(func_obj) < 0)
        return -1;

    func_set_impl(func_obj, impl);
    memset(&func_attr, 0, sizeof(func_attr));
    strncpy(func_attr.tag, func_name, TAG_LEN);
    func_attr.tag[TAG_LEN - 1] = '\0';
    func_attr.obj = func_obj;
    attr_map_add(&ctxt->global_syms, &func_attr);

    return 0;
}

static void prepare(struct lvl_ctxt *ctxt)
{
    struct lvl_func_tbl const *cur_func;

    parser_init();
    entity_map_init(&ctxt->ents);
    attr_map_init(&ctxt->global_syms);

    cur_func = func_tbl;
    while (cur_func->name)
    {
        if (lvl_add_func(ctxt, cur_func->name, cur_func->impl) < 0)
        {
            fprintf(stderr, "ERROR: unable to add function \"%s\" to lvl\n",
                    cur_func->name);
        }
        cur_func++;
    }
}

static void cleanup(struct lvl_ctxt *ctxt)
{
    attr_map_cleanup(&ctxt->global_syms);
    entity_map_cleanup(&ctxt->ents);
}

char const *lvl_entity_spec =
    "entity lvl_geo lvl { \
            string name;\
            string desc;\
            string license;\
            string author;\
            string copyright;\
            real scale;\
            int floor_tex;\
            int ceil_tex;\
            string tex_list[];\
            int seg_tex[];\
            pt2 spawn;\
            pt2 verts[];\
            int seg_pts[][2];\
            real seg_heights[][2];\
            real seg_uv[][2];\
    };";

char const *lvl_game_obj_spec =
    "entity game_obj some_obj { \
        string obj_class; \
        pt2 pos; \
    };";

struct attr_spec
{
    char type[TAG_LEN];
    char ident[TAG_LEN];
};

/*
 * Generates an attr_spec which can be used to check an entity for correctness
 * (this would be the ref parameter for check_attr_map)
 */
static int parse_spec(struct attr_spec **attr_spec_out, int *attr_spec_len_out,
                      char ent_tp_out[TAG_LEN], char const *spec,
                      struct lvl_ctxt *ctxt)
{
    /*
     * The way this is implemented is kind of messy.  We parse out the spec
     * in its own context, then generate the attr_spec from the individual
     * attributes in the entity.  Then the context and the entity are freed.
     */

    struct attr_map *am;
    int attr_idx;
    struct attr *attr;
    struct attr_spec *attr_spec, *attr_spec_tmp;
    int attr_spec_len;

    if (lvl_parse_string(spec, &ctxt->ents, &ctxt->global_syms) < 0)
    {
        fprintf(stderr, "Unable to parse spec\n");
        return -1;
    }

    if (ctxt->ents.n_ents != 1)
    {
        fprintf(stderr, "lvl specs can only have one entity each\n");
        return -1;
    }

    attr_spec = NULL;
    attr_spec_len = 0;

    am = &ctxt->ents.ents->am;
    strncpy(ent_tp_out, ctxt->ents.ents->type, TAG_LEN);
    ent_tp_out[TAG_LEN - 1] = '\0';

    for (attr_idx = 0; attr_idx < am->n_attrs; attr_idx++)
    {
        attr = am->attrs + attr_idx;

        attr_spec_tmp = realloc(attr_spec,
                                sizeof(struct attr_spec) * ++attr_spec_len);
        if (!attr_spec_tmp)
        {
            TRACE_ERR("%s: failed realloc", __func__);
            if (attr_spec)
                free(attr_spec);
            return -1;
        }
        attr_spec = attr_spec_tmp;

        strncpy(attr_spec[attr_spec_len - 1].ident, attr->tag, TAG_LEN);
        attr_spec[attr_spec_len - 1].ident[TAG_LEN - 1] = '\0';

        strncpy(attr_spec[attr_spec_len - 1].type, attr->tp_expect->name,
                TAG_LEN);
        attr_spec[attr_spec_len - 1].type[TAG_LEN - 1] = '\0';
    }

    *attr_spec_out = attr_spec;
    *attr_spec_len_out = attr_spec_len;
    return 0;
}

/*
 * Verify that every attribute which is in ref is present in
 * chk, and that they are the same type.  chk is allowed to have
 * extra attributes which are not present in ref.
 */
static int check_attr_map(struct attr_map *chk, struct attr_spec *ref,
                          int ref_len)
{
    int idx;
    struct attr *attr_chk;

    for (idx = 0; idx < ref_len; idx++)
    {
        attr_chk = attr_map_find(chk, ref[idx].ident);

        if (!attr_chk)
        {
            TRACE_ERR("Missing attribute: %s\n", ref[idx].ident);
            return -1;
        }

        if (strcmp(attr_chk->obj->tp->name, ref[idx].type) != 0)
        {
            TRACE_ERR("Unexpected type for attribute \"%s\" "
                      "(expected \"%s\", got \"%s\"\n",
                      ref[idx].ident, ref[idx].type, attr_chk->obj->tp->name);
            return -1;
        }
    }

    return 0;
}

int validate_entity(struct entity_map *em, char const *ent_name,
                    char const *spec, struct lvl_ctxt *ctxt)
{
    struct entity *ent;
    struct attr_spec *attr_spec;
    int attr_spec_len;
    struct lvl_ctxt ctxt_tmp;
    char ent_tp[TAG_LEN];

    prepare(&ctxt_tmp);

    ent = entity_map_find(&ctxt->ents, ent_name);
    if (!ent)
    {
        TRACE_ERR("Unable to locate %s block\n", ent_name);
        cleanup(&ctxt_tmp);
        return -1;
    }

    if (parse_spec(&attr_spec, &attr_spec_len, ent_tp, spec, &ctxt_tmp) < 0)
    {
        cleanup(&ctxt_tmp);
        return -1;
    }

    if (strcmp(ent_tp, ent->type) != 0)
    {
        TRACE_ERR("ENTITY TYPE MISMATCH: got %s, expected %s\n",
                  ent->type, ent_tp);
        return -1;
    }

    if (check_attr_map(&ent->am, attr_spec, attr_spec_len) < 0)
    {
        cleanup(&ctxt_tmp);
        return -1;
    }

    cleanup(&ctxt_tmp);
    return 0;
}

struct lvl_raw
{
    /* 
     * The BSP code can actually handle tex_paths on a per-segment basis,
     * but for now I'm just going to have the parser do this on a per-lvl
     * basis
     */
    char name[MAX_NAME_LEN];
    char desc[MAX_DESC_LEN];
    char license[MAX_LICENSE_LEN];
    char author[MAX_AUTHOR_LEN];
    char copyright[MAX_COPYRIGHT_LEN];
    char (*wall_tex)[MAX_TEX_PATH_LEN];
    int floor_tex;
    int ceil_tex;
    struct line_seg *segs;
    real_t *verts;
    int n_segs, n_verts, n_wall_tex;
    real_t scale;
    real_t spawn[2];
};

struct game_obj_raw
{
    char class[TAG_LEN];
    real_t pos[2];
};

/* this function exists purely for debugging purposes */
static void lvl_dump_raw_map(FILE *f, struct lvl_raw const *dat)
{
    int idx;

    fprintf(f, "\"%s\" by %s\n", dat->name, dat->author);
    fprintf(f, "%s\n", dat->desc);
    fprintf(f, "%s\n", dat->license);
    fprintf(f, "%s\n", dat->copyright);
    fprintf(f, "Verts:\n");
    for (idx = 0; idx < dat->n_verts; idx++)
    {
        fprintf(f, "\t(%f, %f)\n",
                real_to_double(dat->verts[2 * idx]),
                real_to_double(dat->verts[1 + 2 * idx]));
    }
    for (idx = 0; idx < dat->n_segs; idx++)
    {
        struct line_seg *seg = dat->segs + idx;
        fprintf(f, "seg %d:\n", idx);
        fprintf(f, "\tpoints: %d to %d\n", seg->v[0], seg->v[1]);
        fprintf(f, "\ttexture coords: %f to %f\n",
                real_to_double(seg->r_dat.tc[0]),
                real_to_double(seg->r_dat.tc[1]));
        fprintf(f, "\tvertical extents: %f to %f\n",
                real_to_double(seg->r_dat.y_min),
                real_to_double(seg->r_dat.y_max));
    }
}

/*
 * Checking the return here and printing an error should be
 * purely pedantic because of the spec validation, but I do it anyways
 */
#define GET_ATTR(out, am, name)                                 \
    if (!((out) = attr_map_find_obj((am), (name))))             \
    {                                                           \
        fprintf(stderr, "failed to load \"%s\" attr\n", name);  \
        return -1;                                              \
    }

#define GET_OBJ_LEN(out, obj)                                           \
    if (obj_len((obj), &(out)) < 0)                                     \
    {                                                                   \
        fprintf(stderr, "failed to obtain length of %s\n", #out);       \
        return -1;                                                      \
    }

int get_raw_game_objs(struct game_obj_raw **raw_pp, int *n_obj_p,
                      struct lvl_ctxt *ctxt)
{
    struct entity *ent;
    struct attr_map *am;
    struct game_obj_raw *raw, *raw_tmp, *next;
    int n_obj;
    int idx;
    struct object *class_obj, *pos_obj;

    raw = NULL;
    n_obj = 0;

    for (idx = 0; idx < ctxt->ents.n_ents; idx++)
    {
        ent = ctxt->ents.ents + idx;
        am = &ent->am;

        if (strcmp(ent->type, "game_obj") == 0)
        {
            raw_tmp = realloc(raw, sizeof(struct game_obj_raw) * (n_obj + 1));
            if (!raw_tmp)
            {
                TRACE_ERR("Failed allocation\n");
                free(raw);
                return -1;
            }
            raw = raw_tmp;
            next = raw + n_obj;
            n_obj++;

            TRACE_DBG("%s - processing game_obj \"%s\"\n", __func__, ent->tag);

            GET_ATTR(class_obj, am, "obj_class");
            GET_ATTR(pos_obj, am, "pos");

            obj_str(class_obj, next->class, TAG_LEN);
            if (pos_obj->tp != tp_pt2)
            {
                TRACE_ERR("Failed to access pos attribute for %s\n", ent->tag);
                free(raw);
                return -1;
            }
            memcpy(next->pos, ((struct lvl_pt2*)pos_obj)->val,
                   sizeof(real_t) * 2);
        }
    }

    *n_obj_p = n_obj;
    *raw_pp = raw;
    return 0;
}

int get_raw_data(struct lvl_raw *raw, struct lvl_ctxt *ctxt)
{
    int idx;
    real_t *vert;
    struct entity *lvl_ent;
    struct object *name_obj, *desc_obj, *license_obj, *author_obj, *scale_obj,
        *verts_obj, *seg_pts_obj, *seg_heights_obj, *seg_uv_obj, *spawn_obj,
        *copyright_obj, *floor_tex_obj, *ceil_tex_obj, *wall_tex_obj,
        *seg_tex_obj;
    struct object *curr; /* current value while iterating through array */
    struct object *elem; /* tmp obj used to extract values from arrays */
    struct attr_map *am;
    int seg_pts_len, seg_heights_len, seg_uv_len, wall_tex_len, seg_tex_len;

    lvl_ent = entity_map_find(&ctxt->ents, "lvl");
    if (!lvl_ent)
    {
        fprintf(stderr, "Failed to find level\n");
        return -1;
    }

    am = &lvl_ent->am;
    GET_ATTR(name_obj, am, "name");
    GET_ATTR(desc_obj, am, "desc");
    GET_ATTR(author_obj, am, "author");
    GET_ATTR(license_obj, am, "license");
    GET_ATTR(copyright_obj, am, "copyright");
    GET_ATTR(scale_obj, am, "scale");
    GET_ATTR(verts_obj, am, "verts");
    GET_ATTR(seg_pts_obj, am, "seg_pts");
    GET_ATTR(seg_heights_obj, am, "seg_heights");
    GET_ATTR(seg_uv_obj, am, "seg_uv");
    GET_ATTR(spawn_obj, am, "spawn");
    GET_ATTR(floor_tex_obj, am, "floor_tex");
    GET_ATTR(ceil_tex_obj, am, "ceil_tex");
    GET_ATTR(seg_tex_obj, am, "seg_tex");
    GET_ATTR(wall_tex_obj, am, "tex_list");

    GET_OBJ_LEN(seg_pts_len, seg_pts_obj);
    GET_OBJ_LEN(seg_heights_len, seg_heights_obj);
    GET_OBJ_LEN(seg_uv_len, seg_uv_obj);
    GET_OBJ_LEN(seg_tex_len, seg_tex_obj);
    if (seg_pts_len != seg_heights_len ||
        seg_pts_len != seg_uv_len ||
        seg_pts_len != seg_tex_len)
    {
        fprintf(stderr, "inconsistent segment length\n");
        return -1;
    }
    raw->n_segs = seg_pts_len;
    raw->segs = (struct line_seg*)calloc(raw->n_segs,
                                         sizeof(struct line_seg));

    obj_str(name_obj, raw->name, MAX_NAME_LEN);
    obj_str(author_obj, raw->author, MAX_AUTHOR_LEN);
    obj_str(desc_obj, raw->desc, MAX_DESC_LEN);
    obj_str(license_obj, raw->license, MAX_LICENSE_LEN);
    obj_str(copyright_obj, raw->copyright, MAX_COPYRIGHT_LEN);

    GET_OBJ_LEN(raw->n_verts, verts_obj);
    raw->verts = (real_t*)malloc(2 * raw->n_verts * sizeof(real_t));
    for (idx = 0; idx < raw->n_verts; idx++)
    {
        curr = obj_index(verts_obj, idx);
        if (curr->tp != tp_pt2)
        {
            /*
             * awkward error message, but whatever
             * this shouldn't be possible anyways
             */
            fprintf(stderr, "verts contained a non-pt2 object\n");
            return -1;
        }
        vert = ((struct lvl_pt2*)curr)->val;
        raw->verts[2 * idx] = vert[0];
        raw->verts[2 * idx + 1] = vert[1];
    }

    GET_OBJ_LEN(wall_tex_len, wall_tex_obj);
    raw->wall_tex = calloc(wall_tex_len, sizeof(raw->wall_tex[0]));
    for (idx = 0; idx < wall_tex_len; idx++)
    {
        if (!(curr = obj_index(wall_tex_obj, idx)))
        {
            fprintf(stderr, "Unable to access index %d of wall_tex\n", idx);
            return -1;
        }
        obj_str(curr, raw->wall_tex[idx], sizeof(raw->wall_tex[0]));
    }
    raw->n_wall_tex = wall_tex_len;

    obj_raw_int(floor_tex_obj, &raw->floor_tex);
    obj_raw_int(ceil_tex_obj, &raw->ceil_tex);

    for (idx = 0; idx < raw->n_segs; idx++)
    {
        /* first read in seg_pts */
        if (!(curr = obj_index(seg_pts_obj, idx)))
        {
            fprintf(stderr, "unable to access index %d of seg_pts\n", idx);
            return -1;
        }
        if (!(elem = obj_index(curr, 0)))
        {
            fprintf(stderr, "unable to access seg_pts[%d][0]\n", idx);
            return -1;
        }
        if (obj_raw_int(elem, &raw->segs[idx].v[0]) != 0)
        {
            fprintf(stderr, "non-integer in seg_pts\n");
            return -1;
        }
        if (!(elem = obj_index(curr, 1)))
        {
            fprintf(stderr, "unable to access seg_pts[%d][1]\n", idx);
            return -1;
        }
        if (obj_raw_int(elem, &raw->segs[idx].v[1]) != 0)
        {
            fprintf(stderr, "non-integer in seg_pts\n");
            return -1;
        }

        /* read in seg_tex */
        if (!(curr = obj_index(seg_tex_obj, idx)))
        {
            fprintf(stderr, "unable to read in segment texture indices\n");
            return -1;
        }
        if (obj_raw_int(curr, &raw->segs[idx].r_dat.tex_idx) != 0 ||
            raw->segs[idx].r_dat.tex_idx >= raw->n_wall_tex)
        {
            fprintf(stderr, "unable to read segment texture index\n");
            return -1;
        }

        /* next read in heights */
        if (!(curr = obj_index(seg_heights_obj, idx)))
        {
            fprintf(stderr, "unable to read in segment heights\n");
            return -1;
        }
        if (!(elem = obj_index(curr, 0)))
        {
            fprintf(stderr, "unable to read in segment heights\n");
            return -1;
        }
        if (obj_raw_real(elem, &raw->segs[idx].r_dat.y_min) != 0)
        {
            fprintf(stderr, "segment %d y-minimum is not a real value\n", idx);
            return -1;
        }
        if (!(elem = obj_index(curr, 1)))
        {
            fprintf(stderr, "unable to read in segment heights\n");
            return -1;
        }
        if (obj_raw_real(elem, &raw->segs[idx].r_dat.y_max) != 0)
        {
            fprintf(stderr, "segment %d y-maximum is not a real value\n", idx);
            return -1;
        }

        /* next read in texture coordinates */
        if (!(curr = obj_index(seg_uv_obj, idx)))
        {
            fprintf(stderr, "unable to read in texture coordinates\n");
            return -1;
        }
        if (!(elem = obj_index(curr, 0)))
        {
            fprintf(stderr, "unable to read in texture u-coordinate\n");
            return -1;
        }
        if (obj_raw_real(elem, &raw->segs[idx].r_dat.tc[0]) != 0)
        {
            fprintf(stderr, "segment %d texture u-coordinate is not a real\n",
                    idx);
            return -1;
        }
        if (!(elem = obj_index(curr, 1)))
        {
            fprintf(stderr, "unable to read in texture v-coordinate\n");
            return -1;
        }
        if (obj_raw_real(elem, &raw->segs[idx].r_dat.tc[1]) != 0)
        {
            fprintf(stderr, "segment %d texture v-coordinate is not a real\n",
                    idx);
            return -1;
        }
    }

    if (obj_raw_real(scale_obj, &raw->scale) != 0)
    {
        fprintf(stderr, "scale is not a real number\n");
        return -1;
    }

    if (spawn_obj->tp != tp_pt2)
    {
        fprintf(stderr, "spawn type is not tp_pt2\n");
        return -1;
    }
    memcpy(raw->spawn, ((struct lvl_pt2*)spawn_obj)->val, sizeof(real_t) * 2);

    return 0;
}

int lvl_process_raw_map(struct lvl_raw *raw_map)
{
    int idx;
    real_t *v, *tc;
    for (idx = 0; idx < raw_map->n_verts; idx++)
    {
        v = raw_map->verts + 2 * idx;
        v[0] = real_mul(v[0], raw_map->scale);
        v[1] = real_mul(v[1], raw_map->scale);
    }
    for (idx = 0; idx < raw_map->n_segs; idx++)
    {
        tc = raw_map->segs[idx].r_dat.tc;
        tc[0] = real_mul(tc[0], raw_map->scale);
        tc[1] = real_mul(tc[1], raw_map->scale);
    }
    return 0;
}

static int lvl_raw_map_get_meta(struct lvl_raw const *raw_map,
                                struct game_map *map)
{
    game_map_set_name(map, raw_map->name);
    game_map_set_author(map, raw_map->author);
    game_map_set_desc(map, raw_map->desc);
    game_map_set_license(map, raw_map->license);
    game_map_set_copyright(map, raw_map->copyright);

    return 0;
}

int main(int argc, char **argv)
{
    int idx;
    int opt;
    FILE *in_file = NULL;
    stream_t *in_stream, *out_stream = NULL;
    char const *out_path;
    char const *in_path;
    char const *cmd;
    struct lvl_ctxt ctxt;
    struct lvl_raw raw_map;
    int n_raw_obj;
    struct game_obj_raw *raw_obj;
    struct game_map map;

    out_path = "lvl.out";
    while ((opt = getopt(argc, argv, "o:")) != -1)
    {
        switch (opt)
        {
        case 'o':
            out_path = optarg;
            break;
        default:
            return -1;
        }
    }
    cmd = argv[0];
    argc -= optind;
    argv += optind;

    if (argc != 1)
    {
        print_usage(cmd);
        return -1;
    }
    in_path = *argv;

    if (!(in_file = fopen(in_path, "r")))
    {
        fprintf(stderr, "Unable to open \"%s\" for reading: %s\n",
                in_path, strerror(errno));
        return -1;
    }
    if (!(out_stream = stream_stdio_open(out_path, "wb")))
    {
        fprintf(stderr, "Unable to open \"%s\" for writing: %s\n",
                out_path, strerror(errno));
        return -1;
    }

    prepare(&ctxt);
    if (lvl_parse_file(in_file, &ctxt.ents, &ctxt.global_syms) < 0)
    {
        fprintf(stderr, "Unable to parse input file\n");
        return -1;
    }

    /* validate the lvl block */
    if (validate_entity(&ctxt.ents, "lvl", lvl_entity_spec, &ctxt) < 0)
        return -1;

    /* validate any game_objs there might be */
    for (idx = 0; idx < ctxt.ents.n_ents; idx++)
    {
        if (strcmp(ctxt.ents.ents[idx].type, "game_obj") == 0)
        {
            if (validate_entity(&ctxt.ents, ctxt.ents.ents[idx].tag,
                                lvl_game_obj_spec, &ctxt) < 0)
                return -1;
        }
    }

    TRACE_DBG("lvl block validated\n");

    game_map_init(&map);

    if (get_raw_game_objs(&raw_obj, &n_raw_obj, &ctxt) < 0)
        return -1;
    for (idx = 0; idx < n_raw_obj; idx++)
    {
        if (game_map_add_obj(&map, raw_obj[idx].class,
                             raw_obj[idx].pos) < 0)
        {
            TRACE_ERR("Failed to add game_obj \"%s\"\n", raw_obj[idx].class);
            return -1;
        }
    }
    free(raw_obj);
    raw_obj = NULL;
    n_raw_obj = 0;

    if (get_raw_data(&raw_map, &ctxt) < 0)
        return -1;
    if (lvl_process_raw_map(&raw_map) < 0)
        return -1;
    lvl_raw_map_get_meta(&raw_map, &map);

    map.player_spawn[0] = raw_map.spawn[0];
    map.player_spawn[1] = raw_map.spawn[1];

    /* add texture paths */
    for (idx = 0; idx < raw_map.n_wall_tex; idx++)
        game_map_add_tex(&map, raw_map.wall_tex[idx]);
    map.tex.ceil_tex_idx = raw_map.ceil_tex;
    map.tex.floor_tex_idx = raw_map.floor_tex;

    gen_bsp(&map.rbsp, raw_map.verts, raw_map.n_verts, raw_map.segs,
            raw_map.n_segs);
    gen_bsp(&map.cbsp, raw_map.verts, raw_map.n_verts, raw_map.segs,
            raw_map.n_segs);
    write_map(&map, out_stream);

    game_map_cleanup(&map);
    cleanup(&ctxt);
    parser_cleanup();
    stream_close(out_stream);
    fclose(in_file);

    return 0;
}
