/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/


%{
#include <assert.h>

#include "real.h"
#include "lvl_yacc.h"
#include "tool/mklvl/mklvl.h"

/* TODO: shouldn't this be in a header file somewhere ? */
int yyparse (void);

%}
%%

[0-9]+\.[0-9]+          {
                             yylval.rval = real_from_string(yytext);

                             return TOK_VAL_REAL;
                        }

\"[^\"]*\"       {
                            size_t len = strlen(yytext);

                            assert(yytext[0] == '\"');
                            assert(yytext[len - 1] == '\"');

                            yytext[len - 1] = '\0';
                            strncpy(yylval.sval, yytext + 1, sizeof(yylval.sval));
                            yylval.sval[1022] = '\0';/* TODO: don't hardcode this */

                            return TOK_VAL_STRING;
                      }

\}               return '}';
\{               return '{';
=                return '=';
\[               return '[';
\]               return ']';
\;               return ';';
\(               return '(';
\)               return ')';
\,               return ',';
\-               return '-';
\+               return '+';
\*               return '*';
\/               return '/';
entity           return TOK_ENTITY;
string           return TOK_TP_STRING;
pt2              return TOK_TP_PT2;
int              return TOK_TP_INT;
real             return TOK_TP_REAL;

[a-zA-Z_][0-9a-zA-Z_]*  {
                            /* size_t len = strlen(yytext); */

                            strncpy(yylval.sval, yytext, sizeof(yylval.sval));
                            yylval.sval[1023] = '\0';/* TODO: don't hardcode this */

                            return TOK_IDENT;
                        }
[0-9]+                 {
                            yylval.ival = atoi(yytext);
                            return TOK_VAL_INT;
                       }
[ \t\n]            ;

%%

int yywrap(void)
{
    return 1;
}

int lvl_parse_string(const char *in_str, struct entity_map *em, struct attr_map *global_sym_table)
{
    int err;
    parser_init();

    yy_scan_string(in_str);
    err = yyparse();
    yylex_destroy();

    if (err == 1 || err == 2)
        return -1;

    return lvl_parse_(em, global_sym_table);
}

int lvl_parse_file(FILE *file, struct entity_map *em, struct attr_map *global_sym_table)
{
    int err;
    parser_init();
    yyin = file;
    err = yyparse();
    yyin = NULL;
    yylex_destroy();

    if (err == 1 || err == 2)
        return -1;
    return lvl_parse_(em, global_sym_table);
}
