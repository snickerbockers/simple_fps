/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

%{
#define YYERROR_VERBOSE

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#include "tool/mklvl/mklvl.h"
#include "lvl_yacc.h"
#include "real.h"
#include "trace.h"

extern FILE *yyin;
int yyerror(const char *errmsg, ...);
int yylex();
void parser_init(void);
int lvl_parse_(struct entity_map *em, struct attr_map *global_sym_table);

enum
{
    OP_ASSIGN,
    OP_VAR,
    OP_LITERAL,
    OP_ARRAY,
    OP_VAL,
    OP_CALL,
    OP_NEGATIVE,
    OP_SEQ,
    OP_PROD,
    OP_SUM
};

struct op_data
{
    /*
     * The object being represented by this operation, or NULL if there
     * is none.
     */
    struct object *obj;

    /* Some objects will be anonymous, in which case name[0] == '\0'. */
    char name[TAG_LEN];
};

struct operator
{
    int op;

    struct op_data data;
    struct operator **args;
    int n_args;
};

enum
{
    BLOCK_ENT
};

struct block
{
    /* this is always BLOCK_ENT */
    int tp;

    char name[TAG_LEN];
    char type[TAG_LEN]; /* the specific type of entity */

    struct operator **stmts;
    int stmt_count;
};

static struct block *blocks = NULL;
static int n_blocks = 0;

static struct block new_block;

static struct operator **stmt = NULL;
static int stmt_sz = 0;

static int parser_initialized;

static void stmt_next(void);
static void block_next(const char *type, const char *name, int tp);

/* Nomenclature explanation:
 *     cleanup frees all members of blk but not blk itself
 *     free cleans up the operator and then frees it.
 */
static void block_cleanup(struct block *blk);
static void op_free(struct operator *op);

static struct operator *eval_op_val(struct operator *op, struct attr_map *global_sym_table);

static void stmt_next(void)
{
    assert(stmt && *stmt);
    if (stmt_sz != 1)
        yyerror("incomplete statement");
    new_block.stmts = realloc(new_block.stmts, sizeof(struct operator*) * ++new_block.stmt_count);
    new_block.stmts[new_block.stmt_count - 1] = *stmt;

    free(stmt);
    stmt = NULL;
    stmt_sz = 0;
}

/*
 * type is the entity's type, whereas tp is the block's type (which is always
 * BLOCK_ENT).
 *
 * Yeah, it kinda sucks, but that's what happens when you work on the same pet
 * project for over a year with no real plans.
 */
static void block_next(const char *type, const char *name, int tp)
{
    TRACE_DBG("%s - type == %s, name == %s, tp == %d\n",
              __func__, type, name, tp);

    blocks = realloc(blocks, sizeof(struct block) * ++n_blocks);

    blocks[n_blocks - 1] = new_block;
    strncpy(blocks[n_blocks-1].name, name, TAG_LEN);
    blocks[n_blocks-1].name[TAG_LEN-1] = '\0';
    blocks[n_blocks-1].tp = tp;
    strncpy(blocks[n_blocks-1].type, type, TAG_LEN);
    blocks[n_blocks-1].type[TAG_LEN-1] = '\0';

    memset(&new_block, 0, sizeof(struct block));
}

static void op_push(struct operator *op)
{
    stmt = realloc(stmt, sizeof(struct operator*) * ++stmt_sz);
    stmt[stmt_sz-1] = op;
}

static struct operator *op_pop(void)
{
    assert(stmt_sz > 0);
    struct operator *op = stmt[stmt_sz - 1];
    /* this realloc call is not strictly necessary */
    stmt = realloc(stmt, sizeof(struct operator*) * --stmt_sz);

    return op;
}

static int imin(int v1, int v2)
{
    return v1 < v2 ? v1 : v2;
}

struct array_spec
{
    int len;
    struct array_spec *next;
};

struct array_spec *as;

%}

%union {
    int ival;
    char sval[1024];/* TODO: don't harcode */
    real_t rval;
}

%token TOK_ENTITY
%token TOK_TP_STRING
%token TOK_TP_PT2
%token TOK_TP_REAL
%token TOK_TP_INT
%token TOK_IDENT
%token TOK_VAL_STRING
%token TOK_VAL_REAL
%token TOK_VAL_INT

%left '*' '/'
%left '+' '-'

%%

lvl : entity | lvl entity

entity : TOK_ENTITY TOK_IDENT TOK_IDENT '{' statement_list '}' ';' {
    block_next($<sval>2, $<sval>3, BLOCK_ENT);
}

statement_list : statement | statement_list statement

statement : assignment ';' { stmt_next(); }
            | variable ';' { stmt_next(); }

/* attr : string_attr */
/*        | pt2_attr */
/*        | real_attr */
/*        | int_attr */
/*        | string_array_attr */
/*        | pt2_array_attr */
/*        | real_array_attr */
/*        | int_array_attr */

assignment : variable '=' value {
    if (stmt_sz < 2)
        yyerror("operator stack fucked up somehow");
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_ASSIGN;
    op->n_args = 2;
    op->args = (struct operator**)malloc(sizeof(struct operator*) * 2);
    op->args[1] = op_pop();
    op->args[0] = op_pop();

    assert(op->args[0]->op == OP_VAR);
    assert(op->args[1]->op == OP_VAL);

    op_push(op);
}

scalar : string_variable | int_variable | real_variable | pt2_variable

variable : array_variable | scalar

fixed_array_operator : '[' TOK_VAL_INT ']' {
    if (as)
        yyerror("IDK how this happened");
    as = (struct array_spec*)malloc(sizeof(struct array_spec));
    as->next = NULL;
    as->len = $<ival>2;
} |  fixed_array_operator '[' TOK_VAL_INT ']' {
    struct array_spec *new_as;
    if (!as)
        yyerror("well...fuck");

    new_as = (struct array_spec*)malloc(sizeof(struct array_spec));
    new_as->next = as;
    new_as->len = $<ival>3;
    as = new_as;
}

array_variable : scalar fixed_array_operator {
    struct operator *op;
    struct object *obj;
    struct type const *tp, *tp_encap;
    struct array_spec *as_old;

    op = op_pop();

    if (op->op != OP_VAR)
        yyerror("parsing stack is fucked up");

    tp_encap = op->data.obj->tp;
    obj_free(op->data.obj);

    do
    {
        tp = tp_fixed_array_of(tp_encap, as->len);

        tp_encap = tp;

        as_old = as;
        as = as->next;
        free(as_old);
    } while (as);

    obj = obj_alloc(tp);
    obj_init(obj);

    op->data.obj = obj;
    op_push(op);
} | scalar '[' ']' fixed_array_operator {
    struct operator *op;
    struct object *obj;
    struct type const *tp, *tp_encap;
    struct array_spec *as_old;

    op = op_pop();

    if (op->op != OP_VAR)
        yyerror("parsing stack is fucked up");

    tp_encap = op->data.obj->tp;
    obj_free(op->data.obj);

    while (as)
    {
        tp = tp_fixed_array_of(tp_encap, as->len);

        /* obj = obj_alloc(tp); */
        /* obj_init(obj); */
        /* ^ Why the fuck is this even here? */

        tp_encap = tp;

        as_old = as;
        as = as->next;
        free(as_old);
    }

    tp = tp_array_of(tp_encap);

    obj = obj_alloc(tp);
    obj_init(obj);

    op->data.obj = obj;
    op_push(op);
} | scalar '[' ']' {
    struct operator *op;
    struct object *obj;
    struct type const *tp, *tp_encap;

    op = op_pop();

    if (op->op != OP_VAR)
        yyerror("parsing stack is fucked up");

    tp_encap = op->data.obj->tp;
    obj_free(op->data.obj);

    tp = tp_array_of(tp_encap);

    obj = obj_alloc(tp);
    obj_init(obj);

    op->data.obj = obj;
    op_push(op);
}

string_variable : TOK_TP_STRING TOK_IDENT {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_VAR;
    strncpy(op->data.name, $<sval>2, TAG_LEN);
    op->data.name[TAG_LEN-1] = '\0';
    op->data.obj = obj_alloc(tp_str);
    assert(op->data.obj);
    obj_init(op->data.obj);
    op_push(op);
}

int_variable : TOK_TP_INT TOK_IDENT {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_VAR;
    strncpy(op->data.name, $<sval>2, TAG_LEN);
    op->data.name[TAG_LEN-1] = '\0';
    op->data.obj = obj_alloc(tp_int);
    assert(op->data.obj);
    obj_init(op->data.obj);
    op_push(op);
}

real_variable : TOK_TP_REAL TOK_IDENT {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_VAR;
    strncpy(op->data.name, $<sval>2, TAG_LEN);
    op->data.name[TAG_LEN-1] = '\0';
    op->data.obj = obj_alloc(tp_real);
    assert(op->data.obj);
    obj_init(op->data.obj);
    op_push(op);
}

pt2_variable : TOK_TP_PT2 TOK_IDENT {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_VAR;
    strncpy(op->data.name, $<sval>2, TAG_LEN);
    op->data.name[TAG_LEN-1] = '\0';
    op->data.obj = obj_alloc(tp_pt2);
    assert(op->data.obj);
    obj_init(op->data.obj);
    op_push(op);
}

literal : string_value | literal_array | int_value | real_value

value : sum {
    /* nothing to do here */
}

function_call : TOK_IDENT '(' sequence ')' {
    struct operator *op_call, *op_seq;

    op_seq = op_pop();
    if (op_seq->op != OP_SEQ)
    {
        yyerror("Was expecting OP_SEQ");
        YYERROR;
    }
    op_call = (struct operator*)calloc(1, sizeof(struct operator));
    op_call->op = OP_CALL;
    strncpy(op_call->data.name, $<sval>1, TAG_LEN);
    op_call->data.name[TAG_LEN - 1] = '\0';
    op_call->n_args = 1;
    op_call->args = (struct operator**)malloc(sizeof(struct operator*));
    op_call->args[0] = op_seq;
    op_push(op_call);
} | TOK_IDENT '(' ')' {
    struct operator *op;

    op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_CALL;
    strncpy(op->data.name, $<sval>1, TAG_LEN);
    op->data.name[TAG_LEN - 1] = '\0';
    op_push(op);
}

product : literal {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_VAL;
    op->n_args = 1;
    op->args = (struct operator**)malloc(sizeof(struct operator*));
    *op->args = op_pop();
    /* sanity check: should ensure that (*op->args)->op == OP_CALL or OP_LITERAL */
    op_push(op);
} | function_call {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_VAL;
    op->n_args = 1;
    op->args = (struct operator**)malloc(sizeof(struct operator*));
    *op->args = op_pop();
    /* sanity check: should ensure that (*op->args)->op == OP_CALL or OP_LITERAL */
    op_push(op);
} | unary_operator {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_VAL;
    op->n_args = 1;
    op->args = (struct operator**)malloc(sizeof(struct operator*));
    *op->args = op_pop();
    op_push(op);
} | '(' sum ')' {
    /* parenthetical sub-statement, nothing needs to do here because op_pop() is already OP_VAL */
} | product '*' product {
    /* Insert a call to the mult function */
    struct operator *op_call =
        (struct operator*)calloc(1, sizeof(struct operator));
    struct operator *op_val =
        (struct operator*)calloc(1, sizeof(struct operator));
    struct operator *op_seq = calloc(1, sizeof(struct operator));
    struct operator *op_rhs = op_pop();
    struct operator *op_lhs = op_pop();

    assert(op_lhs->op == OP_VAL);
    assert(op_rhs->op == OP_VAL);

    op_seq->op = OP_SEQ;
    op_seq->n_args = 2;
    op_seq->args = (struct operator**)malloc(2 * sizeof(struct operator*));

    /*
     * eval_op_call reverses the lvl_seq so that the first
     * argument is actually the end of the sequence.
     */
    op_seq->args[1] = op_lhs;
    op_seq->args[0] = op_rhs;

    op_call->op = OP_CALL;
    strncpy(op_call->data.name, "mult", TAG_LEN);
    op_call->data.name[TAG_LEN - 1] = '\0';
    op_call->n_args = 1;
    op_call->args = (struct operator**)malloc(sizeof(struct operator*));
    op_call->args[0] = op_seq;

    op_val->op = OP_VAL;
    op_val->n_args = 1;
    op_val->args = (struct operator**)malloc(sizeof(struct operator*));
    *op_val->args = op_call;
    op_push(op_val);
} | product '/' product {
    /* Insert a call to the div function */
    struct operator *op_call =
        (struct operator*)calloc(1, sizeof(struct operator));
    struct operator *op_val =
        (struct operator*)calloc(1, sizeof(struct operator));
    struct operator *op_seq = calloc(1, sizeof(struct operator));
    struct operator *op_rhs = op_pop();
    struct operator *op_lhs = op_pop();

    assert(op_lhs->op == OP_VAL);
    assert(op_rhs->op == OP_VAL);

    op_seq->op = OP_SEQ;
    op_seq->n_args = 2;
    op_seq->args = (struct operator**)malloc(2 * sizeof(struct operator*));

    /*
     * eval_op_call reverses the lvl_seq so that the first
     * argument is actually the end of the sequence.
     */
    op_seq->args[1] = op_lhs;
    op_seq->args[0] = op_rhs;

    op_call->op = OP_CALL;
    strncpy(op_call->data.name, "div", TAG_LEN);
    op_call->data.name[TAG_LEN - 1] = '\0';
    op_call->n_args = 1;
    op_call->args = (struct operator**)malloc(sizeof(struct operator*));
    op_call->args[0] = op_seq;

    op_val->op = OP_VAL;
    op_val->n_args = 1;
    op_val->args = (struct operator**)malloc(sizeof(struct operator*));
    *op_val->args = op_call;
    op_push(op_val);
}

sum : sum '+' sum {
    /* Insert a call to the add function */
    struct operator *op_call =
        (struct operator*)calloc(1, sizeof(struct operator));
    struct operator *op_val =
        (struct operator*)calloc(1, sizeof(struct operator));
    struct operator *op_seq = calloc(1, sizeof(struct operator));
    struct operator *op_rhs = op_pop();
    struct operator *op_lhs = op_pop();

    assert(op_lhs->op == OP_VAL);
    assert(op_rhs->op == OP_VAL);

    op_seq->op = OP_SEQ;
    op_seq->n_args = 2;
    op_seq->args = (struct operator**)malloc(2 * sizeof(struct operator*));

    /*
     * eval_op_call reverses the lvl_seq so that the first
     * argument is actually the end of the sequence.
     */
    op_seq->args[1] = op_lhs;
    op_seq->args[0] = op_rhs;

    op_call->op = OP_CALL;
    strncpy(op_call->data.name, "add", TAG_LEN);
    op_call->data.name[TAG_LEN - 1] = '\0';
    op_call->n_args = 1;
    op_call->args = (struct operator**)malloc(sizeof(struct operator*));
    op_call->args[0] = op_seq;

    op_val->op = OP_VAL;
    op_val->n_args = 1;
    op_val->args = (struct operator**)malloc(sizeof(struct operator*));
    *op_val->args = op_call;
    op_push(op_val);
} | sum '-' sum {
    /* Insert a call to the sub function */
    struct operator *op_call =
        (struct operator*)calloc(1, sizeof(struct operator));
    struct operator *op_val =
        (struct operator*)calloc(1, sizeof(struct operator));
    struct operator *op_seq = calloc(1, sizeof(struct operator));
    struct operator *op_rhs = op_pop();
    struct operator *op_lhs = op_pop();

    assert(op_lhs->op == OP_VAL);
    assert(op_rhs->op == OP_VAL);

    op_seq->op = OP_SEQ;
    op_seq->n_args = 2;
    op_seq->args = (struct operator**)malloc(2 * sizeof(struct operator*));

    /*
     * eval_op_call reverses the lvl_seq so that the first
     * argument is actually the end of the sequence.
     */
    op_seq->args[1] = op_lhs;
    op_seq->args[0] = op_rhs;

    op_call->op = OP_CALL;
    strncpy(op_call->data.name, "sub", TAG_LEN);
    op_call->data.name[TAG_LEN - 1] = '\0';
    op_call->n_args = 1;
    op_call->args = (struct operator**)malloc(sizeof(struct operator*));
    op_call->args[0] = op_seq;

    op_val->op = OP_VAL;
    op_val->n_args = 1;
    op_val->args = (struct operator**)malloc(sizeof(struct operator*));
    *op_val->args = op_call;
    op_push(op_val);
} | product {
    /* don't actually need to do anything here. */
}

unary_operator : negative_operator

negative_operator : '-' value {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_NEGATIVE;
    op->n_args = 1;
    op->args = (struct operator**)malloc(sizeof(struct operator*));
    op->args[0] = op_pop();
    op_push(op);
}

sequence : value {
    struct operator *op_val, *op_seq;

    op_val = op_pop();

    assert(op_val->op == OP_VAL);
    op_seq = calloc(1, sizeof(struct operator));
    op_seq->op = OP_SEQ;
    op_seq->n_args = 1;
    op_seq->args = (struct operator**)malloc(sizeof(struct operator*));
    op_seq->args[0] = op_val;

    op_push(op_seq);

}          | value ',' sequence {
    struct operator *op_seq, *op_val, *op_lit;
    struct object *seq, *val;

    op_seq = op_pop();
    op_val = op_pop();

    if (op_seq->op != OP_SEQ)
    {
        yyerror("Was expecting OP_SEQ");
        exit(1);
    }

    if (op_val->op != OP_VAL)
    {
        yyerror("Was expecting OP_VAL");
        exit(1);
    }

    op_seq->args = realloc(op_seq->args, sizeof(struct operator*) * ++op_seq->n_args);
    op_seq->args[op_seq->n_args - 1] = op_val;

    op_push(op_seq);
}

literal_array : '{' sequence '}'/*  { */
/*     int arg_idx; */
/*     struct operator *op_seq, *op_val; */

/*     op_seq = op_pop(); */
/*     if (op_seq->op != OP_SEQ) */
/*     { */
/*         yyerror("Was expecting OP_SEQ"); */
/*         exit(1); */
/*     } */

/*     op_val = calloc(1, sizeof(struct operator)); */
/*     op_val->op = OP_VAL; */
/*     op_val->n_args = 1; */
/*     op_val->args = (struct operator**)malloc(sizeof(struct operator*)); */
/*     op_val->args[0] = op_seq; */
/*     op_push(op_val); */
/* } */

string_value : TOK_VAL_STRING {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_LITERAL;
    op->data.obj = obj_alloc(tp_str);
    obj_init(op->data.obj);
    str_set(op->data.obj, $<sval>1);
    op_push(op);
}

int_value : TOK_VAL_INT {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_LITERAL;
    op->data.obj = obj_alloc(tp_int);
    obj_init(op->data.obj);
    ((struct lvl_int*)op->data.obj)->val = $<ival>1;
    op_push(op);
}

real_value : TOK_VAL_REAL {
    struct operator *op = (struct operator*)calloc(1, sizeof(struct operator));
    op->op = OP_LITERAL;
    op->data.obj = obj_alloc(tp_real);
    obj_init(op->data.obj);
    ((struct lvl_real*)op->data.obj)->val = $<rval>1;
    op_push(op);
}

/* real_value : TOK_VAL_REAL { */
/*     struct operator *op = (struct operator*)calloc(1, sizeof(struct operator)); */
/*     op->op = OP_LITERAL; */
/*     op->tp = TP_REAL; */
/*     op->data.val.val_real = $<rval>1; */
/*     op->lhs = op->rhs = NULL; */
/*     op_push(op); */
/* } */

%%

/* sym_table is used to store globally-visible symbols.  Currently it can
 * be read from within lvl scripts but cannot be written to.  It exists
 * chiefly to define function calls but may be used to store globally-accessible
 * variables in the future.
 */
int lvl_parse_(struct entity_map *em, struct attr_map *global_sym_table)
{
    int blk_idx, stmt_idx;
    struct block *blk;
    struct entity new_ent, *entp;
    struct attr attr, *pattr;
    int err;

    /* buffer for holding the obj_as_code output, used for debugging info */
    char *obj_value_str;
    size_t obj_value_str_len = TAG_LEN;
    obj_value_str = malloc(sizeof(char) * obj_value_str_len);

    for (blk_idx = 0; blk_idx < n_blocks; blk_idx++)
    {
        blk = blocks + blk_idx;

        if (blk->tp != BLOCK_ENT)
        {
            fprintf(stderr, "unexpected block type %d\n", blk->tp);
            exit(1);
        }

        entp = entity_map_find(em, blk->name);
        if (!entp)
        {
            entp = &new_ent;
            memset(&new_ent, 0, sizeof(struct entity));

            strncpy(new_ent.tag, blk->name, TAG_LEN);
            new_ent.tag[TAG_LEN-1] = '\0';

            strncpy(new_ent.type, blk->type, TAG_LEN);
            new_ent.type[TAG_LEN-1] = '\0';

            attr_map_init(&new_ent.am);
        }

        if (strcmp(blk->type, entp->type) != 0)
        {
            TRACE_ERR("entity %s type mismatch (expected %s, got %s)\n",
                      entp->tag, blk->type, entp->type);
            exit(1);
        }

        TRACE_DBG("entity %s: type is %s\n", entp->tag, entp->type);

        for (stmt_idx = 0; stmt_idx < blk->stmt_count; stmt_idx++)
        {
            char obj_as_str[STR_LEN];
            char obj_tp_name[TAG_LEN];
            struct operator *stmt;
            struct object *obj_dst, *obj_src;
            struct object *trash;
            struct type const *tp_expect;

            stmt = blk->stmts[stmt_idx];

            if (stmt->op == OP_VAR)
            {
                if ((pattr = attr_map_find(&entp->am, stmt->data.name)))
                {
                    fprintf(stderr, "empty re-declaration of attribute %s\n",
                            stmt->data.name);
                    goto on_error;
                }

                memset(&attr, 0, sizeof(attr));
                strncpy(attr.tag, stmt->data.name, TAG_LEN);
                attr.tag[TAG_LEN - 1] = '\0';
                attr.tp_expect = stmt->data.obj->tp;
                attr_map_add(&entp->am, &attr);

                if (stmt->data.obj)
                    obj_free(stmt->data.obj);
                stmt->data.obj = NULL;

                continue;
            }

            if (stmt->op != OP_ASSIGN || stmt->n_args != 2)
                exit(1);
            if (stmt->args[0]->op != OP_VAR)
                exit(1);
            if (stmt->args[1]->op != OP_VAL)
                exit(1);

            obj_dst = stmt->args[0]->data.obj;

            stmt->args[1] = eval_op_val(stmt->args[1], global_sym_table);
            obj_src = stmt->args[1]->data.obj;
            stmt->args[1]->data.obj = NULL;
            /* obj_src = stmt->args[1]->args[0]->data.obj; */
            if (obj_move(&trash, obj_dst, obj_src) < 0)
            {
                fprintf(stderr, "Failed cast: %s to %s\n", obj_src->tp->name, obj_dst->tp->name);
                goto on_error;
            }
            obj_free(obj_src);

            if ((pattr = attr_map_find(&entp->am, stmt->args[0]->data.name)))
            {
                /* attribute is already in *entp - overwrite if the type is appropriate */
                if (((tp_expect = pattr->tp_expect) && pattr->tp_expect != obj_dst->tp) ||
                    (pattr->obj && (tp_expect = pattr->obj->tp) != obj_dst->tp))
                {
                    char expect_canon_tp[TAG_LEN], actual_canon_tp[TAG_LEN];

                    tp_canon_name(tp_expect, expect_canon_tp, TAG_LEN);
                    tp_canon_name(obj_dst->tp, actual_canon_tp, TAG_LEN);
                    fprintf(stderr, "Attribute %s: type is %s, was expecting %s\n",
                            pattr->tag, actual_canon_tp, expect_canon_tp);
                    exit(1);/* TODO: handle this gracefully */
                }

                if (pattr->obj)
                    obj_free(pattr->obj);
                pattr->obj = obj_dst;
                memset(&stmt->args[0]->data, 0, sizeof(stmt->args[0]->data));

                obj_code(pattr->obj, obj_value_str, obj_value_str_len);
                TRACE_DBG("Attribute %s = %s\n", pattr->tag, obj_value_str);
            }
            else
            {
                strncpy(attr.tag, stmt->args[0]->data.name, TAG_LEN);
                attr.tag[TAG_LEN - 1] = '\0';
                attr.tp_expect = NULL;
                attr.obj = obj_dst;
                attr_map_add(&entp->am, &attr);
                memset(&stmt->args[0]->data, 0, sizeof(stmt->args[0]->data));

                obj_code(attr.obj, obj_value_str, obj_value_str_len);
                TRACE_DBG("Attribute %s = %s\n", attr.tag, obj_value_str);
            }
        }
        if (entp == &new_ent)
            entity_map_add(em, &new_ent);
        block_cleanup(blk);
    }

    if (blocks)
        free(blocks);
    blocks = NULL;
    n_blocks = 0;

    free(obj_value_str);
    return 0;
on_error:

    free(obj_value_str);
    return -1;
}

static struct operator *eval_op_seq(struct operator *op, struct attr_map *global_sym_table)
{
    int idx;
    struct object *seq, *elem;
    struct operator *op_child, *op_val;

    if (op->op != OP_SEQ)
    {
        yyerror("eval_op_val: Was expecting OP_SEQ\n");
        return NULL;
    }

    seq = obj_alloc(tp_seq);
    obj_init(seq);

    for (idx = 0; idx < op->n_args; idx++)
    {
        op_child = eval_op_val(op->args[idx], global_sym_table);
        op->args[idx] = NULL;
        elem = op_child->data.obj;
        seq_prepend(seq, elem);
        op_free(op_child);
    }

    op->n_args = 0;
    free(op->args);
    op->args = NULL;
    op_free(op);

    op_val = (struct operator*)calloc(1, sizeof(struct operator));
    op_val->op = OP_VAL;
    op_val->data.obj = seq;

    return op_val;
}

static struct operator *eval_op_negative(struct operator *op,
                                         struct attr_map *global_sym_table)
{
    struct object *obj_out;
    struct operator *op_out, *op_arg;

    if (op->op != OP_NEGATIVE)
    {
        yyerror("eval_op_negative: was expecting OP_NEGATIVE\n");
        return NULL;
    }

    assert(op->n_args == 1);
    op_arg = eval_op_val(op->args[0], global_sym_table);

    /* calling eval_op_val will free op->args[0] */
    free(op->args);
    op->args = NULL;
    op->n_args = 0;

    obj_out = obj_negative(op_arg->data.obj);

    if (!obj_out)
    {
        yyerror("eval_op_negative: not supported for %s\n", op_arg->data.obj->tp->name);
        return NULL;
    }

    op_free(op);
    op_out = calloc(1, sizeof(struct operator));
    op_out->data.obj = obj_out;
    op_out->op = OP_VAL;

    return op_out;
}

static struct operator *eval_op_call(struct operator *op, struct attr_map *global_sym_table)
{
    struct attr *func_attr;
    struct object *out;
    struct operator *op_args, *op_out;

    if (op->op != OP_CALL)
    {
        yyerror("eval_op_call: was expecting OP_CALL\n");
        return NULL;
    }

    func_attr = attr_map_find(global_sym_table, op->data.name);

    if (!func_attr)
    {
        yyerror("Could not find function %s\n", op->data.name);
        return NULL;
    }

    if (op->n_args)
    {
        assert(op->n_args == 1);
        op_args = eval_op_seq(*op->args, global_sym_table);
        free(op->args);
        op->args = NULL;
        op->n_args = 0;
    }
    else
    {
        op_args = calloc(1, sizeof(struct operator));
        op_args->op = OP_VAL;
        op_args->data.obj = obj_alloc(tp_seq);
        obj_init(op_args->data.obj);
    }


    obj_call(func_attr->obj, &out, op_args->data.obj);

    /*
     * XXX: As a consequence of the way that we free op->args, functions cannot return
     * their own arguments; they must instead return copies of them.  This may be problematic
     * in the future if the scope of lvl expands
     */
    obj_free(op_args->data.obj);
    op_free(op_args);
    op_free(op);

    op_out = calloc(1, sizeof(struct operator));
    op_out->data.obj = out;
    op_out->op = OP_VAL;

    return op_out;
}

static struct operator *eval_op_val(struct operator *op, struct attr_map *global_sym_table)
{
    struct operator *child, *op_ret;
    struct object *func, *call_args;

    if (op->op != OP_VAL)
    {
        yyerror("eval_op_val: Was expecting OP_VAL\n");
        return NULL;
    }

    assert(op->args && (op->n_args == 1));
    child = *op->args;

    switch (child->op)
    {
    case OP_LITERAL:
        op_ret = calloc(1, sizeof(struct operator));
        memcpy(op_ret, child, sizeof(struct operator));
        child->args = NULL;
        child->n_args = 0;
        op_free(child);
        break;
    case OP_SEQ:
        op_ret = eval_op_seq(child, global_sym_table);
        break;
    case OP_CALL:
        op_ret = eval_op_call(child, global_sym_table);
        break;
    case OP_NEGATIVE:
        op_ret = eval_op_negative(child, global_sym_table);
        break;
    default:
        yyerror("Unsupported (so far) operator type %d\n", child->op);
        return NULL;
    }

    free(op->args);
    op->args = NULL;
    op->n_args = 0;
    op_free(op);

    return op_ret;
}

void parser_init(void)
{
    struct type *tp_str_, *tp_array_, *tp_seq_, *tp_int_, *tp_func_,
        *tp_real_, *tp_pt2_;

    if (parser_initialized)
        return;
    parser_initialized = 1;

    tp_func_ = (struct type*)calloc(1, sizeof(struct type));
    strncpy(tp_func_->name, "function", TAG_LEN);
    tp_func_->name[TAG_LEN-1] = '\0';
    tp_func_->op_call = func_call;
    tp_func_->init = func_init;
    tp_func_->sz = sizeof(struct lvl_func);

    tp_str_ = (struct type*)calloc(1, sizeof(struct type));
    strncpy(tp_str_->name, "string", TAG_LEN);
    tp_str_->name[TAG_LEN-1] = '\0';
    tp_str_->op_move = str_move;
    tp_str_->init = str_init;
    tp_str_->op_str = str_as_str;
    tp_str_->op_code = str_as_code;
    tp_str_->sz = sizeof(struct lvl_str);

    tp_array_ = (struct type*)calloc(1, sizeof(struct type));
    strncpy(tp_array_->name, "array", TAG_LEN);
    tp_array_->name[TAG_LEN-1] = '\0';
    tp_array_->op_move = array_move;
    tp_array_->init = array_init;
    tp_array_->sz = sizeof(struct lvl_array);
    tp_array_->op_index = array_index;
    tp_array_->op_len = array_len;
    tp_array_->op_dtor = array_dtor;

    tp_seq_ = (struct type*)calloc(1, sizeof(struct type));
    strncpy(tp_seq_->name, "sequence", TAG_LEN);
    tp_seq_->name[TAG_LEN-1] = '\0';
    tp_seq_->sz = sizeof(struct lvl_seq);
    tp_seq_->init = seq_init;
    tp_seq_->op_index = seq_index;
    tp_seq_->op_len = seq_len;
    tp_seq_->op_dtor = seq_dtor;
    tp_seq_->op_move = seq_move;

    tp_int_ = (struct type*)calloc(1, sizeof(struct type));
    strncpy(tp_int_->name, "int", TAG_LEN);
    tp_int_->name[TAG_LEN-1] = '\0';
    tp_int_->sz = sizeof(struct lvl_int);
    tp_int_->op_move = lvl_int_move;
    tp_int_->op_code = lvl_int_as_code;
    tp_int_->op_mult = lvl_int_mult;
    tp_int_->op_div = lvl_int_div;
    tp_int_->op_add = lvl_int_add;
    tp_int_->op_sub = lvl_int_sub;
    tp_int_->raw_int = lvl_int_raw_int;
    tp_int_->raw_real = lvl_int_raw_real;
    tp_int_->op_negative = lvl_int_negative;

    tp_real_ = (struct type*)calloc(1, sizeof(struct type));
    strncpy(tp_real_->name, "real", TAG_LEN);
    tp_real_->name[TAG_LEN-1] = '\0';
    tp_real_->sz = sizeof(struct lvl_real);
    tp_real_->op_move = lvl_real_move;
    tp_real_->op_code = lvl_real_as_code;
    tp_real_->op_mult = lvl_real_mult;
    tp_real_->op_div = lvl_real_div;
    tp_real_->op_add = lvl_real_add;
    tp_real_->op_sub = lvl_real_sub;
    tp_real_->raw_int = lvl_real_raw_int;
    tp_real_->raw_real = lvl_real_raw_real;
    tp_real_->op_negative = lvl_real_negative;

    tp_pt2_ = (struct type*)calloc(1, sizeof(struct type));
    strncpy(tp_pt2_->name, "pt2", TAG_LEN);
    tp_pt2_->sz = sizeof(struct lvl_pt2);
    tp_pt2_->op_move = lvl_pt2_move;
    tp_pt2_->op_code = lvl_pt2_as_code;
    tp_pt2_->op_mult = lvl_pt2_mult;
    tp_pt2_->op_add = lvl_pt2_add;
    tp_pt2_->op_sub = lvl_pt2_sub;

    tp_func = tp_func_;
    tp_str = tp_str_;
    tp_array = tp_array_;
    tp_seq = tp_seq_;
    tp_int = tp_int_;
    tp_real = tp_real_;
    tp_pt2 = tp_pt2_;
    tp_add(tp_func_);
    tp_add(tp_str_);
    tp_add(tp_array_);
    tp_add(tp_seq_);
    tp_add(tp_int_);
    tp_add(tp_real_);
    tp_add(tp_pt2_);
}

static void block_cleanup(struct block *blk)
{
    int i;

    for (i = 0; i < blk->stmt_count; i++)
        if (blk->stmts[i])
        {
            op_free(blk->stmts[i]);
        }
    if (blk->stmts)
        free(blk->stmts);

    memset(blk, 0, sizeof(struct block));
}

static void op_free(struct operator *op)
{
    /* lvl_parse frees the object.  This works but is it the best way? */
    /* if (op->data.obj) */
    /*     obj_free(op->data.obj); */
    int arg_idx;

    assert((op->args && op->n_args) || !(op->args || op->n_args));

    for (arg_idx = 0; arg_idx < op->n_args; arg_idx++)
    {
        assert(op->args[arg_idx]);
        op_free(op->args[arg_idx]);
    }

    if (op->args)
        free(op->args);

    free(op);
}

int yyerror(const char *errmsg, ...)
{
    va_list args;

    va_start(args, errmsg);
    lvl_err_msg_v(errmsg, args);
    va_end(args);

    return 0;
}
