/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef MKLEVEL_H_
#define MKLEVEL_H_

#include <stdarg.h>

#include "real.h"

#define TAG_LEN 64
#define STR_LEN 128

#define LVL_MAX_PATH_LEN 1024
#define MAX_NAME_LEN 1024
#define MAX_AUTHOR_LEN 1024
#define MAX_DESC_LEN 1024
#define MAX_LICENSE_LEN 1024
#define MAX_COPYRIGHT_LEN 1024
#define MAX_TEX_PATH_LEN TAG_LEN

/*
 *
 * The basic structure of a .lvl file is a number of entities indexed
 * by unique tags.
 * Each of these entities contains a map of several attributes.
 *
 * mklvl will read in all these entities and attributes and use the ones it recognizes
 * to build the output file which contains the map which is actually loaded by the game.
 * 
 * This way, the level editor doesn't need a separate UI for every element in the game;
 * It can provide the user with a simple entity/attr editor and the user can supply the
 * attribute names.  This also means that if you compile a level in an older version of
 * mklvl, unsupported entities and attributes will simply be ignored, preserving backwards
 * compaitibility.
 *
 *
 * typical syntax example:
 *       entity some_ent {
 *           string ui_name = "an example entity";
 *           pt2    position = pt_2(1.5, 0);
 *           real   health = 150.0;
 *           int    shit_idk = 1337;
 *           pt2    verts[] = { pt2(-1.0, 0.0), pt2(0.0, 1.0), pt2(1.0, 0.0) };
 *           int    edges[] = { 0, 1, 2 };
 *       };
 *       entity some_other_ent = {
 *           ...
 *       };
 *       entity yet_another_ent = {
 *           ...
 *       };
 */

struct attr
{
    char tag[TAG_LEN];

    /*
     * tp_expect is the expected type of obj.
     *
     * If obj is NULL but tp_expect is not, then
     * this is an empty slot in an entity spec which is
     * expecting an object of type tp_expect.
     *
     * If obj is not NULL but tp_expect is, then
     * this object was not expected.  This is not
     * considered to be an error; entities can have
     * extraneous members that weren't in the spec.
     *
     * If both obj and tp_expect are non-null, then this
     * is a slot which has been filled.  It is considered an
     * error if obj->tp != tp_expect.
     *
     */
    struct object *obj;
    struct type const *tp_expect;
};

struct attr_map
{
    struct attr *attrs;
    int n_attrs;
};

struct entity
{
    char type[TAG_LEN];
    char tag[TAG_LEN];
    struct attr_map am;
};

struct entity_map
{
    struct entity *ents;
    int n_ents;
};

/* 
 * lvl files are implemented in an object-oriented style;
 * This makes it easy to implement implicit typecasting.
 */
struct object;

struct type
{
    size_t sz;
    char name[TAG_LEN];

    /*
     * encapsulated type; only used for arrays.
     * In the future, I may make struct type itself subclassible
     * so that shit like this doesn't need to be in every instance.
     */
    struct type const *tp_encap;
    int encap_len;

    int (*op_move)(struct object **output, struct object *lhs, struct object *rhs);
    int (*init)(struct object *obj);
    void (*op_dtor)(struct object *obj);

    int (*op_call)(struct object *obj, struct object **output, struct object *args);
    int (*op_mult)(struct object *obj, struct object **output, struct object *rhs);
    int (*op_div)(struct object *obj, struct object **output, struct object *rhs);
    int (*op_add)(struct object *obj, struct object **output, struct object *rhs);
    int (*op_sub)(struct object *obj, struct object **output, struct object *rhs);

    /* sequence protocol. */
    struct object* (*op_index)(struct object *obj, int idx);
    int (*op_len)(struct object *obj);

    void (*op_str)(struct object *obj, char *str_out, int len);
    int (*raw_int)(struct object *obj, int *out);
    int (*raw_real)(struct object *obj, real_t *out);

    struct object* (*op_negative)(struct object *obj);

    /* should return a string that would represent this object as a literal */
    void (*op_code)(struct object *obj, char *str_out, int len);

    /*
     * tp_canon_name_ should return the actual name of the type as presented in source code.
     * if it is different from the name field.  This is chiefly used by fixed-length arrays,
     * which can't store their lengths in row-major format due to the way they're layered.
     *
     * This is hackish and shouldn't have to exist, but is necessary due to the way that
     * I represent different lengths of arrays as being distinct types.  This is in turn
     * necessary because the array object itself is responsible for storing its length.
     * That in turn is necessary so that fixed-length arrays can be assigned from sequences.
     * Ideally I would instead associate the length with the object instead of the type,
     * but then multidimensional arrays become harder to validate.  If I ever expand lvl
     * to be more than a passive storage format, then I should probably revisit this and
     * figure out how to represent multi-dimensional arrays the "right" way.
     *
     * If this function is omitted, then the name field will be returned by the
     * tp_canon_name wrapper function
     */
    void (*tp_canon_name_)(struct type const *tp, char *str_out, int len);

    /*
     * Another "work-around function hook" to address an oddity of arrays.
     * I have chose to implement the C-style syntax of putting the [] operator
     * after the variable name rather than the java-style syntax of placing it
     * after the type.  This means that when you convert an array to code, the
     * variable name actually goes in the middle of the type name.  This function,
     * if implemented, should output a type including the variable name to str_out.
     * If this function is not implemented, the canonical name will be used instead.
     */
    void (*tp_as_code_)(struct type const *tp, char const *var_name, char *str_out, int len);
};

void tp_canon_name(struct type const *tp, char *str_out, int len);
void tp_as_code(struct type const *tp, char const *var_name, char *str_out, int len);

extern struct type *tp_str, *tp_array, *tp_seq, *tp_int, *tp_func, *tp_real, *tp_pt2;

struct object
{
    struct type const *tp;
};

struct lvl_str
{
    struct object obj;

    char txt[STR_LEN];
};

struct lvl_array
{
    struct object obj;

    int len;
    struct object **arr;
};

struct lvl_seq
{
    struct object obj;

    int len;
    struct object **seq;
};

struct lvl_int
{
    struct object obj;

    int val;
};

struct lvl_real
{
    struct object obj;

    real_t val;
};

struct lvl_pt2
{
    struct object obj;

    real_t val[2];
};

/*
 * Structure used to represent functions.  It might improve code
 * recyclability if I made functions a special type of callable
 * object instead of something completely distinct from objects,
 * but I'm not doing that for now because I have no intention of
 * allowing users to define their own functions within a lvl file.
 * Even if I did, I still might not want to make objects callable
 * because I don't want this language to turn into Python.
 */
struct lvl_func
{
    struct object obj;
    int (*impl)(struct object **out, struct object *args);
};

/* parser context */
struct lvl_ctxt
{
    struct entity_map ents;
    struct attr_map global_syms;
};

void attr_map_init(struct attr_map *am);
void attr_map_cleanup(struct attr_map *am);
struct attr *attr_map_find(struct attr_map *am, char const tag[TAG_LEN]);
struct object *attr_map_find_obj(struct attr_map *am, char const tag[TAG_LEN]);
void attr_map_add(struct attr_map *am, struct attr *attr);

void entity_map_init(struct entity_map *em);
void entity_map_cleanup(struct entity_map *em);
struct entity *entity_map_find(struct entity_map *em, char const tag[TAG_LEN]);
void entity_map_add(struct entity_map *em, struct entity *ent);
void entity_as_code(struct entity *ent, char* out, int len);

int lvl_parse_(struct entity_map *em, struct attr_map *global_sym_table);
int lvl_parse_string(const char *in_str, struct entity_map *em, struct attr_map *global_sym_table);
int lvl_parse_file(FILE *file, struct entity_map *em, struct attr_map *global_sym_table);

/*
 * Get the type for an array of encap.
 * The type will automatically be added if necessary
 */
struct type* tp_array_of(struct type const *encap);
struct type* tp_fixed_array_of(struct type const *encap, int len);

struct object *obj_alloc(struct type const *tp);
void obj_free(struct object *obj);
int obj_init(struct object *obj);
int obj_move(struct object **output, struct object *lhs,
             struct object *rhs);
void obj_str(struct object *obj, char *str_out, int len);
void obj_code(struct object *obj, char *str_out, int len);
int obj_raw_int(struct object *obj, int *out);
int obj_raw_real(struct object *obj, real_t *out);
int obj_call(struct object *obj, struct object **output, struct object *args);
int obj_len(struct object *obj, int *len);
struct object *obj_index(struct object *obj, int idx);
struct object *obj_negative(struct object *obj);

int func_init(struct object *obj);
int func_call(struct object *obj, struct object **output, struct object *args);
void func_set_impl(struct object *obj, int (*impl)(struct object**, struct object*));

int str_init(struct object *obj);
void str_set(struct object *obj, char const *txt);
char const* str_get(struct object *obj);
int str_move(struct object **output, struct object *lhs,
             struct object *rhs);
void str_as_str(struct object *obj, char *str_out, int len);
void str_as_code(struct object *obj, char *str_out, int len);

int lvl_int_move(struct object **output, struct object *lhs, struct object *rhs);
void lvl_int_as_code(struct object *obj, char *str_out, int len);
int lvl_int_raw_int(struct object *obj, int *out);
int lvl_int_raw_real(struct object *obj, real_t *out);
void lvl_int_set(struct object *obj, int val);
struct object *lvl_int_negative(struct object *obj);
int lvl_int_mult(struct object *lhs, struct object **output,
                 struct object *rhs);
int lvl_int_div(struct object *lhs, struct object **output,
                struct object *rhs);
int lvl_int_add(struct object *lhs, struct object **output,
                struct object *rhs);
int lvl_int_sub(struct object *lhs, struct object **output,
                struct object *rhs);

int lvl_real_move(struct object **output, struct object *lhs, struct object *rhs);
void lvl_real_as_code(struct object *obj, char *str_out, int len);
int lvl_real_raw_int(struct object *obj, int *out);
int lvl_real_raw_real(struct object *obj, real_t *out);
void lvl_real_set(struct object *obj, real_t val);
struct lvl_real *lvl_real_from_obj(struct object *obj);
struct object *lvl_real_negative(struct object *obj);
int lvl_real_mult(struct object *lhs, struct object **output,
                  struct object *rhs);
int lvl_real_div(struct object *lhs, struct object **output,
                  struct object *rhs);
int lvl_real_add(struct object *lhs, struct object **output,
                 struct object *rhs);
int lvl_real_sub(struct object *lhs, struct object **output,
                 struct object *rhs);

int lvl_pt2_init(struct object *obj);
int lvl_pt2_move(struct object **output, struct object *lhs, struct object *rhs);
void lvl_pt2_as_code(struct object *obj, char *str_out, int len);
void lvl_pt2_set(struct object *obj, real_t const pt_in[2]);
int lvl_pt2_mult(struct object *lhs, struct object **output,
                 struct object *rhs);
int lvl_pt2_add(struct object *lhs, struct object **output,
                struct object *rhs);
int lvl_pt2_sub(struct object *lhs, struct object **output,
                struct object *rhs);

int array_init(struct object *obj);
int array_move(struct object **output, struct object *lhs, struct object *rhs);
struct object *array_index(struct object *obj, int idx);
int array_len(struct object *obj);
int array_append(struct object *obj, struct object *new_elem);
int array_prepend(struct object *obj, struct object *new_elem);
void array_as_code(struct object *obj, char *str_out, int len);
void array_clear(struct object *obj); /* empty the array and delete its contents */
void array_dtor(struct object *obj);
void array_canon_name(struct type const *tp, char *str_out, int len);
void array_tp_as_code(struct type const *tp, char const *var_name, char *str_out, int len);

int seq_init(struct object *obj);
struct object *seq_index(struct object *obj, int idx);
int seq_len(struct object *obj);
int seq_append(struct object *obj, struct object *new_elem);
int seq_prepend(struct object *obj, struct object *new_elem);
void seq_dtor(struct object *obj);
int seq_move(struct object **output, struct object *lhs, struct object *rhs);

void lvl_err_msg(char const *what, ...);
void lvl_err_msg_v(char const *what, va_list arg);

/* tp should be malloc'd.  It will be freed by lvl_cleanup */
void tp_add(struct type *tp);
struct type* tp_find(const char *name);

void cleanup_types(void);
void parser_init(void);
void parser_cleanup(void);
#endif
