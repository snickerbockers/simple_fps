/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <err.h>
#include <stdio.h>
#include <unistd.h>

#include "game_map.h"
#include "stream.h"
#include "bsp.h"

static void dump_bsp_node(FILE *stream, struct bsp_node const *node);
static void print_usage(char const *cmd);
static void dump_bsp_tree(FILE *stream, struct bsp_tree const *tree);

static void print_usage(char const *cmd)
{
    printf("Usage: %s PATH\n", cmd ? cmd : "dump_bsp");
}

static void dump_bsp_node(FILE *stream, struct bsp_node const *node)
{
    fprintf(stream, "{\n");
    fprintf(stream, "\tfront = %u\n", node->front);
    fprintf(stream, "\tback = %u\n", node->back);
    fprintf(stream, "\tline = { 0x%x, 0x%x, 0x%x }\n",
            node->line[0], node->line[1], node->line[2]);
    fprintf(stream, "\tseg_base = %u\n", node->seg_base);
    fprintf(stream, "\tseg_count = %u\n", node->seg_count);
    fprintf(stream, "}");
}

static void dump_bsp_tree(FILE *stream, struct bsp_tree const *tree)
{
    int idx;

    fprintf(stream, "root_node_idx = %d\n", tree->root_node_idx);
    for (idx = 0; idx < tree->n_nodes; idx++)
    {
        fprintf(stream, "nodes[%d] = ", idx);
        dump_bsp_node(stream, tree->nodes + idx);
        fprintf(stream, ";\n");
    }
}

int main(int argc, char **argv)
{
    int opt;
    stream_t *in_stream;
    FILE *out_stream;
    char const *in_path, *cmd, *out_path;
    struct bsp_tree tree;
    real_t spawn_pt[2];
    struct game_map game_map;

    out_stream = stdout;
    cmd = argv[0];
    while ((opt = getopt(argc, argv, "o:")) != -1)
    {
        switch (opt)
        {
        case 'o':
            out_path = optarg;
            if (!(out_stream = fopen(out_path, "w")))
                err(-1, "unable to open %s", out_path);
            break;
        default:
            print_usage(cmd);
            return -1;
        }
    }

    argc -= optind;
    argv += optind;
    if (argc != 1)
    {
        print_usage(cmd);
        exit(-1);
    }
    in_path = *argv;

    game_map_init(&game_map);
    if (!(in_stream = stream_stdio_open(in_path, "rb")))
        err(-1, "unable to open %s", in_path);
    if (load_map(&game_map, in_stream) != 0)
        errx(-1, "unable to load %s", in_path);
    /* level_meta_print(&meta, stdout); */
    /* fprintf(out_stream, "Player Spawn: (0x%x, 0x%x)\n", */
    /*         spawn_pt[0], spawn_pt[1]); */
    fprintf(stderr, "RBSP:\n");
    dump_bsp_tree(out_stream, &game_map.rbsp);
    fprintf(stderr, "CBSP:\n");
    dump_bsp_tree(out_stream, &game_map.cbsp);

    game_map_cleanup(&game_map);
    stream_close(in_stream);
    if (out_stream != stdout)
        fclose(out_stream);

    return 0;
}
