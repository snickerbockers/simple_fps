/*****************************************************************************
 **
 ** Copyright (c) 2015, Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>

#include "angle.h"
#include "real.h"

/* 
 * angle.h declared these as extern, but we can't link in
 * tables.c for obvious reasons
 */
real_t angle_sin_tbl[65536];
real_t angle_cos_tbl[65536];
real_t angle_tan_tbl[65536];

static char const *copyright = 
    "/*****************************************************************************\n"
    " **\n"
    " ** Copyright (c) 2015, Jay Elliott\n"
    " **\n"
    " ** This file is part of simple_fps.\n"
    " **\n"
    " ** simple_fps is free software: you can redistribute it and/or modify\n"
    " ** it under the terms of the GNU General Public License as published by\n"
    " ** the Free Software Foundation, either version 3 of the License, or\n"
    " ** (at your option) any later version.\n"
    " **\n"
    " ** simple_fps is distributed in the hope that it will be useful,\n"
    " ** but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    " ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    " ** GNU General Public License for more details.\n"
    " **\n"
    " ** You should have received a copy of the GNU General Public License\n"
    " ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.\n"
    " **\n"
    " *****************************************************************************/\n"
    ;

static void print_trig_tbl(FILE *out, char const *arr_name, double(*trig_fn)(double))
{
    unsigned int ang;
    double theta;
    int col;

    fprintf(out, "real_t %s[%u] = {\n", arr_name, 1 << (sizeof(angle_t) << 3));

    col = 0;
    for (ang = 0; ang < 0x10000; ang++)
    {
        theta = M_PI * 2.0 * (double)ang / (double)0x10000;
        fprintf(out, "    0x%08x%s",
                real_from_double(trig_fn(theta)), ang != 0xffff ? "," : "");
        if (++col >= 4)
        {
            col = 0;
            fprintf(out, "\n");
        }
    }
    fprintf(out, "};\n");
}

static void print_copyright(FILE *out)
{
    fputs(copyright, out);
}

int main(int argc, char **argv)
{
    FILE *out_file = fopen("tables.c", "w");
    print_copyright(out_file);
    fputc('\n', out_file);
    fputs("#include \"real.h\"", out_file);
    fputc('\n', out_file);
    print_trig_tbl(out_file, "angle_sin_tbl", sin);
    fputc('\n', out_file);
    print_trig_tbl(out_file, "angle_cos_tbl", cos);
    fputc('\n', out_file);
    print_trig_tbl(out_file, "angle_tan_tbl", tan);

    return 0;
}
