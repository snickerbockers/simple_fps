/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** synmaker is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** synmaker is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with synmaker.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdio.h>
#include <string.h>

#include "syn_track.h"
#include "syn_file.h"

#include "sndserv.h"
#include "mixer.h"

#define BUF_LEN (SNDSERV_SAMPLE_FREQ * 10)

int16_t samp_buf[BUF_LEN];

sample_t callback_fn(sample_idx_t sample_idx, void *userarg)
{
    struct syn_track *track = (struct syn_track*)userarg;
    sample_t buf;

    read_samples(&buf, 1, track);

    return buf;
}

void print_usage(char const *cmd_path)
{
    fprintf(stderr, "Usage: %s syn_file\n", cmd_path);
}

int main(int argc, char **argv)
{
    FILE *in_file;
    struct sndserv sndserv;
    struct mixer mixer;
    struct syn_track track;
    struct synth_sound mixer_sound;

    if (argc != 2)
    {
        print_usage(argv[0]);
        return 1;
    }

    in_file = fopen(argv[1], "r");

    if (!in_file)
        err(1, "Unable to open \"%s\"", argv[1]);

    if (sndserv_init(&sndserv) != 0)
    {
        fprintf(stderr, "Failed to initialize sound server\n");
        return 1;
    }

    if (mixer_init(&mixer, &sndserv) != 0)
    {
        fprintf(stderr, "Failed to initialize audio mixer\n");
        sndserv_cleanup(&sndserv);
        return 1;
    }

    parse_syn(in_file, &track);

    fclose(in_file);

    mixer_sound.callback = callback_fn;
    mixer_sound.callback_data = &track;
    mixer_sound.n_samples = 60 * SNDSERV_SAMPLE_FREQ;
    mixer_sound.flags = 0;

    mixer_play_sound(&mixer, &mixer_sound, VOL_FULL);

    int err_code;
    while (1)
    {
        /* read_samples(samp_buf, BUF_LEN, &cur_track); */
        /* pa_simple_write(s, samp_buf, sizeof(samp_buf), &err_code); */
    }

    mixer_cleanup(&mixer);
    sndserv_cleanup(&sndserv);

    return 0;
}
