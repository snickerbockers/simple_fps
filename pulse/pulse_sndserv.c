/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "trace.h"

#include "sndserv.h"

#define SNDSERV_PULSE_BUF_LEN SNDSERV_SAMPLE_FREQ

static void write_to_pulse_stream(pa_stream *p, size_t bytes_needed,
                                  void *userdata)
{
    size_t bytes_to_read;
    struct sndserv *srv;
    static uint8_t buf[SNDSERV_PULSE_BUF_LEN];

    srv = (struct sndserv*)userdata;

    while (bytes_needed)
    {
        bytes_to_read = SNDSERV_PULSE_BUF_LEN * sizeof(buf[0]) < bytes_needed ?
            SNDSERV_PULSE_BUF_LEN * sizeof(buf[0]) : bytes_needed;

        /* TODO: Figure out if bytes_to_read can include partial samples */
        assert(!(bytes_to_read % 2));
        assert(bytes_to_read <= bytes_needed);

        if (srv->sample_source)
        {
            srv->sample_source(buf, bytes_to_read, srv->sample_source_arg);
        }
        else
        {
            memset(buf, 0, bytes_to_read);
        }

        pa_stream_write(p, buf, bytes_to_read, NULL, 0, PA_SEEK_RELATIVE);

        bytes_needed -= bytes_to_read;
    }
}

static void begin_playback(struct sndserv *srv)
{
    pa_sample_spec ss;

    ss.format = PA_SAMPLE_S16LE;
    ss.channels = 2;
    ss.rate = SNDSERV_SAMPLE_FREQ;

    srv->impl.pulse_stream = pa_stream_new(srv->impl.pulse_ctxt, "simple_fps",
                                           &ss, NULL);

    if (!srv->impl.pulse_stream)
    {
        TRACE_ERR("Failed to create new pulse audio stream\n");
        return;
    }

    pa_stream_set_write_callback(srv->impl.pulse_stream,
                                 write_to_pulse_stream, srv);
    pa_stream_connect_playback(srv->impl.pulse_stream, NULL, NULL, 0,
                               NULL, NULL);
}

static void ctxt_state_callback(pa_context *ctxt, void *arg)
{
    if (pa_context_get_state(ctxt) == PA_CONTEXT_READY)
    {
        begin_playback((struct sndserv*)arg);
    }
}

int sndserv_impl_init(struct sndserv *srv)
{
    int err;
    struct sndserv_impl *impl;
    pa_mainloop_api *mainloop_api;

    impl = &srv->impl;

    impl->pulse_mainloop = pa_threaded_mainloop_new();
    if (!impl->pulse_mainloop)
    {
        TRACE_ERR("Unable to instantiate pulse main loop\n");
        return -1;
    }

    mainloop_api = pa_threaded_mainloop_get_api(impl->pulse_mainloop);
    impl->pulse_ctxt = pa_context_new(mainloop_api, "simple_fps");
    if (!impl->pulse_ctxt)
    {
        TRACE_ERR("Unablee to allocate pulse context\n");
        return -1;
    }

    pa_context_set_state_callback(impl->pulse_ctxt, ctxt_state_callback, srv);

    if ((err = pa_context_connect(impl->pulse_ctxt, NULL,
                                  PA_CONTEXT_NOFLAGS, NULL)))
    {
        TRACE_ERR("Unable to connect to pulse context\n");
        pa_threaded_mainloop_free(impl->pulse_mainloop);

        return err;
    }

    pa_threaded_mainloop_start(impl->pulse_mainloop);

    return 0;
}

void sndserv_impl_cleanup(struct sndserv *srv)
{
    struct sndserv_impl *impl;

    impl = &srv->impl;
    if (impl->pulse_stream)
        pa_stream_unref(impl->pulse_stream);
    pa_threaded_mainloop_stop(impl->pulse_mainloop);
    pa_context_disconnect(impl->pulse_ctxt);
    pa_threaded_mainloop_free(impl->pulse_mainloop);

    memset(impl, 0, sizeof(*impl));
}
