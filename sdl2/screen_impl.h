/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#ifndef SCREEN_IMPL_SDL2_H_
#define SCREEN_IMPL_SDL2_H_

#include "screen.h"

#define RED_SHIFT   16
#define GREEN_SHIFT  8
#define BLUE_SHIFT   0
#define ALPHA_SHIFT 24

#define RED_MASK   0x00ff0000
#define GREEN_MASK 0x0000ff00
#define BLUE_MASK  0x000000ff
#define ALPHA_MASK 0xff000000

struct screen_impl
{
    SDL_Window *win;
    SDL_Renderer *ren;
    SDL_Texture *screen_tex;
};

#endif
