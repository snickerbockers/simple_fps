/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <err.h>

#include <SDL2/SDL.h> 

#include "screen.h"
#include "screen_impl.h"

static int init_screen_impl(struct screen_impl *scr, unsigned w, unsigned h);

int create_screen_impl(struct screen *scr, int w, int h)
{
    struct screen_impl *impl =
        (struct screen_impl*)malloc(sizeof(struct screen_impl));

    if (!impl)
        err(1, "Unable to allocate rendering context\n");
    if (init_screen_impl(impl, w, h) != 0)
        errx(1, "unable to initialize screen\n");
    scr->impl = impl;
    scr->screen_w = w;
    scr->screen_h = h;
    scr->frame_buffer = (color_t*)malloc(sizeof(color_t) * w * h);
    if (!scr->frame_buffer)
        err(1, "Unable to allocate frame buffer\n");

    return 0;
}

void destroy_screen_impl(struct screen *scr)
{
    struct screen_impl *impl = scr->impl;

    SDL_Quit();
    free(scr->frame_buffer);
    free(impl);
}

static int init_screen_impl(struct screen_impl *impl, unsigned w, unsigned h)
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_JOYSTICK);

    SDL_CreateWindowAndRenderer(w, h, 0, &impl->win, &impl->ren);
    impl->screen_tex = SDL_CreateTexture(impl->ren,
                                         SDL_PIXELFORMAT_ARGB8888,
                                         SDL_TEXTUREACCESS_STREAMING, w, h);
    if (!impl->screen_tex)
        return -1;

    SDL_ShowCursor(0);
    return 0;
}

void flip_screen(struct screen *scr)
{
    SDL_UpdateTexture(scr->impl->screen_tex, NULL,
                      scr->frame_buffer, scr->screen_w * sizeof(color_t));
    SDL_RenderClear(scr->impl->ren);
    SDL_RenderCopy(scr->impl->ren, scr->impl->screen_tex, NULL, NULL);
    SDL_RenderPresent(scr->impl->ren);
}

void clear_screen(struct screen *scr, color_t color)
{
    // FIXME: using memset is not viable because it casts the value to a char
    unsigned i, n_pixels = scr->screen_w * scr->screen_h;
    for (i = 0; i < n_pixels; i++)
        scr->frame_buffer[i] = color;
}

color_t color_pack_rgb(struct screen const *scr, color_t r, color_t g, color_t b)
{
    return color_pack_rgba(scr, r, g, b, 0xff);
}

color_t color_pack_rgba(struct screen const *scr, color_t r, color_t g, color_t b,
                        color_t a)
{
    return ((b & 0xff) << BLUE_SHIFT) |
        ((g & 0xff) << GREEN_SHIFT) |
        ((r & 0xff) << RED_SHIFT) |
        ((a & 0xff) << ALPHA_SHIFT);
}

color_t color_red(struct screen const *scr, color_t c)
{
    return (c >> RED_SHIFT) & 0xff;
}

color_t color_blue(struct screen const *scr, color_t c)
{
    return (c >> BLUE_SHIFT) & 0xff;
}

color_t color_green(struct screen const *scr, color_t c)
{
    return (c >> GREEN_SHIFT) & 0xff;
}

color_t color_alpha(struct screen const *scr, color_t c)
{
    return (c >> ALPHA_SHIFT) & 0xff;
}

unsigned get_ticks(void)
{
    return SDL_GetTicks();
}
