/*****************************************************************************
 **
 ** Copyright (c) 2015, 2016 Jay Elliott
 **
 ** This file is part of simple_fps.
 **
 ** simple_fps is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** simple_fps is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with simple_fps.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <assert.h>

#include <SDL2/SDL.h>

#include "screen.h"
#include "event.h"
#include "screen_impl.h"
#include "keybind.h"

/* static int handle_key_press(struct screen *scr, struct event *event, */
/*                             SDL_Event const *sdl_event); */
static int handle_mouse_motion(struct screen *scr, struct event *event,
                               SDL_Event const *sdl_event);

int input_poll(struct screen *scr, struct event *event)
{
    int pressed;
    int status;
    SDL_Event sdl_event;

    event->type = EVENT_TYPE_NONE;
    /*
     * Loop through all events returned by SDL2
     * until it finds one it can handle
     */
    while (SDL_PollEvent(&sdl_event))
    {
        switch (sdl_event.type)
        {
        case SDL_QUIT:
            event->type = EVENT_TYPE_QUIT;
            return 1;
        case SDL_KEYDOWN:
        case SDL_KEYUP:
            if ((status = handle_key_press(scr, event, &sdl_event)))
                return status;
            break;
        case SDL_MOUSEMOTION:
            if ((status = handle_mouse_motion(scr, event, &sdl_event)))
                return status;
            break;
        }
    }

    return 0;
}

int handle_key_press(struct screen *scr, struct event *event,
                     SDL_Event const *sdl_event)
{
    int binding;
    int pressed;

    assert(sdl_event->type == SDL_KEYDOWN || sdl_event->type == SDL_KEYUP);

    /*
     * XXX SDL sends continuos events whenever you hold down a key
     * (but only for the latest key to be held if you hold more than one).
     * The common input layer will handle this.
     */
    pressed = sdl_event->key.type == SDL_KEYDOWN;

    if ((binding = find_keybind(sdl_event->key.keysym.scancode)))
    {
        event->type = EVENT_TYPE_KEY;
        event->key_event.keycode = sdl_event->key.keysym.scancode;
        event->key_event.state = pressed ? KEY_PRESSED : KEY_RELEASED;
        event->key_event.binding = binding;
        return 1;
    }
    return 0;
}

static int handle_mouse_motion(struct screen *scr, struct event *event,
                               SDL_Event const *sdl_event)
{
    assert(sdl_event->type == SDL_MOUSEMOTION);

    event->type = EVENT_TYPE_MOUSEMOTION;
    event->mouse_event.x_pos = sdl_event->motion.x;
    event->mouse_event.y_pos = sdl_event->motion.y;
    event->mouse_event.x_rel = sdl_event->motion.xrel;
    event->mouse_event.y_rel = sdl_event->motion.yrel;

    return 1;
}

void get_mouse_pos(struct screen *scr, int *x, int *y)
{
    SDL_GetMouseState(x, y);
}

void set_mouse_pos(struct screen *scr, int x, int y)
{
    SDL_WarpMouseInWindow(scr->impl->win, x, y);
}

void config_default_bindings(struct screen *scr)
{
    cleanup_keybinds();

    bind_key(SDL_SCANCODE_W, KEY_FWD);
    bind_key(SDL_SCANCODE_S, KEY_BACK);
    bind_key(SDL_SCANCODE_A, KEY_LEFT);
    bind_key(SDL_SCANCODE_D, KEY_RIGHT);
    bind_key(SDL_SCANCODE_PAGEUP, KEY_UP);
    bind_key(SDL_SCANCODE_PAGEDOWN, KEY_DOWN);
    bind_key(SDL_SCANCODE_LCTRL, KEY_FIRE);
    bind_key(SDL_SCANCODE_E, KEY_USE);
}
